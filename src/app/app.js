angular.module('app', [
    'ui.router',
    'ui.bootstrap',
    'ui.utils',
    'ui.select',
    'ui.checkbox',
    'ui.scroll',

    'ngDialog',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ngAnimate',
    'ngTable',
    'ngCsv',

    'angular.filter',
    'angular-stripe',
    'toaster',
    'angularMoment',
    'angularSpinner',
    'angular-loading-bar',
    'restangular',

    'directive', //directive
    'services', //services
    'resources', //resources
    'security', //security
    'plugins', //plugins
    'filters', //plugins

    'templates-app',
    'templates-common',

    'app.spinner',
    'app.labels',
    'app.utils',
    'app.loggedIn',
    'app.modules',
    'app.header',
    'app.footer',
    'app.constant'
])

    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider
            .otherwise('/');

        $stateProvider

        // Layout: Blank source
            .state('blank', {
                abstract: true,
                views: {
                    'root': {
                        templateUrl: 'layouts/blank.tpl.html'
                    }
                }
            })

            // init
            .state('init', {
                url: '/',
                resolve: {
                    init: function ($state, $cookieStore, security) {
                        return security.requestCurrentUser()
                            .then(function () {
                                if (security.isAuthenticated()) {
                                    if (!security.isTaxReturnUser()) {
                                        $state.go('loggedIn.modules.dashboard');
                                    } else {
                                        $state.go('loggedIn.modules.tax-return.welcome');
                                    }
                                } else {
                                    $cookieStore.remove('authToken');
                                    $state.go('loginForm');
                                }
                            });
                    }
                }
            })

            // 404
            .state('404', {
                url: '/404',
                views: {
                    'root': {
                        templateUrl: 'modules/pages/404.tpl.html'
                    }
                }
            });
    })

    .config(function (stripeProvider, PAYMENT) {
        stripeProvider.setPublishableKey(PAYMENT.STRIPE_PUBLIC_KEY_LIVE);
    })

    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
    })

    .controller('AppCtrl',
        function ($rootScope, $scope, $sce, i18nNotifications, security, utils, Spinner, $timeout, $interval,
                  toaster, exchange, CONFIGS) {

            // settings application
            $scope.pageTitle = "BaseTax";
            $scope.app = {
                name: 'BaseTax',
                companyName: 'Basetax Limited',
                url: '',
                imageUrl: '',
                description: "",
                version: '2.0',
                support: 'support@basetax.com',
                settings: {
                    themeID: 1,
                    navbarHeaderColor: 'bg-black',
                    navbarCollapseColor: 'bg-white-only',
                    asideColor: 'bg-black',
                    headerFixed: true,
                    asideFixed: false,
                    asideFolded: false,
                    asideDock: false,
                    container: false
                }
            };

            // re-setup rootscope $on
            var $onOrigin = $rootScope.$on;
            $rootScope.$on = function (names, listener) {
                var self = this;

                if (!angular.isArray(names)) {
                    names = [names];
                }

                names.forEach(function (name) {
                    $onOrigin.call(self, name, listener);
                });
            };

            $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                // console.log(fromState, toState);
            });

            $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                // console.log(fromState, toState);
            });

            $scope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
                // console.log(error);
                i18nNotifications.pushForCurrentRoute('errors.route.changeError', 'error', {}, {rejection: error});
            });

            $scope.requests = 0;

            $scope.$on('API:loading:started', function (event, args) {
                Spinner.spin('spinner');
            });

            $scope.$on('API:loading:ended', function (event, args) {
                //Spinner.stop('spinner');
            });


            // TOASTER
            $scope.toaster = {
                type: 'success',
                title: 'Title',
                text: 'Message'
            };

            $scope.pop = function () {
                toaster.pop($scope.toaster.type, $scope.toaster.title, $scope.toaster.text);
            };

            $scope.$on('profile:updated', function () {
                toaster.pop($scope.toaster.type, 'Profile', 'Your changes have been saved');
            });

            exchange.getRates();


            // Params date picker
            $scope.dateFormat = CONFIGS.DATE_FORMAT;
            $scope.dateOptions = CONFIGS.DATE_OPTIONS;

            $scope.isSafari = function () {
                return navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
            };
        });

