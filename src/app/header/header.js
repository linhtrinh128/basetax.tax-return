angular.module('app.header', [])

  .controller('HeaderController',
    function ($scope, $state, security, forms, TaxYear) {

      $scope.user = security.currentUser;

      // tax year
      $scope.taxYears = TaxYear.list;
      $scope.taxYear = TaxYear.current;

      $scope.actions = {

        init: function () {
          //init
        },

        logout: function () {
          security.logout();
        },

        openChangePassword: function () {
          forms.openFormChangePassword();
        },

        isStateTaxReturn: function () {
          return $state.current.name.indexOf('tax-return') != -1;
        }
      };

      $scope.actions.init();
    }
  );
