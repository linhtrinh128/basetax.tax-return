angular.module('modules.tax-return.forms.partnership', [])

  .controller('PartnershipIncomeController',
    function ($scope, $state, $uibModalInstance, TaxYear, partnerships,
              security, utils, CONFIGS, PartnershipSA104) {

      $scope.dataFormat = CONFIGS.DATE_FORMAT;
      $scope.dateOptions = CONFIGS.DATE_OPTIONS;
      $scope.params = {
        opened1: false,
        opened2: false,
        opened3: false,
        opened4: false
      };


      PartnershipSA104.findByTaxYear(TaxYear.current.id)
        .then(function (res) {
          $scope.info = res;
        }, function () {
          $scope.info = new PartnershipSA104();
        });


      // Open date picker
      $scope.open = function ($event, type) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.params['opened' + type] = true;
      };


      // Cancel
      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };


      // save partnership form SA104
      $scope.save = function () {
        $scope.info.save().then(function () {
          $uibModalInstance.dismiss('cancel');
        });
      };
    }
  );
