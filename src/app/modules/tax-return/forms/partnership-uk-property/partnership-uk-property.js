angular.module('modules.tax-return.forms.partnership.partnership-uk-property', [])

  .controller('PartnershipUkPropertyController',
    function ($rootScope, $scope, $uibModalInstance, security, TaxYear, PartnerProperty, utils, CONFIGS) {


      // Params date picker
      $scope.dateFormat = CONFIGS.DATE_FORMAT;
      $scope.dateOptions = CONFIGS.DATE_OPTIONS;

      
      // $scope.ukProperty = {
      //   user_id: security.currentUser.id,
      //   tax_year_id: TaxYear.current.id,
      //   partnership_detail_id: 1,
      //
      //   name: '',
      //   tax_reference: '',
      //   return_period_begin: '',
      //   return_period_end: '',
      //   partnership_income_from_furnished_holiday_letting: 0,
      //
      //   // Furnished holiday lettings in the UK or European Economic Area (EEA)
      //   income_from_furnished_holiday_letting: 0,
      //   rent_rates_insurance_ground_rents_etc_1: 0,
      //   repairs_and_maintenance_1: 0,
      //   finance_charges_including_interest_1: 0,
      //   legal_and_professional_cost_1: 0,
      //   cost_of_services_provided_including_wages_1: 0,
      //   other_expenses_1: 0,
      //   total_expense_1: 0,
      //   net_profit_1: 0,
      //   private_use_1: 0,
      //   balancing_charges_1: 0,
      //   total_private_use_balancing_charge_1: 0,
      //   capital_allowances_1: 0,
      //   tick_allowance_1: 0,
      //   loss_brought_forward_used_against_this_year_profit: 0,
      //   profit_for_the_year_after_losses: 0,
      //   loss_for_the_year: 0,
      //   total_loss_to_carry_forward: 0,
      //   business_is_in_the_EEA: 0,
      //   make_a_period_of_grace_election: 0,
      //
      //   //Other property income
      //   rent_and_other_income_form_uk_property: 0,
      //   tax_deducted: 0,
      //   chargeable_premium: 0,
      //   reverse_premium: 0,
      //   total_income: 0,
      //   rent_rates_insurance_ground_rents_etc_2: 0,
      //   repairs_and_maintenance_2: 0,
      //   finance_charges_including_interest_2: 0,
      //   legal_and_professional_cost_2: 0,
      //   cost_of_services_provided_including_wages_2: 0,
      //   other_expenses_2: 0,
      //   total_expense_2: 0,
      //   net_profit_2: 0,
      //   private_use_2: 0,
      //   balancing_charges_2: 0,
      //   total_private_use_balancing_charge_2: 0,
      //   annual_investment_allowance: 0,
      //   all_other_capital_allowance: 0,
      //   tick_allowance_2: 0,
      //   landlord_energy_saving_allowance: 0,
      //   wear_and_tear: 0,
      //   total_allowance: 0,
      //   profit_or_loss_for_return_period: 0
      // };


      // Watch ukProperty
      $scope.$watch('ukProperty', function () {
        var data = $scope.ukProperty;

        // Furnished holiday lettings in the UK or European Economic Area (EEA)
        data.total_expense_1 = total([
          data.rent_rates_insurance_ground_rents_etc_1,
          data.repairs_and_maintenance_1,
          data.finance_charges_including_interest_1,
          data.legal_and_professional_cost_1,
          data.cost_of_services_provided_including_wages_1,
          data.other_expenses_1
        ]);
        data.net_profit_1 = minus(data.income_from_furnished_holiday_letting, data.total_expense_1);
        data.total_private_use_balancing_charge_1 = total([
          data.private_use_1,
          data.balancing_charges_1
        ]);
        data.profit_for_the_year_after_losses = minus(
          total([data.net_profit_1, data.total_private_use_balancing_charge_1]),
          total([data.capital_allowances_1, data.loss_brought_forward_used_against_this_year_profit])
        );
        data.loss_for_the_year = minus(
          total([data.net_profit_1, data.total_private_use_balancing_charge_1]),
          data.capital_allowances_1
        );

        //Other property income
        data.total_income = total([
          data.rent_and_other_income_form_uk_property,
          data.chargeable_premium,
          data.reverse_premium
        ]);
        data.total_expense_2 = total([
          data.rent_rates_insurance_ground_rents_etc_2,
          data.repairs_and_maintenance_2,
          data.finance_charges_including_interest_2,
          data.legal_and_professional_cost_2,
          data.cost_of_services_provided_including_wages_2,
          data.other_expenses_2
        ]);
        data.net_profit_2 = minus(data.total_income, data.total_expense_2);
        data.total_private_use_balancing_charge_2 = total([
          data.private_use_2,
          data.balancing_charges_2
        ]);
        data.total_allowance = total([
          data.annual_investment_allowance,
          data.all_other_capital_allowance,
          data.landlord_energy_saving_allowance,
          data.wear_and_tear
        ]);

        data.profit_or_loss_for_return_period = minus(
          total([data.net_profit_2, data.total_private_use_balancing_charge_2]),
          data.total_allowance
        );

        $scope.ukProperty = data;
      }, true);


      // Watch ukProperty.return_period_begin
      $scope.$watch('ukProperty.return_period_begin', function () {
        if (!angular.equals($scope.ukProperty.return_period_begin, $scope.dayBegin)) {
          $scope.errDateBegin = checkDay($scope.ukProperty.return_period_end, $scope.ukProperty.return_period_begin);
        }
      }, true);


      // Watch ukProperty.return_period_end
      $scope.$watch('ukProperty.return_period_end', function () {
        if (!angular.equals($scope.ukProperty.return_period_end, $scope.dayEnd)) {
          $scope.errDateEnd = checkDay($scope.ukProperty.return_period_end, $scope.ukProperty.return_period_begin);
        }
      }, true);



      $scope.init = function () {
        PartnerProperty.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.ukProperty = res;

            $scope.dayBegin = angular.copy($scope.ukProperty.return_period_begin);
            $scope.dayEnd = angular.copy($scope.ukProperty.return_period_end);

          }, function () {
            $scope.ukProperty = new PartnerProperty();
          });
      };


      $scope.actions = {


        openRPB: function () {
          $scope.openrpb = true;
        },


        openRPE: function () {
          $scope.openrpe = true;
        },


        save: function (isValid) {
          if (isValid) {
            $scope.ukProperty.save().then(function () {
              $uibModalInstance.dismiss('cancel');
            });
          }
        },


        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };

      /**
       * Check day
       *
       * @param dayjoin
       * @param dayleft
       * @returns {boolean}
       */
      function checkDay(dayjoin, dayleft) {
        if (new Date(dayjoin) > new Date(dayleft)) {
          return true;
        } else {
          return false;
        }
      }

      /**
       * Calculate total
       *
       * @param array
       * @returns {number|Number|*}
       */
      function total(array) {
        $scope.value = 0;
        for (var index in array) {
          $scope.value += parseFloat(array[index]);
        }
        return $scope.value;
      }

      /**
       * Calculate - minus
       *
       * @param value1
       * @param value2
       * @returns {number|Number|*}
       */
      function minus(value1, value2) {
        $scope.value = parseFloat(value1) - parseFloat(value2);
        return $scope.value;
      }
    }
  );
