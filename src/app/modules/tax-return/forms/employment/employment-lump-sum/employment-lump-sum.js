angular.module('modules.tax-return.forms.employment.employment-lump-sum', [])

  .controller('EmploymentLumpSumController',
    function ($scope, $state, $uibModalInstance, utils, security, TaxYear, UkIncome) {
      $scope.showValid = false;

      // form
      // form_type: 24

      $scope.$watch('info', function () {
        var value = parseFloat($scope.info.ASE5a) + parseFloat($scope.info.ASE5b);
        if (!value || isNaN(value)) {
          value = 0;
        }

        $scope.info.ASE5 = value;
      }, true);

      UkIncome.findByTaxYear(TaxYear.current.id, 24)
        .then(function (res) {
          $scope.info = res[index];
        }, function () {
          $scope.info = new UkIncome(24);
        });

      $scope.save = function (isValid) {
        if (isValid) {
          $scope.info.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        }
      };

      // fn close modal
      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  );
