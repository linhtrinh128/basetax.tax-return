angular.module('modules.tax-return.forms.employment', [
  'modules.tax-return.forms.employment.employment-lump-sum',
  'modules.tax-return.forms.employment.student-loan'
])

  .controller('TransactionEmp14Controller',
    function ($scope, $state, $uibModalInstance, $uibModal, utils, Employer, security, apiService,
              TransactionEmp14, TaxYear) {

      $scope.receive_employment = false;
      $scope.student_loan_repaid = false;

      $scope.employers = Employer.list;

      $scope.init = function () {
        TransactionEmp14.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.transaction = res;
          }, function () {
            $scope.transaction = new TransactionEmp14();
          });
      };


      $scope.actions = {

        selectEmployer: function (employer) {
          $scope.transaction.employer = employer;
          $scope.transaction.employer_id = employer.id;
        },

        save: function (isValid) {
          if (isValid) {
            $scope.transaction.save().then(function () {
              $uibModalInstance.dismiss('cancel');
            });
          }
        },

        openEmployment: function () {
          $uibModalInstance.close();
          $uibModal.open({
            templateUrl: 'modules/tax-return/forms/employer/employment-lump-sum/employment-lump-sum.tpl.html',
            controller: 'EmploymentLumpSumController'
          });
        },

        openStudentLoan: function () {
          $uibModalInstance.close();
          $uibModal.open({
            templateUrl: 'modules/tax-return/forms/employer/student-loan/student-loan.tpl.html',
            controller: 'StudentLoanController'
          });
        },

        open: function () {
          // employers.resetCurrent();
          // $uibModal.open({
          //   templateUrl: 'modules/employment/new/employer/employer.tpl.html',
          //   controller: 'CreateEmployerController'
          // });
          var employer = new Employer();
          employer.show();
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );
