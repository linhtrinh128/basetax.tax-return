angular.module('modules.tax-return.forms.employment.student-loan', [])

.controller('StudentLoanController',
    function ($scope, $state, $uibModalInstance, $uibModal, security, transactions, TaxYear, utils) {
        //modal instance
        $scope.modalInstance = $uibModalInstance;
        $scope.modal = $uibModal;

        //show valid
        $scope.showValid = false;

        // object
        $scope.student = {
            amount: 0,
            student_loan_notification: 0,
            student_loan_maybe_repaid: 0
        };

        // check is update
        $scope.isUpdate = false;
        $scope.isCopy = utils.storage.get('copyTransaction') === 'true';
        utils.storage.remove('copyTransaction');
        if(!utils.isEmpty(transactions.current)){
            var student = angular.copy(transactions.current);

            for(var key in $scope.student){
                $scope.student[key] = student[key];
            }
            if(!$scope.isCopy){
                $scope.student.id = student.id;
                $scope.isUpdate = true;
            }

            transactions.resetCurrent();
        }

        // fn save
        $scope.save = function (isValid) {
            $scope.showValid = true;
            if (isValid) {

                $scope.student.modified = new Date();


                if(!$scope.isUpdate){ // create
                    $scope.student.date = new Date();
                    $scope.student.created = new Date();
                    $scope.student.record_type_id = 100;
                    $scope.student.user_id = security.currentUser.id;
                    $scope.student.tax_year_id = TaxYear.current.id;

                    transactions.create($scope.student).then(function(res){
                        $scope.showValid = false;
                        $uibModalInstance.dismiss('cancel');
                    });
                } else { // update
                    transactions.update($scope.student).then(function(res){
                        $scope.showValid = false;
                        $uibModalInstance.dismiss('cancel');
                    });
                }
            }
        };
        // fn close modal
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
);
