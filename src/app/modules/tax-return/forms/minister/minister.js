angular.module('modules.tax-return.forms.minister', [])

  .controller('MinisterController',
    function ($scope, $state, $uibModalInstance, TaxYear, MinisterEmployment) {

      $scope.init = function () {
        MinisterEmployment.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.info = res;
          }, function () {
            $scope.info = new MinisterEmployment();
          });
      };


      $scope.actions = {

        save: function () {
          $scope.info.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );
