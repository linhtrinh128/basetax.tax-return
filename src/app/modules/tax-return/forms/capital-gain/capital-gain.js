angular.module('modules.tax-return.forms.capital-gain', [])

  .controller('CapitalGainController',
    function ($scope, $state, $uibModalInstance, TaxYear, CapitalGain) {

      $scope.init = function () {
        CapitalGain.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.capitalGain = res;

            if (typeof $scope.capitalGain.CGT22 !== 'boolean') {
              $scope.capitalGain.CGT22 = !!parseInt($scope.capitalGain.CGT22);
            }
            if (typeof $scope.capitalGain.CGT23 !== 'boolean') {
              $scope.capitalGain.CGT23 = !!parseInt($scope.capitalGain.CGT23);
            }
            if (typeof $scope.capitalGain.CGT28 !== 'boolean') {
              $scope.capitalGain.CGT28 = !!parseInt($scope.capitalGain.CGT28);
            }
            if (typeof $scope.capitalGain.CGT29 !== 'boolean') {
              $scope.capitalGain.CGT29 = !!parseInt($scope.capitalGain.CGT29);
            }
            if (typeof $scope.capitalGain.CGT35 !== 'boolean') {
              $scope.capitalGain.CGT35 = !!parseInt($scope.capitalGain.CGT35);
            }
            if (typeof $scope.capitalGain.CGT36 !== 'boolean') {
              $scope.capitalGain.CGT36 = !!parseInt($scope.capitalGain.CGT36);
            }
          }, function () {
            $scope.capitalGain = new CapitalGain();
          });
      };


      $scope.actions = {

        save: function () {
          $scope.capitalGain.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );
