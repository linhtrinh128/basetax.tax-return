angular.module('modules.tax-return.forms.foreign-income', [])

  .controller('ForeignIncomeController',
    function ($scope, $state, $uibModalInstance, TaxYear, ForeignIncome) {

      $scope.init = function () {
        ForeignIncome.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.info = res;
          }, function () {
            $scope.info = new ForeignIncome();
          });
      };


      $scope.actions = {

        save: function () {
          $scope.info.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },


        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );
