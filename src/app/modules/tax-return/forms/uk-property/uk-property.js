angular.module('modules.tax-return.forms.uk-property', [])

  .controller('UkPropertyController',
    function ($scope, $rootScope, $uibModalInstance, $uibModal,
              security, TransactionPro14, TaxYear, utils) {

      $scope.noPros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
      $scope.taxYear = TaxYear.current;

      $scope.property = new TransactionPro14();

      $scope.init = function () {
        TransactionPro14.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.property = res;

            if (typeof $scope.property.PRO4a !== 'boolean') {
              $scope.property.PRO4a = !!parseInt($scope.property.PRO4a);
            }
            if (typeof $scope.property.PRO4b !== 'boolean') {
              $scope.property.PRO4b = !!parseInt($scope.property.PRO4b);
            }
            if (typeof $scope.property.PRO2 !== 'boolean') {
              $scope.property.PRO2 = !!parseInt($scope.property.PRO2);
            }
            if (typeof $scope.property.PRO3 !== 'boolean') {
              $scope.property.PRO3 = !!parseInt($scope.property.PRO3);
            }
            if (typeof $scope.property.PRO4 !== 'boolean') {
              $scope.property.PRO4 = !!parseInt($scope.property.PRO4);
            }

            if ($scope.property.date === '0000-00-00') {
              $scope.property.date = null;
            }
          });
      };

      // Watch property => calculate PRO13, PRO15, PRO16
      $scope.$watch('property', function () {
        // PRO 13
        $scope.property.PRO13 = (get(5) + get(10) - (parseFloat(total(6, 9)) + get(12)));

        // PRO 15 - 16
        var value1 = get(13) - get(14);
        $scope.property.PRO15 = value1 > 0 ? value1 : 0;
        $scope.property.PRO16 = value1 < 0 ? value1 : 0;
      }, true);

      // Watch property
      $scope.$watch('property', function () {
        // PRO 38
        var value = (get(20) + get(22) + get(23) + get(30) + get(31)) - (parseFloat(total(24, 29)) + parseFloat(total(32, 37)));
        $scope.property.PRO38 = value || 0;

        // PRO 40 - 41
        var value2 = get(38) - get(39);
        $scope.property.PRO40 = value2 > 0 ? value2 : 0;
        $scope.property.PRO41 = value2 < 0 ? value2 : 0;

        $scope.property.PRO43 = get(41) - get(42);
      }, true);


      $scope.$on('property:created', function () {
        $scope.isUpdate = true;

        if (!utils.isEmpty(TransactionPro14.current)) {
          $scope.property.id = TransactionPro14.current.id;
        }
      });


      $scope.actions = {

        save: function (isValid) {
          if (!isValid) {
            return;
          }
          $scope.property.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        },

        open: function (key) {
          switch (key) {
            case 2:
            case '2':
              $scope.tabFurnished = true;
              $scope.tabRental = false;
              break;
            case 3:
            case '3':
              $scope.tabFurnished = false;
              $scope.tabRental = true;
              break;
          }
        }
      };


      function get(number) {
        return parseFloat($scope.property['PRO' + number]);
      }


      function total(start, end) {
        var value = 0;
        for (var i = start; i <= end; i++) {
          if (get(i) !== undefined) {
            value += get(i);
          }
        }
        return value.toFixed(2);
      }
    }
  );
