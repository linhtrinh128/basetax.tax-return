angular.module('modules.tax-return.forms.lloyds-underwriter', [])

  .controller('LloydsUnderWriterController',
    function ($scope, $state, $uibModalInstance, TaxYear, Underwriter) {
           
      $scope.init = function () {
        Underwriter.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.info = res;
          }, function () {
            $scope.info = new Underwriter();
          });
      };
      

      $scope.actions = {

        save: function () {
          $scope.info.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );