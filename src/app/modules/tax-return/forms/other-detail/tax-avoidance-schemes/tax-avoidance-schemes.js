angular.module('modules.tax-return.forms.other-detail.tax-avoidance-schemes', [])

  .controller('TaxAvoidanceSchemesController',
    function ($scope, $state, $uibModalInstance, otherDetails, security, TaxYear, utils) {

      $scope.info = {
        AIL19a: '',
        AIL19b: '',
        AIL19c: '',
        AIL20a: '',
        AIL20b: '',
        AIL20c: '',
        user_id: security.currentUser.id,
        tax_year_id: TaxYear.current.id,
        form_type: 18
      };

      $scope.isUpdate = false;
      if (!utils.isEmpty(otherDetails.current)) {
        $scope.isUpdate = true;
        $scope.info = angular.copy(otherDetails.current);

        otherDetails.resetCurrent();
      }

      $scope.save = function () {
        $scope.info.modified = new Date();

        if (!$scope.isUpdate) { // create
          $scope.info.created = new Date();
          otherDetails.create($scope.info).then(function (res) {
            $uibModalInstance.dismiss('cancel');
          });
        } else {
          otherDetails.update($scope.info).then(function (res) {
            $uibModalInstance.dismiss('cancel');
          });
        }
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  );
