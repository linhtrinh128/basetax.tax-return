angular.module('modules.tax-return.forms.other-detail.paid-too-much-tax', [])

  .controller('PaidTooMuchTaxController',
    function ($scope, $state, $uibModalInstance, otherDetails, security, TaxYear, utils) {

      /**
       * Info
       */
      $scope.info = {
        FIN4: '',
        FIN5: '',
        FIN6: '',
        FIN7: '',
        FIN8: '',
        FIN9: 0,
        FIN10: 0,
        FIN11: 0,
        FIN12: '',
        FIN13: '',
        FIN14: '',
        user_id: security.currentUser.id,
        tax_year_id: TaxYear.current.id,
        form_type: 13
      };

      /**
       * Init
       */
      $scope.init = function () {
        $scope.isUpdate = false;
        if (!utils.isEmpty(otherDetails.current)) {
          $scope.isUpdate = true;
          $scope.info = angular.copy(otherDetails.current);

          // Parse
          $scope.info.FIN9 = $scope.info.FIN9 + '';
          $scope.info.FIN10 = $scope.info.FIN10 + '';
          $scope.info.FIN11 = $scope.info.FIN11 + '';

          otherDetails.resetCurrent();
        }
      };

      /**
       * ACTIONS
       */
      $scope.actions = {

        /**
         * Save
         */
        save: function () {
          // Parse
          $scope.info.FIN9 = $scope.info.FIN9 == '1' ? 1 : 0;
          $scope.info.FIN10 = $scope.info.FIN10 == '1' ? 1 : 0;
          $scope.info.FIN11 = $scope.info.FIN11 == '1' ? 1 : 0;

          if (!$scope.isUpdate) { // create
            otherDetails.create($scope.info).then(function (res) {
              $uibModalInstance.dismiss('cancel');
            });
          } else {
            otherDetails.update($scope.info).then(function (res) {
              $uibModalInstance.dismiss('cancel');
            });
          }
        },

        /**
         * Cancel
         */
        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );
