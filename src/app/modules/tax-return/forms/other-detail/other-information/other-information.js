angular.module('modules.tax-return.forms.other-detail.other-information', [])

  .controller('OtherInformationController',
    function ($scope, $state, $uibModalInstance, otherDetails, security, TaxYear, utils) {

      $scope.info = {
        AIL1: 0,
        AIL2: 0,
        AIL3: 0,
        AIL4: 0,
        AIL5: '',
        AIL6: 0,
        user_id: security.currentUser.id,
        tax_year_id: TaxYear.current.id,
        form_type: 16
      };

      $scope.isUpdate = false;
      if (!utils.isEmpty(otherDetails.current)) {
        $scope.isUpdate = true;
        $scope.info = angular.copy(otherDetails.current);

        otherDetails.resetCurrent();
      }

      $scope.save = function () {
        $scope.info.modified = new Date();

        if (!$scope.isUpdate) { // create
          $scope.info.created = new Date();
          otherDetails.create($scope.info).then(function (res) {
            $uibModalInstance.dismiss('cancel');
          });
        } else {
          otherDetails.update($scope.info).then(function (res) {
            $uibModalInstance.dismiss('cancel');
          });
        }
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  );
