angular.module('modules.tax-return.forms.other-detail.tax-charges-and-taxable-lump-sums', [])

  .controller('TaxChangesAndTaxableLumpSumsController',
    function ($scope, $state, $uibModalInstance, otherDetails, security, TaxYear, utils) {

      $scope.info = {
        AIL7: 0,
        AIL8: 0,
        AIL9: 0,
        AIL10: 0,
        AIL11: 0,
        AIL12: '',
        AIL13: 0,
        AIL14: 0,
        AIL15: 0,
        AIL16: 0,
        AIL17: 0,
        AIL18: 0,
        user_id: security.currentUser.id,
        tax_year_id: TaxYear.current.id,
        form_type: 17
      };

      $scope.isUpdate = false;
      if (!utils.isEmpty(otherDetails.current)) {
        $scope.isUpdate = true;
        $scope.info = angular.copy(otherDetails.current);

        otherDetails.resetCurrent();
      }

      $scope.save = function () {
        $scope.info.modified = new Date();

        if (!$scope.isUpdate) { // create
          $scope.info.created = new Date();
          otherDetails.create($scope.info).then(function (res) {
            $uibModalInstance.dismiss('cancel');
          });
        } else {
          otherDetails.update($scope.info).then(function (res) {
            $uibModalInstance.dismiss('cancel');
          });
        }
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  );
