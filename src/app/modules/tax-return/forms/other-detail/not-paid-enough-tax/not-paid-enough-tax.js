angular.module('modules.tax-return.forms.other-detail.not-paid-enough-tax', [])

  .controller('NotPaidEnoughTaxController',
    function ($scope, $state, $uibModalInstance, otherDetails, security, TaxYear, utils) {

      /**
       * Info
       */
      $scope.info = {
        FIN2: 0,
        FIN3: 0,
        user_id: security.currentUser.id,
        tax_year_id: TaxYear.current.id,
        form_type: 12
      };

      /**
       * Init
       */
      $scope.init = function () {
        $scope.isUpdate = false;
        if (!utils.isEmpty(otherDetails.current)) {
          $scope.isUpdate = true;
          $scope.info = angular.copy(otherDetails.current);

          $scope.info.FIN2 = $scope.info.FIN2 + '';
          $scope.info.FIN3 = $scope.info.FIN3 + '';

          otherDetails.resetCurrent();
        }
      };


      /**
       * Actions
       */
      $scope.actions = {

        /**
         * Save
         */
        save: function () {
          // Parse
          $scope.info.FIN2 = $scope.info.FIN2 == '1' ? 1 : 0;
          $scope.info.FIN3 = $scope.info.FIN3 == '1' ? 1 : 0;

          if (!$scope.isUpdate) { // create
            otherDetails.create($scope.info).then(function (res) {
              $uibModalInstance.dismiss('cancel');
            });
          } else {
            otherDetails.update($scope.info).then(function (res) {
              $uibModalInstance.dismiss('cancel');
            });
          }
        },

        /**
         * Cancel
         */
        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );
