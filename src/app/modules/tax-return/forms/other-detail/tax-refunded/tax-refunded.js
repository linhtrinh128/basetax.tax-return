angular.module('modules.tax-return.forms.other-detail.tax-refunded', [])

  .controller('TaxRefundedController',
    function ($scope, $state, $uibModalInstance, otherDetails, security, TaxYear, utils) {

      $scope.info = {
        FIN1: 0,
        user_id: security.currentUser.id,
        tax_year_id: TaxYear.current.id,
        form_type: 14
      };

      $scope.isUpdate = false;
      if (!utils.isEmpty(otherDetails.current)) {
        $scope.isUpdate = true;
        $scope.info = angular.copy(otherDetails.current);

        otherDetails.resetCurrent();
      }

      $scope.save = function () {
        $scope.info.modified = new Date();

        if (!$scope.isUpdate) { // create
          $scope.info.created = new Date();
          otherDetails.create($scope.info).then(function (res) {
            $uibModalInstance.dismiss('cancel');
          });
        } else {
          otherDetails.update($scope.info).then(function (res) {
            $uibModalInstance.dismiss('cancel');
          });
        }
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  );
