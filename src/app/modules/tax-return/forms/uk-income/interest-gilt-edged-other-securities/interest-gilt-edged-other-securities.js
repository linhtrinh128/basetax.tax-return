angular.module('modules.tax-return.forms.uk-income.interest-gilt-edged-other-securities', [
  'security'
])

  .controller('InterestGiltEdgedOtherSecuritiesController',
    function ($scope, $state, $uibModalInstance, TaxYear, UkIncome) {


      var form_type = 4;

      $scope.init = function () {
        UkIncome.findByTaxYear(TaxYear.current.id, form_type)
          .then(function (res) {
            $scope.info = res;
          }, function () {
            $scope.info = new UkIncome(form_type);
          });
      };


      $scope.actions = {

        save: function () {
          $scope.info.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );