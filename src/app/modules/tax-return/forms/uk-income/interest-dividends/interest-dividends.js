angular.module('modules.tax-return.forms.uk-income.interest-dividends', [])

  .controller('InterestDividendsController',
    function ($scope, $state, $uibModalInstance, UkIncome, security, TaxYear) {

      var form_type = 1;

      $scope.init = function () {
        UkIncome.findByTaxYear(TaxYear.current.id, form_type)
          .then(function (res) {
            $scope.info = res;
          }, function () {
            $scope.info = new UkIncome(form_type);
          });
      };

      $scope.actions = {

        //Save
        save: function () {
          $scope.info.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },

        //Cancel
        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );
