angular.module('modules.tax-return.forms', [
  //main income
  'modules.tax-return.forms.capital-gain',
  // 'modules.tax-return.forms.employer',
  'modules.tax-return.forms.employment',
  'modules.tax-return.forms.foreign-income',
  'modules.tax-return.forms.lloyds-underwriter',
  'modules.tax-return.forms.minister',
  'modules.tax-return.forms.partnership',
  'modules.tax-return.forms.residence-remittance',
  'modules.tax-return.forms.self-employed',
  'modules.tax-return.forms.trust',
  'modules.tax-return.forms.uk-property',

  //uk-income
  'modules.tax-return.forms.uk-income.interest-dividends',
  'modules.tax-return.forms.uk-income.interest-gilt-edged-other-securities',
  'modules.tax-return.forms.uk-income.life-insurance-gains',
  'modules.tax-return.forms.uk-income.note',
  'modules.tax-return.forms.uk-income.pensions-annuities',
  'modules.tax-return.forms.uk-income.stock-dividends',

  //other-detail
  'modules.tax-return.forms.other-detail.not-paid-enough-tax',
  'modules.tax-return.forms.other-detail.other-information',
  'modules.tax-return.forms.other-detail.paid-too-much-tax',
  'modules.tax-return.forms.other-detail.tax-adviser-detail',
  'modules.tax-return.forms.other-detail.tax-avoidance-schemes',
  'modules.tax-return.forms.other-detail.tax-charges-and-taxable-lump-sums',
  'modules.tax-return.forms.other-detail.tax-refunded',

  // Partnership forms
  'modules.tax-return.forms.partnership.disposable-changeable-asset',
  'modules.tax-return.forms.partnership.other-information',
  'modules.tax-return.forms.partnership.partnership-uk-property',
  'modules.tax-return.forms.partnership.trading-and-professional-income'
])

  .factory('formsTaxReturn',
    function (utilsTaxReturn, $uibModal, TaxYear,
              Employer, TransactionPro14, TransactionEmp14, TAX_RETURN,
              selfEmployeds, Underwriter, PartnershipSA104, NonResident, UkIncome,
              Relief, otherDetails,
              PartnerTrading, DisposalChangeable, PartnerProperty, OtherInformation, SelfEmployed,
              ForeignIncome, MinisterEmployment, TrustIncome, CapitalGain) {

      var service = {

        // Data forms
        forms: {
          employment: null,
          selfEmployed: null,
          partnership: null,
          foreignIncome: null,
          capitalGain: null,
          ministerEmployment: null,
          lloysUnderwriter: null,
          ukProperty: null,
          residentRemittance: null,

          // partnership
          partnerTrading: null,
          partnerProperties: null,
          disposalChangeables: null,
          otherMainIncome: null,
          otherInformation: null
        },

        /**
         * Open form Tax Return
         *
         * @param form string
         */
        open: function (form, object) {
          var options = {};
          switch (form) {
            case TAX_RETURN.KEY.MINISTER:
              options = {
                templateUrl: 'modules/tax-return/forms/minister/minister.tpl.html',
                controller: 'MinisterController'
              };
              break;
            case TAX_RETURN.KEY.SELF_EMPLOYED:
              if (typeof object === 'undefined') {
                object = new SelfEmployed(TaxYear.current.id);
              }
              return object.show();
            case TAX_RETURN.KEY.EMPLOYMENT:
              if (Employer.list.length !== 0) { // open transaction employment
                options = {
                  templateUrl: 'modules/tax-return/forms/employment/employment.tpl.html',
                  controller: 'TransactionEmp14Controller'
                };
              } else {
                var employer = new Employer();
                employer.show();
              }
              break;
            case TAX_RETURN.KEY.UK_PROPERTY:
              options = {
                templateUrl: 'modules/tax-return/forms/uk-property/uk-property.tpl.html',
                controller: 'UkPropertyController'
              };
              break;
            case TAX_RETURN.KEY.LLOYDS_UNDERWRITER:
              options = {
                templateUrl: 'modules/tax-return/forms/lloyds-underwriter/lloyds-underwriter.tpl.html',
                controller: 'LloydsUnderWriterController'
              };
              break;
            case TAX_RETURN.KEY.PARTNERSHIP:
              options = {
                templateUrl: 'modules/tax-return/forms/partnership/partnership.tpl.html',
                controller: 'PartnershipIncomeController',
                size: 'lg'
              };
              break;
            case TAX_RETURN.KEY.FOREIGN_INCOME:
            case TAX_RETURN.KEY.PNS_FOREIGN_INCOME:
              options = {
                templateUrl: 'modules/tax-return/forms/foreign-income/foreign-income.tpl.html',
                controller: 'ForeignIncomeController'
              };
              break;
            case TAX_RETURN.KEY.TRUST:
              options = {
                templateUrl: 'modules/tax-return/forms/trust/trust.tpl.html',
                controller: 'TrustController'
              };
              break;
            case TAX_RETURN.KEY.CAPITAL:
              options = {
                templateUrl: 'modules/tax-return/forms/capital-gain/capital-gain.tpl.html',
                controller: 'CapitalGainController'
              };
              break;
            case TAX_RETURN.KEY.RESIDENT:
              options = {
                templateUrl: 'modules/tax-return/forms/residence-remittance/residence-remittance.tpl.html',
                controller: 'ResidenceRemittanceController'
              };
              break;
            case TAX_RETURN.KEY.INTEREST_DIVIDENDS:
              options = {
                templateUrl: 'modules/tax-return/forms/uk-income/interest-dividends/interest-dividends.tpl.html',
                controller: 'InterestDividendsController'
              };
              break;
            case TAX_RETURN.KEY.UK_PENSIONS:
              options = {
                templateUrl: 'modules/tax-return/forms/uk-income/pensions-annuities/pensions-annuities.tpl.html',
                controller: 'PensionsAnnuitiesController'
              };
              break;
            case TAX_RETURN.KEY.UK_INCOME:
              options = {
                templateUrl: 'modules/tax-return/forms/uk-income/note/note.tpl.html',
                controller: 'NoteController'
              };
              break;
            case TAX_RETURN.KEY.INTEREST_GILT:
              options = {
                templateUrl: 'modules/tax-return/forms/uk-income/interest-gilt-edged-other-securities/interest-gilt-edged-other-securities.tpl.html',
                controller: 'InterestGiltEdgedOtherSecuritiesController'
              };
              break;
            case TAX_RETURN.KEY.LIFE_INSURANCE:
              options = {
                templateUrl: 'modules/tax-return/forms/uk-income/life-insurance-gains/life-insurance-gains.tpl.html',
                controller: 'LifeInsuranceGainsController'
              };
              break;
            case TAX_RETURN.KEY.STOCK:
              options = {
                templateUrl: 'modules/tax-return/forms/uk-income/stock-dividends/stock-dividends.tpl.html',
                controller: 'StockDividendsController'
              };
              break;
            case TAX_RETURN.KEY.PAID_ENOUGH:
              utilsTaxReturn.isExistingByFormType(otherDetails.list, 12, TaxYear.current, function (item) {
                otherDetails.setCurrent(item);
              });
              options = {
                templateUrl: 'modules/tax-return/forms/other-detail/not-paid-enough-tax/not-paid-enough-tax.tpl.html',
                controller: 'NotPaidEnoughTaxController'
              };
              break;
            case TAX_RETURN.KEY.PAID_MUCH:
              utilsTaxReturn.isExistingByFormType(otherDetails.list, 13, TaxYear.current, function (item) {
                otherDetails.setCurrent(item);
              });
              options = {
                templateUrl: 'modules/tax-return/forms/other-detail/paid-too-much-tax/paid-too-much-tax.tpl.html',
                controller: 'PaidTooMuchTaxController'
              };
              break;
            case TAX_RETURN.KEY.PNS_TRADING_PROFESSIONAL:
              options = {
                templateUrl: 'modules/tax-return/forms/trading-professional/trading-and-professional-income.tpl.html',
                controller: 'TradingAndProfessionalIncomeController'
              };
              break;
            case TAX_RETURN.KEY.PNS_OTHER_INFORMATION:
              options = {
                templateUrl: 'modules/tax-return/forms/other-information/other-information.tpl.html',
                controller: 'PSOtherInformationController'
              };
              break;
            case TAX_RETURN.KEY.PNS_DISPOSAL_CHANGEABLE:
              options = {
                templateUrl: 'modules/tax-return/forms/disposal-changeable/disposal-changeable-asset.tpl.html',
                controller: 'DisposalChangeableController',
                size: 'lg'
              };
              break;
            case TAX_RETURN.KEY.PNS_UK_PROPERTY:
              options = {
                templateUrl: 'modules/tax-return/forms/partnership-uk-property/partnership-uk-property.tpl.html',
                controller: 'PartnershipUkPropertyController'
              };
              break;
            default:
              return;
          }

          return $uibModal.open(options);
        },

        /**
         * Check existing form
         *
         * @param form
         * @returns {*}
         */
        checkExistingForm: function (form) {
          switch (form) {
            case TAX_RETURN.KEY.SELF_EMPLOYED:
              return SelfEmployed.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.EMPLOYMENT:
              return TransactionPro14.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.UK_PROPERTY:
              return TransactionPro14.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.LLOYDS_UNDERWRITER:
              return Underwriter.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.PARTNERSHIP:
              return PartnershipSA104.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.RESIDENT:
              return NonResident.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.INTEREST_DIVIDENDS:
              return UkIncome.findByTaxYear(TaxYear.current.id, 1);
            case TAX_RETURN.KEY.UK_PENSIONS:
              return UkIncome.findByTaxYear(TaxYear.current.id, 2);
            case TAX_RETURN.KEY.UK_INCOME:
              return UkIncome.findByTaxYear(TaxYear.current.id, 3);
            case TAX_RETURN.KEY.INTEREST_GILT:
              return UkIncome.findByTaxYear(TaxYear.current.id, 4);
            case TAX_RETURN.KEY.LIFE_INSURANCE:
              return UkIncome.findByTaxYear(TaxYear.current.id, 5);
            case TAX_RETURN.KEY.STOCK:
              return UkIncome.findByTaxYear(TaxYear.current.id, 6);
            case TAX_RETURN.KEY.CHARITABLE:
              return Relief.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.PENSION:
              return Relief.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.RELIEF:
              return Relief.findByTaxYear(TaxYear.current.id);
            // Partnership forms
            case TAX_RETURN.KEY.PNS_TRADING_PROFESSIONAL:
              return PartnerTrading.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.PNS_UK_PROPERTY:
              return PartnerProperty.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.PNS_DISPOSAL_CHANGEABLE:
              return DisposalChangeable.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.PNS_OTHER_INFORMATION:
              return OtherInformation.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.MINISTER: // 21
              return MinisterEmployment.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.TRUST: // 22
              return TrustIncome.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.FOREIGN_INCOME:
            case TAX_RETURN.KEY.PNS_FOREIGN_INCOME: //23
              return ForeignIncome.findByTaxYear(TaxYear.current.id);
            case TAX_RETURN.KEY.CAPITAL: // 24
              return CapitalGain.findByTaxYear(TaxYear.current.id);
          }
        },

        /**
         * Check existing forms partnership
         *
         * @param partnershipForms
         * @returns {*}
         */
        checkExistingFormPartnership: function (partnershipForms) {
          for (var index in partnershipForms) {
            partnershipForms[index].existing = partnershipForms[index].selected && this.checkExistingForm(partnershipForms[index].key);
          }
          return partnershipForms;
        }
      };

      return service;
    });