angular.module('modules.tax-return.forms.partnership.other-information', [])

  .controller('PSOtherInformationController',
    function ($rootScope, $scope, $uibModalInstance, OtherInformation, TaxYear) {

      $scope.init = function () {
        OtherInformation.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.otherInformation = res;

            $scope.otherInformation.POI7 = $scope.otherInformation.POI7 + '';
            $scope.otherInformation.POI8 = $scope.otherInformation.POI8 + '';
            $scope.otherInformation.POI10_1 = $scope.otherInformation.POI10_1 + '';

          }, function () {
            $scope.otherInformation = new OtherInformation();
          });
      };


      $scope.actions = {

        save: function (isValid) {
          if (!isValid) {
            return;
          }
          // if (!$scope.isUpdate) {
          //   OtherInformation.create($scope.otherInformation).then(function (res) {
          //     $uibModalInstance.dismiss('cancel');
          //   });
          // } else {
          //   OtherInformation.update($scope.otherInformation).then(function (res) {
          //     $uibModalInstance.dismiss('cancel');
          //   });
          // }
          $scope.otherInformation.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    });