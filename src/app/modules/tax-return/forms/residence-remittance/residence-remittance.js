angular.module('modules.tax-return.forms.residence-remittance', [])

  .controller('ResidenceRemittanceController',
    function ($scope, $state, $uibModalInstance, NonResident, security, TaxYear, utils, CONFIGS) {

      $scope.dateFormat = CONFIGS.DATE_FORMAT;
      $scope.dateOptions = CONFIGS.DATE_OPTIONS;
      $scope.params = {
        opened1: false,
        opened2: false,
        opened3: false,
        opened4: false,
        opened5: false
      };

      $scope.init = function () {
        NonResident.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.info = res;

            if (typeof $scope.info.NRD1 !== 'boolean') {
              $scope.info.NRD1 = !!parseInt($scope.info.NRD1);
            }
            if (typeof $scope.info.NRD2 !== 'boolean') {
              $scope.info.NRD2 = !!parseInt($scope.info.NRD2);
            }
            if (typeof $scope.info.NRD3 !== 'boolean') {
              $scope.info.NRD3 = !!parseInt($scope.info.NRD3);
            }
            if (typeof $scope.info.NRD3_1 !== 'boolean') {
              $scope.info.NRD3_1 = !!parseInt($scope.info.NRD3_1);
            }
            if (typeof $scope.info.NRD4 !== 'boolean') {
              $scope.info.NRD4 = !!parseInt($scope.info.NRD4);
            }
            if (typeof $scope.info.NRD5 !== 'boolean') {
              $scope.info.NRD5 = !!parseInt($scope.info.NRD5);
            }
            if (typeof $scope.info.NRD8A !== 'boolean') {
              $scope.info.NRD8A = !!parseInt($scope.info.NRD8A);
            }
            if (typeof $scope.info.NRD8 !== 'boolean') {
              $scope.info.NRD8 = !!parseInt($scope.info.NRD8);
            }
            if (typeof $scope.info.NRD9 !== 'boolean') {
              $scope.info.NRD9 = !!parseInt($scope.info.NRD9);
            }
            if (typeof $scope.info.NRD15 !== 'boolean') {
              $scope.info.NRD15 = !!parseInt($scope.info.NRD15);
            }
            if (typeof $scope.info.NRD16 !== 'boolean') {
              $scope.info.NRD16 = !!parseInt($scope.info.NRD16);
            }
            if (typeof $scope.info.NRD23 !== 'boolean') {
              $scope.info.NRD23 = !!parseInt($scope.info.NRD23);
            }
            if (typeof $scope.info.NRD24 !== 'boolean') {
              $scope.info.NRD24 = !!parseInt($scope.info.NRD24);
            }
            if (typeof $scope.info.NRD26 !== 'boolean') {
              $scope.info.NRD26 = !!parseInt($scope.info.NRD26);
            }
            if (typeof $scope.info.NRD28 !== 'boolean') {
              $scope.info.NRD28 = !!parseInt($scope.info.NRD28);
            }
            if (typeof $scope.info.NRD29 !== 'boolean') {
              $scope.info.NRD29 = !!parseInt($scope.info.NRD29);
            }
            if (typeof $scope.info.NRD30 !== 'boolean') {
              $scope.info.NRD30 = !!parseInt($scope.info.NRD30);
            }
            if (typeof $scope.info.NRD31 !== 'boolean') {
              $scope.info.NRD31 = !!parseInt($scope.info.NRD31);
            }
            if (typeof $scope.info.NRD32 !== 'boolean') {
              $scope.info.NRD32 = !!parseInt($scope.info.NRD32);
            }
            if (typeof $scope.info.NRD36 !== 'boolean') {
              $scope.info.NRD36 = !!parseInt($scope.info.NRD36);
            }
            if (typeof $scope.info.NRD38 !== 'boolean') {
              $scope.info.NRD38 = !!parseInt($scope.info.NRD38);
            }
          }, function () {
            $scope.info = new NonResident();
          });
      };


      $scope.actions = {

        save: function () {
          $scope.info.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },


        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        },


        open: function (key) {
          $scope.params['opened' + key] = true;
        }
      };
    }
  );
