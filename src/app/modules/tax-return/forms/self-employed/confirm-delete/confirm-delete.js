angular.module('modules.tax-return.forms.self-employed.confirm-delete', [])

	.controller('SelfEmployedConfirmDeleteController',
		function ($scope, $uibModalInstance) {

			// ok
			$scope.ok = function () {
				$uibModalInstance.close();
			};

			// cancel
			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}
	);
