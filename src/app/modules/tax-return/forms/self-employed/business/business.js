angular.module('modules.tax-return.forms.self-employed.select-business', [])

	.controller('SelfEmployedSelectBusinessController',
		function ($scope, $uibModalInstance, Business) {


			$scope.businesses = Business.list;
			$scope.business = {
				selected: $scope.businesses[0]
			};

			$scope.ok = function (){
				$uibModalInstance.close($scope.business.selected);
			};

			$scope.cancel = function (){
				$uibModalInstance.dismiss('cancel');
			};
		}
	);
