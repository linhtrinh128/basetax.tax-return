angular.module('modules.tax-return.forms.self-employed', [
  'modules.tax-return.forms.self-employed.select-business',
  'modules.tax-return.forms.self-employed.confirm-delete'
])

  .controller('SelfEmployedController',
    function ($scope, $uibModal, $state, $uibModalInstance, utils, security, forms,
              selfEmployeds, SelfEmployed, CONFIGS, data) {

      $scope.dateFormat = CONFIGS.DATE_FORMAT;
      $scope.dateOptions = CONFIGS.DATE_OPTIONS;
      $scope.params = {
        open1: false,
        open2: false,
        open3: false,
        open4: false,
        open5: false,
        open6: false
      };

      $scope.businessId = '';


      // Watch 17-30 => 31
      $scope.$watch('[' +
        'self.FSE17, self.FSE18, self.FSE19, self.FSE20, self.FSE21, self.FSE22, self.FSE23, self.FSE24' +
        'self.FSE25, self.FSE26, self.FSE27, self.FSE28, self.FSE29, self.FSE30]',
        function () {
          $scope.self.FSE31 = total(17, 30);
        }, true);

      // Watch 32-45 => 46
      $scope.$watch('[' +
        'self.FSE32, self.FSE33, self.FSE34, self.FSE35, self.FSE36, self.FSE37, self.FSE38, self.FSE39' +
        'self.FSE40, self.FSE41, self.FSE42, self.FSE43, self.FSE44, self.FSE45]',
        function () {
          $scope.self.FSE46 = total(32, 45);
        }, true);

      // Watch 15,16,31,46 => 47,48
      $scope.$watch('[self.FSE15, self.FSE16, self.FSE31]',
        function () {
          var value = $scope.self.FSE15 ? parseFloat($scope.self.FSE15) : 0;
          value += parseFloat($scope.self.FSE16);
          value -= parseFloat($scope.self.FSE31);
          value += parseFloat($scope.self.FSE46);
          $scope.self.FSE47 = value > 0 ? value : 0;
          $scope.self.FSE48 = value < 0 ? value : 0;
        }, true);

      // Watch 49-56 => 57
      $scope.$watch('[self.FSE49, self.FSE50, self.FSE51, self.FSE52, self.FSE53, self.FSE54, self.FSE55' +
        'self.FSE56]',
        function () {
          $scope.self.FSE57 = total(49, 56);
        }, true);

      // Watch 47,48,61,63 => 64,65
      $scope.$watch('[self.FSE47, self.FSE48, self.FSE61, self.FSE63]',
        function () {
          var value = $scope.self.FSE47 ? parseFloat($scope.self.FSE47) : 0;
          value += $scope.self.FSE48 ? parseFloat($scope.self.FSE48) : 0;
          value -= $scope.self.FSE61 ? parseFloat($scope.self.FSE61) : 0;
          value -= $scope.self.FSE63 ? parseFloat($scope.self.FSE63) : 0;
          $scope.self.FSE64 = value > 0 ? value : 0;
          $scope.self.FSE65 = value < 0 ? value : 0;
        }, true);

      // Watch 64,65,68-72,74,75 => 73,76
      $scope.$watch('self', function () {
        // 64 - 65
        $scope.self.FSE73 = $scope.self.FSE64 ? parseFloat($scope.self.FSE64) : 0;
        $scope.self.FSE73 += $scope.self.FSE65 ? parseFloat($scope.self.FSE65) : 0;
        $scope.self.FSE73 -= parseFloat(total(68, 72));

        $scope.self.FSE76 = $scope.self.FSE73 ? parseFloat($scope.self.FSE73) : 0;
        $scope.self.FSE76 -= $scope.self.FSE74 ? parseFloat($scope.self.FSE74) : 0;
        $scope.self.FSE76 += $scope.self.FSE75 ? parseFloat($scope.self.FSE75) : 0;
      }, true);

      /**
       * Initialize
       */
      $scope.init = function () {
        // Check is Update
        $scope.step = 1;

        // New object Self Employed
        $scope.self = data;
      };


      /**
       * Actions
       */
      $scope.actions = {

        /**
         * Create transaction business
         *
         */
        createTransaction: function () {
          forms.openCreateTransaction();
        },

        /**
         * save
         */
        save: function () {
          // set format date time
          if ($scope.self.FSE6) {
            $scope.self.FSE6 = utils.formatDateTime($scope.self.FSE6);
          }
          if ($scope.self.FSE7) {
            $scope.self.FSE7 = utils.formatDateTime($scope.self.FSE7);
          }
          if ($scope.self.FSE8) {
            $scope.self.FSE8 = utils.formatDateTime($scope.self.FSE8);
          }
          if ($scope.self.FSE9) {
            $scope.self.FSE9 = utils.formatDateTime($scope.self.FSE9);
          }
          if ($scope.self.FSE66) {
            $scope.self.FSE66 = utils.formatDateTime($scope.self.FSE66);
          }
          if ($scope.self.FSE67) {
            $scope.self.FSE67 = utils.formatDateTime($scope.self.FSE67);
          }

          // save
          $scope.self.save().then(function () {
            if ($scope.step < 5) {
              $scope.step += 1;
            } else {
              $uibModalInstance.close($scope.self);
            }
          });
        },

        /**
         * Cancels
         */
        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        },

        /**
         * Open datepicker
         * @param type
         */
        open: function (type) {
          $scope.params['opened' + type] = true;
        },

        /**
         * format date
         */
        formatDate: utils.formatDate,

        // import transaction
        openImport: function () {
          var modalInstance = $uibModal.open({
            templateUrl: 'modules/tax-return/forms/self-employed/business/business.tpl.html',
            controller: 'SelfEmployedSelectBusinessController'
          });
          modalInstance.result
            .then(function (result) {
              $scope.actions.import(result);
            });
        },

        // import
        import: function (business) {
          $scope.self.business_id = business.id;
          $scope.importing = true;
          $scope.self.import().then(function () {
            //
          }).finally(function () {
            $scope.importing = false;
          });
        },

        //jump to
        jumpTo: function (step) {
          if (step >= $scope.step) {
            return;
          } else {
            $scope.step = step;
          }
        },

        // close
        close: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };


      /**
       * total
       * @param start
       * @param end
       * @returns {number}
       */
      function total(start, end) {
        var value = 0;
        for (var i = start; i <= end; i++) {
          if ($scope.self['FSE' + i]) {
            value += $scope.self['FSE' + i] ? parseFloat($scope.self['FSE' + i]) : 0;
          }
        }
        value = Math.round(value * 100) / 100;
        return value;
      }
    }
  );
