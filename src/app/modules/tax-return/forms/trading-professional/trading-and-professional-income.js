angular.module('modules.tax-return.forms.partnership.trading-and-professional-income', [])

  .controller('TradingAndProfessionalIncomeController',
    function ($rootScope, $scope, $uibModalInstance, PartnerTrading, TaxYear, CONFIGS) {

      // Params date picker
      $scope.dateFormat = CONFIGS.DATE_FORMAT;
      $scope.dateOptions = CONFIGS.DATE_OPTIONS;
      
      var save = function (isValid) {
          if (isValid) {
            $scope.partnerTrading.save()
              .then(function () {
                $uibModalInstance.dismiss('cancel');
              });
          }
        },

        init = function () {
          PartnerTrading.findByTaxYear(TaxYear.current.id)
            .then(function (res) {
              $scope.partnerTrading = res;
            }, function () {
              $scope.partnerTrading = new PartnerTrading();
            });
        };

      $scope.init = function () {
        return init();
      };

      $scope.actions = {

        save: function (isValid) {
          return save(isValid);
        },

        openDoB: function () {
          $scope.opened = true;
        },


        open: function () {
          $scope.opened1 = true;
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );