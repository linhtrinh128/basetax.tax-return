angular.module('modules.tax-return.forms.partnership.disposable-changeable-asset', [
  'modules.tax-return.forms.partnership.disposable-changeable-asset.disposal-asset'
])

  .controller('DisposalChangeableController',
    function ($rootScope, $scope, $uibModal, $uibModalInstance, TaxYear, security, utils,
              DisposalChangeable) {

      $scope.total = 0;

      /**
       * Watch disposal
       */
      $scope.$watch('disposal', function () {
        $scope.total = 0;
        for (var index in $scope.disposal.disposal_proceeds) {
          $scope.total += parseFloat($scope.disposal.disposal_proceeds[index].proceed);
        }
      }, true);

      /**
       * Init
       */
      $scope.init = function () {
        DisposalChangeable.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.disposal = res;
          }, function () {
            $scope.disposal = new DisposalChangeable();
          });
      };
      
// TODO fix save disposal

      $scope.actions = {

        addDisposal: function () {
          $scope.disposal.addDisposal()
            .then(function () {
              //
            });
        },

        deleteDisposal: function (index) {
          $scope.disposal.disposal_proceeds.splice(index, 1);
        },

        save: function (isValid) {
          if (isValid) {
            $scope.disposal.save().then(function () {
              $uibModalInstance.dismiss('cancel');
            });
          }
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );