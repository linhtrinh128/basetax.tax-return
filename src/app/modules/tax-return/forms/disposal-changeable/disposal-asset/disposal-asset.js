angular.module('modules.tax-return.forms.partnership.disposable-changeable-asset.disposal-asset', [])

  .controller('DisposalAssetController',
    function ($rootScope, $scope, $uibModalInstance, disposalProceeds) {
      $scope.disposalAsset = {
        description: '',
        share: 0,
        proceed: 0,
        information: ''
      };

      $scope.save = function () {
        disposalProceeds.push($scope.disposalAsset);
        $scope.cancel();
      };

      $scope.cancel = function () {
        //$uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(disposalProceeds);
      };
    }
  );