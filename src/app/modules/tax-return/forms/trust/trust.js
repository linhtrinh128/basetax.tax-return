angular.module('modules.tax-return.forms.trust', [])

  .controller('TrustController',
    function ($scope, $state, $uibModalInstance, TaxYear, TrustIncome) {

      $scope.init = function () {
        TrustIncome.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.trust = res;

            if (typeof $scope.trust.TRU6 !== 'boolean') {
              $scope.trust.TRU6 = !!parseInt($scope.trust.TRU6);
            }
          }, function () {
            $scope.trust = new TrustIncome();
          });
      };


      $scope.actions = {

        save: function () {
          $scope.trust.save().then(function () {
            $uibModalInstance.dismiss('cancel');
          });
        },

        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        }
      };
    }
  );
