angular.module('modules.tax-return.welcome', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.welcome', {
        url: '/welcome',
        views: {
          'main-content': {
            templateUrl: 'modules/tax-return/welcome/welcome.tpl.html',
            controller: 'WelcomeTaxReturnController'
          }
        }
      });
  })

  .controller('WelcomeTaxReturnController',
    function ($scope, $rootScope, $state, TaxYear, TAX_RETURN) {

      // Watch i am a
      $scope.$watch('option.userType', function () {
        switch ($scope.option.userType) {
          case 1:
            for (var i in $scope.individualForms) {
              if ($scope.individualForms[i].key === 'resident-remittance') {
                $scope.individualForms[i].selected = false;
                return;
              }
            }
            break;
          case 2:
          case 3:
            for (var j in $scope.individualForms) {
              if ($scope.individualForms[j].key === 'resident-remittance') {
                $scope.individualForms[j].selected = true;
                return;
              }
            }
            break;
          default:
            break;
        }
      }, true);

      /**
       * Watch option.taxReturnType
       */
      $scope.$watch('option.taxReturnType', function () {
        $scope.initStatus();
      }, true);

      $scope.$watch('taxReturn', function () {
        $scope.getStatus();
      });


      /**
       * select tax year
       */
      $scope.selectYear = function (year) {
        TaxYear.setCurrent(year);
        $scope.taxYear = year;
      };

      // Select tax return type
      $scope.selectTaxReturnType = function (type) {
        $scope.option.taxReturnType = type;
      };

      // start Tax return
      $scope.start = function () {
        var step = $scope.taxReturn.params && $scope.taxReturn.params.step ? $scope.taxReturn.params.step : 1;

        switch ($scope.option.taxReturnType.value) {
          // an individual self assessment
          case TAX_RETURN.TYPE_INDIVIDUAL_VALUE:
            $state.go('loggedIn.modules.tax-return.individual.' + step, {
              "taxYear": $scope.taxYear.description
            });
            break;

          // a partnership return
          case TAX_RETURN.TYPE_PARTNERSHIP_VALUE:
            $state.go('loggedIn.modules.tax-return.partnership.' + step, {
              "taxYear": $scope.taxYear.description
            });
            break;

          default:
            break;
        }
      };
    });
