angular.module('modules.tax-return', [
  'modules.tax-return.utils',
  'modules.tax-return.calculator',
  'modules.tax-return.welcome',
  'modules.tax-return.forms',

  'modules.tax-return.individual',
  'modules.tax-return.partnership',

  'modules.tax-return.thanks'
])

  .config(function ($stateProvider) {

    $stateProvider
      .state('loggedIn.modules.tax-return', {
        url: '/tax-return',
        abstract: true,
        views: {
          'main-content': {
            templateUrl: 'modules/tax-return/tax-return.tpl.html',
            controller: 'TaxReturnController'
          }
        },
        resolve: {
          getTaxReturn: function (TaxReturn) {
            return TaxReturn.all();
          },
          get: function (TaxYear) {
            // return taxYears.setCurrent(utils.selectCurrentYearTaxReturn(taxYears.list));
            return TaxYear.setDefaultYearTaxReturn();
          },
          getAllTaxReturn: function (all) {
            return all.getAllTaxReturn();
          }
        }
      });
  })

  .controller('TaxReturnController',
    function ($scope, $rootScope, $state, $stateParams, $sce, $timeout, utils, security, profile, CONFIGS,
              utilsTaxReturn, TAX_RETURN, formsTaxReturn, calculatorTaxReturn, TaxReturn,
              TaxYear, selfEmployeds, TransactionEmp14,
              IndividualForm, PartnershipForm) {

      $scope.taxYears = angular.copy(TaxYear.list);
      $scope.taxYear = angular.copy(TaxYear.current);
      $scope.taxReturns = angular.copy(TaxReturn.list);
      $scope.taxReturnTypes = [
        {description: "an individual self assessment", value: 1},
        {description: "a partnership return", value: 2}
      ];
      $scope.userTypes = [
        {description: "UK resident", value: 1},
        {description: "Non UK resident", value: 2},
        {description: "Non-domiciled resident", value: 3}
      ];
      $scope.option = {
        taxReturnType: $scope.taxReturnTypes[0],
        userType: 1
      };

      var getStatus = function () {
        var params = $scope.taxReturn.params;

        if (!params) {
          return;
        }

        // Income forms
        for (var i1 in params.incomes) {
          if (params.incomes[i1].form === $scope.individualForms[i1].key ||
            params.incomes[i1].key === $scope.individualForms[i1].key) {

            $scope.individualForms[i1].selected = params.incomes[i1].selected;
            $scope.individualForms[i1].existing = params.incomes[i1].existing;
          }
        }

        // Partnership form
        for (var i3 in $scope.taxReturn.params.partnershipForms) {
          if (params.partnershipForms[i3].form === $scope.partnershipForms[i3].key ||
            params.partnershipForms[i3].key === $scope.partnershipForms[i3].key) {

            $scope.partnershipForms[i3].selected = params.partnershipForms[i3].selected;
            $scope.partnershipForms[i3].existing = params.partnershipForms[i3].existing;
          }
        }

        // I want ot file
        if (typeof $scope.taxReturn.type_id !== 'undefined') { // I want to file Status
          $scope.iWantToFile = $scope.taxReturn.type_id;
          for (var i6 in $scope.taxReturnTypes) {
            if ($scope.taxReturnTypes[i6].value == $scope.iWantToFile) {
              $scope.option.taxReturnTypes = $scope.taxReturnTypes[i6];
            }
          }
        }

        // I am a
        if (typeof params.iAmA !== 'undefined') { // I am a... status
          $scope.option.userType = params.iAmA;
        }

        // Is payment
        if (typeof params.isPayment !== 'undefined') {
          $scope.isPayment = params.isPayment;
        }
      };

      var init = function () {

        $scope.steps = null;
        $scope.welcome = null;
        $scope.iWantToFile = $scope.option.taxReturnType.value; // 1 or 0
        $scope.iAmA = $scope.option.userType = 1; // 1 or 0
        $scope.isPayment = false; // not yet

        // Data forms
        $scope.forms = formsTaxReturn.forms;

        // Initialize income forms, deduction Forms, partnershipForms
        $scope.individualForms = IndividualForm.init();
        $scope.partnershipForms = PartnershipForm.init();

        // Tax return
        $scope.taxReturn = null;
        $scope.initStatus();

        // Get status user
        $scope.getStatus();

        // Check existing Income forms
        angular.forEach($scope.individualForms, function (form) {
          form.checkExisting();
        });

        // Check existing partnership forms
        angular.forEach($scope.partnershipForms, function (form) {
          form.checkExisting();
        });
      };

      var initStatus = function () {
        $scope.taxReturn = {};
        for (var i in $scope.taxReturns) {
          if ($scope.taxReturns[i].tax_year_id === $scope.taxYear.id &&
            $scope.taxReturns[i].type_id === $scope.option.taxReturnType.value) {

            $scope.taxReturn = $scope.taxReturns[i];
            break;
          }
        }
        if (utils.isObjectEmpty($scope.taxReturn)) {
          TaxReturn.create({
            tax_year_id: $scope.taxYear.id,
            type_id: $scope.option.taxReturnType.value
          }).then(function (res) {
            $scope.taxReturn = res;
          });
        }
      };

      var saveStatus = function (step) {
        // list income
        var listIncomes = [];
        for (var i in $scope.individualForms) {
          listIncomes.push({
            key: $scope.individualForms[i].key,
            selected: $scope.individualForms[i].selected,
            existing: $scope.individualForms[i].existing
          });
        }

        // List partnership form
        var listPartnerships = [];
        for (var p in $scope.partnershipForms) {
          listPartnerships.push({
            key: $scope.partnershipForms[p].key,
            selected: $scope.partnershipForms[p].selected,
            existing: $scope.partnershipForms[p].existing
          });
        }

        //save status into userStatus
        var params = {
          step: step,
          iAmA: $scope.option.userType,
          incomes: listIncomes,
          partnershipForms: listPartnerships
        };

        TaxReturn.updateParams({
          id: $scope.taxReturn.id,
          params: params
        }).then(function (res) {
          // security.currentUser.status = res.status;
        });
      };

      var calculateTax = function () {
        $scope.isCalculating = true;
        $scope.cal_result = null;

        formsTaxReturn.checkExistingForm(TAX_RETURN.KEY.EMPLOYMENT);
        formsTaxReturn.checkExistingForm(TAX_RETURN.KEY.UK_PROPERTY);

        calculatorTaxReturn.calculate($scope.taxYear)
          .then(function (res) {
            $scope.cal_result = $sce.trustAsHtml(res.tax_desc);
            $scope.estimatedTax = res.estimatedTax;
          }, function error(error) {
            console.log('Error tax calculation ' + error);
          })
          .finally(function () {
            $scope.isCalculating = false;
          });
      };

      // TODO remove tax year changed
      /**
       * Event tax years current changed
       */
      $scope.$on('taxYears:current:changed', function () {
        $scope.taxYear = TaxYear.current;
        init();
      });

      $scope.$on('taxReturns:list:updated', function () {
        $scope.taxReturns = angular.copy(TaxReturn.list);
        initStatus();
      });

      //Initialize
      $scope.init = function () {
        return init();
      };

      // init status
      $scope.initStatus = function () {
        return initStatus();
      };

      // save status
      $scope.saveStatus = function (step) {
        return saveStatus(step);
      };

      // calculate tax_calculation
      $scope.calculate = function () {
        return calculateTax();
      };

      // get status
      $scope.getStatus = function () {
        return getStatus();
      };
    }
  );

