angular.module('modules.tax-return.utils', [])

  .factory('utilsTaxReturn',
    function () {

      var service = {
        isExistingByYear: function (list, year, callback) {
          for (var j in list) {
            if (list[j] != null) {
              if (parseInt(list[j].tax_year_id) === parseInt(year.id)) {
                if (typeof(callback) === 'function') {
                  callback(list[j]);
                  return true;
                }
              }
            }
          }
          return false;
        },
        
        isExistingByYear2: function (list, year, callback) {
          var temp = [];
          for (var j in list) {
            if (list[j] != null) {
              if (parseInt(list[j].tax_year_id) === parseInt(year.id)) {
                temp.push(list[j]);
              }
            }
          }
          if (temp.length > 0) {
            if (typeof(callback) === 'function') {
              callback(temp);
              return true;
            }
          }
          return false;
        },
        
        isExistingByFormType: function (list, type, year, callback) {
          for (var i in list) {
            if (parseInt(list[i].tax_year_id) === parseInt(year.id)) {
              if (parseInt(list[i].form_type) === parseInt(type)) {
                if (typeof(callback) === 'function') {
                  callback(list[i]);
                }
                return true;
              }
            }
          }
          return false;
        }
      };

      return service;
    }
  );

