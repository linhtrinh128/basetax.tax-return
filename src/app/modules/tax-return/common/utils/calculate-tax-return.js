angular.module('modules.tax-return.calculator', [])

  .factory('calculatorTaxReturn',
    function ($q, calculator, security, formsTaxReturn, TaxYear, CONFIGS, apiService) {

      var service = {

        /**
         * calculate
         *
         * @param taxYear
         * @returns {deferred.promise|{then, catch, finally}}
         */
        calculate: function (taxYear) {
          var deferred = $q.defer();
          apiService.all('tax-calculation' + '/' + TaxYear.current.id)
            .then(function (res) {
              res = res.data;
              if (res.data != null) {
                deferred.resolve(res.data.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });
          return deferred.promise;
        },

        // export
        export: function () {
          var deferred = $q.defer();
          apiService.all('tax-calculation/' + TaxYear.current.id + '/export')
            .then(function (res) {
              res = res.data;
              if (res.data != null) {
                deferred.resolve(res.data.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });
          return deferred.promise;
        }
      };

      return service;
    }
  );

