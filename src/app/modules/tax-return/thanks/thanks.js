angular.module('modules.tax-return.thanks', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.thanks', {
        url: '/thanks',
        views: {
          'main-content': {
            templateUrl: 'modules/tax-return/thanks/thanks.tpl.html',
            controller: 'ThanksTaxReturnController'
          }
        }
      });
  })

  .controller('ThanksTaxReturnController',
    function ($scope, $rootScope, $state) {


    });
