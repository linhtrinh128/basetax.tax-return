angular.module('modules.tax-return.partnership.step-payment-n-file', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.partnership.5', {
        url: '/payment-n-file',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/partnership/step-payment-n-file/step-payment-n-file.tpl.html',
            controller: 'StepPartnershipPaymentNFileController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/partnership/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        }
      });
  })

  .controller('StepPartnershipPaymentNFileController',
    function ($rootScope, $scope, $uibModal, utils, security,
              payments, TaxReturn, messages) {

      $scope.$parent.step = 5;

      $scope.plans = [
        {
          key: 'planA',
          name: 'Plan A',
          price: 29.90,
          per: '/return',
          list_descriptions: [
            '- Online filling'
          ],
          event: function () {
            $scope.pay(this.key, this.price);
          }
        }, {
          key: 'planB',
          name: 'Plan B',
          price: 79.90,
          per: '/return',
          list_descriptions: [
            '- Online filling',
            '- Tax support',
            '- Tax saving tips',
            '- Free tax review'
          ],
          event: function () {
            $scope.pay(this.key, this.price);
          }
        }, {
          key: 'planC',
          name: 'Plan C',
          price: 99.90,
          per: '/return',
          list_descriptions: [
            '- Online filling',
            '- Tax support',
            '- Tax saving tips',
            '- Free tax review',
            '- Personal tax agent',
            '- Tax audit support',
            '- Live chat'
          ],
          event: function () {
            $scope.pay(this.key, this.price);
          }
        }
      ];
      $scope.isPayment = false;
      $scope.pollResponse = false;

      /**
       * Event payment
       */
      $scope.$on('payment', function (event, payment) {
        if (payment.success) {
          $scope.isPayment = true;
        }
      });

      /**
       * Init
       */
      $scope.init = function () {
        payments.checkPaymentTaxReturn($scope.taxYear.id, $scope.$parent.option.taxReturnType.value)
          .then(function (res) {
            $scope.isPayment = res.success;
          });
      };


      /**
       * filling
       *
       */
      $scope.filing = function () {
        TaxReturn.update({
          id: $scope.taxReturn.id,
          submitted: new Date(),
          received_by_hmrc: new Date(),
          status: '2'
        }).then(function (res) {
          $scope.taxReturn = res;
        });

        messages.show({
          title: 'Your tax return has been submitted.',
          content: 'Thank you for your submission. You should be receiving a confirmation by email from Basetax with the ' +
          'detail of response from HMRC. Please also check your junk mail folder. We will contact you if there is ' +
          'any issue with your tax return we will contact you immediately.'
        });
      };

      /**
       * Pay
       *
       * @param payment_type
       */
      $scope.pay = function (payment_type, amount) {
        var param = {
          taxYear: $scope.taxYear,
          plan: payment_type,
          amount: amount,
          typeId: $scope.$parent.option.taxReturnType.value
        };
        payments.showPaymentTaxReturn(param);
      };

      /**
       * Get status
       *
       * @param n
       * @returns {*}
       */
      $scope.getStatus = function (n) {
        switch (n) {
          case '1':
          case 1:
            return 'In progress';
          case '2':
          case 2:
            return 'Filed';
          case '3':
          case 3:
            return 'Rejected';
          default:
            return 'In progress.';
        }
      };

      /**
       * Format date
       */
      $scope.formatDate = utils.formatDate;
    });
