angular.module('modules.tax-return.partnership', [
    'modules.tax-return.partnership.progress-bar',
    'modules.tax-return.partnership.step-income',
    'modules.tax-return.partnership.step-partner-detail',
    'modules.tax-return.partnership.step-partnership-detail',
    'modules.tax-return.partnership.step-payment-n-file',
    'modules.tax-return.partnership.step-summary'
  ])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.partnership', {
        url: '/:taxYear/partnership',
        abstract: true,
        views: {
          'main-content': {
            templateUrl: 'modules/tax-return/partnership/partnership.tpl.html',
            controller: 'PartnershipTaxReturnController'
          }
        }
      });
  })

  .controller('PartnershipTaxReturnController',
    function ($scope, $state, $filter, TAX_RETURN, utils, partnershipDetails) {

      // Steps, offset, progress
      $scope.steps = TAX_RETURN.PARTNERSHIP.STEPS;
      $scope.offset = 100 / ($scope.steps.length - 1);
      $scope.step = 1;
      $scope.progress = 0;

      // Other params
      $scope.isSaved = true;
      $scope.confirmed_date = utils.storage.get('confirm_date');
      $scope.confirm_of_declaration = !!$scope.confirmed_date;
      $scope.errorMessage = null;


      /**
       * Event confirm of declaration
       */
      $scope.$on('confirm_of_declaration', function (event, value) {
        $scope.confirm_of_declaration = value;
        if (value) {
          $scope.confirmed_date = $filter('date')(new Date(), 'dd MMM yyyy hh:mm');
          utils.storage.set('confirm_date', $scope.confirmed_date);
          $scope.errorConfirm = null;
        } else {
          $scope.confirmed_date = null;
        }
      });

      /**
       * Event information:saved
       */
      $scope.$on('information:saved', function (event, value) {
        $scope.isSaved = value;
      });

      // Watch step
      $scope.$watch('step', function (newValue) {
        if (newValue > 0) {
          $scope.welcome = false;
          $scope.progress = $scope.actions.calculateProgress($scope.step);
          $scope.saveStatus($scope.step);
        } else {
          $scope.welcome = true;
        }
      }, true);


      // Partnership Detail
      $scope.init = function () {
        // Get partnership detail
        partnershipDetails.findByTaxYear($scope.taxYear).then(function (res) {
          if (res.length !== 0) {
            $scope.partnershipDetail = angular.copy(res[0]);
            partnershipDetails.setCurrent($scope.partnershipDetail);
          } else {
            partnershipDetails.create({tax_year_id: $scope.taxYear.id})
              .then(function success(res) {
                $scope.partnershipDetail = res;
                partnershipDetails.setCurrent($scope.partnershipDetail);
              });
          }
        });

        // Find tax return status
        for (var i in $scope.taxReturns) {
          if ($scope.taxReturns[i].tax_year_id === $scope.taxYear.id && $scope.taxReturns[i].type_id === 2) {
            $scope.$parent.iWantToFile = 2;
            $scope.$parent.taxReturn = $scope.taxReturns[i];
            break;
          }
        }
      };
      $scope.init();


      // ACTIONS
      $scope.actions = {

        // next
        next: function () {
          // current step
          var arr = $state.$current.self.name.split(".");
          $scope.step = parseInt(arr[arr.length - 1]);

          // Last step
          if ($scope.step === $scope.steps.length) {
            $state.go('^.^.thanks');
          }

          if ($scope.step === 2 && $scope.errorMessage) {
            return;
          }

          // Step 4 summary
          if ($scope.step === 4 && !$scope.confirm_of_declaration) {
            //step personal information
            $scope.errorConfirm = "Please confirm this declaration";
            $scope.confirmed_date = null;
            return;
          }

          // step change
          $scope.$broadcast('partnership:step:change', $scope.step + 1);

          $state.go('^.' + (++$scope.step));
        },

        // previous
        previous: function () {
          var arr = $state.$current.self.name.split(".");
          $scope.step = parseInt(arr[arr.length - 1]);

          if ($scope.step === 1) {
            $state.go('^.^.welcome');
            $scope.welcome = true;
          }

          // step change
          $scope.$broadcast('partnership:step:change', $scope.step - 1);

          $state.go('^.' + (--$scope.step));
        },

        // jump
        jump: function (step) {
          if (step > 0) {
            
            // Step 4 review
            if ($scope.step === 4 && !$scope.confirm_of_declaration) {
              //step personal information
              $scope.errorConfirm = "Please confirm this declaration";
              $scope.confirmed_date = null;
              return;
            } else {
              utils.storage.set('confirm_date', $filter('date')(new Date(), 'dd MMM yyyy hh:mm'));
              $scope.errorConfirm = null;
            }
            
            $scope.step = step;

            // step change
            $scope.$broadcast('partnership:step:change', step);

            // Go state
            $state.go('^.' + step);
          } else {
            $scope.welcome = true;
          }
        },

        // calculate Progress
        calculateProgress: function (step) {
          return $scope.offset * (step - 1);
        }
      };
    });
