angular.module('modules.tax-return.partnership.step-summary', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.partnership.4', {
        url: '/summary',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/partnership/step-summary/step-summary.tpl.html',
            controller: 'StepPartnershipSummaryController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/partnership/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        }
      });
  })

  .controller('StepPartnershipSummaryController',
    function ($scope, $rootScope, $uibModal, utils, $sce, security, TaxYear, formsTaxReturn, Export) {

      $scope.$parent.step = 4;

      /**
       * Event resources updated
       */
      $scope.$on('partner-trading:list:updated', pushForms);
      $scope.$on('partner-properties:list:updated', pushForms);
      $scope.$on('disposal-changeables:list:updated', pushForms);
      $scope.$on('other-main-incomes:list:updated', pushForms);
      $scope.$on('otherInformation:list:updated', pushForms);

      /**
       * Watch confirm_of_declaration
       */
      $scope.$watch('confirm_of_declaration', function () {
        if ($scope.initializing) {
          $rootScope.$broadcast('confirm_of_declaration', $scope.confirm_of_declaration);
        } else {
          $scope.initializing = true;
        }
      });


      /**
       * ACTIONS
       */
      $scope.actions = {

        /**
         * Open form
         *
         * @param form
         */
        open: function (form) {
          formsTaxReturn.open(form);
        },

        /**
         * Check form
         *
         * @param key
         * @returns {*}
         */
        checkForm: function (key) {
          for (var i in $scope.partnershipForms) {
            if ($scope.partnershipForms[i].key === key) {
              return $scope.partnershipForms[i].selected && $scope.partnershipForms[i].existing;
            }
          }
          return false;
        },

        /**
         * Download pdf
         */
        download: function () {
          $scope.isSubmit = true;

          var params = {
            yearId: $scope.taxYear.id
          };

          // Export
          Export.exportTaxReviewPS(params).then(function (data) {
            // Open link
            window.open(data, '_blank');

          }, function error(error) {
            // Error callback
          }).finally(function () {
            $scope.isSubmit = false;
          });
        }
      };


      $scope.formatAmount = utils.formatAmount;


      $scope.formatDate = utils.formatDate;

      /**
       * Init
       */
      $scope.init = function () {

        // Calculate Total Disposal
        $scope.totalDisposal = 0;
        if ($scope.forms.disposalChangeables) {
          for (var j in $scope.forms.disposalChangeables.disposal_proceeds) {
            $scope.totalDisposal += parseFloat($scope.forms.disposalChangeables.disposal_proceeds[j].proceed);
          }
        }

        // Check existing , push forms
        // TODO remove checkExistingFormPartnership
        $scope.partnershipForms = formsTaxReturn.checkExistingFormPartnership($scope.partnershipForms);
      };


      /**
       * Push forms
       */
      function pushForms() {
        $scope.partnershipForms = formsTaxReturn.checkExistingFormPartnership($scope.partnershipForms);

        //confirm again
        $scope.confirm_of_declaration = false;
      }
    }
  );
