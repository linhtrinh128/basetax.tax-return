angular.module('modules.tax-return.partnership.step-income', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.partnership.1', {
        url: '/income',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/partnership/step-income/step-income.tpl.html',
            controller: 'StepPartnershipIncomeController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/partnership/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        }
      });
  })

  .controller('StepPartnershipIncomeController',
    function ($scope, formsTaxReturn) {

      $scope.$parent.step = 1;

      $scope.open = function (form) {
        formsTaxReturn.open(form);
      };
    }
  );
