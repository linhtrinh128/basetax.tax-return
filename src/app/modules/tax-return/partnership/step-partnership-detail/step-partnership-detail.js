angular.module('modules.tax-return.partnership.step-partnership-detail', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.partnership.3', {
        url: '/partnership-detail',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/partnership/step-partnership-detail/step-partnership-detail.tpl.html',
            controller: 'StepPartnershipDetailController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/partnership/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        },
        resolve: {
          $partnershipDetails: function (PartnershipDetail) {
            return PartnershipDetail.all();
          }
        }
      });
  })

  .controller('StepPartnershipDetailController',
    function ($rootScope, $scope, partnershipDetails, PartnershipDetail, TaxYear) {

      $scope.$parent.step = 3;


      /**
       * Init
       */
      $scope.init = function () {

        $scope.partnershipDetails = PartnershipDetail.list;

        for (var i in $scope.partnershipDetails) {
          if ($scope.partnershipDetails[i].tax_year_id === TaxYear.current.id) {
            $scope.partnershipDetail = $scope.partnershipDetails[i];
            break;
          }
        }

        if (!$scope.partnershipDetail) {
          $scope.partnershipDetail = new PartnershipDetail();
        }
      };

      /**
       * Watch partnershipDetail
       */
      $scope.$watch('partnershipDetail', function () {
        if (!angular.equals($scope.partnershipDetail, $scope.temp)) {
          $scope.errorMessage = "You have changed partnership information. Please click \"Save\".";
          $rootScope.$broadcast('information:saved', false);
          $rootScope.$broadcast('information:step', 25);
        } else {
          $scope.errorMessage = false;
          $rootScope.$broadcast('information:saved', true);
        }
      }, true);

      // on
      $scope.$on('partnership:step:change', function (event, step) {
        $scope.partnershipDetail.save();
      });

      /**
       * Save partnership detail
       *
       * @param isValid
       */
      $scope.save = function (isValid) {
        if (isValid) {
          if (!$scope.isUpdate) {
            partnershipDetails.create($scope.partnershipDetail).then(function (res) {
            });
          }
          else {
            partnershipDetails.update($scope.partnershipDetail).then(function (res) {
              $scope.partnershipDetail = angular.copy(res);
              $scope.temp = angular.copy(res);
              $scope.errorMessage = false;
            });
          }
        }
      };
    }
  );
