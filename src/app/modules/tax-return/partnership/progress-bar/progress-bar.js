angular.module('modules.tax-return.partnership.progress-bar', [])
  .controller('ProgressBarCtrl',
    function ($scope) {

      $scope.isSelected = function (progress, step) {
        return parseInt(progress / $scope.offset) + 1 >= step;
      };
    }
  );
