angular.module('modules.tax-return.partnership.step-partner-detail.new-partner', [])

  .controller('CreatePartnerController',
    function ($rootScope, $scope, $uibModalInstance, partnershipDetails, partnerDetails, utils, CONFIGS) {

      $scope.partnerTypes = [
        {name: 'Company'},
        {name: 'Invidual'}
      ];
      $scope.partnerType = $scope.partnerTypes[0];
      $scope.opendoj = false;
      $scope.dateFormat = CONFIGS.DATE_FORMAT;
      $scope.dateOptions = CONFIGS.DATE_OPTIONS;


      /**
       * Partner
       */
      $scope.partner = {
        partnership_detail_id: partnershipDetails.current ? partnershipDetails.current.id : -1,
        type: $scope.partnerType ? $scope.partnerType.value : -1,
        first_name: '',
        last_name: '',
        address: '',
        utr: '',
        ni: '',
        date_joined: '',
        date_left: ''
      };

      /**
       * Watch partner.ni
       */
      $scope.$watch('partner.ni', function (newVal, oldVal) {
        if (angular.equals(newVal, oldVal) || !newVal || newVal === '') {
          return;
        }
        var array = newVal.split('');

        var flag = false;
        for (var i = 0; i < array.length; i++) {
          var char = array[i];
          if (i >= 0 && i <= 1 && /[^\-\d.]/g.test(char)) {
            continue;
          }
          if (i >= 2 && i <= 7 && /[^a-zA-Z]/g.test(char)) {
            continue;
          }
          if (i == 8 && /[^\-\d.]/g.test(char)) {
            continue;
          }

          flag = true;
          $scope.errNI = false;
        }
        if (flag) {
          $scope.partner.ni = oldVal;
        }
      });

      /**
       * Watch partner.date_joined
       */
      $scope.$watch('partner.date_joined', function () {
        if (!angular.equals($scope.partner.date_joined, $scope.dayJoin)) {
          $scope.errDayJoin = checkDay($scope.partner.date_joined, $scope.partner.date_left);
        }
      }, true);

      /**
       * Watch partner.date_left
       */
      $scope.$watch('partner.date_left', function () {
        if (!angular.equals($scope.partner.date_left, $scope.dayLeft)) {
          $scope.errDayLeft = checkDay($scope.partner.date_joined, $scope.partner.date_left);
        }
      }, true);

      /**
       * Init
       */
      $scope.init = function () {
        $scope.partner.type = $scope.partnerTypes[0].name;

        if (!utils.isEmpty(partnerDetails.current)) {
          $scope.partner = angular.copy(partnerDetails.current);

          partnerDetails.resetCurrent();
          $scope.isUpdated = true;
        }

        $scope.dayJoin = angular.copy($scope.partner.date_joined);
        $scope.dayLeft = angular.copy($scope.partner.date_left);
      };


      /**
       * ACTIONS
       */
      $scope.actions = {

        /**
         * Save
         * @param isValid
         */
        save: function (isValid) {
          if (isValid) {
            if (!$scope.isUpdated) {
              partnerDetails.create($scope.partner).then(function (res) {
                $uibModalInstance.dismiss('cancel');
              });
            } else {
              partnerDetails.update($scope.partner).then(function (res) {
                $uibModalInstance.dismiss('cancel');
              });
            }
          }
        },

        /**
         * Cancel
         */
        cancel: function () {
          $uibModalInstance.dismiss('cancel');
        },

        /**
         * Open DoJ
         */
        openDoJ: function () {
          $scope.opendoj = true;
        },

        /**
         * OPen Dol
         */
        openDoL: function () {
          $scope.opendol = true;
        }
      };


      /**
       * Check day
       *
       * @param dayjoin
       * @param dayleft
       * @returns {boolean}
       */
      function checkDay(dayjoin, dayleft) {
        if (new Date(dayjoin) > new Date(dayleft)) {
          return true;
        }
        else {
          return false;
        }
      }
    }
  );
