angular.module('app.taxmapp.partnership.step-partner-detail.new-partner.confirm', [])

.controller('ConfirmDeletePartner',
    function($scope, $uibModalInstance, partners){
        $scope.partner = angular.copy(partners.current);
        partners.resetCurrent();

        $scope.delete = function(){
            partners.delete($scope.partner).then(function(res){
                $uibModalInstance.dismiss('cancel');
            });
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    }
);