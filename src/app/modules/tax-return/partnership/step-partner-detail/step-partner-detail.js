angular.module('modules.tax-return.partnership.step-partner-detail', [
    'modules.tax-return.partnership.step-partner-detail.new-partner'
  ])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.partnership.2', {
        url: '/partner-detail',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/partnership/step-partner-detail/step-partner-detail.tpl.html',
            controller: 'StepPartnerDetailController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/partnership/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        }
      });
  })

  .controller('StepPartnerDetailController',
    function ($rootScope, $scope, $uibModal, utils, security, profile, TaxYear, messages,
              partnerDetails, partnershipDetails, utilsTaxReturn) {


      $scope.$parent.step = 2;
      $scope.count = 0;

      /**
       * Init
       */
      $scope.init = function () {

        // Partner detail
        $scope.partnerDetails = [];

        if ($scope.partnershipDetail) {
          for (var k in partnerDetails.list) {
            if (partnerDetails.list[k].partnership_detail_id == $scope.partnershipDetail.id) {
              $scope.partnerDetails.push(partnerDetails.list[k]);
            }
          }
          $scope.count = $scope.partnerDetails.length;
        } else {
          partnershipDetails.findByTaxYear($scope.taxYear).then(function (res) {
            if (res.length !== 0) {
              $scope.$parent.partnershipDetail = angular.copy(res[0]);
              partnershipDetails.setCurrent($scope.$parent.partnershipDetail);
            } else {
              partnershipDetails.create({tax_year_id: $scope.taxYear.id})
                .then(function success(res) {
                  $scope.$parent.partnershipDetail = res;
                  partnershipDetails.setCurrent($scope.$parent.partnershipDetail);
                });
            }
          });
        }


      };

      /**
       * Partnership details current changed
       */
      $rootScope.$on('partnershipDetails:current:changed', function () {
        if ($scope.partnershipDetail) {
          partnerDetails.findByPartnership($scope.partnershipDetail.id).then(function (res) {
            $scope.partnerDetails = res;

            // Count partner
            $scope.count = $scope.partnerDetails.length;
          });
        }
      });

      /**
       * Partner detail list update
       */
      $scope.$on('partnerDetails:list:updated', function () {
        if ($scope.partnershipDetail) {
          partnerDetails.findByPartnership($scope.partnershipDetail.id).then(function (res) {
            $scope.partnerDetails = res;

            // Count partner
            $scope.count = $scope.partnerDetails.length;
          });
        }
      });

      /**
       * Count
       */
      $scope.$watch('count', function () {
        if ($scope.count < 2) {
          $scope.errorMessage = 'It is mandatory to have a minimum of two partners.';
        } else {
          $scope.errorMessage = false;
        }
      });


      /**
       * ACTIONS
       */
      $scope.actions = {

        /**
         * Open form create new partner
         */
        addPartner: function () {
          $uibModal.open({
            templateUrl: 'modules/tax-return/partnership/step-partner-detail/new-partner/new-partner.tpl.html',
            controller: 'CreatePartnerController'
          });
        },

        /**
         * Delete
         *
         * @param partner
         */
        delete: function (partner) {

          // Show message to confirm delete
          messages.showConfirmDelete({
            resource: 'partner',
            yesCallback: function () {
              partnerDetails.delete(partner);
            }
          });
        },

        // edit
        edit: function (partner) {
          partnerDetails.setCurrent(partner);
          $uibModal.open({
            templateUrl: 'modules/tax-return/partnership/step-partner-detail/new-partner/new-partner.tpl.html',
            controller: 'CreatePartnerController'
          });
        },

        formatDate: utils.formatDate
      };


    }
  );
