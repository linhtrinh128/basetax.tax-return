angular.module('modules.tax-return.individual.step-deduction-relief', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.individual.2', {
        url: '/deduction-relief',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/individual/step-deduction-relief/step-deduction-relief.tpl.html',
            controller: 'StepDeductionReliefController',
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/individual/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        },
        resolve: {
          $reliefs: function (Relief) {
            return Relief.all();
          }
        }
      });
  })

  .controller('StepDeductionReliefController',
    function ($scope, formsTaxReturn, security, TaxYear, Relief) {

      $scope.$parent.step = 2;

      // Get reliefs
      $scope.deductionReliefs = Relief.list;
      for (var i in $scope.deductionReliefs) {
        if ($scope.deductionReliefs[i].tax_year_id === TaxYear.current.id) {
          $scope.deductionRelief = $scope.deductionReliefs[i];
          break;
        }
      }
      if (!$scope.deductionRelief) {
        $scope.deductionRelief = new Relief();
      }

      // on
      $scope.$on('individual:step:change', function (event, step) {
        $scope.deductionRelief.save();
      });
    }
  );
