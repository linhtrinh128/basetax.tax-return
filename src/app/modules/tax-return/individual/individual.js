angular.module('modules.tax-return.individual', [
  'modules.tax-return.individual.progress-bar',
  'modules.tax-return.individual.step-income',
  'modules.tax-return.individual.step-deduction-relief',
  'modules.tax-return.individual.step-personal-information',
  'modules.tax-return.individual.step-tax-calculation',
  'modules.tax-return.individual.step-review-tax-return',
  'modules.tax-return.individual.step-payment-n-file'
])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.individual', {
        url: '/:taxYear/individual',
        abstract: true,
        views: {
          'main-content': {
            templateUrl: 'modules/tax-return/individual/individual.tpl.html',
            controller: 'IndividualTaxReturnController'
          }
        }
      });
  })

  .controller('IndividualTaxReturnController',
    function ($scope, $state, $rootScope, $stateParams, $filter, TAX_RETURN, utils) {

      // Array of Steps
      $scope.steps = TAX_RETURN.INDIVIDUAL.STEPS;
      $scope.offset = 100 / ($scope.steps.length - 1);
      $scope.step = 1;
      $scope.progress = 0;

      //$scope.isSaved = true;
      $scope.confirmed_date = utils.storage.get('confirm_date');
      $scope.confirm_of_declaration = !!$scope.confirmed_date;

      // Is payment
      $scope.isPayment = false;


      /**
       * Event Confirm of declaration
       */
      $scope.$on('confirm_of_declaration', function (event, value) {
        $scope.confirm_of_declaration = value;
        if (value) {
          $scope.confirmed_date = $filter('date')(new Date(), 'dd MMM yyyy hh:mm');
          utils.storage.set('confirm_date', $scope.confirmed_date);
          $scope.errorConfirm = null;
        } else {
          $scope.confirmed_date = null;
        }
      });

      // Watch step
      $scope.$watch('step', function (newValue) {
        if (newValue > 0) {
          $scope.welcome = false;
          $scope.progress = $scope.actions.calculateProgress($scope.step);
          $scope.saveStatus($scope.step);
        } else {
          $scope.welcome = true;
        }
      }, true);

      // Partnership Detail
      $scope.init = function () {
        // Find tax return status
        for (var i in $scope.taxReturns) {
          if ($scope.taxReturns[i].tax_year_id === $scope.taxYear.id && $scope.taxReturns[i].type_id === 1) {
            $scope.$parent.iWantToFile = 1;
            $scope.$parent.taxReturn = $scope.taxReturns[i];
            break;
          }
        }
      };
      $scope.init();


      /**
       * Actions
       * Actions used for tax return
       */
      $scope.actions = {

        // is selected
        isSelected: function (progress, index) {
          if (parseInt(progress / $scope.offset) + 1 >= index) {
            return true;
          }
          return false;
        },

        // next
        next: function () {
          var arr = $state.$current.self.name.split(".");
          $scope.step = parseInt(arr[arr.length - 1]);

          // Last step
          if ($scope.step === $scope.steps.length) {
            $state.go('^.^.thanks');
            return;
          }


          // Step 3 personal information
          /*if ($scope.step === 3 && !$scope.isSaved) { //step personal information
           return;
           }*/

          // Step 5 review tax return
          if ($scope.step === 5 && !$scope.confirm_of_declaration) {
            //step personal information
            $scope.errorConfirm = "Please confirm this declaration";
            $scope.confirmed_date = null;
            return;
          }

          // step change
          $scope.$broadcast('individual:step:change', $scope.step + 1);

          $state.go('^.' + (++$scope.step));
        },

        // previous
        previous: function () {
          var arr = $state.$current.self.name.split(".");
          $scope.step = parseInt(arr[arr.length - 1]);

          if ($scope.step === 1) {
            $state.go('^.^.welcome');
            $scope.welcome = true;
          }

          // step change
          $scope.$broadcast('individual:step:change', $scope.step - 1);

          $state.go('^.' + (--$scope.step));
        },

        // jump
        jump: function (step) {
          if (step > 0) {

            // Step 5 review tax return
            if ($scope.step === 5 && !$scope.confirm_of_declaration) {
              //step personal information
              $scope.errorConfirm = "Please confirm this declaration";
              $scope.confirmed_date = null;
              return;
            } else {
              utils.storage.set('confirm_date', $filter('date')(new Date(), 'dd MMM yyyy hh:mm'));
              $scope.errorConfirm = null;
            }

            $scope.step = step;

            // step change
            $scope.$broadcast('individual:step:change', step);

            // Go state
            //if ($scope.step !== 2) {
            $state.go('^.' + step);
            //}
          } else {
            $scope.welcome = true;
          }
        },

        // calculate progress
        calculateProgress: function (step) {
          return $scope.offset * (step - 1);
        }
      };

      /**
       * Individual step change end
       */
      $scope.$on('individual:step:change:end', function (event, step) {
        console.log('changed to step ' + step + ' end .');
        $state.go('^.' + step);
      });
    });
