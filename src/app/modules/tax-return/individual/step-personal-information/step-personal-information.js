angular.module('modules.tax-return.individual.step-personal-information', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.individual.3', {
        url: '/personal-information',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/individual/step-personal-information/step-personal-information.tpl.html',
            controller: 'StepPersonalInformationController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/individual/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        }
      });
  })

  .controller('StepPersonalInformationController',
    function ($rootScope, $scope, $uibModal, utils, security, profile, CONFIGS, formsTaxReturn,
              otherDetails, Country) {

      /**
       * Information personal user
       */
      $scope.$parent.step = 3;
      $scope.info = angular.copy(security.currentUser);
      $scope.temp = angular.copy($scope.info);
      $scope.countries = Country.list;

      $scope.nonResidentFlag = false;
      $scope.showValid = false;
      $scope.errorMess = false;
      $scope.params = {
        opened: false
      };

      /**
       *
       * @type {*[]}
       */
      $scope.infomationForms = [
        {
          title: 'If you have paid too much tax',
          selected: true,
          open: function () {
            $scope.actions.open('paid-much');
          }
        }, {
          title: 'If you have not paid enough tax',
          selected: true,
          open: function () {
            $scope.actions.open('paid-enough');
          }
        }
      ];

      /**
       * Watch information personal
       */
      /*$scope.$watch('info', function () {

        // Check information
        if (!angular.equals($scope.info, $scope.temp)) {
          $rootScope.$broadcast('information:saved', false);
          $scope.errorMess = true;
        } else {
          $rootScope.$broadcast('information:saved', true);
          $scope.errorMess = false;
        }
      }, true);*/

      /**
       * Init
       */
      $scope.init = function () {
        if ($scope.info && $scope.info.country_id) {

          // country.id = 77: United Kingdom default country
          $scope.info.country = utils.populateById(!!$scope.info.country_id ? $scope.info.country_id : CONFIGS.COUNTRY_ID_UNITED_KINGDOM, $scope.countries);


          if ($scope.info.country) {
            $scope.info.country_id = $scope.info.country.id;
          }
        }

        // Today
        $scope.dt = new Date();
      };


      /**
       * Actions
       */
      $scope.actions = {

        /**
         * Select country
         *
         * @param country
         */
        changeCountry: function () {
          $scope.info.country_id = $scope.info.country.id;
        },


        /**
         * Open form addon
         *
         * @param form
         */
        open: function (form) {
          formsTaxReturn.open(form);
        },


        /**
         * Open Date of birthday
         */
        openDoB: function () {
          $scope.params.opened = true;
        }
      };

      /**
       * Save profile
       *
       * @param isValid
       */
      $scope.save = function (isValid) {
        if (isValid) {
          $scope.showValid = true;
          $scope.info.date_of_birth = utils.formatDateTime($scope.info.date_of_birth);
          $scope.errNI = checkNI($scope.info.national_insurance, 9);

          if ($scope.errNI) {
            return;
          }

          if ($scope.info.country) {
            $scope.info.country_id = $scope.info.country.id;
          }

          if (isValid) {
            $scope.info.modified = new Date();

            delete $scope.info.status;

            profile.update($scope.info).then(function (res) {
              security.currentUser = res;
              // $rootScope.$broadcast('information:saved', true);

              $scope.temp = angular.copy(security.currentUser);
              $scope.info = angular.copy(security.currentUser);

              $scope.errorMess = false;
            });

            $scope.showValid = false;
          }
        }
      };

      // Listen event change step of individual
      $scope.$on('individual:step:change', function () {
        $scope.save(true);
      });

      /**
       * Check NI
       *
       * @param value
       * @param length
       * @returns {boolean}
       */
      function checkNI(value, length) {
        if (utils.isEmpty(value) || value.length === 0) {
          return false;
        } else if (value.length !== parseInt(length)) {
          return true;
        }
        return false;
      }
    }
  );
