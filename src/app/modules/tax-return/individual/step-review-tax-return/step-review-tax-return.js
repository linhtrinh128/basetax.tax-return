angular.module('modules.tax-return.individual.step-review-tax-return', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.individual.5', {
        url: '/review-tax-return',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/individual/step-review-tax-return/step-review-tax-return.tpl.html',
            controller: 'StepReviewTaxReturnController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/individual/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        }
      });
  })

  .controller('StepReviewTaxReturnController',
    function ($rootScope, $scope, $uibModal, $filter, $sce, $http,
              security, utils, forms, formsTaxReturn, Export) {

      $scope.$parent.step = 5;
      $scope.info = angular.copy(security.currentUser);

      $scope.$on('information:saved', function () {
        $scope.info = angular.copy(security.currentUser);
      });

      $scope.$watch('confirm_of_declaration', function () {
        if ($scope.initializing) {
          $rootScope.$broadcast('confirm_of_declaration', $scope.confirm_of_declaration);
        } else {
          $scope.initializing = true;
        }
      }, true);

      /**
       * ACTIONS
       */
      $scope.actions = {

        init: function () {
          $scope.actions.checkFormsExisting();
          $scope.calculate();
        },

        // check forms existing
        checkFormsExisting: function () {
          angular.forEach($scope.individualForms, function (form) {
            form.checkExisting();
          });
        },

        //Open forms
        open: function (form) {
          formsTaxReturn.open(form);
        },

        // Download pdf
        download: function () {
          $scope.isSubmit = true;

          var params = {
            yearId: $scope.taxYear.id
          };

          // Export
          Export.exportTaxReview(params).then(function (data) {
            window.open(data, '_blank');
          }, function error(error) {
            //
          }).finally(function () {
            $scope.isSubmit = false;
          });
        }
      };

      $scope.actions.init();
    }
  );
 