angular.module('modules.tax-return.individual.step-tax-calculation', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.individual.4', {
        url: '/tax-calculation',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/individual/step-tax-calculation/step-tax-calculation.tpl.html',
            controller: 'StepTaxCalculationController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/individual/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        }
      });
  })

  .controller('StepTaxCalculationController',
    function ($scope, calculatorTaxReturn) {

      $scope.$parent.step = 4;

      $scope.calculate();

      $scope.download = function () {
        $scope.isSubmit = true;
        calculatorTaxReturn.export()
          .then(function (data) {
            // Open link
            window.open(data, '_blank');

          }, function error(error) {
            // Error callback
          }).finally(function () {
          $scope.isSubmit = false;
        });
      };
    }
  )
;
