angular.module('modules.tax-return.individual.step-payment-n-file', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.individual.6', {
        url: '/payment-and-file',
        views: {
          'step-content': {
            templateUrl: 'modules/tax-return/individual/step-payment-n-file/step-payment-n-file.tpl.html',
            controller: 'StepPaymentNFileController'
          },
          'progress-bar': {
            templateUrl: 'modules/tax-return/individual/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          }
        }
      });
  })

  .controller('StepPaymentNFileController',
    function ($rootScope, $scope, $uibModal, utils, security, messages, payments, TaxReturn, subscriptions) {

      $scope.$parent.step = 6;

      $scope.pollResponse = false;

      /**
       * Plans
       */
      $scope.plans = [
        {
          key: 'planA',
          name: 'Plan A',
          price: 19.90,
          per: '/return',
          list_descriptions: [
            '- Online filling'
          ],
          event: function () {
            $scope.pay(this.key, this.price);
          }
        }, {
          key: 'planB',
          name: 'Plan B',
          price: 79.90,
          per: '/return',
          list_descriptions: [
            '- Online filling',
            '- Tax support',
            '- Tax saving tips',
            '- Free tax review'
          ],
          event: function () {
            $scope.pay(this.key, this.price);
          }
        }, {
          key: 'planC',
          name: 'Plan C',
          price: 99.90,
          per: '/return',
          list_descriptions: [
            '- Online filling',
            '- Tax support',
            '- Tax saving tips',
            '- Free tax review',
            '- Personal tax agent',
            '- Tax audit support',
            '- Live chat'
          ],
          event: function () {
            $scope.pay(this.key, this.price);
          }
        }
      ];

      /**
       * Init
       */
      $scope.init = function () {
        // user have plan Club, business, pro must not pay for tax return.
        var plan = subscriptions.list[0].plan_id;
        if (plan === 11 || plan === 12 || plan === 13) {
          $scope.isPayment = true;
        } else {
          payments.checkPaymentTaxReturn($scope.taxYear.id, $scope.$parent.option.taxReturnType.value)
            .then(function (res) {
              $scope.isPayment = res.success;
            });
        }
      };

      /**
       * filling
       */
      $scope.filing = function () {
        TaxReturn.update({
          id: $scope.taxReturn.id,
          submitted: new Date(),
          received_by_hmrc: new Date(),
          status: '2'
        }).then(function (res) {
          $scope.fileTaxReturn = res;
          // show message
          var params = {
            title: 'Your tax return has been submitted.',
            content: 'Thank you for your submission. You should be receiving a confirmation by email from Basetax with the ' +
            'detail of response from HMRC. Please also check your junk mail folder. We will contact you if there is ' +
            'any issue with your tax return we will contact you immediately.'
          };
          messages.show(params);
        });
      };


      /**
       * @param payment_type: key = {planA, planB, planC}
       * @param amount:
       */
      $scope.pay = function (payment_type, amount) {
        var param = {
          taxYear: $scope.taxYear,
          plan: payment_type,
          amount: amount,
          typeId: $scope.option.taxReturnType.value
        };
        // Show payment Stripe
        payments.showPaymentTaxReturn(param).result
          .then(function success(res) {
            if (res.success) {
              $scope.isPayment = true;
            }
          });
      };

      /**
       * Get status
       * @param n
       * @returns {*}
       */
      $scope.getStatus = function (n) {
        switch (n) {
          case '1':
          case 1:
            return 'In progress';
          case '2':
          case 2:
            return 'Filed';
          case '3':
          case 3:
            return 'Rejected';
          default:
            return 'In progress.';
        }
      };
    });
