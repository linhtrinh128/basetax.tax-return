angular.module('modules.tax-return.individual.step-income', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loggedIn.modules.tax-return.individual.1', {
        url: '/income',
        views: {
          'progress-bar': {
            templateUrl: 'modules/tax-return/individual/progress-bar/progress-bar.tpl.html',
            controller: 'ProgressBarCtrl'
          },
          'step-content': {
            templateUrl: 'modules/tax-return/individual/step-income/step-income.tpl.html',
            controller: 'StepIncomeControllerController',
          }
        },
        resolve: {
          $selfEmployeds: function (SelfEmployed) {
            return SelfEmployed.all();
          }
        }
      });
  })

  .controller('StepIncomeControllerController',
    function ($scope, $uibModal, utils, utilsTaxReturn, formsTaxReturn,
              SelfEmployed, TaxYear, TAX_RETURN, PartnershipSA104,
              Underwriter, TransactionEmp14,
              TransactionPro14) {

      $scope.$parent.step = 1;

      // Init self employed
      $scope.init = function () {

        SelfEmployed.findByTaxYear(TaxYear.current.id)
          .then(function (res) {
            $scope.selfEmployeds = res;
          });
      };


      $scope.transactionPros14 = TransactionPro14.findByTaxYear(TaxYear.current.id);


      // TODO import property

      $scope.open = function (key) {
        formsTaxReturn.open(key);
      };


      // Open self-employed
      $scope.openSelfEmployed = function (item) {
        if (typeof item === 'undefined') {
          item = new SelfEmployed(TaxYear.current.id);
        }

        item.show();
      };

      /**
       * Delete an instance self employed
       */
      $scope.deleteSelfEmployed = function (selfEmployed) {
        var confirmDelete = $uibModal.open({
          templateUrl: 'modules/tax-return/forms/self-employed/confirm-delete/confirm-delete.tpl.html',
          controller: 'SelfEmployedConfirmDeleteController'
        });
        confirmDelete.result.then(function () {
          selfEmployed.delete().then(function (response) {
            if (response) {
              $scope.selfEmployeds.forEach(function (value, key) {
                if (selfEmployed.id === value.id) {
                  $scope.selfEmployeds.splice(key, 1);
                }
              });
            }
          });
        });
      };
    }
  );
