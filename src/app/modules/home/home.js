var taxReturnModule = angular.module('modules.home', [])

    .config(function ($stateProvider) {
        $stateProvider
            .state('loggedIn.modules.home', {
                url: '/home',
                views: {
                    'main-content': {
                        templateUrl: 'modules/home/home.tpl.html',
                        controller: 'HomeController'
                    }
                }
            });
    });

var HomeController = function ($scope, $rootScope, $state, TaxYear, TAX_RETURN) {

};

HomeController.$inject = ['$scope', '$rootScope', '$state', 'TaxYear', 'TAX_RETURN'];

taxReturnModule.controller('HomeController', HomeController);



