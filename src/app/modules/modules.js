angular.module('app.modules', [
    // modules
    'app.sidebar',

    // Tax return
    'modules.home',
    'modules.tax-return'
])

    .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
        cfpLoadingBarProvider.includeSpinner = false;
    }])

    .config(function ($stateProvider) {
        $stateProvider
            .state('loggedIn.modules', {
                url: '',
                abstract: true,
                views: {
                    'middle-container': {
                        templateUrl: 'layouts/layout-middle-content.tpl.html'
                    },
                    sidebar: {
                        templateUrl: 'sidebar-tr/sidebar-tr.tpl.html',
                        controller: 'SidebarTRController'
                    }
                },
                resolve: {
                    get: function (TaxYear, Country, VehicleType, Currency, PaymentType, UserType) {

                        // taxYears.all();
                        TaxYear.all();
                        Country.all();
                        Currency.all();
                        PaymentType.all();
                        UserType.all();

                        return TaxYear.setDefaultYear();
                    },
                    getAllBookkeeping: function (all) {
                        return all.getAllBookkeeping();
                    }
                }
            });
    });
