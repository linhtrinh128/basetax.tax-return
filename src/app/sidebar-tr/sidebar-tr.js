angular.module('app.sidebar', [])
  .controller('SidebarTRController',
    function ($rootScope, $scope, $state, TAX_RETURN) {

      $scope.groups = [
        {
          title: 'TAX RETURN',
          items: [
            {
              title: 'Welcome',
              sref: 'loggedIn.modules.tax-return.welcome',
              classes: 'fa-dashboard'
            }, {
              title: 'Manage',
              classes: 'fa-briefcase',
              flag: 'true',
              children: [
                {
                  title: 'Business',
                  sref: 'loggedIn.tax-return.business'
                }, {
                  title: 'Employer',
                  sref: 'loggedIn.tax-return.employment'
                }
              ]
            }
          ]
        }
      ];


      $rootScope.$on('$stateChangeStart',
        function (event, toState) {
          $scope.groups = [
            {
              title: 'TAX RETURN',
              items: [
                {
                  title: 'Welcome',
                  sref: 'loggedIn.modules.tax-return.welcome',
                  classes: 'fa-dashboard'
                }, {
                  title: 'Manage',
                  classes: 'fa-briefcase',
                  flag: 'true',
                  children: [
                    {
                      title: 'Business',
                      sref: 'loggedIn.tax-return.business'
                    }, {
                      title: 'Employer',
                      sref: 'loggedIn.tax-return.employment'
                    }
                  ]
                }
              ]
            }
          ];

          var steps = [];

          if (toState.name.indexOf('tax-return.individual') != -1) {

            // Clear
            var group = {
              title: 'INDIVIDUAL',
              items: []
            };

            // Get steps
            steps = TAX_RETURN.INDIVIDUAL.STEPS;

            // Push steps
            for (var i in steps) {
              group.items.push({
                title: steps[i].name,
                sref: 'loggedIn.modules.tax-return.individual.' + (parseInt(i) + 1),
                classes: 'fa-circle-o'
              });
            }

            $scope.groups.push(group);
          } else if (toState.name.indexOf('tax-return.partnership') != -1) {
            // Clear
            var group2 = {
              title: 'PARTNERSHIP',
              items: []
            };

            steps = TAX_RETURN.PARTNERSHIP.STEPS;

            // Push steps
            for (var j in steps) {
              group2.items.push({
                title: steps[j].name,
                sref: 'loggedIn.modules.tax-return.partnership.' + (parseInt(j) + 1),
                classes: 'fa-circle-o'
              });
            }

            $scope.groups.push(group2);
          } else {

          }
        });
    }
  );


