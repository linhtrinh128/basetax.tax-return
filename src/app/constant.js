angular.module('app.constant', [])

    .constant('CONFIGS', {


        // Base URL
        baseURL: function () {
            var hostname = window.location.hostname;

            switch (hostname) {
                case 'localhost':
                    return 'http://localhost/basetax.api/public/v2/';

                case 'basetax.vn':
                case 'ui.basetax.vn':
                case 'ui2.basetax.vn':
                    return 'http://basetax.vn/v2/';

                default:
                case 'basetax.co.uk':
                case 'www.basetax.co.uk':
                    return 'https://basetax.co.uk/v2/';
            }

            //return 'https://basetax.co.uk/v2/';
        },

        /**
         *
         * @returns {string}
         */
        loginUrl: function () {
            return ($location.host() === 'web.beesightsoft.com' || $location.host() === 'localhost') ?
                "http://web.beesightsoft.com/taxmapp/trunk/login/" :
                "https://www.basetax.co.uk/login/";
        },

        /**
         * Max Size File Scan
         */
        MAX_SIZE_FILE_SCAN: 4 * 1048576,

        /**
         *  File type
         */
        FILE_TYPE_IMAGE: ['jpg', 'jpeg', 'gif', 'png'],
        FILE_TYPE_DOC: ['doc', 'docx', 'xls', 'xlsx', 'txt', 'pdf'],
        FILE_TYPE: ['jpg', 'png', 'jpeg', 'doc', 'docx', 'xls', 'xlsx', 'txt', 'pdf', 'gif'],
        FILE_SIZE: 4,

        COUNTRY_ID_UNITED_KINGDOM: 77,

        MILEAGE_RATE: function (year, vehicle) {
            switch (year.id) {
                case 4: //2015-16
                case 5: //2016-17
                    switch (parseInt(vehicle.id)) {
                        case 1: // CAR
                            return {
                                LESS_THAN_1000: 0.45,
                                GREATER_THAN_1000: 0.25
                            };
                        case 2: // VAN
                            return {
                                LESS_THAN_1000: 0.45,
                                GREATER_THAN_1000: 0.25
                            };
                        case 3: // MOTOCYCLE
                            return {
                                LESS_THAN_1000: 0.24,
                                GREATER_THAN_1000: 0.2
                            };
                        case 4: // CYCLE
                            return {
                                LESS_THAN_1000: 0.24,
                                GREATER_THAN_1000: 0.2
                            };
                        default :
                            return {
                                LESS_THAN_1000: 0.45,
                                GREATER_THAN_1000: 0.25
                            };
                    }
                    break;
                default:
                    return {
                        LESS_THAN_1000: 0,
                        GREATER_THAN_1000: 0
                    };
            }
        },

        PLANS: [
            {
                name: 'Club',
                price: 7.50,
                description: 'Tax Return + Software',
                class: 'btn-primary'
            },
            {
                name: 'Professional',
                price: 15.00,
                description: 'Accountant + Software for <i>Sole Trader</i>',
                class: 'btn-info'
            },
            {
                name: 'Business',
                price: 50.00,
                description: "Accountant + Software for <i>Limited Company</i>",
                class: 'btn-danger'
            }
        ],

        DATE_OPTIONS: {
            formatYear: 'yy',
            startingDay: 1,
            class: 'datepicker'
        },

        DATE_FORMAT: 'dd MMM yyyy',
        MONTH_FORMAT: "MMMM yyyy",

        ngTable: {
            page: 1, // show first page
            count: 10, // count per page
            counts: [10, 25, 50, 100]
        },

        TAX_YEAR: {
            DATE_START: '04/06',
            DATE_END: '04/05'
        }
    })


    .constant('ROLE', {
        USER: 'user',
        ADMIN: 'administrator'
    })

    .constant('TAX_RETURN', {
        TYPE_INDIVIDUAL_VALUE: 1,
        TYPE_PARTNERSHIP_VALUE: 2,
        INDIVIDUAL: {
            STEPS: [
                {key: 'income', name: 'Income'},
                {key: 'reduction', name: 'Deduction/Relief'},
                {key: 'information', name: 'Personal Information'},
                {key: 'calculation', name: 'Tax Calculation'},
                {key: 'review', name: 'Review Tax Return'},
                {key: 'payment', name: 'Payment and file'}
            ]
        },
        PARTNERSHIP: {
            STEPS: [
                {key: 'income', name: 'Income'},
                {key: 'partner-detail', name: 'Partner detail'},
                {key: 'partnership-detail', name: 'Partnership detail'},
                {key: 'summary', name: 'Tax summary'},
                {key: 'payment-n-file', name: 'Payment and file'}
            ]
        },
        INCOME_FORMS: [
            {
                title: 'Employment (SA102)',
                selected: false,
                key: 'employment',
                show: 0,
                subTitle: 'If you have P60, P45, Payslip from your employer',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/employment.tpl.html'
            },
            {
                title: 'Minister of Religion (SA102M)',
                selected: false,
                key: 'minister-of-religion',
                show: true,
                subTitle: 'If you are a minister of religion',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/minister-employment.tpl.html'
            },
            {
                title: 'Self Employed (SA103)',
                selected: false,
                key: 'self-employed',
                show: false,
                subTitle: 'If you are a sole trader',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/self-employed.tpl.html'
            },
            {
                title: 'Lloyds Underwriters (SA103L)',
                selected: false,
                key: 'lloyds-underwriter',
                show: true,
                subTitle: '',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/lloys-underwriter.tpl.html'
            },
            {
                title: 'Partnership (SA104)',
                selected: false,
                key: 'partnership',
                show: false,
                subTitle: 'If you are a partner in a partnership',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/partnership.tpl.html'
            },
            {
                title: 'UK Property (SA105)',
                selected: false,
                key: 'uk-property',
                show: false,
                subTitle: 'If you received rental income from tenants',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/uk-property.tpl.html'
            },
            {
                title: 'Foreign Income (SA106)',
                selected: false,
                key: 'foreign-income',
                show: true,
                subTitle: 'If you have any foreign income',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/foreign-income.tpl.html'
            },
            {
                title: 'Trust (SA107)',
                selected: false,
                key: 'trust',
                show: true,
                subTitle: 'If you received any money from a trust',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/trust.tpl.html'
            },
            {
                title: 'Capital Gains (SA108)',
                selected: false,
                key: 'capital-gains',
                show: true,
                subTitle: 'If you have sold your property, assets',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/capital-gain.tpl.html'
            },
            {
                title: 'Resident Remittance (SA109)',
                selected: false,
                key: 'resident-remittance',
                subTitle: ' ',
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/resident-remittance.tpl.html'
            }, {
                title: 'Interest and dividends from UK banks, building societies etc',
                selected: false,
                key: 'interest-and-dividends',
                subTitle: ' ',
                show: true,
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/other-uk-income/interest-dividends.tpl.html'
            }, {
                title: 'UK Pensions, annuities and other state benefits received',
                selected: false,
                key: 'uk-pensions',
                subTitle: ' ',
                show: true,
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/other-uk-income/pensions-annuities.tpl.html'
            }, {
                title: 'Other UK income not included in other sections',
                selected: false,
                key: 'other-uk-income',
                subTitle: ' ',
                show: true,
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/other-uk-income/note.tpl.html'
            }, {
                title: 'Interest from gilt-edged and other UK securities',
                selected: false,
                key: 'interest-gilt-edged-other-securities',
                subTitle: ' ',
                show: true,
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/other-uk-income/interest-gilt-edged-other-securities.tpl.html'
            }, {
                title: 'Life insurance gains',
                selected: false,
                key: 'life-insurance-gains',
                subTitle: ' ',
                show: true,
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/other-uk-income/life-insurance-gains.tpl.html'
            }, {
                title: 'Stock dividends',
                selected: false,
                key: 'stock-dividends',
                subTitle: '',
                show: true,
                existing: false,
                path_review: 'modules/tax-return/individual/step-review-tax-return/review-forms/other-uk-income/stock-dividends.tpl.html'
            }
        ],
        DEDUCTION_FORMS: [
            {
                title: 'Charitable givings',
                key: 'charitable-givings',
                selected: false,
                existing: false
            },
            {
                title: 'Pension scheme contributions',
                key: 'pension-scheme-contributions',
                selected: false,
                existing: false
            },
            {
                title: 'Other tax relief',
                key: 'other-tax-relief',
                selected: false,
                existing: false
            }
        ],
        PARTNERSHIP_FORMS: [
            {
                title: 'Trading and professional income',
                key: 'trading-professional',
                selected: false,
                existing: false
            }, {
                title: 'Partnership Uk property (SA801)',
                key: 'partnership-uk-property',
                selected: false,
                existing: false
            }, {
                title: 'Disposal & Changeable Assets (SA803)',
                key: 'disposal-changeable',
                selected: false,
                existing: false
            }, {
                title: 'Foreign Income (SA802)',
                key: 'foreign-income',
                selected: false,
                existing: false
            }, {
                title: 'Other information',
                key: 'other-information',
                selected: false,
                existing: false
            }
        ],
        KEY: {
            MINISTER: 'minister-of-religion',
            SELF_EMPLOYED: 'self-employed',
            EMPLOYMENT: 'employment',
            LLOYDS_UNDERWRITER: 'lloyds-underwriter',
            PARTNERSHIP: 'partnership',
            FOREIGN_INCOME: 'foreign-income',
            TRUST: 'trust',
            CAPITAL: 'capital-gains',
            RESIDENT: 'resident-remittance',
            UK_PROPERTY: 'uk-property',
            INTEREST_DIVIDENDS: 'interest-and-dividends',
            UK_PENSIONS: 'uk-pensions',
            UK_INCOME: 'other-uk-income',
            INTEREST_GILT: 'interest-gilt-edged-other-securities',
            LIFE_INSURANCE: 'life-insurance-gains',
            STOCK: 'stock-dividends',
            CHARITABLE: 'charitable-givings',
            PENSION: 'pension-scheme-contributions',
            RELIEF: 'other-tax-relief',

            PAID_MUCH: 'paid-much',
            PAID_ENOUGH: 'paid-enough',

            PNS_DISPOSAL_CHANGEABLE: 'disposal-changeable',
            PNS_FOREIGN_INCOME: 'foreign-income',
            PNS_TRADING_PROFESSIONAL: 'trading-professional',
            PNS_OTHER_INFORMATION: 'other-information',
            PNS_UK_PROPERTY: 'partnership-uk-property'
        }
    })

    .constant('PAYMENT', {
        STRIPE_PUBLIC_KEY_TEST: 'pk_test_tXTT1uRa3wepnwpv5In9A9m7',
        STRIPE_PUBLIC_KEY_LIVE: 'pk_live_wIvYtsaETnJH6eqHp2vKiZko'
    })

    .constant('RESOURCES', {
        TAX_YEARS: [
            {
                "id": 1,
                "description": "2012-13",
                "start_year": 2012
            },
            {
                "id": 2,
                "description": "2013-14",
                "start_year": 2013
            },
            {
                "id": 3,
                "description": "2014-15",
                "start_year": 2014
            },
            {
                "id": 4,
                "description": "2015-16",
                "start_year": 2015
            },
            {
                "id": 5,
                "description": "2016-17",
                "start_year": 2016
            }
        ],
        COUNTRIES: [
            {
                "id": 1,
                "code": "AD",
                "name": "Andorra"
            },
            {
                "id": 2,
                "code": "AE",
                "name": "United Arab Emirates"
            },
            {
                "id": 3,
                "code": "AF",
                "name": "Afghanistan"
            },
            {
                "id": 4,
                "code": "AG",
                "name": "Antigua and Barbuda"
            },
            {
                "id": 5,
                "code": "AI",
                "name": "Anguilla"
            },
            {
                "id": 6,
                "code": "AL",
                "name": "Albania"
            },
            {
                "id": 7,
                "code": "AM",
                "name": "Armenia"
            },
            {
                "id": 8,
                "code": "AO",
                "name": "Angola"
            },
            {
                "id": 9,
                "code": "AQ",
                "name": "Antarctica"
            },
            {
                "id": 10,
                "code": "AR",
                "name": "Argentina"
            },
            {
                "id": 11,
                "code": "AS",
                "name": "American Samoa"
            },
            {
                "id": 12,
                "code": "AT",
                "name": "Austria"
            },
            {
                "id": 13,
                "code": "AU",
                "name": "Australia"
            },
            {
                "id": 14,
                "code": "AW",
                "name": "Aruba"
            },
            {
                "id": 15,
                "code": "AX",
                "name": "Åland"
            },
            {
                "id": 16,
                "code": "AZ",
                "name": "Azerbaijan"
            },
            {
                "id": 17,
                "code": "BA",
                "name": "Bosnia and Herzegovina"
            },
            {
                "id": 18,
                "code": "BB",
                "name": "Barbados"
            },
            {
                "id": 19,
                "code": "BD",
                "name": "Bangladesh"
            },
            {
                "id": 20,
                "code": "BE",
                "name": "Belgium"
            },
            {
                "id": 21,
                "code": "BF",
                "name": "Burkina Faso"
            },
            {
                "id": 22,
                "code": "BG",
                "name": "Bulgaria"
            },
            {
                "id": 23,
                "code": "BH",
                "name": "Bahrain"
            },
            {
                "id": 24,
                "code": "BI",
                "name": "Burundi"
            },
            {
                "id": 25,
                "code": "BJ",
                "name": "Benin"
            },
            {
                "id": 26,
                "code": "BL",
                "name": "Saint Barthélemy"
            },
            {
                "id": 27,
                "code": "BM",
                "name": "Bermuda"
            },
            {
                "id": 28,
                "code": "BN",
                "name": "Brunei"
            },
            {
                "id": 29,
                "code": "BO",
                "name": "Bolivia"
            },
            {
                "id": 30,
                "code": "BQ",
                "name": "Bonaire"
            },
            {
                "id": 31,
                "code": "BR",
                "name": "Brazil"
            },
            {
                "id": 32,
                "code": "BS",
                "name": "Bahamas"
            },
            {
                "id": 33,
                "code": "BT",
                "name": "Bhutan"
            },
            {
                "id": 34,
                "code": "BV",
                "name": "Bouvet Island"
            },
            {
                "id": 35,
                "code": "BW",
                "name": "Botswana"
            },
            {
                "id": 36,
                "code": "BY",
                "name": "Belarus"
            },
            {
                "id": 37,
                "code": "BZ",
                "name": "Belize"
            },
            {
                "id": 38,
                "code": "CA",
                "name": "Canada"
            },
            {
                "id": 39,
                "code": "CC",
                "name": "Cocos [Keeling] Islands"
            },
            {
                "id": 40,
                "code": "CD",
                "name": "Democratic Republic of the Congo"
            },
            {
                "id": 41,
                "code": "CF",
                "name": "Central African Republic"
            },
            {
                "id": 42,
                "code": "CG",
                "name": "Republic of the Congo"
            },
            {
                "id": 43,
                "code": "CH",
                "name": "Switzerland"
            },
            {
                "id": 44,
                "code": "CI",
                "name": "Ivory Coast"
            },
            {
                "id": 45,
                "code": "CK",
                "name": "Cook Islands"
            },
            {
                "id": 46,
                "code": "CL",
                "name": "Chile"
            },
            {
                "id": 47,
                "code": "CM",
                "name": "Cameroon"
            },
            {
                "id": 48,
                "code": "CN",
                "name": "China"
            },
            {
                "id": 49,
                "code": "CO",
                "name": "Colombia"
            },
            {
                "id": 50,
                "code": "CR",
                "name": "Costa Rica"
            },
            {
                "id": 51,
                "code": "CU",
                "name": "Cuba"
            },
            {
                "id": 52,
                "code": "CV",
                "name": "Cape Verde"
            },
            {
                "id": 53,
                "code": "CW",
                "name": "Curacao"
            },
            {
                "id": 54,
                "code": "CX",
                "name": "Christmas Island"
            },
            {
                "id": 55,
                "code": "CY",
                "name": "Cyprus"
            },
            {
                "id": 56,
                "code": "CZ",
                "name": "Czech Republic"
            },
            {
                "id": 57,
                "code": "DE",
                "name": "Germany"
            },
            {
                "id": 58,
                "code": "DJ",
                "name": "Djibouti"
            },
            {
                "id": 59,
                "code": "DK",
                "name": "Denmark"
            },
            {
                "id": 60,
                "code": "DM",
                "name": "Dominica"
            },
            {
                "id": 61,
                "code": "DO",
                "name": "Dominican Republic"
            },
            {
                "id": 62,
                "code": "DZ",
                "name": "Algeria"
            },
            {
                "id": 63,
                "code": "EC",
                "name": "Ecuador"
            },
            {
                "id": 64,
                "code": "EE",
                "name": "Estonia"
            },
            {
                "id": 65,
                "code": "EG",
                "name": "Egypt"
            },
            {
                "id": 66,
                "code": "EH",
                "name": "Western Sahara"
            },
            {
                "id": 67,
                "code": "ER",
                "name": "Eritrea"
            },
            {
                "id": 68,
                "code": "ES",
                "name": "Spain"
            },
            {
                "id": 69,
                "code": "ET",
                "name": "Ethiopia"
            },
            {
                "id": 70,
                "code": "FI",
                "name": "Finland"
            },
            {
                "id": 71,
                "code": "FJ",
                "name": "Fiji"
            },
            {
                "id": 72,
                "code": "FK",
                "name": "Falkland Islands"
            },
            {
                "id": 73,
                "code": "FM",
                "name": "Micronesia"
            },
            {
                "id": 74,
                "code": "FO",
                "name": "Faroe Islands"
            },
            {
                "id": 75,
                "code": "FR",
                "name": "France"
            },
            {
                "id": 76,
                "code": "GA",
                "name": "Gabon"
            },
            {
                "id": 77,
                "code": "GB",
                "name": "United Kingdom"
            },
            {
                "id": 78,
                "code": "GD",
                "name": "Grenada"
            },
            {
                "id": 79,
                "code": "GE",
                "name": "Georgia"
            },
            {
                "id": 80,
                "code": "GF",
                "name": "French Guiana"
            },
            {
                "id": 81,
                "code": "GG",
                "name": "Guernsey"
            },
            {
                "id": 82,
                "code": "GH",
                "name": "Ghana"
            },
            {
                "id": 83,
                "code": "GI",
                "name": "Gibraltar"
            },
            {
                "id": 84,
                "code": "GL",
                "name": "Greenland"
            },
            {
                "id": 85,
                "code": "GM",
                "name": "Gambia"
            },
            {
                "id": 86,
                "code": "GN",
                "name": "Guinea"
            },
            {
                "id": 87,
                "code": "GP",
                "name": "Guadeloupe"
            },
            {
                "id": 88,
                "code": "GQ",
                "name": "Equatorial Guinea"
            },
            {
                "id": 89,
                "code": "GR",
                "name": "Greece"
            },
            {
                "id": 90,
                "code": "GS",
                "name": "South Georgia and the South Sandwich Islands"
            },
            {
                "id": 91,
                "code": "GT",
                "name": "Guatemala"
            },
            {
                "id": 92,
                "code": "GU",
                "name": "Guam"
            },
            {
                "id": 93,
                "code": "GW",
                "name": "Guinea-Bissau"
            },
            {
                "id": 94,
                "code": "GY",
                "name": "Guyana"
            },
            {
                "id": 95,
                "code": "HK",
                "name": "Hong Kong"
            },
            {
                "id": 96,
                "code": "HM",
                "name": "Heard Island and McDonald Islands"
            },
            {
                "id": 97,
                "code": "HN",
                "name": "Honduras"
            },
            {
                "id": 98,
                "code": "HR",
                "name": "Croatia"
            },
            {
                "id": 99,
                "code": "HT",
                "name": "Haiti"
            },
            {
                "id": 100,
                "code": "HU",
                "name": "Hungary"
            },
            {
                "id": 101,
                "code": "ID",
                "name": "Indonesia"
            },
            {
                "id": 102,
                "code": "IE",
                "name": "Ireland"
            },
            {
                "id": 103,
                "code": "IL",
                "name": "Israel"
            },
            {
                "id": 104,
                "code": "IM",
                "name": "Isle of Man"
            },
            {
                "id": 105,
                "code": "IN",
                "name": "India"
            },
            {
                "id": 106,
                "code": "IO",
                "name": "British Indian Ocean Territory"
            },
            {
                "id": 107,
                "code": "IQ",
                "name": "Iraq"
            },
            {
                "id": 108,
                "code": "IR",
                "name": "Iran"
            },
            {
                "id": 109,
                "code": "IS",
                "name": "Iceland"
            },
            {
                "id": 110,
                "code": "IT",
                "name": "Italy"
            },
            {
                "id": 111,
                "code": "JE",
                "name": "Jersey"
            },
            {
                "id": 112,
                "code": "JM",
                "name": "Jamaica"
            },
            {
                "id": 113,
                "code": "JO",
                "name": "Jordan"
            },
            {
                "id": 114,
                "code": "JP",
                "name": "Japan"
            },
            {
                "id": 115,
                "code": "KE",
                "name": "Kenya"
            },
            {
                "id": 116,
                "code": "KG",
                "name": "Kyrgyzstan"
            },
            {
                "id": 117,
                "code": "KH",
                "name": "Cambodia"
            },
            {
                "id": 118,
                "code": "KI",
                "name": "Kiribati"
            },
            {
                "id": 119,
                "code": "KM",
                "name": "Comoros"
            },
            {
                "id": 120,
                "code": "KN",
                "name": "Saint Kitts and Nevis"
            },
            {
                "id": 121,
                "code": "KP",
                "name": "North Korea"
            },
            {
                "id": 122,
                "code": "KR",
                "name": "South Korea"
            },
            {
                "id": 123,
                "code": "KW",
                "name": "Kuwait"
            },
            {
                "id": 124,
                "code": "KY",
                "name": "Cayman Islands"
            },
            {
                "id": 125,
                "code": "KZ",
                "name": "Kazakhstan"
            },
            {
                "id": 126,
                "code": "LA",
                "name": "Laos"
            },
            {
                "id": 127,
                "code": "LB",
                "name": "Lebanon"
            },
            {
                "id": 128,
                "code": "LC",
                "name": "Saint Lucia"
            },
            {
                "id": 129,
                "code": "LI",
                "name": "Liechtenstein"
            },
            {
                "id": 130,
                "code": "LK",
                "name": "Sri Lanka"
            },
            {
                "id": 131,
                "code": "LR",
                "name": "Liberia"
            },
            {
                "id": 132,
                "code": "LS",
                "name": "Lesotho"
            },
            {
                "id": 133,
                "code": "LT",
                "name": "Lithuania"
            },
            {
                "id": 134,
                "code": "LU",
                "name": "Luxembourg"
            },
            {
                "id": 135,
                "code": "LV",
                "name": "Latvia"
            },
            {
                "id": 136,
                "code": "LY",
                "name": "Libya"
            },
            {
                "id": 137,
                "code": "MA",
                "name": "Morocco"
            },
            {
                "id": 138,
                "code": "MC",
                "name": "Monaco"
            },
            {
                "id": 139,
                "code": "MD",
                "name": "Moldova"
            },
            {
                "id": 140,
                "code": "ME",
                "name": "Montenegro"
            },
            {
                "id": 141,
                "code": "MF",
                "name": "Saint Martin"
            },
            {
                "id": 142,
                "code": "MG",
                "name": "Madagascar"
            },
            {
                "id": 143,
                "code": "MH",
                "name": "Marshall Islands"
            },
            {
                "id": 144,
                "code": "MK",
                "name": "Macedonia"
            },
            {
                "id": 145,
                "code": "ML",
                "name": "Mali"
            },
            {
                "id": 146,
                "code": "MM",
                "name": "Myanmar [Burma]"
            },
            {
                "id": 147,
                "code": "MN",
                "name": "Mongolia"
            },
            {
                "id": 148,
                "code": "MO",
                "name": "Macao"
            },
            {
                "id": 149,
                "code": "MP",
                "name": "Northern Mariana Islands"
            },
            {
                "id": 150,
                "code": "MQ",
                "name": "Martinique"
            },
            {
                "id": 151,
                "code": "MR",
                "name": "Mauritania"
            },
            {
                "id": 152,
                "code": "MS",
                "name": "Montserrat"
            },
            {
                "id": 153,
                "code": "MT",
                "name": "Malta"
            },
            {
                "id": 154,
                "code": "MU",
                "name": "Mauritius"
            },
            {
                "id": 155,
                "code": "MV",
                "name": "Maldives"
            },
            {
                "id": 156,
                "code": "MW",
                "name": "Malawi"
            },
            {
                "id": 157,
                "code": "MX",
                "name": "Mexico"
            },
            {
                "id": 158,
                "code": "MY",
                "name": "Malaysia"
            },
            {
                "id": 159,
                "code": "MZ",
                "name": "Mozambique"
            },
            {
                "id": 160,
                "code": "NA",
                "name": "Namibia"
            },
            {
                "id": 161,
                "code": "NC",
                "name": "New Caledonia"
            },
            {
                "id": 162,
                "code": "NE",
                "name": "Niger"
            },
            {
                "id": 163,
                "code": "NF",
                "name": "Norfolk Island"
            },
            {
                "id": 164,
                "code": "NG",
                "name": "Nigeria"
            },
            {
                "id": 165,
                "code": "NI",
                "name": "Nicaragua"
            },
            {
                "id": 166,
                "code": "NL",
                "name": "Netherlands"
            },
            {
                "id": 167,
                "code": "NO",
                "name": "Norway"
            },
            {
                "id": 168,
                "code": "NP",
                "name": "Nepal"
            },
            {
                "id": 169,
                "code": "NR",
                "name": "Nauru"
            },
            {
                "id": 170,
                "code": "NU",
                "name": "Niue"
            },
            {
                "id": 171,
                "code": "NZ",
                "name": "New Zealand"
            },
            {
                "id": 172,
                "code": "OM",
                "name": "Oman"
            },
            {
                "id": 173,
                "code": "PA",
                "name": "Panama"
            },
            {
                "id": 174,
                "code": "PE",
                "name": "Peru"
            },
            {
                "id": 175,
                "code": "PF",
                "name": "French Polynesia"
            },
            {
                "id": 176,
                "code": "PG",
                "name": "Papua New Guinea"
            },
            {
                "id": 177,
                "code": "PH",
                "name": "Philippines"
            },
            {
                "id": 178,
                "code": "PK",
                "name": "Pakistan"
            },
            {
                "id": 179,
                "code": "PL",
                "name": "Poland"
            },
            {
                "id": 180,
                "code": "PM",
                "name": "Saint Pierre and Miquelon"
            },
            {
                "id": 181,
                "code": "PN",
                "name": "Pitcairn Islands"
            },
            {
                "id": 182,
                "code": "PR",
                "name": "Puerto Rico"
            },
            {
                "id": 183,
                "code": "PS",
                "name": "Palestine"
            },
            {
                "id": 184,
                "code": "PT",
                "name": "Portugal"
            },
            {
                "id": 185,
                "code": "PW",
                "name": "Palau"
            },
            {
                "id": 186,
                "code": "PY",
                "name": "Paraguay"
            },
            {
                "id": 187,
                "code": "QA",
                "name": "Qatar"
            },
            {
                "id": 188,
                "code": "RE",
                "name": "Réunion"
            },
            {
                "id": 189,
                "code": "RO",
                "name": "Romania"
            },
            {
                "id": 190,
                "code": "RS",
                "name": "Serbia"
            },
            {
                "id": 191,
                "code": "RU",
                "name": "Russia"
            },
            {
                "id": 192,
                "code": "RW",
                "name": "Rwanda"
            },
            {
                "id": 193,
                "code": "SA",
                "name": "Saudi Arabia"
            },
            {
                "id": 194,
                "code": "SB",
                "name": "Solomon Islands"
            },
            {
                "id": 195,
                "code": "SC",
                "name": "Seychelles"
            },
            {
                "id": 196,
                "code": "SD",
                "name": "Sudan"
            },
            {
                "id": 197,
                "code": "SE",
                "name": "Sweden"
            },
            {
                "id": 198,
                "code": "SG",
                "name": "Singapore"
            },
            {
                "id": 199,
                "code": "SH",
                "name": "Saint Helena"
            },
            {
                "id": 200,
                "code": "SI",
                "name": "Slovenia"
            },
            {
                "id": 201,
                "code": "SJ",
                "name": "Svalbard and Jan Mayen"
            },
            {
                "id": 202,
                "code": "SK",
                "name": "Slovakia"
            },
            {
                "id": 203,
                "code": "SL",
                "name": "Sierra Leone"
            },
            {
                "id": 204,
                "code": "SM",
                "name": "San Marino"
            },
            {
                "id": 205,
                "code": "SN",
                "name": "Senegal"
            },
            {
                "id": 206,
                "code": "SO",
                "name": "Somalia"
            },
            {
                "id": 207,
                "code": "SR",
                "name": "Suriname"
            },
            {
                "id": 208,
                "code": "SS",
                "name": "South Sudan"
            },
            {
                "id": 209,
                "code": "ST",
                "name": "São Tomé and Príncipe"
            },
            {
                "id": 210,
                "code": "SV",
                "name": "El Salvador"
            },
            {
                "id": 211,
                "code": "SX",
                "name": "Sint Maarten"
            },
            {
                "id": 212,
                "code": "SY",
                "name": "Syria"
            },
            {
                "id": 213,
                "code": "SZ",
                "name": "Swaziland"
            },
            {
                "id": 214,
                "code": "TC",
                "name": "Turks and Caicos Islands"
            },
            {
                "id": 215,
                "code": "TD",
                "name": "Chad"
            },
            {
                "id": 216,
                "code": "TF",
                "name": "French Southern Territories"
            },
            {
                "id": 217,
                "code": "TG",
                "name": "Togo"
            },
            {
                "id": 218,
                "code": "TH",
                "name": "Thailand"
            },
            {
                "id": 219,
                "code": "TJ",
                "name": "Tajikistan"
            },
            {
                "id": 220,
                "code": "TK",
                "name": "Tokelau"
            },
            {
                "id": 221,
                "code": "TL",
                "name": "East Timor"
            },
            {
                "id": 222,
                "code": "TM",
                "name": "Turkmenistan"
            },
            {
                "id": 223,
                "code": "TN",
                "name": "Tunisia"
            },
            {
                "id": 224,
                "code": "TO",
                "name": "Tonga"
            },
            {
                "id": 225,
                "code": "TR",
                "name": "Turkey"
            },
            {
                "id": 226,
                "code": "TT",
                "name": "Trinidad and Tobago"
            },
            {
                "id": 227,
                "code": "TV",
                "name": "Tuvalu"
            },
            {
                "id": 228,
                "code": "TW",
                "name": "Taiwan"
            },
            {
                "id": 229,
                "code": "TZ",
                "name": "Tanzania"
            },
            {
                "id": 230,
                "code": "UA",
                "name": "Ukraine"
            },
            {
                "id": 231,
                "code": "UG",
                "name": "Uganda"
            },
            {
                "id": 232,
                "code": "UM",
                "name": "U.S. Minor Outlying Islands"
            },
            {
                "id": 233,
                "code": "US",
                "name": "United States"
            },
            {
                "id": 234,
                "code": "UY",
                "name": "Uruguay"
            },
            {
                "id": 235,
                "code": "UZ",
                "name": "Uzbekistan"
            },
            {
                "id": 236,
                "code": "VA",
                "name": "Vatican City"
            },
            {
                "id": 237,
                "code": "VC",
                "name": "Saint Vincent and the Grenadines"
            },
            {
                "id": 238,
                "code": "VE",
                "name": "Venezuela"
            },
            {
                "id": 239,
                "code": "VG",
                "name": "British Virgin Islands"
            },
            {
                "id": 240,
                "code": "VI",
                "name": "U.S. Virgin Islands"
            },
            {
                "id": 241,
                "code": "VN",
                "name": "Vietnam"
            },
            {
                "id": 242,
                "code": "VU",
                "name": "Vanuatu"
            },
            {
                "id": 243,
                "code": "WF",
                "name": "Wallis and Futuna"
            },
            {
                "id": 244,
                "code": "WS",
                "name": "Samoa"
            },
            {
                "id": 245,
                "code": "XK",
                "name": "Kosovo"
            },
            {
                "id": 246,
                "code": "YE",
                "name": "Yemen"
            },
            {
                "id": 247,
                "code": "YT",
                "name": "Mayotte"
            },
            {
                "id": 248,
                "code": "ZA",
                "name": "South Africa"
            },
            {
                "id": 249,
                "code": "ZM",
                "name": "Zambia"
            },
            {
                "id": 250,
                "code": "ZW",
                "name": "Zimbabwe"
            }
        ],
        CURRENCIES: [
            {
                "id": 1,
                "iso": "GBP",
                "name": "Pound",
                "description": "Pound",
                "sign": "£"
            },
            {
                "id": 2,
                "iso": "USD",
                "name": "US Dollar",
                "description": "US Dollar",
                "sign": "$"
            },
            {
                "id": 3,
                "iso": "AUD",
                "name": "Australia Dollar",
                "description": "Australia Dollar",
                "sign": "$"
            },
            {
                "id": 4,
                "iso": "CAD",
                "name": "Canada Dollar",
                "description": "Canada Dollar",
                "sign": "$"
            },
            {
                "id": 5,
                "iso": "EUR",
                "name": "Euro",
                "description": "Euro",
                "sign": "€"
            }
        ],
        PAYMENT_TYPES: [
            {
                "id": 1,
                "description": "Card"
            },
            {
                "id": 2,
                "description": "Cash"
            },
            {
                "id": 3,
                "description": "Bank"
            },
            {
                "id": 4,
                "description": "Other"
            }
        ],
        USER_TYPES: [
            {
                "id": 1,
                "name": "Sole trader",
                "description": "Sole trader"
            },
            {
                "id": 2,
                "name": "Partnership ",
                "description": "Partnership "
            },
            {
                "id": 3,
                "name": "Limited Liability Partnership",
                "description": "Limited Liability Partnership"
            },
            {
                "id": 4,
                "name": "Limited Company",
                "description": "Limited Company"
            },
            {
                "id": 5,
                "name": "Individual",
                "description": "Individual"
            }
        ],
        VEHICLES: [
            {
                "id": 1,
                "description": "Car"
            },
            {
                "id": 2,
                "description": "Van"
            },
            {
                "id": 3,
                "description": "Motorcycle"
            },
            {
                "id": 4,
                "description": "Cycle"
            }
        ],
        RECORD_TYPES: [
            {
                "id": 1,
                "description": "Sales",
                "multiple_per_year": 1,
                "type_group_id": 1,
                "record_type_group": {
                    "id": 1,
                    "description": "Income",
                    "order": 1
                }
            },
            {
                "id": 2,
                "description": "Fees",
                "multiple_per_year": 1,
                "type_group_id": 1,
                "record_type_group": {
                    "id": 1,
                    "description": "Income",
                    "order": 1
                }
            },
            {
                "id": 3,
                "description": "Bank interest",
                "multiple_per_year": 1,
                "type_group_id": 1,
                "record_type_group": {
                    "id": 1,
                    "description": "Income",
                    "order": 1
                }
            },
            {
                "id": 4,
                "description": "Other business income",
                "multiple_per_year": 1,
                "type_group_id": 1,
                "record_type_group": {
                    "id": 1,
                    "description": "Income",
                    "order": 1
                }
            },
            {
                "id": 5,
                "description": "Cost of goods",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 6,
                "description": "Payment to subcontractors",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 7,
                "description": "Wages, salaries and other staff costs",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 8,
                "description": "Car, van and travel expenses",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 9,
                "description": "Rent, rates & power",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 10,
                "description": "Insurance cost",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 11,
                "description": "Repairs and renewals of property & equipment",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 12,
                "description": "Phone & Office costs",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 13,
                "description": "Advertising & Marketing",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 14,
                "description": "Business entertainment costs",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 15,
                "description": "Interest on bank and other loans",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 16,
                "description": "Bank, credit card and other financial charges",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 17,
                "description": "Accountancy & legal",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 18,
                "description": "Other professional fees",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 19,
                "description": "Other business expenses",
                "multiple_per_year": 1,
                "type_group_id": 2,
                "record_type_group": {
                    "id": 2,
                    "description": "Expense",
                    "order": 2
                }
            },
            {
                "id": 20,
                "description": "Business use of home",
                "multiple_per_year": 1,
                "type_group_id": 3,
                "record_type_group": {
                    "id": 3,
                    "description": "Standard Allowances",
                    "order": 3
                }
            },
            {
                "id": 21,
                "description": "Personal use of business premisses",
                "multiple_per_year": 1,
                "type_group_id": 3,
                "record_type_group": {
                    "id": 3,
                    "description": "Standard Allowances",
                    "order": 3
                }
            },
            {
                "id": 22,
                "description": "Business mileage flat-rate scheme",
                "multiple_per_year": 1,
                "type_group_id": 3,
                "record_type_group": {
                    "id": 3,
                    "description": "Standard Allowances",
                    "order": 3
                }
            },
            {
                "id": 23,
                "description": "CIS deduction",
                "multiple_per_year": 1,
                "type_group_id": 4,
                "record_type_group": {
                    "id": 4,
                    "description": "CIS deductions",
                    "order": 4
                }
            },
            {
                "id": 24,
                "description": "Employment record",
                "multiple_per_year": 0,
                "type_group_id": 5,
                "record_type_group": {
                    "id": 5,
                    "description": "Employment",
                    "order": 5
                }
            },
            {
                "id": 25,
                "description": "Company cars and vans benefit",
                "multiple_per_year": 0,
                "type_group_id": 6,
                "record_type_group": {
                    "id": 6,
                    "description": "Employment Benefits",
                    "order": 6
                }
            },
            {
                "id": 26,
                "description": "Fuel for cars and vans",
                "multiple_per_year": 0,
                "type_group_id": 6,
                "record_type_group": {
                    "id": 6,
                    "description": "Employment Benefits",
                    "order": 6
                }
            },
            {
                "id": 27,
                "description": "Private medical dental insurance",
                "multiple_per_year": 0,
                "type_group_id": 6,
                "record_type_group": {
                    "id": 6,
                    "description": "Employment Benefits",
                    "order": 6
                }
            },
            {
                "id": 28,
                "description": "Vouchers credit cards excess mileage allowance",
                "multiple_per_year": 0,
                "type_group_id": 6,
                "record_type_group": {
                    "id": 6,
                    "description": "Employment Benefits",
                    "order": 6
                }
            },
            {
                "id": 29,
                "description": "Goods etc provided by employer",
                "multiple_per_year": 0,
                "type_group_id": 6,
                "record_type_group": {
                    "id": 6,
                    "description": "Employment Benefits",
                    "order": 6
                }
            },
            {
                "id": 30,
                "description": "Accommodation provider by employer",
                "multiple_per_year": 0,
                "type_group_id": 6,
                "record_type_group": {
                    "id": 6,
                    "description": "Employment Benefits",
                    "order": 6
                }
            },
            {
                "id": 31,
                "description": "Other benefits",
                "multiple_per_year": 0,
                "type_group_id": 6,
                "record_type_group": {
                    "id": 6,
                    "description": "Employment Benefits",
                    "order": 6
                }
            },
            {
                "id": 32,
                "description": "Expenses payments received",
                "multiple_per_year": 0,
                "type_group_id": 7,
                "record_type_group": {
                    "id": 7,
                    "description": "Employment Expenses",
                    "order": 7
                }
            },
            {
                "id": 33,
                "description": "Business travel and subsistence",
                "multiple_per_year": 0,
                "type_group_id": 7,
                "record_type_group": {
                    "id": 7,
                    "description": "Employment Expenses",
                    "order": 7
                }
            },
            {
                "id": 34,
                "description": "Fixed expenses deductions",
                "multiple_per_year": 0,
                "type_group_id": 7,
                "record_type_group": {
                    "id": 7,
                    "description": "Employment Expenses",
                    "order": 7
                }
            },
            {
                "id": 35,
                "description": "Professional fees and subscriptions",
                "multiple_per_year": 0,
                "type_group_id": 7,
                "record_type_group": {
                    "id": 7,
                    "description": "Employment Expenses",
                    "order": 7
                }
            },
            {
                "id": 36,
                "description": "Other expenses and capital allowances",
                "multiple_per_year": 0,
                "type_group_id": 7,
                "record_type_group": {
                    "id": 7,
                    "description": "Employment Expenses",
                    "order": 7
                }
            },
            {
                "id": 37,
                "description": "Payments To Registered Pension Schemes",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 38,
                "description": "Retirement Annuity Contract Payments",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 39,
                "description": "Employer Pension Scheme Payments",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 40,
                "description": "Non UK Overseas Pension Scheme Payments",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 41,
                "description": "Gift Aid Payments Made In Year",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 42,
                "description": "One Off Gift Aid Payments",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 43,
                "description": "Gift Aid Payments Carried Back To Previous Year",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 44,
                "description": "Gift Aid Payments Brought Back From Later Year",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 45,
                "description": "Shares Gifted To Charity",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 46,
                "description": "Land And Buildings Gifted To Charity",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 47,
                "description": "Investments Gifted To Non UK Charities",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 48,
                "description": "Donations To Non UK Charities",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 49,
                "description": "Registered Blind",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 50,
                "description": "Surplus Blind Persons Allowance To Spouse",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 51,
                "description": "Local Authority Name",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 52,
                "description": "Surplus Blind Persons Allowance From Spouse",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 53,
                "description": "Venture Capital Trust Share Subscriptions",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 54,
                "description": "Enterprise Investment Scheme Share Subscriptions",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 55,
                "description": "Community Investment Trust Relief",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 56,
                "description": "Annuities And Annual Payments",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 57,
                "description": "Qualifying Loan Interest",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 58,
                "description": "Post Cessation And Other Losses",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 59,
                "description": "Maintenance Or Alimony Payments",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 60,
                "description": "Trade Union Etc Death Benefit Payments",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 61,
                "description": "Employers Widows Orphans Etc Benefit Scheme",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 62,
                "description": "Bonus Security Redemption Distribution Relief",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 63,
                "description": "Seed Enterprise Investment Scheme Amount",
                "multiple_per_year": 0,
                "type_group_id": 8,
                "record_type_group": {
                    "id": 8,
                    "description": "Tax Relief",
                    "order": 8
                }
            },
            {
                "id": 64,
                "description": "Taxed Bank Building Society Etc Interest",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 65,
                "description": "Untaxed Interest Etc",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 66,
                "description": "Company Dividends",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 67,
                "description": "Unit Trust Etc Dividends",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 68,
                "description": "Foreign Dividends",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 69,
                "description": "Annual State Pension",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 70,
                "description": "State Pension Lump Sum",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 71,
                "description": "Other Pensions And Retirement Annuities",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 72,
                "description": "Incapacity Benefit",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 73,
                "description": "Jobseekers Allowance",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 74,
                "description": "Other State Pensions And Benefits",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 75,
                "description": "Other Taxable Income",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 76,
                "description": "Allowable Expenses",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 77,
                "description": "Deemed Income Or Benefits",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 78,
                "description": "Description Of Other Income",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 79,
                "description": "Tax Taken Off Pensions And Retirement Annuities",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 80,
                "description": "Tax Taken Off Incapacity Benefit",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 81,
                "description": "Tax Taken Off Other Taxable Income",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 82,
                "description": "Tax Taken Off Foreign Dividends",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 83,
                "description": "Tax Taken Off Pension Lump Sum",
                "multiple_per_year": 0,
                "type_group_id": 9,
                "record_type_group": {
                    "id": 9,
                    "description": "Other Income",
                    "order": 9
                }
            },
            {
                "id": 84,
                "description": "Property record",
                "multiple_per_year": 1,
                "type_group_id": 10,
                "record_type_group": {
                    "id": 10,
                    "description": "Property",
                    "order": 10
                }
            },
            {
                "id": 85,
                "description": "Furnished Holiday Lettings Income",
                "multiple_per_year": 1,
                "type_group_id": 11,
                "record_type_group": {
                    "id": 11,
                    "description": "Property Income",
                    "order": 11
                }
            },
            {
                "id": 86,
                "description": "Total Rents And Other Income From Property",
                "multiple_per_year": 1,
                "type_group_id": 11,
                "record_type_group": {
                    "id": 11,
                    "description": "Property Income",
                    "order": 11
                }
            },
            {
                "id": 87,
                "description": "Premiums For Grant Of A Lease",
                "multiple_per_year": 1,
                "type_group_id": 11,
                "record_type_group": {
                    "id": 11,
                    "description": "Property Income",
                    "order": 11
                }
            },
            {
                "id": 88,
                "description": "Furnished Holiday Lettings Expenses",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 89,
                "description": "Furnished Holiday Lettings Interest Etc Costs",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 90,
                "description": "Furnished Holiday Lettings Management Etc Fees",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 91,
                "description": "Furnished Holiday Lettings Other Expenses",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 92,
                "description": "Reverse Premiums And Inducements",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 93,
                "description": "Rent Rates Insurance Etc",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 94,
                "description": "Repairs Maintenance And Renewals",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 95,
                "description": "Interest And Other Financial Charges",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 96,
                "description": "Legal Management And Professional Fees",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 97,
                "description": "Costs Of Services Provided",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 98,
                "description": "Other Property Expenses",
                "multiple_per_year": 1,
                "type_group_id": 12,
                "record_type_group": {
                    "id": 12,
                    "description": "Property Expenses",
                    "order": 12
                }
            },
            {
                "id": 99,
                "description": "Child Benefit Record",
                "multiple_per_year": 0,
                "type_group_id": 13,
                "record_type_group": {
                    "id": 13,
                    "description": "Child Benefit",
                    "order": 13
                }
            },
            {
                "id": 100,
                "description": "Student Loan Deducted Record",
                "multiple_per_year": 0,
                "type_group_id": 14,
                "record_type_group": {
                    "id": 14,
                    "description": "Student Loan Deducted",
                    "order": 14
                }
            },
            {
                "id": 101,
                "description": "Service Company Income Record",
                "multiple_per_year": 0,
                "type_group_id": 15,
                "record_type_group": {
                    "id": 15,
                    "description": "Service Company Income",
                    "order": 15
                }
            },
            {
                "id": 102,
                "description": "Loan repayment",
                "multiple_per_year": 1,
                "type_group_id": 19,
                "record_type_group": {
                    "id": 19,
                    "description": "Personal",
                    "order": 19
                }
            },
            {
                "id": 103,
                "description": "Director Loan",
                "multiple_per_year": 1,
                "type_group_id": 19,
                "record_type_group": {
                    "id": 19,
                    "description": "Personal",
                    "order": 19
                }
            },
            {
                "id": 104,
                "description": "Personal Drawings",
                "multiple_per_year": 1,
                "type_group_id": 19,
                "record_type_group": {
                    "id": 19,
                    "description": "Personal",
                    "order": 19
                }
            },
            {
                "id": 105,
                "description": "Capital",
                "multiple_per_year": 1,
                "type_group_id": 19,
                "record_type_group": {
                    "id": 19,
                    "description": "Personal",
                    "order": 19
                }
            }
        ],
        RECORD_TYPE_GROUPS: [
            {
                "id": 1,
                "description": "Income",
                "order": 1,
                "record_types": [
                    {
                        "id": 1,
                        "description": "Sales",
                        "multiple_per_year": 1,
                        "type_group_id": 1
                    },
                    {
                        "id": 2,
                        "description": "Fees",
                        "multiple_per_year": 1,
                        "type_group_id": 1
                    },
                    {
                        "id": 3,
                        "description": "Bank interest",
                        "multiple_per_year": 1,
                        "type_group_id": 1
                    },
                    {
                        "id": 4,
                        "description": "Other business income",
                        "multiple_per_year": 1,
                        "type_group_id": 1
                    }
                ]
            },
            {
                "id": 2,
                "description": "Expense",
                "order": 2,
                "record_types": [
                    {
                        "id": 5,
                        "description": "Cost of goods",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 6,
                        "description": "Payment to subcontractors",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 7,
                        "description": "Wages, salaries and other staff costs",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 8,
                        "description": "Car, van and travel expenses",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 9,
                        "description": "Rent, rates & power",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 10,
                        "description": "Insurance cost",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 11,
                        "description": "Repairs and renewals of property & equipment",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 12,
                        "description": "Phone & Office costs",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 13,
                        "description": "Advertising & Marketing",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 14,
                        "description": "Business entertainment costs",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 15,
                        "description": "Interest on bank and other loans",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 16,
                        "description": "Bank, credit card and other financial charges",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 17,
                        "description": "Accountancy & legal",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 18,
                        "description": "Other professional fees",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    },
                    {
                        "id": 19,
                        "description": "Other business expenses",
                        "multiple_per_year": 1,
                        "type_group_id": 2
                    }
                ]
            },
            {
                "id": 3,
                "description": "Standard Allowances",
                "order": 3,
                "record_types": [
                    {
                        "id": 20,
                        "description": "Business use of home",
                        "multiple_per_year": 1,
                        "type_group_id": 3
                    },
                    {
                        "id": 21,
                        "description": "Personal use of business premisses",
                        "multiple_per_year": 1,
                        "type_group_id": 3
                    },
                    {
                        "id": 22,
                        "description": "Business mileage flat-rate scheme",
                        "multiple_per_year": 1,
                        "type_group_id": 3
                    }
                ]
            },
            {
                "id": 4,
                "description": "CIS deductions",
                "order": 4,
                "record_types": [
                    {
                        "id": 23,
                        "description": "CIS deduction",
                        "multiple_per_year": 1,
                        "type_group_id": 4
                    }
                ]
            },
            {
                "id": 5,
                "description": "Employment",
                "order": 5,
                "record_types": [
                    {
                        "id": 24,
                        "description": "Employment record",
                        "multiple_per_year": 0,
                        "type_group_id": 5
                    }
                ]
            },
            {
                "id": 6,
                "description": "Employment Benefits",
                "order": 6,
                "record_types": [
                    {
                        "id": 25,
                        "description": "Company cars and vans benefit",
                        "multiple_per_year": 0,
                        "type_group_id": 6
                    },
                    {
                        "id": 26,
                        "description": "Fuel for cars and vans",
                        "multiple_per_year": 0,
                        "type_group_id": 6
                    },
                    {
                        "id": 27,
                        "description": "Private medical dental insurance",
                        "multiple_per_year": 0,
                        "type_group_id": 6
                    },
                    {
                        "id": 28,
                        "description": "Vouchers credit cards excess mileage allowance",
                        "multiple_per_year": 0,
                        "type_group_id": 6
                    },
                    {
                        "id": 29,
                        "description": "Goods etc provided by employer",
                        "multiple_per_year": 0,
                        "type_group_id": 6
                    },
                    {
                        "id": 30,
                        "description": "Accommodation provider by employer",
                        "multiple_per_year": 0,
                        "type_group_id": 6
                    },
                    {
                        "id": 31,
                        "description": "Other benefits",
                        "multiple_per_year": 0,
                        "type_group_id": 6
                    }
                ]
            },
            {
                "id": 7,
                "description": "Employment Expenses",
                "order": 7,
                "record_types": [
                    {
                        "id": 32,
                        "description": "Expenses payments received",
                        "multiple_per_year": 0,
                        "type_group_id": 7
                    },
                    {
                        "id": 33,
                        "description": "Business travel and subsistence",
                        "multiple_per_year": 0,
                        "type_group_id": 7
                    },
                    {
                        "id": 34,
                        "description": "Fixed expenses deductions",
                        "multiple_per_year": 0,
                        "type_group_id": 7
                    },
                    {
                        "id": 35,
                        "description": "Professional fees and subscriptions",
                        "multiple_per_year": 0,
                        "type_group_id": 7
                    },
                    {
                        "id": 36,
                        "description": "Other expenses and capital allowances",
                        "multiple_per_year": 0,
                        "type_group_id": 7
                    }
                ]
            },
            {
                "id": 8,
                "description": "Tax Relief",
                "order": 8,
                "record_types": [
                    {
                        "id": 37,
                        "description": "Payments To Registered Pension Schemes",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 38,
                        "description": "Retirement Annuity Contract Payments",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 39,
                        "description": "Employer Pension Scheme Payments",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 40,
                        "description": "Non UK Overseas Pension Scheme Payments",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 41,
                        "description": "Gift Aid Payments Made In Year",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 42,
                        "description": "One Off Gift Aid Payments",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 43,
                        "description": "Gift Aid Payments Carried Back To Previous Year",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 44,
                        "description": "Gift Aid Payments Brought Back From Later Year",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 45,
                        "description": "Shares Gifted To Charity",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 46,
                        "description": "Land And Buildings Gifted To Charity",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 47,
                        "description": "Investments Gifted To Non UK Charities",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 48,
                        "description": "Donations To Non UK Charities",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 49,
                        "description": "Registered Blind",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 50,
                        "description": "Surplus Blind Persons Allowance To Spouse",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 51,
                        "description": "Local Authority Name",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 52,
                        "description": "Surplus Blind Persons Allowance From Spouse",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 53,
                        "description": "Venture Capital Trust Share Subscriptions",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 54,
                        "description": "Enterprise Investment Scheme Share Subscriptions",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 55,
                        "description": "Community Investment Trust Relief",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 56,
                        "description": "Annuities And Annual Payments",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 57,
                        "description": "Qualifying Loan Interest",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 58,
                        "description": "Post Cessation And Other Losses",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 59,
                        "description": "Maintenance Or Alimony Payments",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 60,
                        "description": "Trade Union Etc Death Benefit Payments",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 61,
                        "description": "Employers Widows Orphans Etc Benefit Scheme",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 62,
                        "description": "Bonus Security Redemption Distribution Relief",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    },
                    {
                        "id": 63,
                        "description": "Seed Enterprise Investment Scheme Amount",
                        "multiple_per_year": 0,
                        "type_group_id": 8
                    }
                ]
            },
            {
                "id": 9,
                "description": "Other Income",
                "order": 9,
                "record_types": [
                    {
                        "id": 64,
                        "description": "Taxed Bank Building Society Etc Interest",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 65,
                        "description": "Untaxed Interest Etc",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 66,
                        "description": "Company Dividends",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 67,
                        "description": "Unit Trust Etc Dividends",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 68,
                        "description": "Foreign Dividends",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 69,
                        "description": "Annual State Pension",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 70,
                        "description": "State Pension Lump Sum",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 71,
                        "description": "Other Pensions And Retirement Annuities",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 72,
                        "description": "Incapacity Benefit",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 73,
                        "description": "Jobseekers Allowance",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 74,
                        "description": "Other State Pensions And Benefits",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 75,
                        "description": "Other Taxable Income",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 76,
                        "description": "Allowable Expenses",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 77,
                        "description": "Deemed Income Or Benefits",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 78,
                        "description": "Description Of Other Income",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 79,
                        "description": "Tax Taken Off Pensions And Retirement Annuities",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 80,
                        "description": "Tax Taken Off Incapacity Benefit",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 81,
                        "description": "Tax Taken Off Other Taxable Income",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 82,
                        "description": "Tax Taken Off Foreign Dividends",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    },
                    {
                        "id": 83,
                        "description": "Tax Taken Off Pension Lump Sum",
                        "multiple_per_year": 0,
                        "type_group_id": 9
                    }
                ]
            },
            {
                "id": 10,
                "description": "Property",
                "order": 10,
                "record_types": [
                    {
                        "id": 84,
                        "description": "Property record",
                        "multiple_per_year": 1,
                        "type_group_id": 10
                    }
                ]
            },
            {
                "id": 11,
                "description": "Property Income",
                "order": 11,
                "record_types": [
                    {
                        "id": 85,
                        "description": "Furnished Holiday Lettings Income",
                        "multiple_per_year": 1,
                        "type_group_id": 11
                    },
                    {
                        "id": 86,
                        "description": "Total Rents And Other Income From Property",
                        "multiple_per_year": 1,
                        "type_group_id": 11
                    },
                    {
                        "id": 87,
                        "description": "Premiums For Grant Of A Lease",
                        "multiple_per_year": 1,
                        "type_group_id": 11
                    }
                ]
            },
            {
                "id": 12,
                "description": "Property Expenses",
                "order": 12,
                "record_types": [
                    {
                        "id": 88,
                        "description": "Furnished Holiday Lettings Expenses",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 89,
                        "description": "Furnished Holiday Lettings Interest Etc Costs",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 90,
                        "description": "Furnished Holiday Lettings Management Etc Fees",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 91,
                        "description": "Furnished Holiday Lettings Other Expenses",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 92,
                        "description": "Reverse Premiums And Inducements",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 93,
                        "description": "Rent Rates Insurance Etc",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 94,
                        "description": "Repairs Maintenance And Renewals",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 95,
                        "description": "Interest And Other Financial Charges",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 96,
                        "description": "Legal Management And Professional Fees",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 97,
                        "description": "Costs Of Services Provided",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    },
                    {
                        "id": 98,
                        "description": "Other Property Expenses",
                        "multiple_per_year": 1,
                        "type_group_id": 12
                    }
                ]
            },
            {
                "id": 13,
                "description": "Child Benefit",
                "order": 13,
                "record_types": [
                    {
                        "id": 99,
                        "description": "Child Benefit Record",
                        "multiple_per_year": 0,
                        "type_group_id": 13
                    }
                ]
            },
            {
                "id": 14,
                "description": "Student Loan Deducted",
                "order": 14,
                "record_types": [
                    {
                        "id": 100,
                        "description": "Student Loan Deducted Record",
                        "multiple_per_year": 0,
                        "type_group_id": 14
                    }
                ]
            },
            {
                "id": 15,
                "description": "Service Company Income",
                "order": 15,
                "record_types": [
                    {
                        "id": 101,
                        "description": "Service Company Income Record",
                        "multiple_per_year": 0,
                        "type_group_id": 15
                    }
                ]
            },
            {
                "id": 16,
                "description": "Asset",
                "order": 16,
                "record_types": []
            },
            {
                "id": 17,
                "description": "Liability",
                "order": 17,
                "record_types": []
            },
            {
                "id": 18,
                "description": "Capital",
                "order": 18,
                "record_types": []
            },
            {
                "id": 19,
                "description": "Personal",
                "order": 19,
                "record_types": [
                    {
                        "id": 102,
                        "description": "Loan repayment",
                        "multiple_per_year": 1,
                        "type_group_id": 19
                    },
                    {
                        "id": 103,
                        "description": "Director Loan",
                        "multiple_per_year": 1,
                        "type_group_id": 19
                    },
                    {
                        "id": 104,
                        "description": "Personal Drawings",
                        "multiple_per_year": 1,
                        "type_group_id": 19
                    },
                    {
                        "id": 105,
                        "description": "Capital ",
                        "multiple_per_year": 1,
                        "type_group_id": 19
                    }
                ]
            }
        ],
        DUE_ON_OPTIONS: [
            'Due upon receipt',
            'Due within 7 days',
            'Due within 14 days',
            'Due within 21 days',
            'Due within 30 days',
            'Paid'
        ],
        INVOICE_STATUS: ['Approved', 'Queried', 'Rejected']
    });

