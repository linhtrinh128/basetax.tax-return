angular.module('app.loggedIn', [])

    .config(function ($stateProvider, securityAuthorizationProvider) {

        $stateProvider
            .state('loggedIn', {
                abstract: true,
                views: {
                    'root': {
                        templateUrl: 'layouts/layout-navigation-content.tpl.html',
                        controller: 'LoggedInController'
                    }
                },
                resolve: {
                    authenticatedUser: securityAuthorizationProvider.requireAuthenticatedUser
                }
            });
    })

    .controller('LoggedInController', function ($scope) {
        // Do nothing
    });
