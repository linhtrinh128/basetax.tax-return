angular.module('directive', [
  'directive.cf-modal',
  'directive.edit-input',
  'directive.format-currency',
  'directive.keypress-numeric',
  'directive.search-box'
])
  .directive('pageTitle', pageTitle)
  .directive('sideNavigation', sideNavigation)
  .directive('iboxTools', iboxTools)
  .directive('minimalizaSidebar', minimalizaSidebar)
  .directive('vectorMap', vectorMap)
  .directive('sparkline', sparkline)
  .directive('icheck', icheck)
  .directive('ionRangeSlider', ionRangeSlider)
  .directive('dropZone', dropZone)
  .directive('responsiveVideo', responsiveVideo)
  .directive('autoResize', autoResize)
  .directive('fixHeight', fixHeight)
  .directive('uiToggleClass', uiToogleClass)
  .directive('uiNav', uiNav)
  .directive('uiScroll', uiScroll)
  .directive('uiButterbar', ['$rootScope', '$anchorScroll', uiButterbar])
  .directive('validForm', validForm)
  .directive('fileReader', fileReader);

function fileReader() {
    return {
    scope: {
      fileReader: "="
    },
    link: function (scope, element) {
      $(element).on('change', function (changeEvent) {
        var files = changeEvent.target.files;
        if (files.length) {
          var r = new FileReader();
          r.onload = function (e) {
            var contents = e.target.result;
            scope.$apply(function () {
              scope.fileReader = contents;
            });
          };

          r.readAsText(files[0]);
        }
      });
    }
  };
}
function uiToogleClass($timeout, $document) {
  return {
    restrict: 'AC',
    link: function (scope, el, attr) {
      el.on('click', function (e) {
        e.preventDefault();
        //var array = [];
        var classes = attr.uiToggleClass.split(','),
          targets = (attr.target && attr.target.split(',')) || new Array(el),
          key = 0;
        angular.forEach(classes, function (_class) {
          var target = targets[(targets.length && key)];
          if (( _class.indexOf('*') !== -1 ) && magic(_class, target)) {
          }
          $(target).toggleClass(_class);
          key++;
        });
        $(el).toggleClass('active');

        function magic(_class, target) {
          var patt = new RegExp('\\s' +
            _class.replace(/\*/g, '[A-Za-z0-9-_]+').split(' ').join('\\s|\\s') +
            '\\s', 'g');
          var cn = ' ' + $(target)[0].className + ' ';
          while (patt.test(cn)) {
            cn = cn.replace(patt, ' ');
          }
          $(target)[0].className = $.trim(cn);
        }
      });
    }
  };
}

function uiNav($timeout) {
  return {
    restrict: 'AC',
    link: function (scope, el, attr) {
      var _window = $(window),
        _mb = 768,
        wrap = $('.app-aside'),
        next,
        backdrop = '.dropdown-backdrop';
      // unfolded
      el.on('click', 'a', function (e) {
        var a = next && next.trigger('mouseleave.nav') ? true : false;
        var _this = $(this);
        _this.parent().siblings(".active").toggleClass('active');
        var b = _this.next().is('ul') && _this.parent().toggleClass('active') && e.preventDefault() ? true : false;
        // mobile
        var c = _this.next().is('ul') || ( ( _window.width() < _mb ) && $('.app-aside').removeClass('show off-screen') ) ? true : false;
      });

      // folded & fixed
      el.on('mouseenter', 'a', function (e) {
        var a = next && next.trigger('mouseleave.nav') ? true : false;
        $('> .nav', wrap).remove();
        if (!$('.app-aside-fixed.app-aside-folded').length || ( _window.width() < _mb ) || $('.app-aside-dock').length) {
          return;
        }
        var _this = $(e.target),
          top,
          w_h = $(window).height(),
          offset = 50,
          min = 150;

        var b = !_this.is('a') && (_this = _this.closest('a')) ? true : false;
        if (_this.next().is('ul')) {
          next = _this.next();
        } else {
          return;
        }

        _this.parent().addClass('active');
        top = _this.parent().position().top + offset;
        next.css('top', top);
        if (top + next.height() > w_h) {
          next.css('bottom', 0);
        }
        if (top + min > w_h) {
          next.css('bottom', w_h - top - offset).css('top', 'auto');
        }
        next.appendTo(wrap);

        next.on('mouseleave.nav', function (e) {
          $(backdrop).remove();
          next.appendTo(_this.parent());
          next.off('mouseleave.nav').css('top', 'auto').css('bottom', 'auto');
          _this.parent().removeClass('active');
        });

        var c = $('.smart').length && $('<div class="dropdown-backdrop"/>').insertAfter('.app-aside').on('click', function (next) {
          var a = next && next.trigger('mouseleave.nav') ? true : false;
        }) ? true : false;

      });

      wrap.on('mouseleave', function (e) {
        var a = next && next.trigger('mouseleave.nav') ? true : false;
        $('> .nav', wrap).remove();
      });
    }
  };
}

//Scroll
function uiScroll($location, $anchorScroll) {
  return {
    restrict: 'AC',
    link: function (scope, el, attr) {
      el.on('click', function (e) {
        $location.hash(attr.uiScroll);
        $anchorScroll();
      });
    }
  };
}

function uiButterbar($rootScope, $anchorScroll) {
  return {
    restrict: 'AC',
    template: '<span class="bar"></span>',
    link: function (scope, el, attrs) {
      el.addClass('butterbar hide');

      function start(event) {
        $anchorScroll();
        el.removeClass('hide').addClass('active');
      }

      function end(event) {
        event.targetScope.$watch('$viewContentLoaded', function () {
          el.addClass('hide').removeClass('active');
        });
      }

      scope.$on('butterBar:start', start);

      scope.$on('butterBar:end', end);
    }
  };
}

function validForm() {
  return {
    require: '^form',
    restrict: 'A',
    link: function ($scope, $ele, $attrs, $controller) {

      var currentForm = getCurrentForm($scope);
      if (!currentForm || !currentForm.$name) {
        //console.log('cannot found');
      }

      $scope.$watch($controller.$name + ".$valid", function (isValid, lastValue) {
        if (isValid !== undefined) {
          $scope.$emit($controller.$name + ':changed', {
            isValid: isValid,
            element: $ele,
            expression: this.exp,
            scope: $scope,
            ctrl: $controller
          });
        }
      });

      function getCurrentForm(scope) {
        var form = null;
        var requiredFormProps = ["$error", "$name", "$dirty", "$pristine", "$valid", "$invalid", "$addControl", "$removeControl", "$setValidity", "$setDirty"];

        for (var p in scope) {
          if (_.isObject(scope[p]) && !_.isFunction(scope[p]) && !_.isArray(scope[p]) && p.substr(0, 1) !== "$") {
            var props = _.keys(scope[p]);
            if (props.length < requiredFormProps.length) {
              continue;
            }

            var bool = false;
            for (var key in requiredFormProps) {
              bool = _.contains(props, requiredFormProps[key]);
            }

            if (bool) {
              form = scope[p];
              break;
            }
          }
        }
        return form;
      }
    }
  };
}

function pageTitle($rootScope, $timeout) {
  return {
    link: function (scope, element) {
      var listener = function (event, toState, toParams, fromState, fromParams) {
        // Default title - load on Dashboard 1
        var title = 'INSPINIA | Responsive Admin Theme';
        // Create your own title pattern
        if (toState.data && toState.data.pageTitle) {
          title = 'INSPINIA | ' + toState.data.pageTitle;
        }
        $timeout(function () {
          element.text(title);
        });
      };
      $rootScope.$on('$stateChangeStart', listener);
    }
  };
}

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
  return {
    restrict: 'A',
    link: function (scope, element) {
      // Call the metsiMenu plugin and plug it to sidebar navigation
      $timeout(function () {
        element.metisMenu();

      });
    }
  };
}

/**
 * responsibleVideo - Directive for responsive video
 */
function responsiveVideo() {
  return {
    restrict: 'A',
    link: function (scope, element) {
      var figure = element;
      var video = element.children();
      video
        .attr('data-aspectRatio', video.height() / video.width())
        .removeAttr('height')
        .removeAttr('width');

      //We can use $watch on $window.innerWidth also.
      $(window).resize(function () {
        var newWidth = figure.width();
        video
          .width(newWidth)
          .height(newWidth * video.attr('data-aspectRatio'));
      }).resize();
    }
  };
}

/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) {
  return {
    restrict: 'A',
    scope: true,
    templateUrl: 'views/common/ibox_tools.html',
    controller: function ($scope, $element) {
      // Function for collapse ibox
      $scope.showhide = function () {
        var ibox = $element.closest('div.ibox');
        var icon = $element.find('i:first');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        // Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        $timeout(function () {
          ibox.resize();
          ibox.find('[id^=map-]').resize();
        }, 50);
      };
      // Function for close ibox
      $scope.closebox = function () {
        var ibox = $element.closest('div.ibox');
        ibox.remove();
      };
    }
  };
}

/**
 * minimalizaSidebar - Directive for minimalize sidebar
 */
function minimalizaSidebar($timeout) {
  return {
    restrict: 'A',
    template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary btn-sm " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
    controller: function ($scope, $element) {
      $scope.minimalize = function () {
        $("body").toggleClass("mini-navbar");
        if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
          // Hide menu in order to smoothly turn on when maximize menu
          $('#side-menu').hide();
          // For smoothly turn on menu
          setTimeout(
            function () {
              $('#side-menu').fadeIn(500);
            }, 100);
        } else if ($('body').hasClass('fixed-sidebar')) {
          $('#side-menu').hide();
          setTimeout(
            function () {
              $('#side-menu').fadeIn(500);
            }, 300);
        } else {
          // Remove all inline style from jquery fadeIn function to reset menu state
          $('#side-menu').removeAttr('style');
        }
      };
    }
  };
}

/**
 * vectorMap - Directive for Vector map plugin
 */
function vectorMap() {
  return {
    restrict: 'A',
    scope: {
      myMapData: '='
    },
    link: function (scope, element, attrs) {
      element.vectorMap({
        map: 'world_mill_en',
        backgroundColor: "transparent",
        regionStyle: {
          initial: {
            fill: '#e4e4e4',
            "fill-opacity": 0.9,
            stroke: 'none',
            "stroke-width": 0,
            "stroke-opacity": 0
          }
        },
        series: {
          regions: [
            {
              values: scope.myMapData,
              scale: ["#1ab394", "#22d6b1"],
              normalizeFunction: 'polynomial'
            }
          ]
        }
      });
    }
  };
}


/**
 * sparkline - Directive for Sparkline chart
 */
function sparkline() {
  return {
    restrict: 'A',
    scope: {
      sparkData: '=',
      sparkOptions: '='
    },
    link: function (scope, element, attrs) {
      scope.$watch(scope.sparkData, function () {
        render();
      });
      scope.$watch(scope.sparkOptions, function () {
        render();
      });
      var render = function () {
        $(element).sparkline(scope.sparkData, scope.sparkOptions);
      };
    }
  };
}

/**
 * icheck - Directive for custom checkbox icheck
 */
function icheck($timeout) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function ($scope, element, $attrs, ngModel) {
      return $timeout(function () {
        var value;
        value = $attrs['value'];

        $scope.$watch($attrs['ngModel'], function (newValue) {
          $(element).iCheck('update');
        });

        return $(element).iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green'

        }).on('ifChanged', function (event) {
          if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
            $scope.$apply(function () {
              return ngModel.$setViewValue(event.target.checked);
            });
          }
          if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
            return $scope.$apply(function () {
              return ngModel.$setViewValue(value);
            });
          }
        });
      });
    }
  };
}

/**
 * ionRangeSlider - Directive for Ion Range Slider
 */
function ionRangeSlider() {
  return {
    restrict: 'A',
    scope: {
      rangeOptions: '='
    },
    link: function (scope, elem, attrs) {
      elem.ionRangeSlider(scope.rangeOptions);
    }
  };
}

/**
 * dropZone - Directive for Drag and drop zone file upload plugin
 */
function dropZone() {
  return function (scope, element, attrs) {
    element.dropzone({
      url: "/upload",
      maxFilesize: 100,
      paramName: "uploadfile",
      maxThumbnailFilesize: 5,
      init: function () {
        scope.files.push({file: 'added'});
        this.on('success', function (file, json) {
        });
        this.on('addedfile', function (file) {
          scope.$apply(function () {
            alert(file);
            scope.files.push({file: 'added'});
          });
        });
        this.on('drop', function (file) {
          alert('file');
        });
      }
    });
  };
}

// auto resize
function autoResize($timeout) {
  return {
    restrict: 'A',
    link: function ($scope, $element, $attrs) {

      $scope.minimalize = function () {
        // Minimalize menu when screen is less than 768px
        if ($(this).width() < 769) {
          $('body').addClass('body-small');
        } else {
          $('body').removeClass('body-small');
        }
      };

      $timeout($scope.minimalize);

      $(window).bind('load resize', $scope.minimalize);
    }
  };
}

// fix height
function fixHeight($timeout) {
  return {
    restrict: 'A',
    link: function ($scope, $element, $attrs) {
      // Full height of sidebar
      function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");
      }

      $(window).bind("load resize click scroll", function () {
        if (!$("body").hasClass('body-small')) {
          fix_height();
        }
      });

      fix_height();
    }
  };
}