angular.module('directive.edit-input', [])
  .directive('editInput', function($rootScope){
    return {
      restrict: 'AE',
      scope: {
        item: '=',
        event: '&'
      },
      replace: true,
      templateUrl: 'directives/edit-input/edit-input.tpl.html',
      link: function($scope, $element, $attrs){

        $scope.isEdit = false;
        $scope.disabled = false;

        $scope.type = 'dollar';
        $scope.percent = false;
        if(typeof($attrs.percent) !== 'undefined'){
          $scope.type = 'percent';
        }

        if(typeof($attrs.type) !== 'undefined'){
          if($attrs.type !== 'none') {
            $scope.type = 'other';
            $scope.data_type = $attrs.type;
          } else {
            $scope.type = $attrs.type;
          }
        }

        $attrs.$observe('disabled', function(value){
          $scope.disabled = $scope.$eval(value);
        });

        $scope.edit= function(){
          if(!$scope.disabled) {
            $scope.isEdit = true;
          }
        };

        $scope.blur = function(item){
          $scope.isEdit = false;
          $scope.item = item;
          $scope.event();

          $rootScope.$broadcast('input:updated');
        };
      }
    };
  });
