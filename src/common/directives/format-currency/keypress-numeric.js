angular.module('directive.keypress-numeric', [])
  .directive('ngBlur',

  function ($rootScope, utils, $timeout, $filter) {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: true,
      link: function (scope, element, attrs, ngModel) {
        $timeout(function () {
          if (utils.isEmpty(ngModel.$viewValue) || isNaN(ngModel.$viewValue)) {
            ngModel.$viewValue = $filter('currency')(0, '', 2);
            element[0].value = $filter('currency')(0, '', 2);
            setUp();
          } else {
            setUp();
          }
        });

        function setUp() {
          var value = utils.fixed(ngModel.$viewValue);
          if (value === 0) {
            element[0].value = '-';
          } else if (isNaN(value)) {
            element[0].value = $filter('currency')(0, '', 2);
          } else {
            element[0].value = $filter('currency')(ngModel.$viewValue, '', 2);
          }
        }

        element.bind('blur', function (e) {
            setUp();

            if (angular.isDefined(scope.ngBlur)) {
              scope.ngBlur();
            }
          }
        )
        ;
      }
    };
  })

  .directive('ngFocus',
  function (utils) {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: true,
      link: function (scope, element, attrs, ngModel) {
        function setDecimal(input, places) {
          if (isNaN(input)) {
            return 0;
          }
          var factor = "1" + new Array(+(places > 0 && places + 1)).join("0");
          return Math.round(input * factor) / factor;
        }

        element.bind('focus', function (e) {
          element[0].value = ngModel.$viewValue ? setDecimal(ngModel.$viewValue, 2) : '';
        });
      }
    };
  });