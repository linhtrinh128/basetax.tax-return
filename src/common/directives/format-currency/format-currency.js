angular.module('directive.format-currency', [])

    .filter('isEmpty', function () {
        return function (input) {
            if (parseFloat(input) === 0) {
                return '0.00';
            }
            return input;
        };
    })

    //Filter Pattern
    .filter('filterPattern', function () {
        return function (input, pattern, abs) {
            if (input === undefined) {
                return 0;
            }
            if (pattern.indexOf('numbers') != -1) {
                if (abs) {
                    input = input.replace(/[^0-9d.]/g, "");
                } else {
                    input = input.replace(/[^\-\d.]/g, "");
                }
            }
            else if (pattern.indexOf('alphabets') != -1) {
                input = input.replace(/[^a-zA-Z ]/g, "");
            }
            else if (pattern.indexOf('alphaNumeric') != -1) {
                input = input.replace(/[^a-zA-Z\d]/g, "");
            }

            return input;
        };
    })

    //Directive fliter input
    .directive('filteredInput', function ($filter) {
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: true,
            link: function ($scope, $element, $attrs, $controller) {
                $controller.$parsers.unshift(function (val) {
                    var newVal = $filter('filterPattern')(val, $attrs.filteredInput, $attrs.abs !== undefined ? true : false);

                    $element.val(newVal);
                    return newVal;
                });
            }
        };
    })

    //Format Currency
    .directive('formatCurrency', function ($parse, utils, $filter, $timeout) {
        return {
            restrict: 'A', // only activate on element attribute
            require: 'ngModel', // get a hold of NgModelController
            link: function (scope, element, attrs, ngModel) {

                function render() {

                    if (!ngModel.$viewValue || isNaN(ngModel.$viewValue)) {
                        var value = parseFloat(ngModel.$viewValue);

                        if (isNaN(value)) {
                            element[0].value = $filter('currency')(0, '', 2);
                        } else {
                            if (value === 0) {
                                element[0].value = '-';
                            } else {
                                element[0].value = $filter('currency')(ngModel.$viewValue, '', 2);
                            }
                        }
                    }
                }

                // Specify how UI should be updated
                ngModel.$render = render;

                attrs.$observe('ngModel', function (value) { // Got ng-model bind path here
                    scope.$watch(value, function (newValue) { // Watch given path for changes
                        render();
                    });
                });

            }
        };
    })

    .directive('formatPercent', function (utils, $filter) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$render = function () {
                    if (!ngModel.$viewValue || isNaN(ngModel.$viewValue)) {
                        var value = parseFloat(ngModel.$viewValue);

                        if (isNaN(value)) {
                            element[0].value = $filter('currency')(0, '', 2);
                        } else {
                            if (value === 0) {
                                element[0].value = $filter('currency')(0, '', 2);
                            } else {
                                element[0].value = $filter('currency')(utils.percent(ngModel.$viewValue), '', 2);
                            }
                        }
                    }
                };
            }
        };
    });