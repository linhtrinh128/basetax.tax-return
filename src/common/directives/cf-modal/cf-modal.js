angular.module('directive.cf-modal', [])

    .factory('cfModal', function ($uibModal) {
        return {
            open: function (options) {
                return $uibModal.open({
                    size: 'sm',
                    templateUrl: 'directives/cf-modal/cf-modal-single-button.tpl.html',
                    controller: function ($scope, $uibModalInstance) {

                        $scope.content = options.content || 'Modal content';
                        $scope.title = options.title || '';

                        $scope.isClose = options.isClose || false;

                        $scope.actions = {
                            close: function () {
                                $uibModalInstance.dismiss('cancel');
                            },
                            ok: function () {
                                if (options.ok && typeof(options.ok === 'function')) {
                                    options.ok($uibModalInstance);
                                } else {
                                    $uibModalInstance.dismiss('cancel');
                                }
                            }
                        };
                    }
                });
            }
        };
    })

    .directive('cfModal', function ($uibModal, utils) {
        return {
            restrict: 'A',
            scope: {
                item: '=',
                title: '@',
                content: '@',
                accept: '&',
                cancel: '&',
                active: '='
            },
            link: function ($scope, $element, $attrs, $ctrl) {

                $element.bind('click', function () {
                    var modalInstance = $uibModal.open({
                        //size: $attrs.size || 'md',
                        backdrop: true,
                        templateUrl: 'directives/cf-modal/cf-modal.tpl.html',
                        resolve: {
                            title: function () {
                                return $attrs.title || 'Confirm';
                            },
                            content: function () {
                                return $attrs.content || 'Please confirm you actions. Thanks';
                            },
                            $item: function () {
                                return $scope.item;
                            },
                            $accept: function () {
                                return $scope.accept;
                            },
                            $cancel: function () {
                                return $scope.cancel;
                            }
                        },
                        controller: function ($scope, $uibModalInstance, $item, $accept, $cancel, title, content) {
                            $scope.title = title;
                            $scope.content = content;

                            $scope.error = false;

                            $scope.actions = {
                                accept: function () {
                                    $accept($item, $uibModalInstance);
                                    $scope.actions.cancel();
                                },
                                cancel: function () {
                                    $uibModalInstance.dismiss('cancel');
                                }
                            };
                        }
                    });

                    modalInstance.opened.then(function () {
                        $scope.active = true;
                    });

                    modalInstance.result.then(function () {
                        $scope.active = false;
                    }, function (reason) {
                        utils.detectFails(reason, ['backdrop', 'cancel', 'escape'], function () {
                            $scope.active = false;
                        });
                    });
                });
            }
        };
    });