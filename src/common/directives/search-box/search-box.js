angular.module('directive.search-box', [])

.directive('searchBox', function(){
    return {
      restrict: 'AE',
      scope: {
        search: '&',
        cancel: '&',
        keyword: '=ngModel'
      },
      templateUrl: 'directives/search-box/search-box.tpl.html',
      replace: true,
      link: function($scope, $element, $attrs){

        $scope.isCancel = false;
        $scope.isSearch = false;

        if($attrs.search){
          $scope.isSearch = true;
        }

        $scope.actions = {
          cancel: function(){
            $scope.cancel();
          },
          search: function(){
            $scope.search();
          },
          keyPress: function($event){
            var keyCode = $event.which || $event.keyCode;

            switch (keyCode) {
              case 13: // enter
              {
                if ($scope.keyword !== undefined) {
                  $scope.actions.search();
                }
                break;
              }
            }
          }
        };

        $scope.$watch('keyword', function(){
          if($attrs.cancel && $scope.keyword){
            $scope.isCancel = true;
          } else {
            $scope.isCancel = false;
          }
        });
      }
    };
  });