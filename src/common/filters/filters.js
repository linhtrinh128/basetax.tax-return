angular.module('filters', [])

  .filter('percentage', percentage)
  .filter('startFrom', startFrom);

function percentage($filter) {
  return function (input, decimals) {
    return $filter('number')(input * 100, decimals) + '%';
  };
}

function startFrom() {
  return function (input, start) {
    start = +start; //parse to int
    return input.slice(start);
  };
}