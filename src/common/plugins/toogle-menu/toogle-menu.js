angular.module('toogle-menu', [])

.directive('toogleMenu', function($timeout){
    return {
        restrict: 'AE',
        scope: true,
        link: function(scope, element, attrs){
            // main menu visibility toggle
            $('.navbar.main .btn-navbar').click(function(){
                $('.container-fluid:first').toggleClass('menu-hidden');
                $('#menu').toggleClass('hidden-xs');
            });

            $timeout(function(){
                $(window).resize();
            });
        }
    };
});
