angular.module('app.breadcrumb', [
    'modules.transaction.create'
])

.directive('breadcrumb', function () {

    return {
        restrict: 'AE',
        scope: {
            name: '=',
            modalInstance: '=',
            modal: '='
        },
        templateUrl: 'plugins/breadcrumb/breadcrumb.tpl.html',
        link: function (scope, ele, attrs) {

            scope.goParentModal = function () {
                scope.modalInstance.dismiss('cancel');

                scope.modal.open({
                    templateUrl: 'taxmapp/transaction/new/create-transaction.tpl.html',
                    controller: 'TransactionCreationController'
                });
            };
        }
    };
});
