angular.module('directive.inputs', [])

.directive('textBox', function(){
        return {
            restrict: 'AE',
            scope: {
                datasource: '='
            },
            templateUrl: 'plugins/components/inputs/textbox.tpl.html',
            link: function($scope, $element, $attr){
                $scope.label = $attr.label;
                $scope.id = $attr.id;
            }
        };
    }
);