angular.module('input-number', [])

.directive('ngSymbolPound',
    function (utils) {
        return{
            restrict: 'A',
            require: 'ngModel',
            scope: true,
            link: function (scope, element, attrs, ngModel) {
                attrs.$observe('ngModel', function (value) {
                    scope.$watch(value, function () {
                        element[0].value = ngModel.$viewValue.toFixed(2);
                    });
                });
            }
        };
    }
)

.directive("limitTo", [function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(element).on("keydown", function() {
                if (this.value.length == limit) {
                    return false;
                }
            });
        }
    };
}])

.directive('ngBlur',
    function ($parse, utils, $timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: true,
            link: function (scope, element, attrs, ngModel) {

                $timeout(function () {
                    if (!ngModel.$viewValue || isNaN(ngModel.$viewValue)) {
                        ngModel.$viewValue = (0).toFixed(2);
                        element[0].value = (0).toFixed(2);
                    }
                });

                function setUp() {
                    var value = parseFloat(ngModel.$viewValue);
                    if (isNaN(value) || value === 0) {
                        element[0].value = (0).toFixed(2);
                    } else if (value < 0) {
                        element[0].value = '(' + utils.formatAmount(Math.abs(value).toFixed(2)) + ')';
                    } else {
                        element[0].value = '' + utils.formatAmount(Math.abs(value).toFixed(2));
                    }
                }

                element.bind('blur', function (e) {
                    setUp();
                });
            }
        };
    }
)

.directive('ngFocus',
    function ($parse, utils) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: true,
            link: function (scope, element, attrs, ngModel) {

                element.bind('focus', function (e) {
                    element[0].value = ngModel.$viewValue ? parseFloat(ngModel.$viewValue) : '';
                });
            }
        };
    }
)
.filter('filterPattern', ['$window',
    function ($window) {
        return function (input, pattern) {
            if (input === undefined) {
                return '0';
            }
            if (pattern.indexOf('number') !== -1) {
                input = input.replace(/[^\-\d.]/g, '');
            } else if (pattern.indexOf('alphabet') !== -1) {
                input = input.replace(/[^a-zA-Z]/g, '');
            } else if (pattern.indexOf('alphaNumeric') !== -1) {
                input = input.replace(/[^a-zA-Z\d]/g, '');
            } else if (pattern.indexOf('percentage') !== -1) {
                input = input.replace(/[^\-\d.]/g, '');
                if(!$window.isNaN(input)) {
                    if(input.charAt(input.length - 1) === '.') {
                        return input;
                    }
                    var num = parseFloat(input);
                    if (num < 0) {
                        return 0;
                    }
                    if (num > 100) {
                        return 100;
                    }
                    return Math.round(num * 100)/100;
                }
            } else if (pattern.indexOf('money') !== -1) {
                input = input.replace(/[^\-\d.]/g, '');
                if(!$window.isNaN(input)) {
                    if(input.charAt(input.length - 1) === '.') {
                        return input;
                    }
                    var num2 = parseFloat(input);
                    if (num2 < 0) {
                        return 0;
                    }
                    if (num2 > 999999) {
                        return 999999;
                    }
                    return Math.round(num2 * 100)/100;
                }
            } else if (pattern.indexOf('sort-code') !== -1) {
                input = input.replace(/[^\-\d]/g, '');
                if (input.length == 2) { return input + '-';}
                if (input.length == 5) { return input + '-';}
            } else if (pattern.indexOf('telephone') !== -1) {
                input = input.replace(/[^\-\d+() ]/g, '');
            }

            return input;
        };
    }]
)

.directive('filteredInput', function ($filter) {
    return{
        restrict: 'A',
        require: 'ngModel',
        scope: true,
        link: function (scope, element, attrs, controller) {

            controller.$parsers.unshift(function (val) {
                var newVal = $filter('filterPattern')(val, attrs.filteredInput);
                element[0].value = newVal;

                return newVal;
            });
        }
    };
})

.filter('formatPattern2',
    function(){
        return function(input, alphabet, digit){
            if(input === undefined){
                return '';
            }

            var array = input.split('');
            var char = array[array.length-1];

            var index = array.indexOf(char);
            if(index !== -1 && index >= 1 && index <= 2){

            }

        };
    }
)
.directive('formatNationalInsurance', function($filter){
    return{
        restrict: 'A',
        require: 'ngModel',
        scope: true,
        link: function(scope, element, attrs, controller){
            controller.$parsers.unshift(function(val){
                var newVal = $filter('formatPattern2')(val, attrs.alphabet, attrs.digit);
                element[0].value = newVal;

                return newVal;
            });
        }
    };
})
.directive("numberFormat", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attr, ngModelCtrl) {
                var numberParse = function (value) {
                    var numbers = value && value.replace(/-/g, "");
                    if (/^\d{10}$/.test(numbers)) {
                        return numbers;
                    }

                    return undefined;
                };
                var numberFormat = function (value) {
                    var numbers = value && value.replace(/-/g,"");
                    var matches = numbers && numbers.match(/^(\d{3})(\d{3})(\d{4})$/);

                    if (matches) {
                        return matches[1] + "/" + matches[2]  + matches[3];
                    }

                    return undefined;
                };
                ngModelCtrl.$parsers.push(numberParse);
                ngModelCtrl.$formatters.push(numberFormat);

                element.bind("blur", function () {
                    var value = numberFormat(element.val());
                    var isValid = !!value;
                    if (isValid) {
                        ngModelCtrl.$setViewValue(value);
                        ngModelCtrl.$render();
                    }

                    ngModelCtrl.$setValidity("required", isValid);
                    scope.$apply();
                });
            }
        };
    })


.directive('numbersOnly', function(){
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue === undefined) {
                        return '';
                    }
                    var transformedInput = inputValue.replace(/[^0-9]/g, '');
                    if (transformedInput!=inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    return transformedInput;
                });
            }
        };
    })
.directive('datePickerDefault',
    function(){
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: true,
            link: function (scope, element, attrs, ngModel) {

                function checkDate(dtString, key) {
                    if (!key) {
                        key = '-';
                    }
                    var date = dtString.split(' ');

                    var format = date[0].split(key);

                    if (format.length === 3 && isNaN(Date.parse(date))) {
                        return true;
                    }
                    return false;
                }

                if(isNaN(ngModel.$viewValue)){
                    element[0].value = '';
                }
            }
        };
    }
);
