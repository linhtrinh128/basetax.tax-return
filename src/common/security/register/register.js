angular.module('security.register', [
  'services.localizedMessages'
])

  .config(function ($stateProvider) {

    $stateProvider
      .state('registerForm', {
        parent: 'blank',
        url: '/register',
        views: {
          '': {
            templateUrl: 'security/register/register.tpl.html',
            controller: 'RegisterController'
          }
        },
        resolve: {
          getUserTypes: function (UserType) {
            if (UserType.list == null) {
              return UserType.all();
            }
            return UserType.list;
          }
        }
      });

  })

  .controller('RegisterController',
    function ($scope, $state, security, localizedMessages, UserType, messages) {

      $scope.user = {
        email: '',
        password: '',
        user_type_id: ''
      };

      $scope.authError = null;

      $scope.opened = false;

      $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
      };

      $scope.$watch('user', function () {
        //console.log($scope.user.gender);
      }, true);

      $scope.userTypes = angular.copy(UserType.list);
      $scope.userType = $scope.userTypes[0];

      $scope.action = {
        checkFormValid: function (list) {
          for (var i in list) {
            if (list[i].$invalid) {
              return true;
            }
          }

          return false;
        },

        register: function () {
          $scope.user.user_type_id = $scope.userType.id;

          security.register($scope.user).then(
            function success(loggedIn) {
              if (!loggedIn) {
                $scope.authError = localizedMessages.get('crud.register.error');
              } else {
                $state.go('init');
              }
            }, function error(error) {
              //$scope.authError = localizedMessages.get('login.error.serverError', {exception: x});

              switch (error.code) {
                case 400:
                  $scope.errors = error.error;
                  $scope.saveReason = localizedMessages.get('crud.validation.error');
                  var options = {
                    template: 'message-errors',
                    title: $scope.saveReason,
                    content: $scope.errors
                  };
                  messages.show(options);
                  break;

                default:
                  break;
              }
            });
        }
      };
    }
  );