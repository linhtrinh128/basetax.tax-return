angular.module('security.forgot-password', [])

    .config(function ($stateProvider) {

        $stateProvider
            .state('forgotForm', {
                parent: 'blank',
                url: '/forgot-password',
                views: {
                    '': {
                        templateUrl: 'security/forgot-password/forgot-password.tpl.html',
                        controller: 'ForgotPasswordController'
                    }
                }
            });

    })

    .controller('ForgotPasswordController',
        function ($scope, $state, security, localizedMessages, Toaster, messages) {

            $scope.email = '';
            $scope.isError = false;
            $scope.isCollapsed = true;

            $scope.actions = {
                reset: function () {
                    $scope.isError = false;
                    security.forgotPassword({
                        email: $scope.email
                    }).then(function success(res) {

                        $scope.isCollapsed = !$scope.isCollapsed;

                    }, function error(err) {
                        switch (err.code) {
                            case 404:
                            {
                                $scope.isError = !$scope.isError;
                                messages.show({
                                    title: 'Forgot password',
                                    content: 'This email is not existing in system.'
                                });
                                $scope.authError = null;
                                $scope.authReason = null;
                                break;
                            }
                            case 400:
                            {
                                $scope.isError = !$scope.isError;
                                messages.show({
                                    title: 'Forgot password',
                                    content: err.error
                                });
                                $scope.forgotForm.email.$invalid = false;
                                $scope.authError = null;
                                $scope.authReason = null;
                                break;
                            }
                        }
                    });
                }
            };
        }
    );
