angular.module('security.change-password', [])

    .controller('ChangePasswordController',
        function ($scope, $uibModalInstance, security, localizedMessages, Toaster, messages) {

            $scope.password = '';
            $scope.new_password = '';
            $scope.confirm_password = '';

            $scope.actions = {
                save: function () {
                    security.changePassword({
                        password: $scope.password,
                        new_password: $scope.new_password,
                        confirm_password: $scope.confirm_password
                    }).then(function success(response) {
                        $uibModalInstance.dismiss('cancel');
                    }, function error(err) {
                        switch (err.code) {
                            case 404:
                            {
                                messages.show({
                                    title: 'Forgot password',
                                    content: 'This email is not existing in system.'
                                });
                                break;
                            }
                            case 400:
                            {
                                $scope.isError = !$scope.isError;
                                messages.show({
                                    title: 'Forgot password',
                                    content: err.error,
                                    template: 'message-errors'
                                });
                                break;
                            }
                        }
                    });
                },

                cancel: function () {
                    $uibModalInstance.dismiss('cancel');
                }
            };
        }
    );
