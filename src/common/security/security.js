angular.module('security.service', [
    'security.retryQueue',
    'security.login',
    'security.register',
    'security.forgot-password',
    'security.reset-password',
    'security.change-password'
])

    .factory('security',
        function ($rootScope, $cookieStore, $q, $location, securityRetryQueue, apiService, Restangular,$http) {

            // Redirect to the given url (defaults to '/')
            function redirect(url) {
                url = url || '/';
                $location.path(url);
            }

            var register = function (object) {
                var deferred = $q.defer();

                var request = apiService.post('register', object);

                request.then(function success(res) {
                    if (res.data !== null) {
                        res = res.data;

                        if (res.success) {
                            var authToken = res.data.token;
                            $cookieStore.put('authToken', authToken);
                            apiService.setAuthTokenHeader(authToken);

                            service.currentUser = res.data.user;

                            deferred.resolve(service.currentUser);
                        } else {
                            deferred.resolve(false);
                        }
                    } else {
                        deferred.resolve(false);
                    }
                }, function error(error) {
                    deferred.reject(error.data);
                });

                return deferred.promise;
            };

            // Fn is tax return
            var isTaxReturn = function () {

                // user type id is 2 or 5 (Partnership or Individual)
                var isTaxReturn = (service.currentUser.user_type_id == '2' || service.currentUser.user_type_id == '5' );

                return (isTaxReturn);
            };

            // Register handler for when an item is added to the retry queue
            securityRetryQueue.onItemAddedCallbacks.push(function (retryItem) {
                if (securityRetryQueue.hasMore()) {
                    redirect('/login');
                }
            });

            // Require authorized
            $rootScope.$on('response:authorized:error', function (event, rejection) {
                if (service.currentUser) {
                    service.currentUser = undefined;
                }
            });

            // The public API of the service
            var service = {

                // Holds the object of the current logged in user
                currentUser: undefined,
                //role: ROLES.OWNER,

                // Get the first reason for needing a login
                getLoginReason: function () {
                    return securityRetryQueue.retryReason();
                },

                // Give up trying to login and clear the retry queue
                cancelLogin: function () {
                    redirect();
                },

                // register
                register: function (object) {
                    return register(object);
                },

                // Attempt to authenticate a user by the given username and password
                login: function (logIn) {

                    var deferred = $q.defer();

                    // Make request to API

                    var baseSecurity = Restangular.all('login');
                    var request = baseSecurity.post(logIn);

                    // var request = apiService.post('login', {
                    //     email: logIn.email,
                    //     password: logIn.password,
                    //     is_remember: logIn.is_remember
                    // });

                    // When server response is received...
                    request.then(function (res) { //response
                        if (res.data !== null) {
                            res = res.data;

                            if (res.success) {

                                var authToken = res.data.token;

                                // Set the authentication token in a cookie
                                $cookieStore.put('authToken', authToken);

                                // Set the Request Header 'Authorization'
                                apiService.setAuthTokenHeader(authToken);

                                // If successful, set current user object
                                service.currentUser = res.data.user;

                                deferred.resolve(service.currentUser);
                            } else {
                                deferred.resolve(false);
                            }
                        } else {
                            deferred.resolve(false);
                        }
                    }, function error(error) {
                        deferred.reject(error.data);
                    });

                    return deferred.promise;
                },

                // update user
                update: function (id, params) {
                    var deferred = $q.defer();

                    // Make request to API
                    apiService.customPUT('user', id, params).then(function (res) {

                        if (res.object !== null) {
                            res = res.object;

                            if (res.success) {
                                service.currentUser = res.data;

                                $rootScope.$broadcast('profile:updated');

                                deferred.resolve(service.currentUser);
                            } else {
                                deferred.resolve(false);
                            }
                        } else {
                            deferred.resolve(false);
                        }
                    }, function (err) {
                        deferred.resolve(err);
                    });

                    return deferred.promise;
                },

                changePassword: function (params) {
                    var deferred = $q.defer();

                    apiService.post('change-password', params).then(function success(response) {
                        deferred.resolve(response);
                    }, function error(error) {
                        deferred.reject(error.data);
                    });

                    return deferred.promise;
                },

                forgotPassword: function (params) {
                    var deferred = $q.defer();

                    apiService.post('forgot-password', params).then(function success(response) {
                        deferred.resolve(response);
                    }, function error(error) {
                        deferred.reject(error.data);
                    });

                    return deferred.promise;
                },

                confirmResetPassword: function (params) {
                    var deferred = $q.defer();

                    apiService.post('confirm-reset-password', params).then(function success(response) {
                        deferred.resolve(response);
                    }, function error(error) {
                        deferred.reject(error.data);
                    });

                    return deferred.promise;
                },

                resetPassword: function (param) {
                    var deferred = $q.defer();

                    apiService.post('reset-password', param).then(function success(response) {
                        deferred.resolve(response);
                    }, function error(error) {
                        deferred.reject(error.data);
                    });

                    return deferred.promise;
                },

                /**
                 * logout
                 *
                 * @param redirectTo
                 * @returns {deferred.promise|{then, catch, finally}}
                 */
                logout: function (redirectTo) {

                    var deferred = $q.defer();

                    if (arguments.length === 0) {
                        redirectTo = '/login';
                    }

                    var request = apiService.post('logout');

                    request.then(function success(res) {
                        var response = res.data;

                        // Remove authToken and currentUser cookie
                        $cookieStore.remove('authToken');

                        // Reset current user object
                        service.currentUser = null;

                        // Redirect to supplied route
                        redirect(redirectTo);

                        //deferred.resolve(response.data);

                    }, function error(error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },

                // Ask the backend to see if a user is already authenticated - this may be from a previous session.
                requestCurrentUser: function () {
                    // Otherwise check if a session exists on the server
                    var deferred = $q.defer();
                    var authToken;

                    if ($cookieStore.get('authToken') || typeof $cookieStore.get('authToken') !== 'undefined' ||
                        $cookieStore.get('authToken2') || $cookieStore.get('authToken3')) {
                        authToken = $cookieStore.get('authToken');
                        if (!authToken) {
                            authToken = $cookieStore.get('authToken2');
                            if (!authToken) {
                                authToken = $cookieStore.get('authToken3');
                            }
                        }
                    }

                    if (authToken !== undefined) {
                        apiService.setAuthTokenHeader(authToken);

                        if (!service.isAuthenticated()) {
                            apiService.find('me').get().then(function (res) {

                                res = res.data;
                                if (res.success) {
                                    // If successful, set current user object
                                    service.currentUser = res.data;
                                    //service.role = service.currentUser.permission.role;

                                    deferred.resolve(service.currentUser);
                                } else {
                                    deferred.resolve(false);
                                }
                            }, function () {
                                deferred.resolve(false);
                            });
                        } else {
                            deferred.resolve(service.currentUser);
                        }
                    } else {
                        deferred.resolve(false);
                    }
                    return deferred.promise;
                },

                // Is the current user authenticated?
                isAuthenticated: function () {
                    return !!service.currentUser;
                },

                // Is tax return user
                isTaxReturnUser: function () {
                    return isTaxReturn();
                }
            };

            return service;

        });
