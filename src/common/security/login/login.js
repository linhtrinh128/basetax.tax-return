angular.module('security.login', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('loginForm', {
        parent: 'blank',
        url: '/login',
        views: {
          '': {
            templateUrl: 'security/login/login.tpl.html',
            controller: 'LoginController'
          }
        }
      });
  })

  .controller('LoginController',
    function ($scope, $state, $cookieStore, security, localizedMessages, cfModal) {

      $scope.user = {
        email: '',
        password: '',
        is_remember: false
      };

      $scope.isError = {
        email: false,
        password: false
      };

      $scope.$on('loggedIn', function (event, loggedIn) {
        if (loggedIn) {
          $state.go('init');
        }
      });

      $scope.authError = null;

      if (security.getLoginReason()) {
        if (security.isAuthenticated()) {
          $scope.authReason = localizedMessages.get('login.reason.notAuthorized');
        }
        else if ($cookieStore.get('authToken')) {
          $scope.authReason = localizedMessages.get('login.reason.sessionExpired');
        }
        else {
          $scope.authReason = localizedMessages.get('login.reason.notAuthenticated');
        }
      }

      $scope.action = {

        login: function () {
          $scope.authError = null;

          $scope.isError = {
            email: false,
            password: false
          };

          security.login($scope.user).then(function success(loggedIn) {
            if (!loggedIn) {
              $scope.authError = localizedMessages.get('login.error.invalidCredentials');
              $scope.authReason = null;
            } else {
              $state.go('init');
            }
          }, function error(error) {

            $scope.authError = null;
            $scope.authReason = null;

            switch (error.code) {
              case 302:
              {
                $scope.authReason = localizedMessages.get('login.error.userCanceled');
                cfModal.open({
                  content: $scope.authReason
                });
                break;
              }
              case 400:
              {
                if (error.status_code == 'NOT_ADMIN') {
                  $scope.authReason = localizedMessages.get('login.error.userNotAdministrator');
                } else {
                  if (error.error.email !== undefined) {
                    $scope.authReason = error.error.email[0];
                    $scope.isError.email = !$scope.isError.email;
                  } else if (error.error.password !== undefined) {
                    $scope.authReason = error.error.password[0];
                    $scope.isError.password = true;
                  } else {
                    $scope.authReason = localizedMessages.get('login.error.invalidCredentials');
                  }
                }
                cfModal.open({
                  content: $scope.authReason
                });
                break;
              }
              case 404:
              {
                $scope.authError = localizedMessages.get('login.error.userExisting');
                $scope.isError.email = !$scope.isError.email;
                cfModal.open({
                  content: $scope.authError
                });
                break;
              }
              default:
              {
                $scope.authError = localizedMessages.get('login.error.serverError', {exception: error.error});
                cfModal.open({
                  content: $scope.authError
                });
              }
            }
          });
        },

        checkError: function (item) {
          if (item === 'email' && $scope.isError.email === true) {
            $scope.isError.email = !$scope.isError.email;
          } else if (item === 'password' && $scope.isError.password === true) {
            $scope.isError.password = !$scope.isError.password;
          }
        }
      };
    }
  );
