angular.module('security.reset-password', [])
    .config(function ($stateProvider) {
        $stateProvider
            .state('resetPasswordForm', {
                parent: 'blank',
                url: '/reset-password/{token}',
                views: {
                    '': {
                        templateUrl: 'security/reset-password/reset-password.tpl.html',
                        controller: 'ResetPasswordController'
                    }
                },
                resolve: {
                    init: function ($state, $stateParams, security, cfModal, utils) {
                        return security.confirmResetPassword({token: $stateParams.token})
                            .then(function success(res) {
                                return true;
                            }, function error(error) {
                                switch (error.code) {
                                    case 400:
                                    {
                                        cfModal.open({
                                            content: "Your request is invalid"
                                        }).result.then(function () {
                                            $state.go('forgotForm');
                                        }, function (reason) {
                                            utils.detectFails(reason, ['backdrop', 'cancel'], function () {
                                                $state.go('forgotForm');
                                            });
                                        });
                                        break;
                                    }
                                    case 404:
                                    {
                                        cfModal.open({
                                            content: "Cannot found your request. Please try again."
                                        }).result.then(function () {
                                            $state.go('forgotForm');
                                        }, function (reason) {
                                            utils.detectFails(reason, ['backdrop', 'cancel'], function () {
                                                $state.go('forgotForm');
                                            });
                                        });
                                        break;
                                    }
                                }

                            });
                    }
                }
            });
    })

    .controller('ResetPasswordController',
        function ($scope, $state, security, localizedMessages, cfModal, $stateParams) {

            $scope.error = {
                new_password: false,
                confirm_password: false
            };

            $scope.user = {
                token: $stateParams.token,
                new_password: '',
                confirm_password: ''
            };

            $scope.authError = null;
            $scope.authReason = null;

            $scope.action = {
                // reset password
                reset: function () {
                    security.resetPassword($scope.user).then(function success(res) {

                        $scope.error = {
                            new_password: false,
                            confirm_password: false
                        };

                        cfModal.open({
                            content: 'Your password has been updated.'
                        }).result.then(function () {
                            $state.go('loginForm');
                        });

                    }, function error(error) {
                        switch (error.code) {
                            case 400:
                            {
                                var content = '';
                                if (error.error.new_password !== undefined) {
                                    content = error.error.new_password[0];
                                    $scope.error.new_password = true;
                                } else if (error.error.confirm_password !== undefined) {
                                    content = error.error.confirm_password[0];
                                    $scope.error.confirm_password = true;
                                }

                                cfModal.open({
                                    content: content
                                });

                                break;
                            }
                            case 404:
                            {
                                cfModal.open({
                                    content: 'Your password had reset.'
                                });
                                $scope.authError = error.error;
                                $scope.authReason = null;
                                break;
                            }
                        }
                    });
                },

                checkError: function (item) {
                    if (item === 'new_password' && $scope.error.new_password === true) {
                        $scope.error.new_password = !$scope.error.new_password;
                    } else if (item === 'confirm_password' && $scope.error.confirm_password === true) {
                        $scope.error.confirm_password = !$scope.error.confirm_password;
                    }
                }
            };
        }
    );