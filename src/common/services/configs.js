/*
	modules app.configs is configuration values of app
*/

angular.module('services.configs', [])

.factory('configs', function($location) {

    var service = {
		loginUrl: ($location.host() === 'web.beesightsoft.com' || $location.host() === 'localhost') ?
            "http://web.beesightsoft.com/taxmapp/trunk/login/" :
            "https://www.basetax.co.uk/login/"
    };

    return service;
});
