angular.module('services.payments.stripe', [])


	.controller('PaymentStripeController',
		function ($scope, $rootScope, $http, $filter, stripe, security, apiService, $uibModalInstance,
				  param) {

			/**
			 * Load resources
			 */
			$scope.user = security.currentUser;


			/**
			 * Scope payment mask
			 */
			$scope.payment = {
				name: $scope.user.name,
				email: $scope.user.email,
				telephone: $scope.user.telephone ? $scope.user.telephone : '',
				tax_year: parseInt(param.taxYear.start_year) + 1,
				tax_year_id: param.taxYear.id,
				plan: param.plan, // plan A, plan B, plan C
				amount: param.amount, // amount base on plan & type
				type_id: param.typeId, // type of tax return
				card: {
					number: '',
					cvc: '',
					exp_month: '',
					exp_year: '',
					expiration: ''
				}
			};
			$scope.isSubmit = false;

			/**
			 * Expiration
			 */
			$scope.$watch('payment.card.expiration', function (newValue) {
				if (newValue) {
					changeExpiry(newValue);
				}
			}, true);


			/**
			 * Charge stripe
			 *
			 * @returns {*}
			 */
			$scope.charge = function () {
				$scope.isSubmit = true;

				return stripe.card.createToken($scope.payment.card)
					// Promise create Token
					.then(function (response) {
						console.log('token created for card ending in ', response.card.last4);

						var payment = angular.copy($scope.payment);
						payment.card = void 0;
						payment.token = response.id;
						//return $http.post('http://localhost/taxmapp-api/public/v2/payment', payment);
						return apiService.post('payment', payment);
					})
					// Promise payment
					.then(function successPayment(payment) {
						$uibModalInstance.close(payment.data);
					})
					.finally(function () {
						$scope.isSubmit = false;
					})
					// Catch exception
					.catch(function (err) {
						if (err.type && /^Stripe/.test(err.type)) {
							console.log('Stripe error: ', err.message);
						}
						else {
							console.log('Other error occurred, possibly with your API', err.message);
						}
					});
			};


			/**
			 * CHANGE EXPIRY
			 *
			 * @param value
			 */
			function changeExpiry(value) {
				if (value.length < 6) {
					return value;
				} else {
					value = parseInt(value);
					$scope.payment.card.exp_month = Math.floor(value / 10000);
					$scope.payment.card.exp_year = value % 10000;
				}
			}
		});
