angular.module('services.payments', [
  'services.payments.stripe'
])

  .factory('payments', function ($rootScope, $uibModal, $q, apiService, TaxYear) {

    var service = {

      /**
       * Show payment for tax return
       *
       * @param payment_type
       */
      showPaymentTaxReturn: function (param) {
        var modalInstance = $uibModal.open({
          templateUrl: 'services/payment/stripe/payment-stripe.tpl.html',
          controller: 'PaymentStripeController',
          resolve: {
            param: function () {
              return param;
            }
          }
        });
        return modalInstance;
      },


      /**
       * Check payment tax return
       *
       * @param tax_return_id
       */
      checkPaymentTaxReturn: function (tax_return_id, type_id) {
        var deferred = $q.defer();
        apiService.get('payment/' + tax_return_id + '/' + type_id).then(function (res) {
          deferred.resolve(res.data);
        }, function error(err) {
          deferred.reject(err);
        });
        return deferred.promise;
      }
    };

    return service;
  });
