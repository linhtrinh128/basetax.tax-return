angular.module('services.exchange', [])

    .factory('exchange',
        function (apiService, utils, $rootScope, $q, $http) {

            var service = {

                /**
                 * get rate exchange from OpenExchangeRate.org
                 */
                getRates: function(){
                    var deferred = $q.defer();

                    $http.get('https://api.fixer.io/latest?base=GBP').then(function (res) {
                        if(res.data) {
                            fx.base = res.data.base;
                            fx.date = new Date();
                            fx.rates = res.data.rates;
                        }

                        fx.settings = {
                            from : "GBP",
                            to : "USD"
                        };

                    }, function error(error) {
                        deferred.reject(error.data);
                    });

                    return deferred.promise;
                }
            };

            return service;
        }
    );
