angular.module('services.calculator', [])

    .factory('calculator',
        function (apiService, utils, $rootScope, $q, $http, CONFIGS, types, recordTypeGroups, formData, forms,
                  transactions, TaxYear) {

            var service = {

                total: {
                    business: {
                        income: 0,
                        expense: 0,
                        sub_income: [],
                        sub_expense: [],
                        netProfit: 0,
                        standardAllowance: 0,
                        disAllowableExpenses: 0,
                        cisDeduction: 0
                    },
                    employment: {
                        salary: 0,
                        benefitExpenses: 0,
                        total: 0
                    },
                    property: {
                        rentalIncome: 0,
                        rentalExpenses: 0,
                        total: 0
                    },
                    income: 0,
                    estimatedTax: 0,
                    totalTax: 0,
                    C4NIC: 0,
                    taxDeducted: 0
                },


                /**
                 * Calculate tax return
                 *
                 * @param params
                 * @param year
                 * @returns {deferred.promise|{then, catch, finally}}
                 */
                calculateTaxReturn: function (params, year) {
                    return calculateTaxReturn(params, year);
                },

                /**
                 * Calculate Total Amount of transactions
                 *
                 * @param trans
                 */
                calculateTotalAmount: function (trans) {
                    return calculateTotalAmount(trans);
                },

                /**
                 * Calculate total income of transactions
                 *
                 * @param transactions
                 */
                calculateTotalBusinessIncome: function (transactions) {
                    return calculateTotalBusinessIncome(transactions);
                },

                /**
                 * Calculate total Expense of transactions
                 *
                 * @param transactions
                 */
                calculateTotalBusinessExpense: function (transactions) {
                    return calculateTotalBusinessExpense(transactions);
                },

                /**
                 * Calculate for sub expense.
                 * @param item
                 * @param list
                 */
                addRecordType: function (item, list) {
                    return addRecordType(item, list);
                },

                /**
                 * Add amount to subcategory
                 * @param item
                 * @param recordType
                 */
                addSubcategory: function (item, recordType) {
                    return addSubcategory(item, recordType);
                },

                /**
                 * Calculate Standard Allowance
                 *
                 * @param transactions
                 */
                calculateTotalBusinessStandardAllowance: function (transactions) {
                    return calculateTotalBusinessStandardAllowance(transactions);
                },

                /**
                 * Calculate Disallowable Expense
                 *
                 */
                calculateTotalBusinessDisAllowableExpense: function () {
                    return calculateTotalBusinessDisAllowableExpense();
                },

                // calculate business cisDeduction
                calculateTotalBusinessCisDeduction: function (transactions) {
                    return calculateTotalBusinessCisDeduction(transactions);
                },

                /**
                 * Calculate Business Net Profit
                 */
                calculateBusinessNetProfit: function () {
                    return calculateBusinessNetProfit();
                },

                /**
                 * Calculate benefit and expense
                 *
                 * @param transactions_emp
                 */
                calculateStudentLoan: function (transactions_emp) {
                    return calculateStudentLoan(transactions_emp);
                },

                /**
                 * Calculate benefit and expense
                 *
                 * @param transactions_emp
                 */
                calculateBenefitAndExpenses: function (transactions_emp) {
                    return calculateBenefitAndExpenses(transactions_emp);
                },

                /**
                 * Calculate Salary
                 *
                 * @param transactions_emp
                 */
                calculateSalary: function (transactions_emp) {
                    return calculateSalary(transactions_emp);
                },

                /**
                 * Calculate Rental Income of Transactions Property
                 *
                 * @param transactions_pro
                 */
                calculateRentalIncome: function (transactions_pro) {
                    return calculateRentalIncome(transactions_pro);
                },

                /**
                 * Calculate Rental Expense
                 *
                 * @param transactions_pro
                 */
                calculateRentalExpense: function (transactions_pro) {
                    return calculateRentalExpense(transactions_pro);
                },

                /**
                 * Calculate Total Income
                 */
                calculateToTalIncome: function () {
                    return calculateToTalIncome();
                },

                /**
                 * calculate tax deducted
                 */
                calculateTaxDeducted: function (transactions_emp) {
                    return calculateTaxDeducted(transactions_emp);
                },

                /**
                 * Calculate Quick Estimated Tax
                 *
                 */
                calculateQuickEstimatedTax: function () {
                    return calculateQuickEstimatedTax();
                },

                /**
                 * calculate amount for work at home
                 *
                 * @param hours
                 * @returns {number}
                 */
                calculateAmountWorkAtHome: function (hours) {
                    return calculateAmountWorkAtHome(hours);
                },


                /**
                 * Calculate accumulated mileages
                 *
                 */
                calculateAccumulatedMileage: function () {
                    return calculateAccumulatedMileage();
                },


                /**
                 * Calculate Claim allowance Mileage in current year
                 *
                 * @param mileage
                 * @param vehicle
                 * @returns {number}
                 */
                calculateClaimAllowance: function (mileage, vehicle) {
                    return calculateClaimAllowance(mileage, vehicle);
                }
            };


            /**
             * Calculate tax return
             *
             * @param params
             * @param year
             * @returns {deferred.promise|{then, catch, finally}}
             */
            var calculateTaxReturn = function (params, year) {
                    var resource = 'tax-calculation';
                    var deferred = $q.defer();

                    var url = CONFIGS.baseURL() + resource + '/' + year;

                    $http.post(url, params).then(function (res) {
                        deferred.resolve(res.data);
                    }, function error(error) {
                        deferred.reject(error.data);
                    });


                    return deferred.promise;
                },

                /**
                 * Calculate Total Amount of transactions
                 *
                 * @param trans
                 */
                calculateTotalAmount = function (trans) {
                    var total = 0;
                    for (var index in trans) {
                        if (trans[index].amount && !isNaN(parseFloat(trans[index].amount))) {
                            total += parseFloat(trans[index].amount);
                        }
                    }
                    return total;
                },

                /**
                 * Calculate total income of transactions
                 *
                 * @param transactions
                 */
                calculateTotalBusinessIncome = function (transactions) {
                    service.total.business.sub_income = [];

                    recordTypeGroups.find(types.income).then(function (res) {
                        var recordTypes = res.record_types;
                        var trans = [];

                        for (var i in transactions) {
                            for (var j in recordTypes) {
                                if (transactions[i].record_type_id === recordTypes[j].id) {
                                    trans.push(transactions[i]);

                                    // find subcategory
                                    var subcategory = null;
                                    for (var k in recordTypes[j].subcategories) {
                                        if (recordTypes[j].subcategories[k].id === transactions[i].sub_category_id) {
                                            subcategory = recordTypes[j].subcategories[k];
                                        }
                                    }

                                    service.addRecordType({
                                        id: recordTypes[j].id,
                                        description: recordTypes[j].description,
                                        value: transactions[i].amount,
                                        subcategory: subcategory
                                    }, service.total.business.sub_income);
                                }
                            }
                        }

                        service.total.business.income = service.calculateTotalAmount(trans);
                    });
                },

                /**
                 * Calculate total Expense of transactions
                 *
                 * @param transactions
                 */
                calculateTotalBusinessExpense = function (transactions) {
                    service.total.business.sub_expense = [];

                    recordTypeGroups.find(types.expense).then(function (res) {
                        var recordTypes = res.record_types;

                        var trans = [];
                        for (var i in transactions) {
                            for (var j in recordTypes) {
                                if (parseInt(transactions[i].record_type_id) === parseInt(recordTypes[j].id)) {
                                    trans.push(transactions[i]);

                                    // find subcategory
                                    var subcategory = null;
                                    for (var k in recordTypes[j].subcategories) {
                                        if (recordTypes[j].subcategories[k].id === transactions[i].sub_category_id) {
                                            subcategory = recordTypes[j].subcategories[k];
                                        }
                                    }

                                    // add to record type
                                    service.addRecordType({
                                        id: recordTypes[j].id,
                                        description: recordTypes[j].description,
                                        value: transactions[i].amount,
                                        subcategory: subcategory
                                    }, service.total.business.sub_expense);
                                }
                            }
                        }

                        // Make list flat
                        var subExpenses = [];
                        for (var m in service.total.business.sub_expense) {
                            var subExpense = service.total.business.sub_expense[m];
                            subExpenses.push({
                                description: subExpense.description,
                                value: subExpense.value
                            });
                            for (var n in subExpense.subcategories) {
                                subExpenses.push({
                                    description: subExpense.subcategories[n].description,
                                    value: subExpense.subcategories[n].value,
                                    is_subcategory: true
                                });
                            }
                        }
                        service.total.business.sub_expense = subExpenses;

                        service.total.business.expense = service.calculateTotalAmount(trans);
                    });
                },

                /**
                 * Calculate for sub expense.
                 * @param item
                 * @param list
                 */
                addRecordType = function (item, list) {
                    if (parseFloat(item.value) === 0) {
                        return;
                    }

                    for (var i = 0; i < list.length; i++) {
                        if (item.id === list[i].id) {
                            list[i].value += parseFloat(item.value);

                            // has subcategory
                            if (item.subcategory) {
                                service.addSubcategory(item, list[i]);
                            }
                            return;
                        }
                    }

                    list.push({
                        id: item.id,
                        description: item.description,
                        value: parseFloat(item.value)
                    });

                    // has subcategory
                    if (item.subcategory) {
                        service.addSubcategory(item, list[i]);
                    }
                },

                /**
                 * Add amount to subcategory
                 * @param item
                 * @param recordType
                 */
                addSubcategory = function (item, recordType) {
                    if (typeof recordType.subcategories === 'undefined') {
                        recordType.subcategories = [];
                    }
                    // fetch subcategories
                    for (var j in recordType.subcategories) {
                        var subcategory = recordType.subcategories[j];
                        if (item.subcategory.id === subcategory.id) {
                            subcategory.value += item.value;
                            return;
                        }
                    }
                    // if not found
                    recordType.subcategories.push({
                        id: item.subcategory.id,
                        description: item.subcategory.name,
                        value: item.value
                    });
                },

                /**
                 * Calculate Standard Allowance
                 *
                 * @param transactions
                 */
                calculateTotalBusinessStandardAllowance = function (transactions) {
                    // total of work at home and mileage
                    var trans = [];

                    for (var index in transactions) {
                        if (parseInt(transactions[index].record_type_id) === parseInt(types.mileage) ||
                            parseInt(transactions[index].record_type_id) === parseInt(types.workAtHome)) {
                            trans.push(transactions[index]);
                        }
                    }

                    service.total.business.standardAllowance = service.calculateTotalAmount(trans);
                },

                /**
                 * Calculate Disallowable Expense
                 *
                 */
                calculateTotalBusinessDisAllowableExpense = function () {
                    formData.fetch(utils.getTaxYear(TaxYear.current.description)).then(function (res) {
                        var total = 0;

                        for (var i = 0; i < res.length; i++) {
                            for (var index = 32; index <= 45; index++) {
                                total += parseFloat(res[i]['FSE' + index] || 0);
                            }
                        }

                        service.total.business.disAllowableExpenses = total;
                    });
                },

                /**
                 * Calculate Total Business CIS deduction
                 *
                 * @param transactions
                 */
                calculateTotalBusinessCisDeduction = function (transactions) {
                    var cis = 0;

                    for (var index in transactions) {
                        var tran = transactions[index];
                        if (tran.cis_deduction && !isNaN(parseFloat(tran.cis_deduction))) {
                            cis += parseFloat(tran.cis_deduction);
                        }
                    }

                    service.total.business.cisDeduction = cis;
                },

                /**
                 * Calculate Business Net Profit
                 */
                calculateBusinessNetProfit = function () {
                    var bus = service.total.business;
                    bus.netProfit = bus.income - bus.expense - bus.standardAllowance + bus.disAllowableExpenses;
                },

                /**
                 * Calculate benefit and expense
                 *
                 * @param transactions_emp
                 */
                calculateStudentLoan = function (transactions_emp) {
                    var total = 0;
                    for (var j in transactions_emp) {
                        var transaction = transactions_emp[j];

                        // New formular
                        if (!utils.isEmpty(transaction.student_loan)) {
                            total += transaction.student_loan;
                        }
                    }

                    service.total.employment.studentLoan = total;
                },

                /**
                 * Calculate benefit and expense
                 *
                 * @param transactions_emp
                 */
                calculateBenefitAndExpenses = function (transactions_emp) {
                    var total = 0;
                    for (var j in transactions_emp) {
                        var transaction = transactions_emp[j];

                        // New formular
                        if (!utils.isEmpty(transaction.tax_deducted)) {
                            total += transaction.tax_deducted;
                        }
                    }

                    service.total.employment.benefitExpenses = total;
                },

                /**
                 * Calculate Salary
                 *
                 * @param transactions_emp
                 */
                calculateSalary = function (transactions_emp) {
                    var total = 0;

                    // Old formular
                    for (var index in transactions_emp) {
                        var transaction = transactions_emp[index];

                        if (!utils.isEmpty(transaction.amount)) {
                            total += transaction.amount;
                        }
                    }

                    service.total.employment.salary = total;
                },

                /**
                 * Calculate Rental Income of Transactions Property
                 *
                 * @param transactions_pro
                 */
                calculateRentalIncome = function (transactions_pro) {
                    var total = 0;
                    for (var index in transactions_pro) {
                        var transaction = transactions_pro[index];

                        // New formular
                        if (transaction.record_type.type_group_id == types.groups.propertyIncome) {
                            if (!utils.isEmpty(transaction.amount)) {
                                total += transaction.amount;
                            }
                        }
                    }

                    service.total.property.rentalIncome = total;
                },

                /**
                 * Calculate Rental Expense
                 *
                 * @param transactions_pro
                 */
                calculateRentalExpense = function (transactions_pro) {
                    var total = 0;
                    for (var index in transactions_pro) {
                        var transaction = transactions_pro[index];

                        // New formular
                        if (transaction.record_type.type_group_id == types.groups.propertyExpense) {
                            if (!utils.isEmpty(transaction.amount)) {
                                total += transaction.amount;
                            }
                        }
                    }

                    service.total.property.rentalExpenses = total;
                },

                /**
                 * Calculate Total Income
                 */
                calculateToTalIncome = function () {
                    var total = service.total;
                    total.income = total.employment.salary +
                        total.property.rentalIncome -
                        total.property.rentalExpenses +
                        total.business.netProfit;
                },

                /**
                 * calculate tax deducted
                 */
                calculateTaxDeducted = function (transactions_emp) {
                    var total = 0;

                    for (var index in transactions_emp) {
                        var transaction = transactions_emp[index];

                        if (!utils.isEmpty(transaction.tax_deducted)) {
                            total += parseFloat(transaction.tax_deducted);
                        }
                    }

                    service.total.taxDeducted = total;
                },

                /**
                 * Calculate Quick Estimated Tax
                 *
                 */
                calculateQuickEstimatedTax = function () {
                    var resource = 'quick-est-tax';
                    var deferred = $q.defer();
                    var url = resource + '/' + TaxYear.current.id;
                    var object = {
                        net_business: service.total.business.netProfit > 0 ? service.total.business.netProfit : 0,
                        total_employment: service.total.employment.salary,
                        net_property: service.total.property.rentalIncome - service.total.property.rentalExpenses,
                        tax_deducted: service.total.taxDeducted
                    };

                    apiService.post(url, object)
                        .then(function (res) {
                            deferred.resolve(res.data.data);
                        }, function error(error) {
                            deferred.reject(error.data);
                        });

                    return deferred.promise;
                },

                /**
                 * calculate amount for work at home
                 *
                 * @param hours
                 * @returns {number}
                 */
                calculateAmountWorkAtHome = function (hours) {
                    var h = parseFloat(hours);

                    if (!h || isNaN(h)) {
                        h = 0;
                    }

                    if (25 <= h && h <= 50) {
                        return 10;
                    } else if (50 <= h && h <= 100) {
                        return 18;
                    } else if (h > 100) {
                        return 26;
                    }

                    return 0;
                },


                /**
                 * Calculate accumulated mileages
                 *
                 */
                calculateAccumulatedMileage = function () {
                    var accumulated = {
                        mileage: 0,
                        cost: 0
                    };

                    var year = TaxYear.current;
                    var list = angular.copy(transactions.list);

                    for (var index in list) {
                        if (parseFloat(list[index].record_type_id) === types.mileage && parseFloat(list[index].tax_year_id) === year.id) {
                            if (!isNaN(parseFloat(list[index].mileage))) {
                                accumulated.mileage += parseFloat(list[index].mileage);
                            }

                            if (!isNaN(parseFloat(list[index].claim_allowance))) {
                                accumulated.cost += parseFloat(list[index].claim_allowance);
                            }
                        }
                    }
                    return accumulated;
                },


                /**
                 * Calculate Claim allowance Mileage in current year
                 *
                 * @param mileage
                 * @param vehicle
                 * @returns {number}
                 */
                calculateClaimAllowance = function (mileage, vehicle) {
                    // Call calculate accumulated
                    var accumulated = calculateAccumulatedMileage();
                    accumulated.mileage += parseFloat(mileage);

                    // Get rate mileage
                    var rate = CONFIGS.MILEAGE_RATE(TaxYear.current, vehicle);

                    // Calculate result
                    var claim = Math.min(accumulated.mileage, 10000) * (rate.LESS_THAN_1000 - rate.GREATER_THAN_1000) +
                        accumulated.mileage * rate.GREATER_THAN_1000 - accumulated.cost;

                    return Math.round(claim * 100) / 100;
                };


            return service;
        }
    );
