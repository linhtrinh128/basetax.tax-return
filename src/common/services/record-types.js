angular.module('services.record-types', [])

    .constant('types', {

        income: 1, // income
        expense: 2, // expense

        workAtHome: 20, // work at home
        mileage: 22, // Standard Allowances


        otherIncome: 9, // other income



        personal: 19, //Personal
        standardAllowance: 3, //StandardAllowance
        others: {},

        groups: {
            income: 1, // income
            expense: 2, // expense
            standardAllowances: 3, // Standard Allowances

            employment: 5, // employment
            employmentBenefits: 6,
            employmentExpenses: 7,

            property: 10, // property
            propertyIncome: 11,
            propertyExpense: 12,
            childBenefit: 13, // child benefit
            studentLoan: 14, // student loan
            serviceCompany: 15, // service company
        }
    });
