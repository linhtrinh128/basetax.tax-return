angular.module('services.toaster', [])
  .factory('Toaster',
    function ($rootScope, toaster) {

      var type = {
        success: 'success',
        warning: 'warning',
        error: 'error'
      };

      var service = {
        success: function (title, content) {
          if (!content) {
            service.pop(type.success, title);
          } else {
            service.pop(type.success, title, content);
          }
        },

        warning: function (title, content) {
          service.pop(type.warning, title, content);
        },

        error: function (title, content) {
          if (!content) {
            service.pop(type.error, title);
          } else {
            service.pop(type.error, title, content);
          }
        },

        pop: function (type, title, content) {
          if (!title) {
            title = '';
          }
          if (!content) {
            content = 'Content';
          }
          toaster.pop(type, title, content);
        }
      };

      return service;
    })
;