angular.module('services.modes', [])

    .factory('modes', function ($rootScope) {

        var service = {


            // Worker function for pushing a new notification onto the correct array/queue
            addNotification: function (notificationsArray, notificationObj) {
                if (!angular.isObject(notificationObj)) {
                    throw new Error("Only objects can be added to the notification service.");
                }
                notificationsArray.push(notificationObj);
                return notificationObj;
            },

            checkState: function ($state, utils) {
                // State
                if ($state && $state.includes('home.taxmapp.self-assessment-landing')) {
                    switch ($state.current.name) {
                        case 'home.taxmapp.self-assessment-landing':
                            utils.storage.set('loginBy', 'login-landing');
                            $rootScope.$broadcast('loginBy');
                            break;
                        case 'home.taxmapp.dashboard':
                            utils.storage.set('loginBy', 'login-app');
                            $rootScope.$broadcast('loginBy');
                            break;
                        default :
                            utils.storage.set('loginBy', 'login-app');
                            $rootScope.$broadcast('loginBy');
                            break;
                    }
                }
            }
        };

        return service;
    });

