angular.module('services.apiReject', [])

    .factory('apiReject', function ($scope, error, callback, messages, localizedMessages) {
        var service = {
            handleError: function (error, callback) {
                switch (error.code) {
                    case 400:
                        $scope.errors = error.error;
                        $scope.saveReason = localizedMessages.get('crud.validation.error');

                        if (typeof callback === 'function') {
                            callback();
                        } else {
                            var options = {
                                template: 'message-errors',
                                title: 'Error: Validation from server',
                                content: $scope.errors
                            };

                            messages.show(options);
                        }

                        break;
                    default:
                        break;
                }
            }
        };

        return service;
    });
