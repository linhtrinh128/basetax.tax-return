angular.module('services.forms', [])

  .factory('forms',
    function (apiService, utils, $rootScope, $q, $http, $uibModal, CONFIGS) {


      var service = {

        /**
         * open create transaction
         */
        openCreateTransaction: function () {
          $uibModal.open({
            templateUrl: 'modules/transaction/new/create-transaction.tpl.html',
            controller: 'CreateTransactionController'
          });
        },

        /**
         * open create income
         */
        openCreateIncome: function () {
          var options = {
            templateUrl: 'modules/transaction/new/business/add-income/income.tpl.html',
            controller: 'IncomeController',
            resolve: {
              title: function () {
                return 'Create Income';
              },
              income: function () {
                return null;
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * open form edit income
         * @param object
         */
        openEditIncome: function (object) {
          var options = {
            templateUrl: 'modules/transaction/new/business/add-income/income.tpl.html',
            controller: 'IncomeController',
            resolve: {
              title: function () {
                return 'Edit Income';
              },
              income: function () {
                return object;
              }
            }
          };
          $uibModal.open(options);
        },

        /**
         * open create expense
         */
        openCreateExpense: function () {
          var options = {
            templateUrl: 'modules/transaction/new/business/add-expenses/expenses.tpl.html',
            controller: 'ExpensesController',
            resolve: {
              title: function () {
                return 'Create Expense';
              },
              expense: function () {
                return null;
              }
            }
          };

          $uibModal.open(options);
        },


        /**
         * open create expense
         */
        openEditExpense: function (obj) {
          var options = {
            templateUrl: 'modules/transaction/new/business/add-expenses/expenses.tpl.html',
            controller: 'ExpensesController',
            resolve: {
              title: function () {
                return 'Edit Expense';
              },
              expense: function () {
                return obj;
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * open create mileage
         */
        openCreateMileage: function () {
          var options = {
            templateUrl: 'modules/transaction/new/business/business-mileage/mileage.tpl.html',
            controller: 'MileageController',
            resolve: {
              title: function () {
                return 'Create Mileage';
              },
              mileage: function () {
                return null;
              }
            }
          };

          $uibModal.open(options);
        },


        /**
         * open create mileage
         */
        openEditMileage: function (obj) {
          var options = {
            templateUrl: 'modules/transaction/new/business/business-mileage/mileage.tpl.html',
            controller: 'MileageController',
            resolve: {
              title: function () {
                return 'Edit Mileage';
              },
              mileage: function () {
                return obj;
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * open create work at home
         *
         */
        openCreateWorkAtHome: function () {
          var options = {
            templateUrl: 'modules/transaction/new/business/work-at-home/work-at-home.tpl.html',
            controller: 'WorkAtHomeController',
            resolve: {
              title: function () {
                return 'Create Work at home';
              },
              workAtHome: function () {
                return null;
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * open create work at home
         *
         */
        openEditWorkAtHome: function (obj) {
          var options = {
            templateUrl: 'modules/transaction/new/business/work-at-home/work-at-home.tpl.html',
            controller: 'WorkAtHomeController',
            resolve: {
              title: function () {
                return 'Edit Work at home';
              },
              workAtHome: function () {
                return obj;
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * Open create business
         */
        openCreateBusiness: function () {
          $uibModal.open({
            templateUrl: 'modules/business/new/business/business.tpl.html',
            controller: 'CreateBusinessController',
            resolve: {
              title: function () {
                return 'Create business';
              }
            }
          });
        },

        /**
         * open edit Business
         */
        openEditBusiness: function () {
          $uibModal.open({
            templateUrl: 'modules/business/new/business/business.tpl.html',
            controller: 'CreateBusinessController',
            resolve: {
              title: function () {
                return 'Edit business';
              }
            }
          });
        },

        /**
         * Open form create project
         */
        openCreateProject: function () {
          $uibModal.open({
            templateUrl: 'modules/project/new/project/project.tpl.html',
            controller: 'CreateProjectController',
            resolve: {
              title: function () {
                return 'Create project';
              }
            }
          });
        },

        /**
         * Open edit create project
         */
        openEditProject: function () {
          $uibModal.open({
            templateUrl: 'modules/project/new/project/project.tpl.html',
            controller: 'CreateProjectController',
            resolve: {
              title: function () {
                return 'Edit project';
              }
            }
          });
        },

        /**
         * Open create customer
         */
        openCreateCustomer: function () {
          $uibModal.open({
            templateUrl: 'modules/customer/new/customer.tpl.html',
            controller: 'CreateCustomerController',
            resolve: {
              title: function () {
                return 'Create customer';
              }
            }
          });
        },


        /**
         * Open edit customer
         */
        openEditCustomer: function () {
          this.openCreateCustomer();
        },


        /**
         * Open create suppliers
         */
        // openCreateSupplier: function (successCallback) {
        //   $uibModal.open({
        //     templateUrl: 'modules/supplier/new/supplier/supplier.tpl.html',
        //     controller: 'CreateSupplierController',
        //     resolve: {
        //       title: function () {
        //         return 'Create supplier';
        //       },
        //       successCallback: function () {
        //         return successCallback;
        //       }
        //     }
        //   });
        // },


        /**
         * Open edit supplier
         *
         */
        // openEditSupplier: function () {
        //   this.openCreateSupplier();
        // },


        /**
         * Open NIC
         */
        openNIC: function () {
          $uibModal.open({
            templateUrl: 'modules/business/new/nic/nic.tpl.html',
            controller: 'NICController'
          });
        },


        /**
         * Open self employed
         */
        openSelfEmployed: function () {
          $uibModal.open({
            templateUrl: 'modules/business/new/self-employed/self-employed.tpl.html',
            controller: 'SelfEmployedController',
            resolve: {
              getSelfEmployed: function (selfEmployeds) {
                if (selfEmployeds.list == null || selfEmployeds.list.length === 0) {
                  return selfEmployeds.all();
                }
                return selfEmployeds.list;
              }
            }
          });
        },

        /**
         *
         */
        openFormCreateEmployer: function () {
          $uibModal.open({
            templateUrl: 'modules/employment/new/employer/employer.tpl.html',
            controller: 'CreateEmployerController'
          });
        },

        /**
         * open form edit employer
         */
        openFormEditEmployer: function () {
          $uibModal.open({
            templateUrl: 'modules/employment/new/employer/employer.tpl.html',
            controller: 'CreateEmployerController'
          });
        },

        /**
         * Open form create transaction employer
         */
        openFormCreateTransactionEmployer: function () {
          $uibModal.open({
            templateUrl: 'modules/employment/new/transaction-emp.tpl.html',
            controller: 'CreateTransactionEmploymentController',
            resolve: {
              title: function () {
                return 'Create transaction employment';
              }
            }
          });
        },

        /**
         * Open create property
         *
         */
        openCreateProperty: function () {
          $uibModal.open({
            templateUrl: 'modules/property/new/property.tpl.html',
            controller: 'CreatePropertyController',
            resolve: {
              title: function () {
                return "Create property";
              }
            }
          });
        },


        /**
         * Open form edit property
         */
        openEditProperty: function () {
          $uibModal.open({
            templateUrl: 'modules/property/new/property.tpl.html',
            controller: 'CreatePropertyController',
            resolve: {
              title: function () {
                return "Edit property";
              }
            }
          });
        },

        /**
         * Open form create income property
         */
        openFormCreateTransactionProperty: function (type) {
          var options = {
            templateUrl: 'modules/property/transaction/new/transaction-pro.tpl.html',
            controller: 'CreateTransactionPropertyController',
            resolve: {
              title: function () {
                return 'Add Property ' + type.substring(0, 1).toUpperCase() + type.substring(1);
              },
              type: function () {
                return type;
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * Open form create income property
         */
        openFormEditTransactionProperty: function (type) {
          var options = {
            templateUrl: 'modules/property/transaction/new/transaction-pro.tpl.html',
            controller: 'CreateTransactionPropertyController',
            resolve: {
              title: function () {
                return 'Edit ' + type.substring(0, 1).toUpperCase() + type.substring(1) + ' Property';
              },
              type: function () {
                return type;
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * open form create tenant
         */
        openFormCreateTenant: function () {
          var options = {
            templateUrl: 'modules/tenant/new/tenant/tenant.tpl.html',
            controller: 'CreateTenantController',
            resolve: {
              title: function () {
                return 'Add tenant';
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * open form edit tenant
         */
        openFormEditTenant: function () {
          var options = {
            templateUrl: 'modules/tenant/new/tenant/tenant.tpl.html',
            controller: 'CreateTenantController',
            resolve: {
              title: function () {
                return 'Edit tenant';
              }
            }
          };

          $uibModal.open(options);
        },

        /**
         * open form upgrade subscription
         * @param plan
         */
        openFormUpgradeSubscription: function (plan) {
          $uibModal.open({
            templateUrl: 'modules/account/upgrade/upgrade.tpl.html',
            controller: 'UpgradePlanController',
            resolve: {
              plan: function () {
                for (var index in CONFIGS.PLANS) {
                  if (CONFIGS.PLANS[index].name === plan) {
                    return CONFIGS.PLANS[index];
                  }
                }
              }
            }
          });
        },

        /**
         * open form create subcategory
         */
        openFormCreateSubcategory: function () {
          $uibModal.open({
            templateUrl: 'modules/subcategory/new/subcategory.tpl.html',
            controller: 'CreateSubcategoryController',
            resolve: {
              title: function () {
                return 'Create subcategory';
              }
            }
          });
        },

        /**
         * open form edit subcategory
         */
        openFormEditSubcategory: function () {
          $uibModal.open({
            templateUrl: 'modules/subcategory/new/subcategory.tpl.html',
            controller: 'CreateSubcategoryController',
            resolve: {
              title: function () {
                return 'Edit subcategory';
              }
            }
          });
        },


        /**
         * open form change password
         */
        openFormChangePassword: function () {
          $uibModal.open({
            templateUrl: 'security/change-password/change-password.tpl.html',
            controller: 'ChangePasswordController'

          });
        },

        /**
         * Open form backup
         */
        openFormBackup: function () {
          $uibModal.open({
            templateUrl: 'modules/backup/backup.tpl.html',
            controller: 'BackupController'
          });
        }


      };


      return service;
    }
  );