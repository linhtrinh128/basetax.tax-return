angular.module('services.export', [])

  .factory('Export',
    function (apiService, utils, $rootScope, $q) {

      var resource = 'export';

      var service = {

        /**
         * Export transation list
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        exportTransactionListToPdf: function (object) {
          var deferred = $q.defer();

          apiService.post(resource + '/transaction-list-pdf', object)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * Export transaction list vat
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        exportTransactionListVATToPdf: function (object) {
          var deferred = $q.defer();

          apiService.post(resource + '/transaction-vat-list-pdf', object)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * exportTransactionListCISToPdf
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        exportTransactionListCISToPdf: function (object) {
          var deferred = $q.defer();

          apiService.post(resource + '/transaction-cis-list-pdf', object)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * Export profit and loss
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        exportProfitAndLossToPdf: function (object) {
          var deferred = $q.defer();

          apiService.post(resource + '/profit-and-loss-pdf', object)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * Export mileage
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        exportMileage: function (object) {
          var deferred = $q.defer();

          apiService.post(resource + '/mileage-pdf', object)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * Export Invoice
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        exportInvoice: function (object) {
          var deferred = $q.defer();

          apiService.post(resource + '/invoice-pdf', object)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * Export Tax review
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        exportTaxReview: function (object) {
          var deferred = $q.defer();

          apiService.post(resource + '/tax-review-pdf', object)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * Export tax review partnership
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        exportTaxReviewPS: function (object) {
          var deferred = $q.defer();

          apiService.post(resource + '/tax-review-ps-pdf', object)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * Backup
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        backup: function (id) {
          var deferred = $q.defer();
          apiService.post('backup/' + id)
            .then(function (res) {
              res = res.data;
              if (res != null) {
                deferred.resolve(res);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });
          return deferred.promise;
        }
      };

      return service;
    }
  );