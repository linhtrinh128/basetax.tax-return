angular.module('app.utils', [
  'app.constant'
])

  .factory('utils', function ($filter, $q, CONFIGS, messages, $uibModal) {

    var service = {
      monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      requests: [],

      detectFails: function (reason, reasons, callback) { // array

        if (reason === undefined || typeof(reason) !== 'string') {
          reason = '';
        }

        if (reasons === undefined || !angular.isArray(reasons)) {
          reasons = [];
        }

        reasons.filter(function (item) {
          if (reason.indexOf(item) !== -1 && typeof(callback) === 'function') {
            callback();
          }
        });
      },

      pushRequest: function (requests, object) {
        if (!requests) {
          requests = [];
        }
        requests.push(object);
      },

      pullRequest: function (requests, object) {
        for (var i = 0; i < requests.length; i++) {
          switch (requests[i].type.toLowerCase()) {
            case 'request':
            {
              if (requests[i].api === object.api) {
                requests.splice(i, 1);
                return;
              }
              break;
            }
            default:
            {
              if (requests[i].fromState === object.fromState && requests[i].toState === object.toState) {
                requests.splice(i, 1);
                return;
              }
              break;
            }
          }
        }
      },

      /**
       * sort list
       * @param list list, string property, boolean reverse
       * @return list
       * */
      sort: function (list, property, reverse) {
        if (reverse === undefined) {
          reverse = false;
        }

        if (!angular.isArray(list)) {
          return [];
        } else {
          var newList = $filter('orderBy')(list, property, reverse);
          return newList.filter(function (n) {
            return n !== undefined;
          });
        }
      },

      setObject: function (source, target) {
        for (var key in source) {
          if (typeof(target[key]) !== 'undefined' && typeof(target[key]) === 'object') {
            target[key] = source[key];
          }
        }
      },

      getObject: function (source) {

        var list = {};

        for (var key in source) {
          if (typeof(source[key]) === 'object') {
            list[key] = source[key];
          }
        }

        return list;
      },

      // get created by
      createdBy: function (user) {
        if (user) {
          if (user.firstname || user.lastname) {
            var fullName = (user.firstname !== null ? user.firstname : '') + ' ' + (user.lastname !== null ? user.lastname : '');
            return fullName !== '' ? fullName : 'No name';
          }
          return 'No name';
        }
        return null;
      },

      // get updated by
      updatedBy: function (user) {
        if (user) {
          if (user.firstname || user.lastname) {
            return '(' + (user.firstname !== null ? user.firstname : '') + ' ' + (user.lastname !== null ? user.lastname : '') + ')';
          }
          return '';
        }
        return '';
      },

      // filter source
      filterSource: function (query, source) {
        var deferred = $q.defer();

        var items = _.chain(source)
          .filter(function (x) {
            return x.toLowerCase().indexOf(query.toLowerCase()) > -1;
          })
          .take(10)
          .value();

        deferred.resolve(items);

        return deferred.promise;
      },

      // Check array
      arrayPopulated: function (input) {
        if (!service.isEmpty(input) && input.length > 0) {
          return true;
        }
        return false;
      },

      // Helper function for checking if a variable is an integer
      isInteger: function (possibleInteger) {
        return Object.prototype.toString.call(possibleInteger) !== "[object Array]" && /^[\d]+$/.test(possibleInteger);
      },

      // get Id
      getId: function (object) {
        if (object != null && typeof object === 'object') {
          return parseInt(object.id);
        } else if (service.isInteger(object)) {
          return parseInt(object);
        }
        return 0;
      },

      // populate by id
      populateById: function (object, array) {
        for (var item in array) {
          if (this.getId(array[item]) === this.getId(object)) {
            return array[item];
          }
        }
        return undefined;
      },

      /**
       * file type
       */
      fileTypes: ['JPG', 'JPEG', 'PNG', 'GIF', 'TIF', 'PDF', 'DOC', 'DOCX', 'XLS', 'XLSX'],

      /**
       * Check file type
       * @param file
       * @returns {*}
       */
      checkFileType: function (file) {
        for (var i = 0; i < CONFIGS.FILE_TYPE_IMAGE.length; i++) {
          if (file.name.toLowerCase().indexOf(CONFIGS.FILE_TYPE_IMAGE[i].toLowerCase()) !== -1) {
            return 'picture';
          }
        }
        return 'doc';
      },

      /**
       * Check file type image
       *
       * @param file
       * @returns {boolean}
       */
      checkFileTypeImg: function (file) {
        for (var index in CONFIGS.FILE_TYPE_IMAGE) {
          if (file.name.toLowerCase().indexOf(CONFIGS.FILE_TYPE_IMAGE[index].toLowerCase()) !== -1) {
            return true;
          }
        }
        alert('File "' + file.name + '" is not supported. Please try with a jpg, jpeg file.');
        return false;
      },

      /**
       * Check file type by {types}
       * @param file
       * @param types
       * @returns {boolean}
       */
      checkFileTypeBy: function (file, types) {
        for (var index in types) {
          if (file.name.toLowerCase().indexOf(types[index].toLowerCase()) !== -1) {
            return true;
          }
        }
        return false;
      },


      /**
       * Check file size
       *
       * @param file
       * @returns {boolean}
       */
      checkFileSize: function (file) {
        if (file.size < CONFIGS.MAX_SIZE_FILE_SCAN) {
          return true;
        }

        messages.show({
          title: 'Check file size',
          content: 'File "' + file.name + '" has size larger than ' +
          (CONFIGS.MAX_SIZE_FILE_SCAN / 1048576) +
          'Mb. Please try with a smaller file.'
        });

        return false;
      },

      /**
       * check file size image
       *
       * @param file
       * @returns {boolean}
       */
      checkFileSizeImg: function (file) {
        if (file.size < CONFIGS.MAX_SIZE_FILE_SCAN) {
          return true;
        }

        messages.show({
          title: 'Check file size',
          content: 'File "' + file.name + '" has size larger than ' +
          (CONFIGS.MAX_SIZE_FILE_SCAN / 1048576) +
          'Mb. Please try with a smaller file.'
        });

        return false;
      },

      // base4 encode
      base64Encode: function (binary) {
        //return btoa(unescape(encodeURIComponent(binary)));
        var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var out = "", i = 0, len = binary.length, c1, c2, c3;
        while (i < len) {
          c1 = binary.charCodeAt(i++) & 0xff;
          if (i == len) {
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt((c1 & 0x3) << 4);
            out += "==";
            break;
          }
          c2 = binary.charCodeAt(i++);
          if (i == len) {
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
            out += CHARS.charAt((c2 & 0xF) << 2);
            out += "=";
            break;
          }
          c3 = binary.charCodeAt(i++);
          out += CHARS.charAt(c1 >> 2);
          out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
          out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
          out += CHARS.charAt(c3 & 0x3F);
        }
        return out;
      },

      isEmpty: function (input) {
        if (angular.isUndefined(input) || input == null) {
          return true;
        }
        return false;
      },

      isZero: function (input) {
        if (service.isEmpty(input) || service.fixed(input) === 0) {
          return true;
        }
        return false;
      },

      /**
       * check is objet empty
       *
       * @param obj
       * @returns {boolean}
       */
      isObjectEmpty: function (obj) {
        // null and undefined are "empty"
        if (obj == null) {
          return true;
        }

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0) {
          return false;
        }
        if (obj.length === 0) {
          return true;
        }

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        if (Object.getOwnPropertyNames(obj).length > 0) {
          return false;
        }

        return true;
      },

      storage: {
        set: function (key, data) {

          var value = data;
          if (typeof(data) === 'object') {
            value = angular.copy(JSON.stringify(data));
          }

          window.localStorage.setItem(key, value);
        },
        get: function (key) {
          return window.localStorage.getItem(key) || undefined;
        },
        clear: function () {
          window.localStorage.clear();
        },
        remove: function (key) {
          window.localStorage.removeItem(key);
        }
      },

      dateFormat: function (datetime, formatStr) {
        if (!datetime) {
          datetime = new Date();
        }
        if (!formatStr) {
          formatStr = 'yyyy-MM-dd';
        }

        return $filter('date')(datetime, formatStr);
      },

      setDatetime: function (year, month, date) {
        if (!year) {
          return new Date();
        } else if (year instanceof Date) {
          var d = new Date(year);
          return new Date(d.getFullYear(), d.getMonth() + 1, 0);
        }

        return new Date(year, month ? month : 1, date ? date : 0);
      },

      nextMonth: function (year, month) {
        if (!year) {
          var current = new Date();
          return current.setMonth(current.getMonth() + 1);
        } else if (year instanceof Date) {
          return new Date(year.getFullYear(), year.getMonth() + 1, 0);
        }

        return new Date(year, month ? (month + 1) : 1, 0);
      },

      /**
       * select Current Tax Year for bookkeeping (default)
       *
       * @param items
       * @returns {*}
       */
      selectCurrentYear: function (years) {
        var today = Date.today();
        for (var index in years) {
          var start_year = years[index].start_year;
          var range = {
            start: new Date(CONFIGS.TAX_YEAR.DATE_START + '/' + start_year),
            end: new Date(CONFIGS.TAX_YEAR.DATE_END + '/' + (start_year + 1))
          };
          if (today.between(range.start, range.end)) {
            return years[index];
          }
        }
        return years[0];
      },

      /**
       * select current year for tax return
       *
       * @param items
       * @returns {*}
       */
      selectCurrentYearTaxReturn: function (years) {
        var today = Date.today();
        for (var index in years) {
          var start_year = years[index].start_year;
          var range = {
            start: new Date(CONFIGS.TAX_YEAR.DATE_START + '/' + start_year),
            end: new Date(CONFIGS.TAX_YEAR.DATE_END + '/' + (start_year + 1))
          };
          if (today.between(range.start, range.end)) {
            return years[index - 1];
          }
        }
        return years[0];
      },

      /**
       * Function format Date time
       *
       * @params: string date
       * @returns: string date (year-month-day)
       * */
      formatDateTime: function (dateString) {
        if (service.isEmpty(dateString)) {
          return '';
        }
        var date = new Date(dateString);

        if (isNaN(date.getTime())) {
          return '';
        }

        var year = date.getFullYear(),
          month = date.getMonth() + 1,
          day = date.getDate();

        if (month < 10) {
          month = '0' + month;
        }
        if (day < 10) {
          day = '0' + day;
        }

        return year + '-' + month + '-' + day;
      },


      /**
       * format Date Time 2
       * @param string date
       * @return string date (format dd MMM yyyy)
       * */
      formatDateTime2: function (date) {
        if (service.isEmpty(date)) {
          return;
        }

        if (service.isBrowser(navigator.userAgent) == 'unknown') {
          date = new Date(date.replace(/-/g, "/"));
        } else {
          date = new Date(date);
        }

        if (isNaN(date.getTime())) {
          return;
        }

        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return getDate(date.getDate()) + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();

        function getDate(day) {
          var res = '';
          var mod = day % 10,
            dec = parseInt(day / 10);
          if (dec === 1) {
            res = day + 'th';
          } else {
            switch (mod) {
              case 1:
              {
                res = day + 'st';
                break;
              }
              case 2:
              {
                res = day + 'nd';
                break;
              }
              case 3:
              {
                res = day + 'rd';
                break;
              }
              default:
              {
                res = day + 'th';
                break;
              }
            }
          }

          return res;
        }
      },

      /**
       * format Date
       *
       * @param date
       * @returns {string}
       */
      formatDate: function (date) {
        if (service.isEmpty(date)) {
          return;
        }

        if (service.isBrowser(navigator.userAgent) == 'unknown') {
          date = new Date(date.replace(/-/g, "/"));
        } else {
          date = new Date(date);
        }

        if (isNaN(date.getTime())) {
          return;
        }

        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return getDate(date.getDate()) + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();

        function getDate(day) {
          var res = '';
          var mod = day % 10,
            dec = parseInt(day / 10);
          if (dec === 1) {
            res = day + 'th';
          } else {
            switch (mod) {
              case 1:
              {
                res = day + 'st';
                break;
              }
              case 2:
              {
                res = day + 'nd';
                break;
              }
              case 3:
              {
                res = day + 'rd';
                break;
              }
              default:
              {
                res = day + 'th';
                break;
              }
            }
          }

          return res;
        }
      },


      /**
       * Convert object date to string
       * @params: date
       * @return: string
       * */
      shortDate: function (date) {
        var d = new Date(date);

        var day = d.getDate(),
          month = d.getMonth() + 1,
          year = d.getFullYear();

        return year + '-' + (month >= 10 ? month : ('0' + month)) + '-' + day;
      },

      /**
       * get first year
       * @param string
       * @return integer of first year
       * */
      getTaxYear: function (years) {
        var numbers = years.split('-');
        return parseInt(numbers[0]) + 1;
      },

      // format amount
      formatAmount: function (amount) {
        if (service.isEmpty(amount)) {
          return '';
        }
        if (typeof(amount) === 'number') {
          amount = amount.toFixed(2);
        }

        var dec = parseInt(amount.split('.')[0]),
          float = amount.split('.')[1] ? amount.split('.')[1] : '00';

        var string = '';
        var navigate = false;
        if (dec < 0 || dec == -0) {
          navigate = true;
          dec = Math.abs(dec);
        }

        var list = [];
        while (dec >= 1000) {
          var val = dec % 1000;
          if (val < 10) {
            val = '00' + val.toString();
          } else if (val < 100) {
            val = '0' + val.toString();
          }

          list.unshift(val);
          dec = parseInt(dec / 1000);
        }
        list.unshift(dec);

        var number = list.toString() + '.' + float;

        if (navigate) {
          return '(' + number + ')';
        }
        return number;
      },

      /**
       * format Length of string
       * */
      formatLength: function (str, len) {
        if (typeof str != 'string') {
          return;
        }

        if (str.length >= len) {
          str = str.substring(0, len) + '...';
        }
        return str;
      },


      isBrowser: function (browser) {

        if (browser === undefined) {
          if (navigator.userAgent.indexOf(browser) !== -1) {
            return true;
          }
          return false;
        } else {
          if (navigator.userAgent.indexOf("Chrome") != -1) {
            return 'chrome';
          }
          else if (navigator.userAgent.indexOf("Opera") != -1) {
            return 'opera';
          }
          else if (navigator.userAgent.indexOf("Firefox") != -1) {
            return 'firefox';
          }
          else if (navigator.userAgent.indexOf("MSIE") != -1) {
            return 'ie';
          }
          return 'unknown';
        }
      },

      fixed: function (value, fixed) {
        if (!value) {
          return 0;
        }
        if (service.isEmpty(fixed)) {
          fixed = 2;
        }

        value = parseFloat(value);
        if (isNaN(value)) {
          return 0;
        }

        return parseFloat(value.toFixed(fixed));
      },

      multi: function (source, target) {
        if (!source || !target) {
          return 0;
        }

        return service.fixed(source, target);
      },

      devide: function (source, target) {
        if (!source || !target) {
          return 0;
        }

        return service.fixed(source / target, 4);
      },

      minus: function (input) {
        if (typeof(input) !== 'object') {
          return 0;
        }

        var total = service.fixed(input[0]);
        for (var i = 1; i < input.length; i++) {
          total -= service.fixed(input[i]);
        }

        return service.fixed(total);
      },

      sum: function (input) {

        if (typeof(input) !== 'object') {
          return 0;
        }

        var total = 0;
        for (var i = 0; i < input.length; i++) {
          total += service.fixed(input[i]);
        }

        return service.fixed(total);
      },

      pow: function (base, exponent) {
        if (!exponent) {
          exponent = 1;
        }

        if (!base) {
          base = 1;
        }

        return service.fixed(Math.pow(base, exponent));
      },

      average: function (input) {
        if (typeof(input) !== 'object') {
          return 0;
        }

        return service.fixed(service.sum(input) / input.length);
      },

      checkIfLoad: function (item, list) {
        var id = service.getId(item);

        if (angular.isArray(list)) {
          for (var index in list) {
            if (service.getId(list[index]) === id) {
              return list[index];
            }
          }
        }
        return false;
      },


      /**
       * Append in list
       *
       * @param item
       * @param list
       * @returns {boolean}
       */
      appendInList: function (item, list) {
        if (!service.checkIfLoad(item, list)) {
          if (list == null) {
            list = [];
          }
          list.push(item);
          return service.sort(list, 'modified', true);
        }
        return false;
      },

      /**
       * Update item in list
       *
       * @param item
       * @param list
       * @returns {*}
       */
      updateInList: function (item, list) {
        if (angular.isArray(list)) {
          for (var index in list) {
            var current = list[index];
            if (service.getId(current.id) === service.getId(item.id)) {
              for (var prop in current) {
                if (item[prop] != null && !service.deepCompare(current[prop], item[prop])) {
                  current[prop] = item[prop];
                }
              }

              //list[index] = item;
              return list;
            }
          }
        }
        return false;
      },

      /**
       * Update item in list
       *
       * @param item
       * @param list
       * @returns {*}
       */
      updateNewObject: function (newObject, OldObject) {
        if (service.getId(OldObject.id) === service.getId(newObject.id)) {
          for (var prop in OldObject) {
            if (newObject[prop] != null && !service.deepCompare(OldObject[prop], newObject[prop])) {
              OldObject[prop] = newObject[prop];
            }
          }
          return OldObject;
        }
        return false;
      },

      /**
       * deep compare
       *
       * @returns {boolean}
       */
      deepCompare: function () {
        var i, l, leftChain, rightChain;

        function compare2Objects(x, y) {
          var p;

          // remember that NaN === NaN returns false
          // and isNaN(undefined) returns true
          if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
            return true;
          }

          // Compare primitives and functions.
          // Check if both arguments link to the same object.
          // Especially useful on step when comparing prototypes
          if (x === y) {
            return true;
          }

          // Works in case when functions are created in constructor.
          // Comparing dates is a common scenario. Another built-ins?
          // We can even handle functions passed across iframes
          if ((typeof x === 'function' && typeof y === 'function') ||
            (x instanceof Date && y instanceof Date) ||
            (x instanceof RegExp && y instanceof RegExp) ||
            (x instanceof String && y instanceof String) ||
            (x instanceof Number && y instanceof Number)) {
            return x.toString() === y.toString();
          }

          // At last checking prototypes as good a we can
          if (!(x instanceof Object && y instanceof Object)) {
            return false;
          }

          if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
            return false;
          }

          if (x.constructor !== y.constructor) {
            return false;
          }

          if (x.prototype !== y.prototype) {
            return false;
          }

          // Check for infinitive linking loops
          if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
            return false;
          }

          // Quick checking of one object beeing a subset of another.
          // todo: cache the structure of arguments[0] for performance
          for (p in y) {
            if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
              return false;
            }
            else if (typeof y[p] !== typeof x[p]) {
              return false;
            }
          }

          for (p in x) {
            if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
              return false;
            }
            else if (typeof y[p] !== typeof x[p]) {
              return false;
            }

            switch (typeof (x[p])) {
              case 'object':
              case 'function':

                leftChain.push(x);
                rightChain.push(y);

                if (!compare2Objects(x[p], y[p])) {
                  return false;
                }

                leftChain.pop();
                rightChain.pop();
                break;

              default:
                if (x[p] !== y[p]) {
                  return false;
                }
                break;
            }
          }

          return true;
        }

        if (arguments.length < 1) {
          return true; //Die silently? Don't know how to handle such case, please help...
          // throw "Need two or more arguments to compare";
        }

        for (i = 1, l = arguments.length; i < l; i++) {

          leftChain = []; //Todo: this can be cached
          rightChain = [];

          if (!compare2Objects(arguments[0], arguments[i])) {
            return false;
          }
        }

        return true;
      },

      /**
       * Remove item in list
       *
       * @param item
       * @param list
       * @returns {*}
       */
      removeFromList: function (item, list) {
        if (angular.isArray(list)) {
          for (var index in list) {
            if (service.getId(list[index].id) === service.getId(item.id)) {
              list.splice(parseInt(index), 1);
              return list;
            }
          }
        }
        return false;
      },

      /**
       * merge list 1 to list 2
       * @param list1 list, list2 list
       * @return
       * */
      mergeList: function (list1, list2) {
        for (var index in list1) {
          list2.push(list1[index]);
        }
        return list2;
      },

      /**
       * Merge object
       *
       * @param root
       * @param list
       * @returns {*}
       */
      merge: function (root, list) {
        var temp = angular.copy(root);

        for (var key in root) {
          for (var index in list) {
            if (service.isEmpty(temp[key]) && !service.isEmpty(list[index][key])) {
              temp[key] = list[index][key];
            }
          }
        }
        return temp;
      },

      /**
       *
       * @param obj1
       * @param obj2
       * @returns {{}}
       */
      merge2: function (obj1, obj2) {
        var result = {}; // return result
        for (var i in obj1) {      // for every property in obj1
          if ((i in obj2) && (typeof obj1[i] === "object") && (i !== null)) {
            result[i] = this.merge2(obj1[i], obj2[i]); // if it's an object, merge
          } else {
            result[i] = ((obj1[i] != null && obj1[i] !== '' && obj1[i] !== 0) ? obj1[i] : obj2[i]); // add it to result
          }
        }
        for (i in obj2) { // add the remaining properties from object 2
          if (i in result) { //conflict
            continue;
          }
          result[i] = obj2[i];
        }
        return result;
      },

      /**
       * Clone Objedt 1 to object 2 by mask
       *
       * @param obj1
       * @param obj2
       * @returns {*}
       */
      cloneObjectByMask: function (obj, mask) {
        if (typeof obj == "undefined") {
          return mask;
        }
        for (var key in mask) {
          if (obj[key]) {
            mask[key] = obj[key] || null;
          }
        }
        return mask;
      },

      /**
       * find objects in list by tax year
       * @params object year, list list
       * @returns list list
       * */
      findByTaxYear: function (year, list) {
        var newList = [];
        if (angular.isArray(list)) {
          for (var i in list) {
            if (this.getId(list[i].tax_year_id) === this.getId(year.id)) {
              newList.push(list[i]);
            }
          }
        }
        return newList;
      },

      /**
       * generate string report date time
       * */
      generateStringDateTime: function (year) {
        var d = new Date();
        var str_name =
          (parseInt(year.start_year) + 1) + '-' +
          (d.getMonth() + 1 < 10 ? ('0' + d.getMonth() + 1) : (d.getMonth() + 1)) +
          (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()) +
          (d.getYear() + 1900) + '-' +
          (d.getHours() < 10 ? '0' + d.getHours() : d.getHours()) +
          (d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes()) +
          (d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds());
        return str_name;
      },

      /**
       * generate invoice_no
       */
      generateInvoiceNo: function (list) {
        if (list == null || list.length === 0) {
          return 1;
        }

        var max_no = parseInt(list[0].invoice_no);
        for (var index in list) {
          if (parseInt(list[index].invoice_no) > max_no) {
            max_no = parseInt(list[index].invoice_no);
          }
        }
        return max_no + 1;
      },


      // confirm delete
      confirmDelete: function () {
        var params = {
          templateUrl: 'tpl/confirm-delete.tpl.html',
          controller: function ($scope, $uibModalInstance) {
            $scope.ok = function () {
              $uibModalInstance.close();
            };
            $scope.cancel = function () {
              $uibModalInstance.dismiss('cancel');
            };
          },
          size: 'sm'
        };

        return $uibModal.open(params).result;
      }
    };

    return service;
  });
