angular.module('services', [
  'restangular',
  'services.calculator',
  'services.export',
  'services.exchange',
  'services.forms',
  'services.messages',
  'services.payments',
  'services.record-types',
  'services.i18nMessages',
  'services.notifications',
  'services.api',
  'services.toaster'
]);
