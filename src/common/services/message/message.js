angular.module('services.messages', [])
  .factory('messages', function ($rootScope, $uibModal) {
    var service = {
      // show
      show: function (options) {
        var params = {
          templateUrl: 'services/message/message.tpl.html',
          controller: 'MessageController',
          resolve: {
            $options: function () {
              return options;
            }
          }
        };

        switch (options.template) {
          case 'message-errors':
            params.templateUrl = 'services/message/tpl/message-errors.tpl.html';
            break;
          case 'expiration-date':
            params.templateUrl = 'services/message/tpl/expiration-date.tpl.html';
            break;
          default:
            params.templateUrl = 'services/message/tpl/message.tpl.html';
            break;
        }

        //pop-up
        return $uibModal.open(params);
      },

      // show confirm delete
      showConfirmDelete: function (options) {
        var params = {
          templateUrl: 'services/message/tpl/confirm-delete.tpl.html',
          controller: 'ConfirmDeleteController',
          resolve: {
            $options: function () {
              return options;
            }
          }
        };

        return $uibModal.open(params);
      },

      /**
       * confirm delete
       *
       * @param options
       */
      showConfirm: function (options) {
        var params = {
          templateUrl: 'services/message/tpl/confirm.tpl.html',
          controller: 'ConfirmController',
          resolve: {
            $options: function () {
              return options;
            }
          }
        };

        // pop-up
        return $uibModal.open(params);
      },


      /**
       * show Confirm Cancel Subscription
       *
       * @param options
       */
      showConfirmCancelSubscription: function (options) {
        var params = {
          templateUrl: 'services/message/tpl/confirm-cancel-subscription.tpl.html',
          controller: 'ConfirmCancelSubscriptionController',
          resolve: {
            $options: function () {
              return options;
            }
          }
        };

        //pop-up
        return $uibModal.open(params);
      },

      /**
       *
       * @param options
       */
      showMessageHasExpired: function (options) {
        var params = {
          templateUrl: 'services/message/tpl/expiration-date.tpl.html',
          controller: 'MessageHasExpiredController',
          resolve: {
            $options: function () {
              return options;
            }
          }
        };

        //pop-up
        return $uibModal.open(params);
      }
    };

    return service;
  })


  .controller('MessageController', function ($scope, $uibModalInstance, $options) {

    $scope.title = $options.title || 'Title';
    $scope.content = $options.content || 'Default content';
    $scope.footer = $options.footer || '';
    $scope.typeOfContent = typeof $scope.content;

    /**
     * Actions cancel
     */
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    if (typeof $options.callbackFooter === 'function') {
      $scope.callbackFooter = function () {
        $options.callbackFooter();
        $scope.cancel();
      };
    }

    if (typeof $options.closeEvent === 'function') {
      $scope.eventCloseBtn = function () {
        $options.closeEvent();
        $uibModalInstance.close(true);
      };
    } else {
      $scope.eventCloseBtn = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  })

  .controller('ConfirmDeleteController', function ($scope, $uibModalInstance, $options) {

    $scope.title = 'Confirm delete.';
    $scope.content = 'Are you sure to want to delete this ' + $options.resource + ' ? ';

    /**
     * Actions cancel
     */
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };


    /**
     * Actions Yes
     */
    $scope.yes = function () {
      $scope.cancel();
      if (typeof $options.yesCallback === 'function') {
        $options.yesCallback();
      }
    };

    /**
     * Actions No
     */
    $scope.no = function () {
      $scope.cancel();
    };
  })

  .controller('ConfirmController', function ($scope, $uibModalInstance, $options) {

    $scope.title = $options.title;
    $scope.content = $options.content;

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.yes = function () {
      $scope.cancel();
      if (typeof $options.yesCallback === 'function') {
        $options.yesCallback();
      }
    };

    $scope.no = function () {
      $scope.cancel();
    };
  })

  .controller('ConfirmCancelSubscriptionController', function ($scope, $uibModalInstance, $options) {

    $scope.title = 'Confirm cancel subscription.';
    $scope.content = 'Are you sure to want to cancel this subscription ?';

    /**
     * Actions cancel
     */
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };


    /**
     * Actions Yes
     */
    $scope.yes = function () {
      $scope.cancel();
      if (typeof $options.yesCallback === 'function') {
        $options.yesCallback();
      }
    };

    /**
     * Actions No
     */
    $scope.no = function () {
      $scope.cancel();
    };
  })

  .controller('MessageHasExpiredController', function ($scope, $state, $uibModalInstance) {

    /**
     * Actions cancel
     */
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };


    /**
     * Actions Yes
     */
    $scope.upgrade = function () {
      $state.go('loggedIn.modules.account');
      $scope.cancel();
    };
  });
