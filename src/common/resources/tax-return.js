angular.module('resources.tax-returns', [])

  .factory('TaxReturn',
    function (apiService, utils, $rootScope, $q) {
      var resource = 'tax-return';

      var service = {
        list: null,

        /**
         * Get all resources
         */
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              $rootScope.$broadcast('taxReturns:created', res.data);

              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('taxReturns:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('taxReturns:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              $rootScope.$broadcast('taxReturns:deleted', res.object);

              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('taxReturns:list:updated', res.object);
              }

              deferred.resolve(item);

            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        /**
         * update params
         *
         * @param object
         * @returns {deferred.promise|{then, catch, finally}}
         */
        updateParams: function (object) {
          var deferred = $q.defer();

          apiService.customPUTAdvance(resource, object.id, object, 'params')
            .then(function (res) {
              res = res.data;
              if (res.data != null) {
                if (utils.updateInList(res.data, service.list)) {
                  $rootScope.$broadcast('taxReturns:list:updated', service.list);
                }
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            });

          return deferred.promise;
        }
      };

      return service;
    }
  );
