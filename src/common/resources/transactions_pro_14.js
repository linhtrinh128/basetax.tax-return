angular.module('resources.transactions_pro_14', [])

  .factory('TransactionPro14',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {

      var _resource = 'transaction-pro-14';

      var TransactionPro14 = function () {
        Model.apply(this, arguments);


        this.record_type_id = 84;
        this.property_id = -1;
        this.tax_year_id = TaxYear.current.id;
        this.date = new Date();

        this.PRO1 = 0;
        this.PRO2 = 0;
        this.PRO3 = 0;
        this.PRO4 = 0;
        this.PRO4a = 0;
        this.PRO4b = 0;

        this.PRO5 = 0;
        this.PRO6 = 0;
        this.PRO7 = 0;
        this.PRO8 = 0;
        this.PRO9 = 0;
        this.PRO10 = 0;
        this.PRO11 = 0;
        this.PRO12 = 0;
        this.PRO13 = 0;
        this.PRO14 = 0;
        this.PRO15 = 0;
        this.PRO16 = 0;
        this.PRO17 = 0;
        this.PRO18 = 0;
        this.PRO19 = 0;

        this.PRO20 = 0;
        this.PRO21 = 0;
        this.PRO22 = 0;
        this.PRO23 = 0;
        this.PRO24 = 0;
        this.PRO25 = 0;
        this.PRO26 = 0;
        this.PRO27 = 0;
        this.PRO28 = 0;
        this.PRO29 = 0;
        this.PRO30 = 0;
        this.PRO31 = 0;
        this.PRO32 = 0;
        this.PRO33 = 0;
        this.PRO34 = 0;
        this.PRO35 = 0;
        this.PRO36 = 0;
        this.PRO37 = 0;
        this.PRO38 = 0;
        this.PRO39 = 0;
        this.PRO40 = 0;
        this.PRO41 = 0;
        this.PRO42 = 0;
        this.PRO43 = 0;
      };

      // List reliefs
      TransactionPro14.list = [];

      // get all TransactionPro14s
      TransactionPro14.all = function () {
        return Model.all(_resource).then(function (response) {
          TransactionPro14.list = [];
          angular.forEach(response, function (data) {
            var instance = new TransactionPro14();
            instance.fill(data);
            TransactionPro14.list.push(instance);
          });
        });
      };

      // find by tax year
      TransactionPro14.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in TransactionPro14.list) {
          var item = TransactionPro14.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      TransactionPro14.prototype = new Model();

      // fill
      TransactionPro14.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      TransactionPro14.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            TransactionPro14.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      TransactionPro14.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in TransactionPro14.list) {
            if (TransactionPro14.list[i].id === self.id) {
              TransactionPro14.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return TransactionPro14;
    });