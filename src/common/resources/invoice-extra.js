angular.module('resources.invoice-extra', [])
  .factory('InvoiceExtra',
    function () {
      this.list = null;

      var InvoiceExtra = function () {
        this.name = '';
        this.description = '';
        this.quantity = 1;
        this.vat = 0;
        this.amount = 0;
      };

      // override
      return InvoiceExtra;
    });
