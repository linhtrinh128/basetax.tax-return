angular.module('resources.users', [])

    .factory('profile',
        function (apiService, utils, $rootScope, $q, security) {
            // resource is resource connect to api
            var resource = 'profile';


            var service = {
                /**
                 * Update profile
                 *
                 * @param object
                 * @returns {deferred.promise|{then, catch, finally}}
                 */
                update: function (object) {
                    var deferred = $q.defer();

                    apiService.customPUT(resource, object.id, object).then(function (res) {
                        res = res.data;

                        if (res.data != null) {
                            if (utils.updateNewObject(res.data, security.currentUser)) {
                                $rootScope.$broadcast('profile:updated', res.data);
                            }

                            deferred.resolve(res.data);
                        } else {
                            deferred.reject(false);
                        }

                    }, function error(error) {
                        deferred.reject(error.data);
                    });

                    return deferred.promise;
                }
            };

            return service;
        }
    )

    .factory('users', function () {
        var service = {
            count: null
        };
        return service;
    });
