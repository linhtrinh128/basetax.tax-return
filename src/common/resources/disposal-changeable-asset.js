angular.module('resources.disposal-changeable-asset', [])

  // .factory('disposalChangeables',
  //   function (apiService, utils, $rootScope, $q) {
  //     // resource is resource connect to api
  //     var resource = 'disposal-changeable';
  //
  //
  //     var service = {
  //       current: null,
  //       list: null,
  //
  //       // find all
  //       all: function () {
  //         var deferred = $q.defer();
  //
  //         apiService.all(resource).then(function (res) {
  //           res = res.data;
  //
  //           if (res.data != null) {
  //             service.list = [];
  //
  //             service.list = utils.sort(res.data.data, 'modified', true);
  //
  //             deferred.resolve(service.list);
  //           } else {
  //             deferred.reject(false);
  //           }
  //         }, function error(error) {
  //           deferred.reject(error.data);
  //         });
  //
  //         return deferred.promise;
  //       },
  //
  //       findByTaxYear: function (year) {
  //         var deferred = $q.defer();
  //
  //         var list = [];
  //
  //         if (angular.isArray(service.list)) {
  //           for (var index in service.list) {
  //             if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
  //               list.push(service.list[index]);
  //             }
  //           }
  //
  //           deferred.resolve(list);
  //         }
  //
  //         return deferred.promise;
  //       },
  //
  //       find: function (id) {
  //         var deferred = $q.defer();
  //
  //         if (utils.checkIfLoad(id, service.list)) {
  //           deferred.resolve(item);
  //         } else {
  //           apiService.get(resource, id).then(function (res) {
  //             res = res.data;
  //
  //             if (res.data != null) {
  //               if (utils.appendInList(res.data, service.list)) {
  //                 $rootScope.$broadcast('disposalChangeables:list:updated', service.list);
  //               }
  //
  //               deferred.resolve(res.data);
  //             } else {
  //               deferred.reject(false);
  //             }
  //           }, function error(error) {
  //             deferred.reject(error.data);
  //
  //           });
  //         }
  //
  //         return deferred.promise;
  //       },
  //
  //       create: function (object) {
  //         var deferred = $q.defer();
  //
  //         apiService.post(resource, object).then(function (res) {
  //           res = res.data;
  //
  //           if (res.data != null) {
  //             $rootScope.$broadcast('disposalChangeables:created', res.data);
  //
  //             if (utils.appendInList(res.data, service.list)) {
  //               $rootScope.$broadcast('disposalChangeables:list:updated', service.list);
  //             }
  //
  //             deferred.resolve(res.data);
  //           } else {
  //             deferred.reject(false);
  //           }
  //
  //         }, function error(error) {
  //           deferred.reject(error.data);
  //         });
  //
  //         return deferred.promise;
  //       },
  //
  //       update: function (object) {
  //         var deferred = $q.defer();
  //
  //         apiService.customPUT(resource, object.id, object).then(function (res) {
  //           res = res.data;
  //
  //           if (res.data != null) {
  //             if (utils.updateInList(res.data, service.list)) {
  //               $rootScope.$broadcast('disposalChangeables:list:updated', service.list);
  //             }
  //
  //             deferred.resolve(res.data);
  //           } else {
  //             deferred.reject(false);
  //           }
  //
  //         }, function error(error) {
  //           deferred.reject(error.data);
  //         });
  //
  //         return deferred.promise;
  //       },
  //
  //       delete: function (item) {
  //         var deferred = $q.defer();
  //
  //         apiService.remove(resource, item.id).then(function (res) {
  //           res = res.data;
  //
  //           if (res.success) {
  //             $rootScope.$broadcast('disposalChangeables:deleted', res.object);
  //
  //             if (utils.removeFromList(item, service.list)) {
  //               $rootScope.$broadcast('disposalChangeables:list:updated', res.object);
  //             }
  //
  //             deferred.resolve(item);
  //
  //           } else {
  //             deferred.reject(false);
  //           }
  //         }, function error(error) {
  //           deferred.reject(error.data);
  //         });
  //
  //         return deferred.promise;
  //       },
  //
  //       setCurrent: function (object) {
  //         service.current = object;
  //         $rootScope.$broadcast('disposalChangeables:current:changed', service.current);
  //       },
  //
  //       resetCurrent: function () {
  //         service.current = null;
  //         $rootScope.$broadcast('disposalChangeables:current:changed', service.current);
  //       }
  //     };
  //
  //     return service;
  //   }
  // )

  .factory('DisposalChangeable',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {
      var _resource = 'disposal-changeable';

      var DisposalChangeable = function (form_type) {
        Model.apply(this, arguments);

        this.name = '';
        this.tax_preference = '';
        this.information = '';
        this.disposal_proceeds = [];

        this.form_type = form_type;
        this.tax_year_id = TaxYear.current.id;
      };

      // List reliefs
      DisposalChangeable.list = [];

      // get all DisposalChangeables
      DisposalChangeable.all = function () {
        return Model.all(_resource).then(function (response) {
          DisposalChangeable.list = [];
          for (var i in response) {
            var instance = new DisposalChangeable();
            instance.fill(response[i]);
            DisposalChangeable.list.push(instance);
          }
        });
      };

      // find by tax year
      DisposalChangeable.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;

        for (var i in DisposalChangeable.list) {
          var item = DisposalChangeable.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }

        if (!flag) {
          deferred.reject(false);
        }

        return deferred.promise;
      };

      //extend
      DisposalChangeable.prototype = new Model();


      // fill
      DisposalChangeable.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      DisposalChangeable.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            DisposalChangeable.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      DisposalChangeable.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in DisposalChangeable.list) {
            if (DisposalChangeable.list[i].id === self.id) {
              DisposalChangeable.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      // function delete an instance self-employed
      DisposalChangeable.prototype.addDisposal = function () {
        var self = this;
        var modalInstance = $uibModal.open({
          templateUrl: 'modules/tax-return/forms/disposal-changeable/disposal-asset/disposal-asset.tpl.html',
          controller: 'DisposalAssetController',
          resolve: {
            disposalProceeds: function () {
              return self.disposal_proceeds;
            }
          }
        });
        modalInstance.result.then(function (res) {
          self.disposal_proceeds = res;
        });
        return modalInstance;
      };

      //override
      return DisposalChangeable;
    });
