angular.module('resources.customers', [])
  .factory('Customer',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, Country) {
      var _resource = 'customer';
      var Customer = function () {
        Model.apply(this, arguments);

        this.user_id = -1;
        this.country_id = 77;//uk
        this.country = utils.populateById(77, Country.list);//uk
        this.company_name = '';
        this.description = '';
        this.address = '';
        this.address_line1 = '';
        this.address_line2 = '';
        this.city = '';
        this.postcode = '';
        this.telephone = '';
        this.email = '';
        this.contact = '';
        this.created = '';
        this.modified = '';
      };

      // List customers
      Customer.list = [];

      // get all Customers
      Customer.all = function () {
        return Model.all(_resource).then(function (response) {
          Customer.list = [];
          for (var i in response) {
            var instance = new Customer();
            instance.fill(response[i]);
            Customer.list.push(instance);
          }
        });
      };

      //extend
      Customer.prototype = new Model();

      // fill
      Customer.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      Customer.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/customer/new/customer.tpl.html',
          controller: 'CreateCustomerController',
          size: 'md',
          resolve: {
            title: function () {
              return "Create Customer";
            },
            $customer: function () {
              return self;
            }
          }
        });
      };

      // method get
      Customer.prototype.get = function () {
        this.resource = _resource;
        return Model.prototype.save.apply(this, arguments);
      };

      // function save include create, update
      Customer.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            Customer.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      Customer.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in Customer.list) {
            if (Customer.list[i].id === self.id) {
              Customer.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      // send to client
      Customer.prototype.send = function (params) {

      };

      // view pdf
      Customer.prototype.exportPdf = function (params) {

      };

      //override
      return Customer;
    });
