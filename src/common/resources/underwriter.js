angular.module('resources.underwriters', [])

  .factory('Underwriter',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {

      var _resource = 'underwriter';

      var Underwriter = function () {
        Model.apply(this, arguments);

        this.LUN1 = 0;
        this.LUN2 = 0;
        this.LUN3 = 0;
        this.LUN4 = 0;
        this.LUN5 = 0;
        this.LUN6 = 0;
        this.LUN7 = 0;
        this.LUN8 = 0;
        this.LUN9 = 0;
        this.LUN10 = 0;
        this.LUN11 = 0;
        this.LUN12 = 0;
        this.LUN13 = 0;
        this.LUN14 = 0;
        this.LUN15 = 0;
        this.LUN16 = 0;
        this.LUN17 = 0;
        this.LUN18 = 0;
        this.LUN19 = 0;
        this.LUN20 = 0;
        this.LUN21 = 0;
        this.LUN22 = 0;
        this.LUN23 = 0;
        this.LUN24 = 0;
        this.LUN25 = 0;
        this.LUN26 = 0;
        this.LUN27 = 0;
        this.LUN28 = 0;
        this.LUN29 = 0;
        this.LUN30 = 0;
        this.LUN31 = 0;
        this.LUN32 = 0;
        this.LUN33 = 0;
        this.LUN34 = 0;
        this.LUN35 = 0;
        this.LUN36 = 0;
        this.LUN37 = 0;
        this.LUN38 = 0;
        this.LUN39 = 0;
        this.LUN40 = 0;
        this.LUN41 = 0;
        this.LUN42 = 0;
        this.LUN43 = 0;
        this.LUN44 = 0;
        this.LUN45 = 0;
        this.LUN46 = 0;
        this.LUN47 = 0;
        this.LUN48 = 0;
        this.LUN49 = 0;
        this.LUN50 = 0;
        this.LUN51 = 0;
        this.LUN52 = 0;
        this.LUN53 = 0;
        this.LUN54 = 0;
        this.LUN55 = 0;
        this.LUN56 = 0;
        this.LUN57 = 0;
        this.LUN58 = 0;
        this.LUN59 = 0;
        this.LUN60 = 0;
        this.LUN61 = 0;
        this.LUN62 = 0;
        this.LUN63 = 0;
        this.LUN64 = 0;
        this.LUN65 = 0;
        this.LUN66 = '';

        this.form_type = 19;
        this.tax_year_id = TaxYear.current.id;
      };

      // List reliefs
      Underwriter.list = [];

      // get all Underwriters
      Underwriter.all = function () {
        return Model.all(_resource).then(function (response) {
          Underwriter.list = [];
          for (var i in response) {
            var instance = new Underwriter();
            instance.fill(response[i]);
            Underwriter.list.push(instance);
          }
        });
      };

      // find by tax year
      Underwriter.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in Underwriter.list) {
          var item = Underwriter.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      Underwriter.prototype = new Model();

      // fill
      Underwriter.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      Underwriter.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            Underwriter.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      Underwriter.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in Underwriter.list) {
            if (Underwriter.list[i].id === self.id) {
              Underwriter.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return Underwriter;
    });
