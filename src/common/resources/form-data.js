angular.module('resources.form-data', [])

  .factory('formData',
    function (apiService, utils, $rootScope, $q) {

      // resource is resource connect to api
      var resource = 'form';

      var service = {

        current: null,
        list: null,


        /**
         * fetch data
         *
         * @param year
         * @returns {deferred.promise|{then, catch, finally}}
         */
        fetch: function (year) {
          var deferred = $q.defer();

          if (service.list != null && service.list.length > 0) {
            deferred.resolve(service.list);
          } else {
            apiService.find(resource, year).get().then(function (res) {
              res = res.data;

              if (res.success) {
                service.list = res.data;
                deferred.resolve(res.data);
              } else {
                deferred.resolve(list);
              }
            });
          }

          return deferred.promise;
        }
      };

      return service;
    }
  );
