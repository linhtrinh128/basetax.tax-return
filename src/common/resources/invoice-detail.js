angular.module('resources.invoiceDetails', [])
  .factory('invoiceDetails',
    function (apiService, utils, $rootScope, $q) {
      // resource is resource connect to api
      var resource = 'invoice-detail';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        findByTaxYear: function (year) {
          var deferred = $q.defer();

          var list = [];

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
                list.push(service.list[index]);
              }
            }

            deferred.resolve(list);
          }

          return deferred.promise;
        },

        find: function (id) {
          var deferred = $q.defer();

          if (utils.checkIfLoad(id, service.list)) {
            deferred.resolve(item);
          } else {
            apiService.get(resource, id).then(function (res) {
              res = res.data;

              if (res.data != null) {
                if (utils.appendInList(res.data, service.list)) {
                  $rootScope.$broadcast('invoiceDetails:list:updated', service.list);
                }

                $rootScope.$broadcast('invoiceDetails:created', res.data);

                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);

            });
          }

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              $rootScope.$broadcast('invoiceDetails:created', res.data);

              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('invoiceDetails:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('invoiceDetails:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              $rootScope.$broadcast('invoiceDetails:deleted', res.object);

              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('invoiceDetails:list:updated', res.object);
              }

              deferred.resolve(item);

            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('invoiceDetails:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('invoiceDetails:current:changed', service.current);
        }
      };

      return service;
    }
  )

  .factory('InvoiceDetail',
    function (Product) {
      var _resource = 'invoice-detail';
      this.list = null;

      var InvoiceDetail = function (invoice_id) {
        this.invoice_id = invoice_id;
        this.product_id = -1;
        this.product = new Product();
        this.quantity = 1;
        this.amount = 0;
        this.vat = 0;
        this.description = '';
      };

      InvoiceDetail.prototype.setProduct = function (Product) {
        var self = this;
        this.product = Product;
        this.description = Product.description;
      };

      //override
      return InvoiceDetail;
    });
