angular.module('resources.suppliers', [])

  .factory('suppliers',
    function (apiService, utils, $rootScope, $q) {
      // resource is resource connect to api
      var resource = 'supplier';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        findByTaxYear: function (year) {
          var deferred = $q.defer();

          var list = [];

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
                list.push(service.list[index]);
              }
            }

            deferred.resolve(list);
          }

          return deferred.promise;
        },

        find: function (id) {
          var deferred = $q.defer();

          if (utils.checkIfLoad(id, service.list)) {
            deferred.resolve(item);
          } else {
            apiService.get(resource, id).then(function (res) {
              res = res.data;

              if (res.data != null) {
                if (utils.appendInList(res.data, service.list)) {
                  $rootScope.$broadcast('suppliers:list:updated', service.list);
                }

                $rootScope.$broadcast('suppliers:created', res.data);

                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);

            });
          }

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              $rootScope.$broadcast('suppliers:created', res.data);

              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('suppliers:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('suppliers:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              $rootScope.$broadcast('suppliers:deleted', res.object);

              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('suppliers:list:updated', res.object);
              }

              deferred.resolve(item);

            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        search: function (options) {
          var deferred = $q.defer();

          var list = [];
          var results = [];

          if (!options.type) {
            options.type = 'all';
          }

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(options.taxYear.id)) {
                list.push(service.list[index]);
              }
            }

            switch (options.type) {
              case 'search':
              {
                for (index in list) {
                  if (list[index].name && list[index].name.toLowerCase().indexOf(options.text.toLowerCase()) !== -1) {
                    results.push(list[index]);
                  }
                }

                break;
              }
              default:
              {
                results = list;
                break;
              }
            }
          }

          deferred.resolve(results);

          return deferred.promise;
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('suppliers:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('suppliers:current:changed', service.current);
        }
      };

      return service;
    }
  )

  .factory('Supplier',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, Country) {
      var _resource = 'supplier';
      var Supplier = function () {
        Model.apply(this, arguments);

        this.user_id = -1;
        this.country_id = 77;//uk
        this.country = utils.populateById(77, Country.list);//uk
        this.company_name = '';
        this.description = '';
        this.address = '';
        this.address_line1 = '';
        this.address_line2 = '';
        this.city = '';
        this.postcode = '';
        this.telephone = '';
        this.email = '';
        this.contact = '';
        this.created = '';
        this.modified = '';
      };

      // List customers
      Supplier.list = [];

      // get all
      Supplier.all = function () {
        return Model.all(_resource).then(function (response) {
          Supplier.list = [];
          for (var i in response) {
            var instance = new Supplier();
            instance.fill(response[i]);
            Supplier.list.push(instance);
          }
        });
      };

      //extend
      Supplier.prototype = new Model();

      // fill
      Supplier.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      Supplier.prototype.show = function () {
        var self = this;
        var params = {
          templateUrl: 'modules/supplier/new/supplier/supplier.tpl.html',
          controller: 'CreateSupplierController',
          size: 'md',
          resolve: {
            $supplier: function () {
              return self;
            }
          }
        };
        return $uibModal.open(params);
      };

      // method get
      Supplier.prototype.get = function () {
        this.resource = _resource;
        return Model.prototype.save.apply(this, arguments);
      };

      // function save include create, update
      Supplier.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            Supplier.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      Supplier.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in Supplier.list) {
            if (Supplier.list[i].id === self.id) {
              Supplier.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };


      return Supplier;
    });
