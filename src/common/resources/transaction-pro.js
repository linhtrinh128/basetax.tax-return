angular.module('resources.transactions-pro', [])

  .factory('transactionsPro', function (apiService, utils, $rootScope, $q) {
    
    // resource is resource connect to api
    var resource = 'transaction-pro';


    var service = {
      current: null,
      list: null,

      // find all
      all: function () {
        var deferred = $q.defer();

        apiService.all(resource).then(function (res) {
          res = res.data;

          if (res.data != null) {
            service.list = [];

            service.list = utils.sort(res.data.data, 'modified', true);

            deferred.resolve(service.list);
          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      findByTaxYear: function (year) {
        var deferred = $q.defer();

        var list = [];

        if (angular.isArray(service.list)) {
          for (var index in service.list) {
            if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
              list.push(service.list[index]);
            }
          }

          deferred.resolve(list);
        }

        return deferred.promise;
      },

      find: function (id) {
        var deferred = $q.defer();

        if (utils.checkIfLoad(id, service.list)) {
          deferred.resolve(item);
        } else {
          apiService.get(resource, id).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('transactionsPro:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);

          });
        }

        return deferred.promise;
      },

      create: function (object) {
        var deferred = $q.defer();

        apiService.post(resource, object).then(function (res) {
          res = res.data;

          if (res.data != null) {
            $rootScope.$broadcast('transactionsPro:created', res.data);

            if (utils.appendInList(res.data, service.list)) {
              $rootScope.$broadcast('transactionsPro:list:updated', service.list);
            }

            deferred.resolve(res.data);
          } else {
            deferred.reject(false);
          }

        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      update: function (object) {
        var deferred = $q.defer();

        apiService.customPUT(resource, object.id, object).then(function (res) {
          res = res.data;

          if (res.data != null) {
            if (utils.updateInList(res.data, service.list)) {
              $rootScope.$broadcast('transactionsPro:list:updated', service.list);
            }

            deferred.resolve(res.data);
          } else {
            deferred.reject(false);
          }

        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      delete: function (item) {
        var deferred = $q.defer();

        apiService.remove(resource, item.id).then(function (res) {
          res = res.data;

          if (res.success) {
            $rootScope.$broadcast('transactionsPro:deleted', res.object);

            if (utils.removeFromList(item, service.list)) {
              $rootScope.$broadcast('transactionsPro:list:updated', res.object);
            }

            deferred.resolve(item);

          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      setCurrent: function (object) {
        service.current = object;
        $rootScope.$broadcast('transactionsPro:current:changed', service.current);
      },

      resetCurrent: function () {
        service.current = null;
        $rootScope.$broadcast('transactionsPro:current:changed', service.current);
      }
    };

    return service;
  });