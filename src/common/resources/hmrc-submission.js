angular.module('resources.hmrc-submissions', [])
  .factory('HmrcSubmissions',
    function (apiService, utils, $rootScope, $q) {

      var service = {

        // generate xml
        generateXml: function (id) {
          var deferred = $q.defer();
          apiService.all('generate-xml/' + id)
            .then(function (res) {
              res = res.data;
              if (res.data != null) {
                deferred.resolve(res.data.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });
          return deferred.promise;
        },

        //getParams
        getParams: function (id) {
          var deferred = $q.defer();
          apiService.all('params/' + id)
            .then(function (res) {
              res = res.data;
              if (res.data != null) {
                deferred.resolve(res.data.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });
          return deferred.promise;
        },

        // submit
        submit: function (xml) {
          var deferred = $q.defer();
          apiService.post('submit', xml)
            .then(function (res) {
              res = res.data;
              if (res.data != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });
          return deferred.promise;
        },

        // poll
        poll: function (xml) {
          var deferred = $q.defer();
          apiService.post('poll', xml)
            .then(function (res) {
              res = res.data;
              if (res.data != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });
          return deferred.promise;
        }
      };

      return service;
    });