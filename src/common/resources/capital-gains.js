angular.module('resources.capital-gains', [])

  .factory('CapitalGain',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {

      var _resource = 'capital-gains';

      var CapitalGain = function () {
        Model.apply(this, arguments);

        this.id = null;
        this.user_id = null;
        this.tax_year_id = TaxYear.current.id;

        // CapitalGain
        this.CGT3 = 0;
        this.CGT4 = 0;
        this.CGT5 = 0;
        this.CGT6 = 0;
        this.CGT7 = 0;
        this.CGT8 = 0;
        this.CGT9 = 0;
        this.CGT10 = 0;
        this.CGT11 = 0;
        this.CGT12 = 0;
        this.CGT13 = 0;
        this.CGT14 = 0;
        this.CGT15 = 0;
        this.CGT16 = 0;
        this.CGT17 = 0;
        this.CGT18 = 0;
        this.CGT19 = 0;
        this.CGT20 = 0;
        this.CGT21 = 0;
        this.CGT22 = 0;
        this.CGT23 = 0;
        this.CGT24 = 0;
        this.CGT25 = 0;
        this.CGT26 = 0;
        this.CGT27 = 0;
        this.CGT28 = 0;
        this.CGT29 = 0;
        this.CGT30 = 0;
        this.CGT31 = 0;
        this.CGT32 = 0;
        this.CGT33 = 0;
        this.CGT34 = 0;
        this.CGT35 = 0;
        this.CGT36 = 0;
        this.CGT37 = '';
      };

      // List other main incomes
      CapitalGain.list = [];

      // get all CapitalGains
      CapitalGain.all = function () {
        return Model.all(_resource).then(function (response) {
          CapitalGain.list = [];
          for (var i in response) {
            var instance = new CapitalGain();
            instance.fill(response[i]);
            CapitalGain.list.push(instance);
          }
        });
      };

      // find by tax year
      CapitalGain.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;

        for (var i in CapitalGain.list) {
          var item = CapitalGain.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }

        if (!flag) {
          deferred.reject(false);
        }

        return deferred.promise;
      };

      //extend
      CapitalGain.prototype = new Model();

      // fill
      CapitalGain.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      CapitalGain.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            CapitalGain.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      CapitalGain.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in CapitalGain.list) {
            if (CapitalGain.list[i].id === self.id) {
              CapitalGain.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      return CapitalGain;
    });
