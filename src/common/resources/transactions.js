angular.module('resources.transactions', [])

	.factory('transactions', function (apiService, utils, $rootScope, $q) {
		// resource is resource connect to api
		var resource = 'transaction';

		var service = {
			current: null,
			list: null,
			count: null,

			// find all
			all: function () {
				var deferred = $q.defer();

				apiService.all(resource).then(function (res) {
					res = res.data;

					if (res.data != null) {
						service.list = res.data.data;
						deferred.resolve(service.list);
					} else {
						deferred.reject(false);
					}
				}, function error(error) {
					deferred.reject(error.data);
				});

				return deferred.promise;
			},

			get: function (query) {
				var deferred = $q.defer();

				apiService.all(resource, query).then(function (res) {
					res = res.data;

					if (res.data != null) {
						service.list = res.data.data;
						deferred.resolve(service.list);
					} else {
						deferred.reject(false);
					}
				}, function error(error) {
					deferred.reject(error.data);
				});

				return deferred.promise;
			},

			findByTaxYear: function (year) {
				var deferred = $q.defer();

				var list = [];

				if (angular.isArray(service.list)) {
					for (var index in service.list) {
						if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
							list.push(service.list[index]);
						}
					}

					deferred.resolve(list);
				}

				return deferred.promise;
			},

			find: function (id) {
				var deferred = $q.defer();

				if (utils.checkIfLoad(id, service.list)) {
					deferred.resolve(item);
				} else {
					apiService.get(resource, id).then(function (res) {
						res = res.data;

						if (res.data != null) {
							if (utils.appendInList(res.data, service.list)) {
								$rootScope.$broadcast('transactions:list:updated', service.list);
							}

							deferred.resolve(res.data);
						} else {
							deferred.reject(false);
						}
					}, function error(error) {
						deferred.reject(error.data);

					});
				}

				return deferred.promise;
			},

			create: function (object) {
				var deferred = $q.defer();

				apiService.post(resource, object).then(function (res) {
					res = res.data;

					if (res.data != null) {
						$rootScope.$broadcast('transactions:created', res.data);

						if (utils.appendInList(res.data, service.list)) {
							$rootScope.$broadcast('transactions:list:updated', service.list);
						}

						deferred.resolve(res.data);
					} else {
						deferred.reject(false);
					}

				}, function error(error) {
					deferred.reject(error.data);
				});

				return deferred.promise;
			},

			update: function (object) {
				var deferred = $q.defer();

				apiService.customPUT(resource, object.id, object).then(function (res) {
					res = res.data;

					if (res.data != null) {
						if (utils.updateInList(res.data, service.list)) {
							$rootScope.$broadcast('transactions:list:updated', service.list);
						}

						deferred.resolve(res.data);
					} else {
						deferred.reject(false);
					}

				}, function error(error) {
					deferred.reject(error.data);
				});

				return deferred.promise;
			},

			delete: function (item) {
				var deferred = $q.defer();

				apiService.remove(resource, item.id).then(function (res) {
					res = res.data;

					if (res.success) {
						$rootScope.$broadcast('transactions:deleted', res.object);

						if (utils.removeFromList(item, service.list)) {
							$rootScope.$broadcast('transactions:list:updated', res.object);
						}

						deferred.resolve(item);

					} else {
						deferred.reject(false);
					}
				}, function error(error) {
					deferred.reject(error.data);
				});

				return deferred.promise;
			},

			search: function (options) {
				var deferred = $q.defer();

				var list = [];
				var results = [];

				if (!options.type) {
					options.type = 'all';
				}

				if (angular.isArray(service.list)) {
					for (var index in service.list) {
						if (utils.getId(service.list[index].tax_year_id) === utils.getId(options.taxYear.id)) {
							list.push(service.list[index]);
						}
					}

					switch (options.type) {
						case 'search':
						{
							for (index in list) {
								if (list[index].name && list[index].name.toLowerCase().indexOf(options.text.toLowerCase()) !== -1) {
									results.push(list[index]);
								}
							}

							break;
						}
						default:
						{
							results = list;
							break;
						}
					}
				}

				deferred.resolve(results);

				return deferred.promise;
			},

			setCurrent: function (object) {
				service.current = object;
				$rootScope.$broadcast('transactions:current:changed', service.current);
			},

			resetCurrent: function () {
				service.current = null;
				$rootScope.$broadcast('transactions:current:changed', service.current);
			}
		};

		return service;
	})

	.factory('Transaction', function (apiService, $q) {
		var resource = 'transaction';

		var Transaction = function (record_type_id, tax_year_id) {
			this.id = null;
			this.record_type_id = record_type_id;
			this.tax_year_id = tax_year_id;
			this.payment_type_id = null;
			this.user_id = null;
			this.business_id = null;
			this.employer_id = null;
			this.property_id = null;
			this.customer_id = null;
			this.supplier_id = null;
			this.vehicle_type_id = null;
			this.sub_category_id = null;
			this.name = null;
			this.description = null;
			this.reference = null;
			this.amount = null;
			this.vat = null;
			this.cis_deduction = null;
			this.date = null;
			this.scan = null;
			this.notes = null;
			this.disallowable = null;
			this.claim_allowance = null;
			this.mileage = null;
			this.month = null;
			this.time_spent = null;
			this.number_of_residents = null;
			this.tax_salary = null;
			this.other_income = null;
			this.tax_taken_off = null;
			this.number_of_children = null;
			this.student_loan_notification = null;
			this.student_loan_maybe_repaid = null;
			this.p11d = null;
			this.recurring_id = null;
			this.foreign_currency_selected = null;
			this.foreign_currency_id = null;
			this.foreign_rate = null;
			this.foreign_amount = null;
		};

		Transaction.prototype.getData = function (id) {
			var self = this;
			apiService.get(resource, id).then(function (response) {
				var item = response.data.data;
				angular.forEach(self, function(value, key){
					self[key] = item[key];
				});
			}, function error(error) {
				console.log(error);
			});
		};

		return Transaction;
	});