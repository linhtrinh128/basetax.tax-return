angular.module('resources.import-transactions', [])

  .factory('importTransactions', function (apiService, $q) {

    // resource is resource connect to api
    var resource = 'import-transaction';

    var service = {
      list: null,

      // import
      import: function (data) {
        var deferred = $q.defer();
        apiService.post(resource, data)
          .then(function (res) {
            var item = res.data.data;
            deferred.resolve(item);
          }, function error(error) {
            deferred.reject(error.data);
          });
        return deferred.promise;
      }

      // find all
      // all: function () {
      //   var deferred = $q.defer();
      //
      //   apiService.all(resource).then(function (res) {
      //     res = res.data;
      //
      //     if (res.data != null) {
      //       service.list = [];
      //
      //       service.list = utils.sort(res.data.data, 'modified', true);
      //
      //       deferred.resolve(service.list);
      //     } else {
      //       deferred.reject(false);
      //     }
      //   }, function error(error) {
      //     deferred.reject(error.data);
      //   });
      //
      //   return deferred.promise;
      // },

      // update: function (object) {
      //   var deferred = $q.defer();
      //
      //   apiService.customPUT(resource, object.id, object).then(function (res) {
      //     res = res.data;
      //
      //     if (res.data != null) {
      //       if (utils.updateInList(res.data, service.list)) {
      //         $rootScope.$broadcast('importTransactions:list:updated', service.list);
      //       }
      //
      //       deferred.resolve(res.data);
      //     } else {
      //       deferred.reject(false);
      //     }
      //
      //   }, function error(error) {
      //     deferred.reject(error.data);
      //   });
      //
      //   return deferred.promise;
      // },

      /**
       * delete import transaction
       *
       * @param item
       * @returns {deferred.promise|{then, catch, finally}}
       */
      // delete: function (item) {
      //   var deferred = $q.defer();
      //
      //   apiService.remove(resource, item.id).then(function (res) {
      //     res = res.data;
      //
      //     if (res.success) {
      //       $rootScope.$broadcast('importTransactions:deleted', res.object);
      //
      //       if (utils.removeFromList(item, service.list)) {
      //         $rootScope.$broadcast('importTransactions:list:updated', res.object);
      //       }
      //
      //       deferred.resolve(item);
      //
      //     } else {
      //       deferred.reject(false);
      //     }
      //   }, function error(error) {
      //     deferred.reject(error.data);
      //   });
      //
      //   return deferred.promise;
      // },

      /**
       * move to transaction
       *
       * @param item
       * @returns {deferred.promise|{then, catch, finally}}
       */
      // move: function (object) {
      //   var deferred = $q.defer();
      //
      //   apiService.post(resource + '/move', object).then(function (res) {
      //     res = res.data;
      //     if (res.data != null) {
      //       $rootScope.$broadcast('transactions:created', res.data);
      //       if (transactions.list = utils.appendInList(res.data, transactions.list)) {
      //         $rootScope.$broadcast('transactions:list:updated', transactions.list);
      //       }
      //       if (utils.removeFromList(object, service.list)) {
      //         $rootScope.$broadcast('importTransactions:list:updated', res.object);
      //       }
      //       deferred.resolve(res.data);
      //     } else {
      //       deferred.reject(false);
      //     }
      //   }, function error(error) {
      //     deferred.reject(error.data);
      //   });
      //
      //   return deferred.promise;
      // },

      /**
       * import transaction from excel
       * @param file
       * @returns {deferred.promise|{then, catch, finally}}
       */
      // upload: function (file) {
      //   var deferred = $q.defer();
      //
      //   var fd = new FormData();
      //   fd.append('file', file);
      //
      //   apiService.findAll(resource + '/upload')
      //     .withHttpConfig({transformRequest: angular.identity})
      //     .post(fd, undefined, {'Content-Type': undefined})
      //     .then(function (res) {
      //         if (res.data != null) {
      //           var transactions = res.data.data;
      //           if (transactions.length > 0) {
      //             service.list = transactions;
      //             $rootScope.$broadcast('importTransactions:list:updated', service.list);
      //           }
      //           deferred.resolve(res.data.data);
      //         } else {
      //           deferred.reject(false);
      //         }
      //       },
      //       function error(error) {
      //         deferred.reject(error.data);
      //       }
      //     );
      //
      //   return deferred.promise;
      // }
    };

    return service;
  })
;