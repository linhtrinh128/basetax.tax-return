angular.module('resources.reliefs', [])

  .factory('reliefs', function (apiService, utils, $rootScope, $q) {
    // resource is resource connect to api
    var resource = 'relief';

    var service = {
      current: null,
      list: null,

      // find all
      all: function () {
        var deferred = $q.defer();

        apiService.all(resource).then(function (res) {
          res = res.data;

          if (res.data != null) {
            service.list = [];

            service.list = utils.sort(res.data.data, 'modified', true);

            deferred.resolve(service.list);
          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },


      find: function (id) {
        var deferred = $q.defer();

        if (utils.checkIfLoad(id, service.list)) {
          deferred.resolve(item);
        } else {
          apiService.get(resource, id).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('reliefs:list:updated', service.list);
              }

              $rootScope.$broadcast('reliefs:created', res.data);

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);

          });
        }

        return deferred.promise;
      },

      create: function (object) {
        var deferred = $q.defer();

        apiService.post(resource, object).then(function (res) {
          res = res.data;

          if (res.data != null) {
            $rootScope.$broadcast('reliefs:created', res.data);

            if (utils.appendInList(res.data, service.list)) {
              $rootScope.$broadcast('reliefs:list:updated', service.list);
            }

            deferred.resolve(res.data);
          } else {
            deferred.reject(false);
          }

        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      update: function (object) {
        var deferred = $q.defer();

        apiService.customPUT(resource, object.id, object).then(function (res) {
          res = res.data;

          if (res.data != null) {
            if (utils.updateInList(res.data, service.list)) {
              $rootScope.$broadcast('reliefs:list:updated', service.list);
            }

            deferred.resolve(res.data);
          } else {
            deferred.reject(false);
          }

        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      delete: function (item) {
        var deferred = $q.defer();

        apiService.remove(resource, item.id).then(function (res) {
          res = res.data;

          if (res.success) {
            $rootScope.$broadcast('reliefs:deleted', res.object);

            if (utils.removeFromList(item, service.list)) {
              $rootScope.$broadcast('reliefs:list:updated', res.object);
            }

            deferred.resolve(item);

          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      setCurrent: function (object) {
        service.current = object;
        $rootScope.$broadcast('reliefs:current:changed', service.current);
      },

      resetCurrent: function () {
        service.current = null;
        $rootScope.$broadcast('reliefs:current:changed', service.current);
      }
    };

    return service;
  })

  .factory('Relief',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {
      
      var _resource = 'relief';
      
      var Relief = function () {
        Model.apply(this, arguments);

        this.REL5 = null;
        this.REL6 = null;
        this.REL7 = null;
        this.REL8 = null;
        this.REL9 = null;
        this.REL10 = null;
        this.REL11 = null;
        this.REL12 = null;
        this.AOR2 = null;
        this.AOR3 = null;
        this.AOR4 = null;
        this.AOR5 = null;
        this.AOR6 = null;
        this.AOR7 = null;
        this.AOR8 = null;
        this.AOR9 = null;
        this.AOR10 = null;
        this.REL1 = null;
        this.REL2 = null;
        this.REL3 = null;
        this.REL4 = null;

        this.tax_year_id = TaxYear.current.id;
      };

      // List reliefs
      Relief.list = [];

      // get all Reliefs
      Relief.all = function () {
        return Model.all(_resource).then(function (response) {
          Relief.list = [];
          for (var i in response) {
            var instance = new Relief();
            instance.fill(response[i]);
            Relief.list.push(instance);
          }
        });
      };

      // find by tax year
      Relief.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in Relief.list) {
          var item = Relief.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      Relief.prototype = new Model();

      // fill
      Relief.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      Relief.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            Relief.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      Relief.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in Relief.list) {
            if (Relief.list[i].id === self.id) {
              Relief.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return Relief;
    });