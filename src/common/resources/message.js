angular.module('resources.message', [])

    .factory('Messages', function ($rootScope, $q, apiService) {

        var service = {
            // send message
            send: function (object) {
                var deferred = $q.defer();

                var request = apiService.post('messages', object);

                request.then(function success(res) {
                    if (res.data !== null) {
                        res = res.data;
                        deferred.resolve(res);
                    }
                }, function error(error) {
                    deferred.reject(error.data);
                });

                return deferred.promise;
            }
        };

        return service;
    });
