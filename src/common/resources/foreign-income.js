angular.module('resources.foreign-incomes', [])

  .factory('ForeignIncome',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {

      var _resource = 'foreign-incomes';

      var ForeignIncome = function () {
        Model.apply(this, arguments);

        this.tax_year_id = TaxYear.current.id;

        // Foreign Income
        this.FOR1 = 0;
        this.FOR2 = 0;
        this.FOR3 = 0;
        this.FOR4 = 0;
        this.FOR5 = 0;
        this.FOR6 = 0;
        this.FOR7 = 0;
        this.FOR8 = 0;
        this.FOR9 = 0;
        this.FOR10 = 0;
        this.FOR11 = 0;
        this.FOR12 = 0;
        this.FOR13 = 0;
        this.FOR14 = 0;
        this.FOR15 = 0;
        this.FOR16 = 0;
        this.FOR17 = 0;
        this.FOR18 = 0;
        this.FOR19 = 0;
        this.FOR20 = 0;
        this.FOR21 = 0;
        this.FOR22 = 0;
        this.FOR23 = 0;
        this.FOR24 = 0;
        this.FOR25 = 0;
        this.FOR26 = 0;
        this.FOR27 = 0;
        this.FOR28 = 0;
        this.FOR29 = 0;
        this.FOR30 = 0;
        this.FOR31 = 0;
        this.FOR32 = 0;
        this.FOR33 = 0;
        this.FOR34 = '';
        this.FOR35 = 0;
        this.FOR36 = '';
        this.FOR37 = 0;
        this.FOR38 = 0;
        this.FOR39 = 0;
        this.FOR40 = 0;
        this.FOR41 = 0;
        this.FOR42 = 0;
        this.FOR43 = 0;
        this.FOR44 = '';
        this.FOR45 = 0;
        this.FOR46 = 0;
        this.FORA = '';
        this.FORB = '';
        this.FORC = 0;
        this.FORE = 0;
        this.FORF = 0;
        this.FOR4A = '';
        this.FOR4B = 0;
        this.FOR4C = 0;
        this.FOR4D = 0;
        this.FOR4E = 0;
        this.FOR4F = 0;
        this.FOR6A = '';
        this.FOR6B = 0;
        this.FOR6C = 0;
        this.FOR6D = 0;
        this.FOR6E = 0;
        this.FOR6F = 0;
        this.FOR9A = '';
        this.FOR9B = 0;
        this.FOR9C = 0;
        this.FOR9D = 0;
        this.FOR9E = 0;
        this.FOR9F = 0;
        this.FOR11A = '';
        this.FOR11B = 0;
        this.FOR11C = 0;
        this.FOR11D = 0;
        this.FOR11E = 0;
        this.FOR11F = 0;
        this.FOR13A = '';
        this.FOR13B = 0;
        this.FOR13C = 0;
        this.FOR13D = 0;
        this.FOR13E = 0;
        this.FOR13F = 0;
        this.FOR30A = '';
        this.FOR30B = 0;
        this.FOR30C = 0;
        this.FOR30D = 0;
        this.FOR30E = 0;
        this.FOR30F = 0;
      };

      // List other main incomes
      ForeignIncome.list = [];

      // get all ForeignIncomes
      ForeignIncome.all = function () {
        return Model.all(_resource).then(function (response) {
          ForeignIncome.list = [];
          for (var i in response) {
            var instance = new ForeignIncome();
            instance.fill(response[i]);
            ForeignIncome.list.push(instance);
          }
        });
      };

      // find by tax year
      ForeignIncome.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;

        for (var i in ForeignIncome.list) {
          var item = ForeignIncome.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }

        if (!flag) {
          deferred.reject(false);
        }

        return deferred.promise;
      };

      //extend
      ForeignIncome.prototype = new Model();

      // fill
      ForeignIncome.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      ForeignIncome.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            ForeignIncome.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      ForeignIncome.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in ForeignIncome.list) {
            if (ForeignIncome.list[i].id === self.id) {
              ForeignIncome.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      return ForeignIncome;
    });
