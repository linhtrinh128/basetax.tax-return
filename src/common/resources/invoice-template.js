angular.module('resources.invoiceTemplates', [])
  .factory('invoiceTemplates',
    function (apiService, utils, $rootScope, $q) {
      // resource is resource connect to api
      var resource = 'invoice-template';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        findByTaxYear: function (year) {
          var deferred = $q.defer();

          var list = [];

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
                list.push(service.list[index]);
              }
            }

            deferred.resolve(list);
          }

          return deferred.promise;
        },

        find: function (id) {
          var deferred = $q.defer();

          if (utils.checkIfLoad(id, service.list)) {
            deferred.resolve(item);
          } else {
            apiService.get(resource, id).then(function (res) {
              res = res.data;

              if (res.data != null) {
                if (utils.appendInList(res.data, service.list)) {
                  $rootScope.$broadcast('invoiceTemplates:list:updated', service.list);
                }

                $rootScope.$broadcast('invoiceTemplates:created', res.data);

                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);

            });
          }

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              $rootScope.$broadcast('invoiceTemplates:created', res.data);

              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('invoiceTemplates:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('invoiceTemplates:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              $rootScope.$broadcast('invoiceTemplates:deleted', res.object);

              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('invoiceTemplates:list:updated', res.object);
              }

              deferred.resolve(item);

            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('invoiceTemplates:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('invoiceTemplates:current:changed', service.current);
        }
      };

      return service;
    }
  )
  .factory('InvoiceTemplate',
    function (Model, $uibModal) {
      var _resource = 'invoice-template';
      this.list = null;

      var InvoiceTemplate = function () {
        this.user_id = -1;
        this.invoice_id = -1;
        this.product_id = -1;
        this.business_id = -1;
        this.business = '';
        this.name = '';
        this.address = '';
        this.vat = 0;
        this.telephone = '';
        this.email ='';
        this.scan = '';
        this.bank_name = '';
        this.sort_code = '';
        this.account_number = 0;
        this.with_vat = false;
        this.created = '';
        this.modified = '';
      };

      // get all invoice templates
      InvoiceTemplate.all = function () {
        return Model.all(_resource).then(function (response) {
          InvoiceTemplate.list = [];
          angular.forEach(response, function (data) {
            var instance = new InvoiceTemplate();
            instance.fill(data);
            InvoiceTemplate.list.push(instance);
          });
        });
      };

      // fill
      InvoiceTemplate.prototype.fill = function (data) {
        Model.prototype.fill.apply(this, arguments);
      };

      // function show
      InvoiceTemplate.prototype.show = function () {
        var self = this;
        var options = {
          templateUrl: 'modules/invoice/invoice-template/new/invoice-template/invoice-template.tpl.html',
          controller: 'CreateInvoiceTemplateController',
          resolve: {
            title: function () {
              return "Create Invoice template";
            },
            $invoiceTemplate: function(){
              return self;
            }
          }
        };
        return $uibModal.open(options);
      };

      // method get
      InvoiceTemplate.prototype.get = function () {
        this.resource = _resource;
        return Model.prototype.get.apply(this, arguments);
      };

      // function save include create, update
      InvoiceTemplate.prototype.save = function () {
        var self = this;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        return originSave;
      };

      // function delete an instance self-employed
      InvoiceTemplate.prototype.delete = function () {
        this.resource = _resource;
        return Model.prototype.delete.apply(this, arguments);
      };

      return InvoiceTemplate;
    });