angular.module('resources.invoices', [])
  .factory('Invoice',
    function (Model, apiService, $q, utils, RESOURCES, InvoiceDetail, InvoiceTemplate, InvoiceExtra, $uibModal, Customer) {
      var _resource = 'invoice';
      this.list = null;

      var Invoice = function () {
        Model.apply(this, arguments);

        this.invoice_no = generateInvoiceNo(Invoice.list);
        this.customer = new Customer();
        this.customer_id = 0;
        this.business = {};
        this.business_id = '';
        this.invoice_details = [new InvoiceDetail()];
        this.extra = [];
        this.payment_type_id = 1; // 1 -> Card
        this.invoice_template = new InvoiceTemplate();
        this.invoice_template_id = -1;
        this.date = utils.shortDate(new Date());
        this.due_on = utils.shortDate(new Date());
        this.due_selection = RESOURCES.DUE_ON_OPTIONS[0];
        this.notes = '';
        this.vat = '';
        this.amount = '';
        this.total = '';
        this.paid = 0;
        this.status = 'Queried';
        this.auto_create_income = 0;
        this.foreign_currency_selected = '';
        this.foreign_currency_id = '';
        this.foreign_amount = '';
        this.currency = {};
      };

      //generate invoice_no
      function generateInvoiceNo(list) {
        var max = 0;
        for (var i = 0; i < list.length; i++) {
          var invoice = list[i];
          var invoice_no = parseInt(invoice.invoice_no);
          if (invoice_no > max) {
            max = invoice_no;
          }
        }
        return max + 1;
      }

      // get all invoices
      Invoice.all = function () {
        return Model.all(_resource).then(function (response) {
          Invoice.list = [];
          angular.forEach(response, function (data) {
            var instance = new Invoice();
            instance.fill(data);

            // customer
            var customer = instance.customer;
            instance.customer = new Customer();
            instance.customer.fill(customer);

            Invoice.list.push(instance);
          });
        });
      };

      // export
      Invoice.export = function (params) {
        var self = this;
        var deferred = $q.defer();
        apiService.get(_resource + '/export', params)
          .then(function (response) {
            var item = response.data.data;
            deferred.resolve(item);
          }, function (error) {
            deferred.reject(error);
          });
        return deferred.promise;
      };

      //extend
      Invoice.prototype = new Model();

      // fill
      Invoice.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      Invoice.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/invoice/list/new/invoice.tpl.html',
          controller: 'CreateInvoiceController',
          size: 'lg',
          resolve: {
            title: function () {
              return "Create Invoice";
            },
            $invoice: function () {
              return self;
            }
          }
        });
      };

      // method get
      Invoice.prototype.get = function () {
        this.resource = _resource;
        return Model.prototype.save.apply(this, arguments);
      };

      // function save include create, update
      Invoice.prototype.save = function () {
        var self = this;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        return originSave;
      };

      // function delete an instance self-employed
      Invoice.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        return originDelete;
      };

      // send to client
      Invoice.prototype.send = function (params) {
        var self = this;
        var deferred = $q.defer();
        apiService.post(this.resource + '/' + this.id + '/send-to-client', params)
          .then(function (response) {
            var item = response.data.data;
            angular.extend(self, item);
            deferred.resolve(self);
          }, function error(error) {
            deferred.reject(error);
          });
        return deferred.promise;
      };

      // view pdf
      Invoice.prototype.exportPdf = function (params) {
        var self = this;
        var deferred = $q.defer();
        apiService.post(this.resource + '/' + this.id + '/export-pdf', params)
          .then(function (response) {
            var item = response.data.data;
            deferred.resolve(item);
          }, function (error) {
            deferred.reject(error);
          });
        return deferred.promise;
      };

      //override
      return Invoice;
    });