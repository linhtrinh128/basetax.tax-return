angular.module('resources.partner-trading', [])

  .factory('PartnerTrading',
    function (Model, $uibModal, $q, TaxYear) {

      var _resource = 'partner-trading';

      var PartnerTrading = function () {

        Model.apply(this, arguments);

        this.partnership_detail_id = 1;
        this.start_of_period = null;
        this.end_of_period = null;
        this.account_details_are_not_required = null;
        this.accounts_do_not_cover_period_from_last_accounting_date = null;
        this.change_of_accounting_date = null;
        this.do_figures_include_vat = null;
        this.is_this_trade_using_the_cash_basis = null;
        this.turnover = null;
        this.cost_of_sales = null;
        this.construction_industry_subcontractor_costs = null;
        this.other_direct_costs = null;
        this.gross_profit_loss = null;
        this.other_income_profits = null;
        this.employee_costs = null;
        this.premises_costs = null;
        this.repairs = null;
        this.general_administrative_expenses = null;
        this.motor_expenses = null;
        this.travel_and_subsistence = null;
        this.advertising_promotional_and_entertainment = null;
        this.legal_and_professional_costs = null;
        this.bad_debts = null;
        this.interest_and_alternative_finance_payments = null;
        this.other_finance_charges = null;
        this.depreciation_and_loss_profit_on_sale = null;
        this.other_expenses_including_partnership_charges = null;
        this.total_expenses = null;
        this.disallowable_construction_industry_subcontractor_costs = null;
        this.disallowable_other_direct_costs = null;
        this.disallowable_employee_costs = null;
        this.disallowable_premises_costs = null;
        this.disallowable_repairs = null;
        this.disallowable_general_administrative_expenses = null;
        this.disallowable_motor_expenses = null;
        this.disallowable_travel_and_subsistence = null;
        this.disallowable_advertising_promotional_and_entertainment = null;
        this.disallowable_legal_and_professional_costs = null;
        this.disallowable_bad_debts = null;
        this.disallowable_interest_and_alternative_finance_payments = null;
        this.disallowable_other_finance_charges = null;
        this.disallowable_depreciation_and_loss_profit_on_sale = null;
        this.disallowable_other_expenses_including_partnership_charges = null;
        this.total_disallowable_expenses = null;
        this.net_profit_loss = null;
        this.annual_investment_allowance = null;
        this.capital_allowances_on_cars_costing_more_than_12000 = null;
        this.capital_allowances_at_18 = null;
        this.capital_allowances_at_8 = null;
        this.agricultural_or_industrial_buildings_allowance = null;
        this.business_premises_renovation_allowance = null;
        this['100_and_other_enhanced_capital_allowances'] = null;
        this.total_capital_allowances = null;
        this.goods_etc_taken_for_personal_use_and_other_adjustments = null;
        this.total_additions = null;
        this.deductions_from_profit = null;
        this.total_deductions = null;
        this.net_taxable_profit_allowable_loss = null;
        this.adjustment_on_change_of_basis = null;
        this.construction_industry_deductions_from_contractors = null;
        this.tax_taken_off_trading_income = null;
        this.net_partnership_charges_for_taxyear = null;
        this.equipment_machinery_and_vehicle = null;
        this.other_fixed_assets = null;
        this.stock_and_work_in_progress = null;
        this.trade_debtors = null;
        this.bank_building_society_balances = null;
        this.cash_in_hand = null;
        this.total_assets = null;
        this.trade_creditors = null;
        this.loans_and_overdrafts = null;
        this.other_liabilities_and_accruals = null;
        this.total_liabilities = null;
        this.net_business_assets = null;
        this.balance_at_start_of_period = null;
        this.net_profit_or_loss = null;
        this.capital_introduced = null;
        this.drawings = null;
        this.balance_at_end_of_period = null;
        this.additional_information = null;

        this.tax_year_id = TaxYear.current.id;
      };

      // List
      PartnerTrading.list = [];

      // get all PartnerTradings
      PartnerTrading.all = function () {
        return Model.all(_resource).then(function (response) {
          PartnerTrading.list = [];
          for (var i in response) {
            var instance = new PartnerTrading();
            instance.fill(response[i]);
            PartnerTrading.list.push(instance);
          }
        });
      };

      // find by tax year
      PartnerTrading.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in PartnerTrading.list) {
          var item = PartnerTrading.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      PartnerTrading.prototype = new Model();

      // fill
      PartnerTrading.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      PartnerTrading.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/tax-return/forms/partnership/partnership.tpl.html',
          controller: 'PartnershipIncomeController',
          size: 'lg',
          resolve: {
            $partnership: function () {
              return self;
            }
          }
        });
      };

      // function save include create, update
      PartnerTrading.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            PartnerTrading.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance partnership (SA104)
      PartnerTrading.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in PartnerTrading.list) {
            if (PartnerTrading.list[i].id === self.id) {
              PartnerTrading.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return PartnerTrading;
    }
  );
