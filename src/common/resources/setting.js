angular.module('resources.settings', [])

  .factory('settings',
    function (apiService, utils) {

      var settings = {
        defaultBusiness: null,
        defaultCustomer: null,
        defaultSupplier: null,
        defaultTenant: null,
        defaultRecordType: {
          // Base on Customer or supplier selected
          customers: [],
          suppliers: []
        }
      };

      utils.cloneObjectByMask(utils.storage.get('settings'), settings);


      /**
       * Service
       */
      var service = {

        /**
         * save setting user
         */
        saveSettings: function () {
          utils.storage.set('settings', settings);
        },

        /**
         * get settings
         */
        getSettings: function () {
          utils.cloneObjectByMask(utils.storage.get('settings'), settings);
        },

        /**
         * find record type default
         *
         * @param type
         * @param id
         * @returns {*}
         */
        findRecordTypeDefault: function (type, id) {
          switch (type) {
            case 'customers':
              for (var c in settings.defaultRecordType.customers) {
                var customer = settings.defaultRecordType.customers[c];

                if (customer.id === id) {
                  return customer.record_type_id ? customer.record_type_id : customer.sub_category_id;
                }
              }
              break;
            case 'suppliers':
              for (var j in settings.defaultRecordType.suppliers) {
                var supplier = settings.defaultRecordType.suppliers[j];

                if (supplier.id === id) {
                  return supplier.record_type_id ? supplier.record_type_id : supplier.sub_category_id;
                }
              }
              break;
            default:
              break;
          }
        },


        /**
         * get record type default by customer
         *
         * @param customer_id
         * @returns {*}
         */
        getRecordTypeDefaultByCustomer: function (customer_id) {
          this.getSettings();

          for (var index in settings.defaultRecordType.customers) {
            var item = settings.defaultRecordType.customers[index];

            if (item.id == customer_id) {
              return item;
            }
          }
        },

        /**
         * get record type default by customer
         *
         * @param customer_id
         * @returns {*}
         */
        getRecordTypeDefaultBySupplier: function (supplier_id) {
          this.getSettings();

          for (var index in settings.defaultRecordType.suppliers) {
            var item = settings.defaultRecordType.suppliers[index];

            if (item.id == supplier_id) {
              return item;
            }
          }
        },

        /**
         * save record type default by customer
         *
         * @param customer
         */
        saveRecordTypeDefaultCustomer: function (customer) {
          for (var index in settings.defaultRecordType.customers) {
            var item = settings.defaultRecordType.customers[index];

            if (item.id == customer.id) {
              item = customer;
              return;
            }
          }

          settings.defaultRecordType.customers.push(customer);

          this.saveSettings();
        },

        /**
         * save record type default by supplier
         * @param supplier
         */
        saveRecordTypeBySupplier: function (supplier) {
          for (var index in settings.defaultRecordType.suppliers) {
            var item = settings.defaultRecordType.suppliers[index];

            if (item.id == supplier.id) {
              item = supplier;
              return;
            }
          }

          settings.defaultRecordType.suppliers.push(supplier);

          this.saveSettings();
        }


      };

      return service;
    }
  );
