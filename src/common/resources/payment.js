angular.module('resources.payment', [])

    .factory('payments', function (apiService, utils, $rootScope, $q) {
        // resource is resource connect to api
        var resource = 'payment';

        // connect api success and return reponse
        function apiSuccess(response) {
            return response.data;
        }

        // create object
        function pay(object) {
            return apiService.post(resource, object).then(apiSuccess);
        }

        // check payment
        function checkPayment(year){
            return apiService.find(resource, year).get().then(apiSuccess);
        }

        var service = {

            // find all
            pay: function (object) {
                var deferred = $q.defer();

                pay(object).then(function (res) {
                    deferred.resolve({
                        error: res.error,
                        success: res.success,
                        notes: res.notes
                    });
                });

                return deferred.promise;
            },

            checkPayment: function(year){
                var deferred = $q.defer();

                checkPayment(year).then(function (res) {
                    deferred.resolve({
                        success: res.success,
                        notes: res.message
                    });
                });

                return deferred.promise;
            }
        };

        return service;
    });
