angular.module('resources.properties', [])
  .factory('Property',
    function (Model, $uibModal) {

      var _resource = 'property';

      var Property = function () {
        Model.apply(this, arguments);

        this.user_id = -1;
        this.name = '';
        this.address = '';
        this.created = '';
        this.modified = '';
      };

      // List properties
      Property.list = [];

      // get all Property
      Property.all = function () {
        return Model.all(_resource).then(function (response) {
          Property.list = [];
          for (var i in response) {
            var instance = new Property();
            instance.fill(response[i]);
            Property.list.push(instance);
          }
        });
      };

      //extend
      Property.prototype = new Model();

      // fill
      Property.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      Property.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/property/new/property.tpl.html',
          controller: 'CreatePropertyController',
          resolve: {
            $property: function () {
              return self;
            }
          }
        });
      };

      // method get
      Property.prototype.get = function () {
        this.resource = _resource;
        return Model.prototype.save.apply(this, arguments);
      };

      // function save include create, update
      Property.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            Property.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      Property.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in Property.list) {
            if (Property.list[i].id === self.id) {
              Property.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return Property;
    });
