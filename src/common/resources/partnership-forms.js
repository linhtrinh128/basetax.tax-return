angular.module('resources.partnership-forms', [])
  .factory('PartnershipForm',
    function (Model, apiService, $q, TAX_RETURN, formsTaxReturn) {

      this.list = null;

      var PartnershipForm = function () {

        Model.apply(this, arguments);

        this.title = null;
        this.key = null;
        this.type = null;
        this.subTitle = null;
        this.show = true;
        this.selected = false;
        this.existing = false;
        this.path_review = null;
      };

      // init individual forms
      PartnershipForm.init = function () {
        var forms = [];
        var form;

        for (var i in TAX_RETURN.PARTNERSHIP_FORMS) {
          var psForm = TAX_RETURN.PARTNERSHIP_FORMS[i];
          form = new PartnershipForm();
          form.fill(psForm);
          forms.push(form);
        }

        return forms;
      };

      // extend
      PartnershipForm.prototype = new Model();

      // fill
      PartnershipForm.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      PartnershipForm.prototype.checkExisting = function () {
        var self = this;
        var deferred = $q.defer();

        formsTaxReturn.checkExistingForm(this.key)
          .then(function () {
            self.existing = true;
          }, function () {
            self.existing = false;
          });

        deferred.resolve(self);
      };

      //override
      return PartnershipForm;
    });