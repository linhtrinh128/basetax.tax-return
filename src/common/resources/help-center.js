angular.module('resources.help-center', [])

    .factory('helpCenter',
        function (apiService, utils, $rootScope, $q) {

            // resource is resource connect to api
            var resource = 'help-center';

            var service = {
                current: null,
                list: null,

                // send help message
                send: function (object) {
                    var deferred = $q.defer();

                    apiService.post(resource, object).then(function (res) {
                        deferred.resolve(res.data);
                    }, function error(error) {
                        deferred.reject(error.data);
                    });

                    return deferred.promise;
                }
            };

            return service;
        }
    );
