angular.module('resources.subscriptions', [])

  .factory('subscriptions',
    function (apiService, utils, $rootScope, $q) {
      // resource is resource connect to api
      var resource = 'subscription';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        find: function (id) {
          var deferred = $q.defer();

          if (utils.checkIfLoad(id, service.list)) {
            deferred.resolve(item);
          } else {
            apiService.get(resource, id).then(function (res) {
              res = res.data;

              if (res.data != null) {
                if (utils.appendInList(res.data, service.list)) {
                  $rootScope.$broadcast('subscriptions:list:updated', service.list);
                }

                $rootScope.$broadcast('subscriptions:created', res.data);

                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);

            });
          }

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              $rootScope.$broadcast('subscriptions:created', res.data);

              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('subscriptions:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('subscriptions:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              $rootScope.$broadcast('subscriptions:deleted', res.object);

              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('subscriptions:list:updated', res.object);
              }

              deferred.resolve(item);

            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        upgrade: function (object) {
          var deferred = $q.defer();

          apiService.post('subscription/upgrade', object).then(function (res) {
            res = res.data;
            if (res.success) {
              $rootScope.$broadcast('subscriptions:upgraded', res.object);
              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('subscriptions:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('subscriptions:current:changed', service.current);
        }
      };

      return service;
    }
  );
