angular.module('resources.currencies', [])

  .factory('Currency',
    function (apiService, utils, $rootScope, $q, RESOURCES) {

      // resource is resource connect to api
      var resource = 'currency';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          this.list = RESOURCES.CURRENCIES;
          return this.list;
        }
      };

      return service;
    }
  );
