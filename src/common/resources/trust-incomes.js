angular.module('resources.trust-incomes', [])

  .factory('TrustIncome',
    function (Model, apiService, $q, TaxYear) {

      var _resource = 'trust-incomes';

      var TrustIncome = function () {
        Model.apply(this, arguments);

        this.id = null;
        this.user_id = null;
        this.tax_year_id = TaxYear.current.id;

        // Trust
        this.TRU1 = 0;
        this.TRU2 = 0;
        this.TRU3 = 0;
        this.TRU4 = 0;
        this.TRU5 = 0;
        this.TRU6 = 0;
        this.TRU7 = 0;
        this.TRU8 = 0;
        this.TRU9 = 0;
        this.TRU10 = 0;
        this.TRU11 = 0;
        this.TRU12 = 0;
        this.TRU13 = 0;
        this.TRU14 = 0;
        this.TRU15 = 0;
        this.TRU16 = 0;
        this.TRU17 = 0;
        this.TRU18 = 0;
        this.TRU19 = 0;
        this.TRU20 = 0;
        this.TRU21 = 0;
        this.TRU22 = 0;
        this.TRU23 = 0;
        this.TRU24 = 0;
        this.TRU25 = '';
      };

      // List other main incomes
      TrustIncome.list = [];

      // get all TrustIncomes
      TrustIncome.all = function () {
        return Model.all(_resource).then(function (response) {
          TrustIncome.list = [];
          for (var i in response) {
            var instance = new TrustIncome();
            instance.fill(response[i]);
            TrustIncome.list.push(instance);
          }
        });
      };

      // find by tax year
      TrustIncome.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;

        for (var i in TrustIncome.list) {
          var item = TrustIncome.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }

        if (!flag) {
          deferred.reject(false);
        }

        return deferred.promise;
      };

      //extend
      TrustIncome.prototype = new Model();

      // fill
      TrustIncome.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      TrustIncome.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            TrustIncome.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      TrustIncome.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in TrustIncome.list) {
            if (TrustIncome.list[i].id === self.id) {
              TrustIncome.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      return TrustIncome;
    });
