angular.module('resources.record-type-groups', [])

  .factory('recordTypeGroups',
    function (apiService, utils, $rootScope, $q, RESOURCES) {
      // resource is resource connect to api
      var resource = 'record-type-group';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          this.list = RESOURCES.RECORD_TYPE_GROUPS;
          return this.list;
        },

        /**
         * find a resources with id
         *
         * @param id
         * @returns {deferred.promise|{then, catch, finally}}
         */
        find: function (id) {
          var deferred = $q.defer();
          var item;

          if (item = utils.checkIfLoad(id, service.list)) {
            deferred.resolve(item);
          } else {
            apiService.get(resource, id).then(function (res) {
              res = res.data;

              if (res.data != null) {
                if (utils.appendInList(res.data, service.list)) {
                  $rootScope.$broadcast('recordTypeGroups:list:updated', service.list);
                }

                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);

            });
          }

          return deferred.promise;
        },


        /**
         * get record types belongs to record_type_group
         * included subcategory
         *
         * @param id
         */
        getRecordTypes: function (id) {
          // find group
          var group = {};
          for (var g in this.list) {
            if (this.list[g].id == id) {
              group = this.list[g];
            }
          }

          // find record type and subcategory
          var list = [];
          for (var i in group.record_types) {
            var type = group.record_types[i];
            list.push(type);
            for (var j in type.subcategories) {
              type.subcategories[j].description = ' - ' + type.subcategories[j].name;
              list.push(type.subcategories[j]);
            }
          }

          return list;
        }
      };

      return service;
    });
