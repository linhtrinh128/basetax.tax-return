angular.module('resources.businesses', [])
  
  .factory('Business',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, Country) {
      var _resource = 'business';
      var Business = function () {
        Model.apply(this, arguments);

        this.id = null;
        this.user_id = -1;
        this.currency_id = 77;//uk
        this.currency = {};
        this.country_id = 77;//uk
        this.country = utils.populateById(77, Country.list);//uk
        this.name = '';
        this.description = '';
        this.address = '';
        this.address_line1 = '';
        this.address_line2 = '';
        this.city = '';
        this.postcode = '';
        this.date_started = '';
        this.cis_registered = '';
        this.vat_registered = '';
        this.vat_number = '';
        this.bank_account_name = '';
        this.bank_name = '';
        this.sort_code = '';
        this.bank_account_number = '';
        this.created = '';
        this.modified = '';
      };

      // List businesses
      Business.list = [];

      // get all Businesss
      Business.all = function () {
        return Model.all(_resource).then(function (response) {
          Business.list = [];
          for (var i in response) {
            var instance = new Business();
            instance.fill(response[i]);
            Business.list.push(instance);
          }
        });
      };

      //extend
      Business.prototype = new Model();

      // fill
      Business.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      Business.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/business/new/business/business.tpl.html',
          controller: 'CreateBusinessController',
          size: 'md',
          resolve: {
            title: function () {
              return "Create Business";
            },
            $business: function () {
              return self;
            }
          }
        });
      };

      // method get
      Business.prototype.get = function () {
        this.resource = _resource;
        return Model.prototype.save.apply(this, arguments);
      };

      // function save include create, update
      Business.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            Business.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      Business.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in Business.list) {
            if (Business.list[i].id === self.id) {
              Business.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      // send to client
      Business.prototype.send = function (params) {

      };

      // view pdf
      Business.prototype.exportPdf = function (params) {

      };

      //override
      return Business;
    });
