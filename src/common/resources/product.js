angular.module('resources.products', [])

  .factory('products',
    function (apiService, utils, $rootScope, $q) {
      // resource is resource connect to api
      var resource = 'product';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('products:list:updated', service.list);
              }

              $rootScope.$broadcast('products:created', res.data);

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('products:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('products:list:updated', res.object);
                deferred.resolve(item);
              }
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('products:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('products:current:changed', service.current);
        }
      };

      return service;
    }
  )

  .factory('Product', function (Model, $uibModal) {
    var _resource = 'product';
    this.list = null;
    var Product = function () {
      this.user_id = -1;
      this.name = '';
      this.unit_price = 0;
      this.description = null;
      this.tax_rate = 0;
      this.created = new Date();
      this.modified = new Date();
    };

    // get all products
    Product.all = function () {
      return Model.all(_resource).then(function (response) {
        Product.list = [];
        angular.forEach(response, function (data) {
          var instance = new Product();
          instance.fill(data);
          Product.list.push(instance);
        });
      });
    };

    // fill
    Product.prototype.fill = function (data) {
      Model.prototype.fill.apply(this, arguments);
    };

    // function show
    Product.prototype.show = function () {
      var self = this;
      return $uibModal.open({
        templateUrl: 'modules/invoice/product/new/product.tpl.html',
        controller: 'CreateProductController',
        resolve: {
          title: function () {
            return "Create Product";
          },
          $product: function () {
            return self;
          }
        }
      });
    };

    // function save include create, update
    Product.prototype.save = function () {
      this.resource = _resource;
      return Model.prototype.save.apply(this, arguments);
    };

    // function delete an instance self-employed
    Product.prototype.delete = function () {
      this.resource = _resource;
      return Model.prototype.delete.apply(this, arguments);
    };

    return Product;
  });
