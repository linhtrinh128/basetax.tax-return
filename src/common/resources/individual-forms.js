angular.module('resources.individual-forms', [])
  .factory('IndividualForm',
    function (Model, apiService, $q, TAX_RETURN, formsTaxReturn) {

      this.list = null;

      var IndividualForm = function () {
        Model.apply(this, arguments);

        this.title = null;
        this.key = null;
        this.type = null;
        this.subTitle = null;
        this.show = true;
        this.selected = false;
        this.existing = false;
        this.path_review = null;
      };

      // init individual forms
      IndividualForm.init = function () {
        var forms = [];
        var form;

        for (var i in TAX_RETURN.INCOME_FORMS) {
          var incomeForm = TAX_RETURN.INCOME_FORMS[i];
          form = new IndividualForm();
          form.fill(incomeForm);
          forms.push(form);
        }

        for (var j in TAX_RETURN.DEDUCTION_FORMS) {
          var deductionForm = TAX_RETURN.DEDUCTION_FORMS[j];
          form = new IndividualForm();
          form.fill(deductionForm);
          forms.push(form);
        }

        return forms;
      };

      // extend
      IndividualForm.prototype = new Model();

      // fill
      IndividualForm.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      IndividualForm.prototype.checkExisting = function () {
        var self = this;
        var deferred = $q.defer();

        formsTaxReturn.checkExistingForm(this.key)
          .then(function () {
            self.existing = true;
          }, function () {
            self.existing = false;
          });

        deferred.resolve(self);
      };

      //override
      return IndividualForm;
    });