angular.module('resources.non-residents', [])

  .factory('NonResident',
    function (Model, $uibModal, $q, TaxYear) {

      var _resource = 'non-resident';
      var NonResident = function () {
        Model.apply(this, arguments);

        this.NRD1 = 0;
        this.NRD2 = 0;
        this.NRD3 = 0;
        this.NRD3_1 = 0;
        this.NRD4 = 0;
        this.NRD5 = 0;
        this.NRD6 = '';
        this.NRD7 = '';
        this.NRD8 = 0;
        this.NRD8A = 0;
        this.NRD9 = 0;
        this.NRD10 = '';
        this.NRD11 = '';
        this.NRD12 = '';
        this.NRD13 = '';
        this.NRD14 = '';
        this.NRD15 = 0;
        this.NRD16 = 0;
        this.NRD17 = '';
        this.NRD17A = '';
        this.NRD17B = '';
        this.NRD18 = '';
        this.NRD18A = '';
        this.NRD18B = '';
        this.NRD19 = '';
        this.NRD19A = '';
        this.NRD19B = '';
        this.NRD20 = 0;
        this.NRD21 = 0;
        this.NRD22 = 0;
        this.NRD23 = 0;
        this.NRD24 = 0;
        this.NRD25 = '';
        this.NRD26 = 0;
        this.NRD27 = '';
        this.NRD28 = 0;
        this.NRD29 = 0;
        this.NRD30 = 0;
        this.NRD31 = 0;
        this.NRD32 = 0;
        this.NRD33 = 0;
        this.NRD34 = 0;
        this.NRD35 = 0;
        this.NRD36 = 0;
        this.NRD37 = 0;
        this.NRD37A = '';
        this.NRD38 = 0;
        this.NRD39 = '';

        this.form_type = 26;
        this.tax_year_id = TaxYear.current.id;
      };

      // List 
      NonResident.list = [];

      // get all NonResidents
      NonResident.all = function () {
        return Model.all(_resource).then(function (response) {
          NonResident.list = [];
          for (var i in response) {
            var instance = new NonResident();
            instance.fill(response[i]);
            NonResident.list.push(instance);
          }
        });
      };

      // find by tax year
      NonResident.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in NonResident.list) {
          var item = NonResident.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      NonResident.prototype = new Model();

      // fill
      NonResident.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      NonResident.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/tax-return/forms/partnership/partnership.tpl.html',
          controller: 'PartnershipIncomeController',
          size: 'lg',
          resolve: {
            $partnership: function () {
              return self;
            }
          }
        });
      };

      // function save include create, update
      NonResident.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            NonResident.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance partnership (SA104)
      NonResident.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in NonResident.list) {
            if (NonResident.list[i].id === self.id) {
              NonResident.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return NonResident;
    }
  );
