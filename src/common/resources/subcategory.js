angular.module('resources.subcategories', [])

  .factory('subcategories', function ($rootScope, $q, apiService, utils) {

    var resource = 'subcategory';

    var service = {
      current: null,
      list: null,

      // find all
      all: function () {
        var deferred = $q.defer();

        apiService.all(resource).then(function (res) {
          res = res.data;

          if (res.data != null) {
            service.list = [];

            service.list = utils.sort(res.data.data, 'modified', true);

            deferred.resolve(service.list);
          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      create: function (object) {
        var deferred = $q.defer();

        apiService.post(resource, object).then(function (res) {
          res = res.data;

          if (res.data != null) {
            if (utils.appendInList(res.data, service.list)) {
              $rootScope.$broadcast('subcategories:list:updated', service.list);
            }

            $rootScope.$broadcast('subcategories:created', res.data);

            deferred.resolve(res.data);
          } else {
            deferred.reject(false);
          }

        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      update: function (object) {
        var deferred = $q.defer();

        apiService.customPUT(resource, object.id, object).then(function (res) {
          res = res.data;

          if (res.data != null) {
            if (utils.updateInList(res.data, service.list)) {
              $rootScope.$broadcast('subcategories:list:updated', service.list);
            }

            deferred.resolve(res.data);
          } else {
            deferred.reject(false);
          }

        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      delete: function (item) {
        var deferred = $q.defer();

        apiService.remove(resource, item.id).then(function (res) {
          res = res.data;

          if (res.success) {
            if (utils.removeFromList(item, service.list)) {
              $rootScope.$broadcast('subcategories:list:updated', res.object);
              deferred.resolve(item);
            }
          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      setCurrent: function (object) {
        service.current = object;
        $rootScope.$broadcast('subcategories:current:changed', service.current);
      },

      resetCurrent: function () {
        service.current = null;
        $rootScope.$broadcast('subcategories:current:changed', service.current);
      }
    };

    return service;
  });