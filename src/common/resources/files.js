angular.module('resources.files', [])

  .factory('files',
    function (apiService, utils, $rootScope, $q, $http, CONFIGS,
              transactions, transactionsEmp, transactionsPro, invoiceTemplates) {

      // resource is resource connect to api
      var resource = 'file';

      var service = {
        /**
         * get image transaction
         * @param transaction_id
         * @returns {deferred.promise|{then, catch, finally}}
         */
        getImageTransaction: function (transaction_id) {
          var deferred = $q.defer();

          apiService.get(resource + '/transaction/' + transaction_id)
            .then(function (res) {
              if (res.data != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },


        /**
         * get image transaction pro
         *
         * @param transaction_id
         * @returns {deferred.promise|{then, catch, finally}}
         */
        getImageTransactionPro: function (transaction_id) {
          var deferred = $q.defer();

          apiService.get(resource + '/transaction-pro/' + transaction_id)
            .then(function (res) {
              if (res.data != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },


        /**
         * get image transaction emp
         *
         * @param transaction_id
         * @returns {deferred.promise|{then, catch, finally}}
         */
        getImageTransactionEmp: function (transaction_id) {
          var deferred = $q.defer();

          apiService.get(resource + '/transaction-emp/' + transaction_id)
            .then(function (res) {
              if (res.data != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * get image logo invoice template
         *
         * @param transaction_id
         * @returns {deferred.promise|{then, catch, finally}}
         */
        getImageLogoInvoiceTemplate: function (invoice_template_id) {
          var deferred = $q.defer();

          apiService.get(resource + '/invoice-template/' + invoice_template_id)
            .then(function (res) {
              if (res.data != null) {
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * update image transaction
         *
         * @param transaction_id
         * @param file
         * @returns {deferred.promise|{then, catch, finally}}
         */
        uploadImageTransaction: function (transaction_id, file) {
          var deferred = $q.defer();

          var fd = new FormData();
          fd.append('scan', file);

          apiService.findAll(resource + '/transaction/' + transaction_id + '/upload')
            .withHttpConfig({transformRequest: angular.identity})
            .post(fd, undefined, {'Content-Type': undefined})
            .then(function (res) {
              if (res.data != null) {
                if (utils.updateInList(res.data.data, transactions.list)) {
                  $rootScope.$broadcast('transactions:list:updated');
                }
                deferred.resolve(res.data.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },


        /**
         * update image transaction pro
         *
         * @param transaction_id
         * @param file
         * @returns {deferred.promise|{then, catch, finally}}
         */
        uploadImageTransactionPro: function (transaction_id, file) {
          var deferred = $q.defer();

          var fd = new FormData();
          fd.append('scan', file);

          apiService.findAll(resource + '/transaction-pro/' + transaction_id + '/upload')
            .withHttpConfig({transformRequest: angular.identity})
            .post(fd, undefined, {'Content-Type': undefined})
            .then(function (res) {
              if (res.data != null) {
                if (utils.updateInList(res.data.data, transactionsPro.list)) {
                  $rootScope.$broadcast('transactionsPro:list:updated');
                }
                deferred.resolve(res.data.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },


        /**
         * update image transaction emp
         *
         * @param transaction_id
         * @param file
         * @returns {deferred.promise|{then, catch, finally}}
         */
        uploadImageTransactionEmp: function (transaction_id, file) {
          var deferred = $q.defer();

          var fd = new FormData();
          fd.append('scan', file);

          apiService.findAll(resource + '/transaction-emp/' + transaction_id + '/upload')
            .withHttpConfig({transformRequest: angular.identity})
            .post(fd, undefined, {'Content-Type': undefined})
            .then(function (res) {
              if (res.data != null) {
                if (utils.updateInList(res.data.data, transactionsEmp.list)) {
                  $rootScope.$broadcast('transactionsEmp:list:updated');
                }
                deferred.resolve(res.data.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },


        /**
         * upload image logo invoice template
         *
         * @param invoice_template_id
         * @param file
         * @returns {deferred.promise|{then, catch, finally}}
         */
        uploadImageLogoInvoiceTemplate: function (invoice_template_id, file) {
          var deferred = $q.defer();

          var fd = new FormData();
          fd.append('scan', file);

          apiService.findAll(resource + '/invoice-template/' + invoice_template_id + '/upload')
            .withHttpConfig({transformRequest: angular.identity})
            .post(fd, undefined, {'Content-Type': undefined})
            .then(function (res) {
              if (res.data != null) {
                if (utils.updateInList(res.data.data, invoiceTemplates.list)) {
                  $rootScope.$broadcast('invoiceTemplates:list:updated');
                }
                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * delete file scan transaction
         *
         * @param transaction_id
         * @returns {deferred.promise|{then, catch, finally}}
         */
        deleteFileScanTransaction: function (transaction_id) {
          var deferred = $q.defer();

          apiService.remove(resource + '/transaction', transaction_id)
            .then(function (res) {
              deferred.resolve(res);
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * delete file scan transaction property
         *
         * @param transaction_id
         * @returns {deferred.promise|{then, catch, finally}}
         */
        deleteFileScanTransactionPro: function (transaction_id) {
          var deferred = $q.defer();

          apiService.remove(resource + '/transaction-pro', transaction_id)
            .then(function (res) {
              deferred.resolve(res);
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * delete file scan transaction employer
         *
         * @param transaction_id
         * @returns {deferred.promise|{then, catch, finally}}
         */
        deleteFileScanTransactionEmp: function (transaction_id) {
          var deferred = $q.defer();

          apiService.remove(resource + '/transaction-emp', transaction_id)
            .then(function (res) {
              deferred.resolve(res);
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },

        /**
         * upload image logo invoice template
         *
         * @param invoice_template_id
         * @param file
         * @returns {deferred.promise|{then, catch, finally}}
         */
        deleteImageLogoInvoiceTemplate: function (invoice_template_id) {
          var deferred = $q.defer();

          apiService.remove(resource + '/invoice-template', invoice_template_id)
            .then(function (res) {
              deferred.resolve(res);
            }, function error(error) {
              deferred.reject(error.data);
            });

          return deferred.promise;
        },
      };

      return service;
    });