angular.module('resources.user-types', [])

  .factory('UserType', function ($rootScope, $q, apiService, utils, RESOURCES) {

    // resource is resource connect to api
    var resource = 'user-type';

    var service = {
      current: null,
      list: null,

      // find all
      all: function () {
        this.list = RESOURCES.USER_TYPES;
        return this.list;
      }
    };

    return service;
  });