angular.module('resources.other-main-incomes', [])

  .factory('OtherMainIncome',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {

      var _resource = 'other-main-income';

      var OtherMainIncome = function (form_type) {
        Model.apply(this, arguments);

        // CapitalGain
        this.CGT3 = 0;
        this.CGT4 = 0;
        this.CGT5 = 0;
        this.CGT6 = 0;
        this.CGT7 = 0;
        this.CGT8 = 0;
        this.CGT9 = 0;
        this.CGT10 = 0;
        this.CGT11 = 0;
        this.CGT12 = 0;
        this.CGT13 = 0;
        this.CGT14 = 0;
        this.CGT15 = 0;
        this.CGT16 = 0;
        this.CGT17 = 0;
        this.CGT18 = 0;
        this.CGT19 = 0;
        this.CGT20 = 0;
        this.CGT21 = 0;
        this.CGT22 = 0;
        this.CGT23 = 0;
        this.CGT24 = 0;
        this.CGT25 = 0;
        this.CGT26 = 0;
        this.CGT27 = 0;
        this.CGT28 = 0;
        this.CGT29 = 0;
        this.CGT30 = 0;
        this.CGT31 = 0;
        this.CGT32 = 0;
        this.CGT33 = 0;
        this.CGT34 = 0;
        this.CGT35 = 0;
        this.CGT36 = 0;
        this.CGT37 = '';

        // Foreign Income
        this.FOR1 = 0;
        this.FOR2 = 0;
        this.FOR3 = 0;
        this.FOR4 = 0;
        this.FOR5 = 0;
        this.FOR6 = 0;
        this.FOR7 = 0;
        this.FOR8 = 0;
        this.FOR9 = 0;
        this.FOR10 = 0;
        this.FOR11 = 0;
        this.FOR12 = 0;
        this.FOR13 = 0;
        this.FOR14 = 0;
        this.FOR15 = 0;
        this.FOR16 = 0;
        this.FOR17 = 0;
        this.FOR18 = 0;
        this.FOR19 = 0;
        this.FOR20 = 0;
        this.FOR21 = 0;
        this.FOR22 = 0;
        this.FOR23 = 0;
        this.FOR24 = 0;
        this.FOR25 = 0;
        this.FOR26 = 0;
        this.FOR27 = 0;
        this.FOR28 = 0;
        this.FOR29 = 0;
        this.FOR30 = 0;
        this.FOR31 = 0;
        this.FOR32 = 0;
        this.FOR33 = 0;
        this.FOR34 = '';
        this.FOR35 = 0;
        this.FOR36 = '';
        this.FOR37 = 0;
        this.FOR38 = 0;
        this.FOR39 = 0;
        this.FOR40 = 0;
        this.FOR41 = 0;
        this.FOR42 = 0;
        this.FOR43 = 0;
        this.FOR44 = '';
        this.FOR45 = 0;
        this.FOR46 = 0;
        this.FORA = '';
        this.FORB = '';
        this.FORC = 0;
        this.FORE = 0;
        this.FORF = 0;
        this.FOR4A = '';
        this.FOR4B = 0;
        this.FOR4C = 0;
        this.FOR4D = 0;
        this.FOR4E = 0;
        this.FOR4F = 0;
        this.FOR6A = '';
        this.FOR6B = 0;
        this.FOR6C = 0;
        this.FOR6D = 0;
        this.FOR6E = 0;
        this.FOR6F = 0;
        this.FOR9A = '';
        this.FOR9B = 0;
        this.FOR9C = 0;
        this.FOR9D = 0;
        this.FOR9E = 0;
        this.FOR9F = 0;
        this.FOR11A = '';
        this.FOR11B = 0;
        this.FOR11C = 0;
        this.FOR11D = 0;
        this.FOR11E = 0;
        this.FOR11F = 0;
        this.FOR13A = '';
        this.FOR13B = 0;
        this.FOR13C = 0;
        this.FOR13D = 0;
        this.FOR13E = 0;
        this.FOR13F = 0;
        this.FOR30A = '';
        this.FOR30B = 0;
        this.FOR30C = 0;
        this.FOR30D = 0;
        this.FOR30E = 0;
        this.FOR30F = 0;

        // Minister
        this.MOR1 = '';
        this.MOR2 = 0;
        this.MOR3 = 0;
        this.MOR4 = 0;
        this.MOR5 = 0;
        this.MOR6 = 0;
        this.MOR7 = 0;
        this.MOR8 = 0;
        this.MOR9 = 0;
        this.MOR10 = 0;
        this.MOR11 = 0;
        this.MOR12 = 0;
        this.MOR13 = 0;
        this.MOR14 = 0;
        this.MOR15 = 0;
        this.MOR16 = 0;
        this.MOR17 = 0;
        this.MOR18 = 0;
        this.MOR19 = 0;
        this.MOR20 = 0;
        this.MOR21 = 0;
        this.MOR22 = 0;
        this.MOR23 = 0;
        this.MOR24 = 0;
        this.MOR25 = 0;
        this.MOR26 = 0;
        this.MOR27 = 0;
        this.MOR28 = 0;
        this.MOR29 = 0;
        this.MOR30 = 0;
        this.MOR31 = 0;
        this.MOR32 = 0;
        this.MOR33 = 0;
        this.MOR34 = 0;
        this.MOR35 = 0;
        this.MOR36 = 0;
        this.MOR37 = 0;
        this.MOR38 = 0;
        this.MOR39 = 0;

        // Trust
        this.TRU1 = 0;
        this.TRU2 = 0;
        this.TRU3 = 0;
        this.TRU4 = 0;
        this.TRU5 = 0;
        this.TRU6 = 0;
        this.TRU7 = 0;
        this.TRU8 = 0;
        this.TRU9 = 0;
        this.TRU10 = 0;
        this.TRU11 = 0;
        this.TRU12 = 0;
        this.TRU13 = 0;
        this.TRU14 = 0;
        this.TRU15 = 0;
        this.TRU16 = 0;
        this.TRU17 = 0;
        this.TRU18 = 0;
        this.TRU19 = 0;
        this.TRU20 = 0;
        this.TRU21 = 0;
        this.TRU22 = 0;
        this.TRU23 = 0;
        this.TRU24 = 0;
        this.TRU25 = '';

        this.form_type = form_type;
        this.tax_year_id = TaxYear.current.id;
      };

      // List other main incomes
      OtherMainIncome.list = [];

      // get all OtherMainIncomes
      OtherMainIncome.all = function () {
        return Model.all(_resource).then(function (response) {
          OtherMainIncome.list = [];
          for (var i in response) {
            var instance = new OtherMainIncome();
            instance.fill(response[i]);
            OtherMainIncome.list.push(instance);
          }
        });
      };

      // find by tax year
      OtherMainIncome.findByTaxYear = function (tax_year_id, form_type) {
        var deferred = $q.defer();
        var flag = false;

        for (var i in OtherMainIncome.list) {
          var item = OtherMainIncome.list[i];
          if (item.tax_year_id === tax_year_id && item.form_type === form_type) {
            deferred.resolve(item);
            flag = true;
          }
        }

        if (!flag) {
          deferred.reject(false);
        }

        return deferred.promise;
      };

      //extend
      OtherMainIncome.prototype = new Model();

      // fill
      OtherMainIncome.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      OtherMainIncome.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            OtherMainIncome.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      OtherMainIncome.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in OtherMainIncome.list) {
            if (OtherMainIncome.list[i].id === self.id) {
              OtherMainIncome.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      return OtherMainIncome;
    });
