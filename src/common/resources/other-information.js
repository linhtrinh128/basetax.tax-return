angular.module('resources.other-information', [])

  .factory('OtherInformation',
    function (Model, $uibModal, $q, TaxYear) {

      var _resource = 'other-information';
      var OtherInformation = function () {
        Model.apply(this, arguments);

        this.POI7 = '';
        this.POI7_7 = '';
        this.POI7_8 = '';
        this.POI7_9 = '';
        this.POI8 = '';
        this.POI9_1 = '';
        this.POI9_2 = '';
        this.POI9_3 = '';
        this.POI9_4 = '';
        this.POI9_5 = '';
        this.POI9_6 = '';
        this.POI10_1 = '';
        this.POI10_2 = '';
        this.POI10_3 = '';
        this.POI10_4 = '';
        this.POI10_5 = '';

        this.tax_year_id = TaxYear.current.id;
      };

      // List partnerships
      OtherInformation.list = [];

      // get all OtherInformations
      OtherInformation.all = function () {
        return Model.all(_resource).then(function (response) {
          OtherInformation.list = [];
          for (var i in response) {
            var instance = new OtherInformation();
            instance.fill(response[i]);
            OtherInformation.list.push(instance);
          }
        });
      };

      // find by tax year
      OtherInformation.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in OtherInformation.list) {
          var item = OtherInformation.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      OtherInformation.prototype = new Model();

      // fill
      OtherInformation.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      // OtherInformation.prototype.show = function () {
      //   var self = this;
      //   return $uibModal.open({
      //     templateUrl: 'modules/tax-return/forms/partnership/partnership.tpl.html',
      //     controller: 'PartnershipIncomeController',
      //     size: 'lg',
      //     resolve: {
      //       $partnership: function () {
      //         return self;
      //       }
      //     }
      //   });
      // };

      // function save include create, update
      OtherInformation.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            OtherInformation.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance partnership (SA104)
      OtherInformation.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in OtherInformation.list) {
            if (OtherInformation.list[i].id === self.id) {
              OtherInformation.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return OtherInformation;
    }
  );
