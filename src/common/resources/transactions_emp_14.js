angular.module('resources.transactions_emp_14', [])

  .factory('TransactionEmp14',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {

      var _resource = 'transaction-emp-14';

      var TransactionEmp14 = function () {
        Model.apply(this, arguments);

        this.employer_id = -1;
        this.employer = {};
        this.record_type_id = 24;
        this.p11d = 0;
        this.EMP1 = 0;
        this.EMP2 = 0;
        this.EMP3 = 0;
        this.EMP9 = 0;
        this.EMP10 = 0;
        this.EMP11 = 0;
        this.EMP12 = 0;
        this.EMP13 = 0;
        this.EMP14 = 0;
        this.EMP15 = 0;
        this.EMP16 = 0;
        this.EMP17 = 0;
        this.EMP18 = 0;
        this.EMP19 = 0;
        this.EMP20 = 0;

        this.record_type_id = 84;
        this.property_id = -1;
        this.tax_year_id = TaxYear.current.id;
      };

      // List reliefs
      TransactionEmp14.list = [];

      // get all TransactionEmp14s
      TransactionEmp14.all = function () {
        return Model.all(_resource).then(function (response) {
          TransactionEmp14.list = [];
          for (var i in response) {
            var instance = new TransactionEmp14();
            instance.fill(response[i]);
            TransactionEmp14.list.push(instance);
          }
        });
      };

      // find by tax year
      TransactionEmp14.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in TransactionEmp14.list) {
          var item = TransactionEmp14.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      TransactionEmp14.prototype = new Model();

      // fill
      TransactionEmp14.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      TransactionEmp14.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            TransactionEmp14.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      TransactionEmp14.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in TransactionEmp14.list) {
            if (TransactionEmp14.list[i].id === self.id) {
              TransactionEmp14.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return TransactionEmp14;
    });