angular.module('resources.other-details', [])

  .factory('otherDetails',
    function (apiService, utils, $rootScope, $q) {
      
      var resource = 'other-detail';
      
      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        findByTaxYear: function (year) {
          var deferred = $q.defer();

          var list = [];

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
                list.push(service.list[index]);
              }
            }

            deferred.resolve(list);
          }

          return deferred.promise;
        },

        find: function (id) {
          var deferred = $q.defer();

          if (utils.checkIfLoad(id, service.list)) {
            deferred.resolve(item);
          } else {
            apiService.get(resource, id).then(function (res) {
              res = res.data;

              if (res.data != null) {
                if (utils.appendInList(res.data, service.list)) {
                  $rootScope.$broadcast('otherDetails:list:updated', service.list);
                }

                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);

            });
          }

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              $rootScope.$broadcast('otherDetails:created', res.data);

              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('otherDetails:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('otherDetails:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              $rootScope.$broadcast('otherDetails:deleted', res.object);

              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('otherDetails:list:updated', res.object);
              }

              deferred.resolve(item);

            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        search: function (options) {
          var deferred = $q.defer();

          var list = [];
          var results = [];

          if (!options.type) {
            options.type = 'all';
          }

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(options.taxYear.id)) {
                list.push(service.list[index]);
              }
            }

            switch (options.type) {
              case 'search':
              {
                for (index in list) {
                  if (list[index].name && list[index].name.toLowerCase().indexOf(options.text.toLowerCase()) !== -1) {
                    results.push(list[index]);
                  }
                }

                break;
              }
              default:
              {
                results = list;
                break;
              }
            }
          }

          deferred.resolve(results);

          return deferred.promise;
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('otherDetails:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('otherDetails:current:changed', service.current);
        }
      };

      return service;
    }
  )

  .factory('OtherDetail',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {

      var _resource = 'other-detail';

      var OtherDetail = function (form_type) {
        Model.apply(this, arguments);

        this.ASE3 = 0;
        this.ASE5a = 0;
        this.ASE5b = 0;
        this.ASE5 = 0;
        this.ASE6 = 0;

        // InterestDividendsController
        this.INC1 = 0;
        this.INC2 = 0;
        this.INC3 = 0;
        this.INC4 = 0;
        this.INC5 = 0;
        this.INC6 = 0;

        // InterestGiltEdgedOtherSecuritiesController
        this.AOI1 = 0;
        this.AOI2 = 0;
        this.AOI3 = 0;

        // LifeInsuranceGainsController
        this.AOI4 = 0;
        this.AOI5 = 0;
        this.AOI6 = 0;
        this.AOI7 = 0;
        this.AOI8 = 0;
        this.AOI9 = 0;
        this.AOI10 = 0;
        this.AOI11 = 0;

        //NoteController
        this.INC16 = 0;
        this.INC17 = 0;
        this.INC18 = 0;
        this.INC19 = 0;
        this.INC20 = '';

        //PensionsAnnuitiesController
        this.INC7 = 0;
        this.INC8 = 0;
        this.INC9 = 0;
        this.INC10 = 0;
        this.INC11 = 0;
        this.INC12 = 0;
        this.INC13 = 0;
        this.INC14 = 0;
        this.INC15 = 0;

        // StockDividendsController
        this.AOI12 = 0;
        this.AOI13 = 0;

        this.form_type = form_type;
        this.tax_year_id = TaxYear.current.id;
      };

      // List reliefs
      OtherDetail.list = [];

      // get all OtherDetails
      OtherDetail.all = function () {
        return Model.all(_resource).then(function (response) {
          OtherDetail.list = [];
          for (var i in response) {
            var instance = new OtherDetail();
            instance.fill(response[i]);
            OtherDetail.list.push(instance);
          }
        });
      };

      // find by tax year
      OtherDetail.findByTaxYear = function (tax_year_id, form_type) {
        var deferred = $q.defer();
        var flag = false;

        for (var i in OtherDetail.list) {
          var item = OtherDetail.list[i];
          if (item.tax_year_id === tax_year_id && item.form_type === form_type) {
            deferred.resolve(item);
            flag = true;
          }
        }

        if (!flag) {
          deferred.reject(false);
        }

        return deferred.promise;
      };

      //extend
      OtherDetail.prototype = new Model();

      // fill
      OtherDetail.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      OtherDetail.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            OtherDetail.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      OtherDetail.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in OtherDetail.list) {
            if (OtherDetail.list[i].id === self.id) {
              OtherDetail.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      return OtherDetail;
    });
