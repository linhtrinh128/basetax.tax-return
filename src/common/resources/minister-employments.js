angular.module('resources.minister-employments', [])

  .factory('MinisterEmployment',
    function (Model, apiService, $q, TaxYear) {

      var _resource = 'minister-employments';

      var MinisterEmployment = function () {
        Model.apply(this, arguments);

        this.id = null;
        this.user_id = null;
        this.tax_year_id = TaxYear.current.id;

        // Minister
        this.MOR1 = '';
        this.MOR2 = 0;
        this.MOR3 = 0;
        this.MOR4 = 0;
        this.MOR5 = 0;
        this.MOR6 = 0;
        this.MOR7 = 0;
        this.MOR8 = 0;
        this.MOR9 = 0;
        this.MOR10 = 0;
        this.MOR11 = 0;
        this.MOR12 = 0;
        this.MOR13 = 0;
        this.MOR14 = 0;
        this.MOR15 = 0;
        this.MOR16 = 0;
        this.MOR17 = 0;
        this.MOR18 = 0;
        this.MOR19 = 0;
        this.MOR20 = 0;
        this.MOR21 = 0;
        this.MOR22 = 0;
        this.MOR23 = 0;
        this.MOR24 = 0;
        this.MOR25 = 0;
        this.MOR26 = 0;
        this.MOR27 = 0;
        this.MOR28 = 0;
        this.MOR29 = 0;
        this.MOR30 = 0;
        this.MOR31 = 0;
        this.MOR32 = 0;
        this.MOR33 = 0;
        this.MOR34 = 0;
        this.MOR35 = 0;
        this.MOR36 = 0;
        this.MOR37 = 0;
        this.MOR38 = 0;
        this.MOR39 = 0;
      };

      // List other main incomes
      MinisterEmployment.list = [];

      // get all MinisterEmployments
      MinisterEmployment.all = function () {
        return Model.all(_resource).then(function (response) {
          MinisterEmployment.list = [];
          for (var i in response) {
            var instance = new MinisterEmployment();
            instance.fill(response[i]);
            MinisterEmployment.list.push(instance);
          }
        });
      };

      // find by tax year
      MinisterEmployment.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;

        for (var i in MinisterEmployment.list) {
          var item = MinisterEmployment.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }

        if (!flag) {
          deferred.reject(false);
        }

        return deferred.promise;
      };

      //extend
      MinisterEmployment.prototype = new Model();

      // fill
      MinisterEmployment.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      MinisterEmployment.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            MinisterEmployment.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      MinisterEmployment.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in MinisterEmployment.list) {
            if (MinisterEmployment.list[i].id === self.id) {
              MinisterEmployment.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      return MinisterEmployment;
    });
