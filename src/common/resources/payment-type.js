angular.module('resources.payment-types', [])

  .factory('PaymentType',
    function (apiService, utils, $rootScope, $q, RESOURCES) {

      // resource is resource connect to api
      var resource = 'payment-type';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          this.list = RESOURCES.PAYMENT_TYPES;
          return this.list;
        }
      };

      return service;
    });
