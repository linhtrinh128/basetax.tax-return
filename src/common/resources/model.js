angular.module('resources.model', [])

  .factory('Model', function (apiService, $q) {

    var Model = function () {
      this.id = null;
    };

    // get all
    Model.all = function (url) {
      var deferred = $q.defer();
      apiService.all(url)
        .then(function (response) {
          var list = response.data.data.data;
          deferred.resolve(list);
        }, function error(error) {
          deferred.reject(error);
        });
      return deferred.promise;
    };

    // method get
    Model.prototype.get = function () {
      var self = this;
      var deferred = $q.defer();
      apiService.get(this.resource, this.id)
        .then(function (response) {
          var item = response.data.data.data;
          angular.forEach(self, function (value, key) {
            if (item[key]) {
              self[key] = item[key];
            }
          });
          deferred.resolve(true);
        }, function error(error) {
          deferred.reject(error);
        });
      return deferred.promise;
    };

    // method save
    Model.prototype.save = function () {
      var deferred = $q.defer();
      var self = this;
      if (!this.id) {
        apiService.post(this.resource, this)
          .then(function (response) {
            var item = response.data.data;
            angular.extend(self, item);
            deferred.resolve(self);
          }, function error(error) {
            deferred.reject(error);
          });
      } else {
        apiService.customPUT(this.resource, this.id, this)
          .then(function (response) {
            var item = response.data.data;
            angular.extend(self, item);
            deferred.resolve(self);
          }, function error(error) {
            deferred.reject(error);
          });
      }
      return deferred.promise;
    };

    // function delete an instance self-employed
    Model.prototype.delete = function () {
      var deferred = $q.defer();
      apiService.remove(this.resource, this.id)
        .then(function (response) {
          if (response.data.success) {
            deferred.resolve(true);
          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });
      return deferred.promise;
    };

    //fill
    Model.prototype.fill = function (data) {
      var self = this;
      angular.extend(self, data);
    };

    return Model;
  });