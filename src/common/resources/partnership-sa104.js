angular.module('resources.partnership-sa104', [])
  
  .factory('PartnershipSA104',
    function (Model, $uibModal, $q, TaxYear) {

      var _resource = 'partnership';
      var PartnershipSA104 = function () {
        Model.apply(this, arguments);

        this.FPS1 = null;
        this.FPS2 = null;
        this.FPS3 = null;
        this.FPS4 = null;
        this.FPS5 = null;
        this.FPS6 = null;
        this.FPS7 = null;
        this.FPS8 = null;
        this.FPS9 = null;
        this.FPS10 = null;
        this.FPS11 = null;
        this.FPS12 = null;
        this.FPS13 = null;
        this.FPS14 = null;
        this.FPS15 = null;
        this.FPS16 = null;
        this.FPS17 = null;
        this.FPS18 = null;
        this.FPS19 = null;
        this.FPS20 = null;
        this.FPS21 = null;
        this.FPS22 = null;
        this.FPS23 = null;
        this.FPS24 = null;
        this.FPS25 = null;
        this.FPS26 = null;
        this.FPS27 = null;
        this.FPS28 = null;
        this.FPS29 = null;
        this.FPS30 = null;
        this.FPS31 = null;
        this.FPS32 = null;
        this.FPS33 = null;
        this.FPS34 = null;
        this.FPS35 = null;
        this.FPS36 = null;
        this.FPS37 = null;
        this.FPS38 = null;
        this.FPS39 = null;
        this.FPS40 = null;
        this.FPS41 = null;
        this.FPS42 = null;
        this.FPS43 = null;
        this.FPS44 = null;
        this.FPS45 = null;
        this.FPS46 = null;
        this.FPS47 = null;
        this.FPS48 = null;
        this.FPS49 = null;
        this.FPS50 = null;
        this.FPS51 = null;
        this.FPS52 = null;
        this.FPS53 = null;
        this.FPS54 = null;
        this.FPS55 = null;
        this.FPS56 = null;
        this.FPS57 = null;
        this.FPS58 = null;
        this.FPS59 = null;
        this.FPS60 = null;
        this.FPS61 = null;
        this.FPS62 = null;
        this.FPS63 = null;
        this.FPS64 = null;
        this.FPS65 = null;
        this.FPS66 = null;
        this.FPS67 = null;
        this.FPS68 = null;
        this.FPS69 = null;
        this.FPS70 = null;
        this.FPS71 = null;
        this.FPS72 = null;
        this.FPS73 = null;
        this.FPS74 = null;
        this.FPS75 = null;
        this.FPS76 = null;
        this.FPS77 = null;
        this.FPS78 = null;
        this.FPS79 = null;
        this.FPS80 = null;
        this.FPS81 = null;
        this.FPS82 = null;
        
        this.form_type = 20;
        this.date = new Date();
        this.tax_year_id = TaxYear.current.id;
      };

      // List partnerships
      PartnershipSA104.list = [];

      // get all PartnershipSA104s
      PartnershipSA104.all = function () {
        return Model.all(_resource).then(function (response) {
          PartnershipSA104.list = [];
          for (var i in response) {
            var instance = new PartnershipSA104();
            instance.fill(response[i]);
            PartnershipSA104.list.push(instance);
          }
        });
      };

      // find by tax year
      PartnershipSA104.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in PartnershipSA104.list) {
          var item = PartnershipSA104.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      PartnershipSA104.prototype = new Model();

      // fill
      PartnershipSA104.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      PartnershipSA104.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/tax-return/forms/partnership/partnership.tpl.html',
          controller: 'PartnershipIncomeController',
          size: 'lg',
          resolve: {
            $partnership: function () {
              return self;
            }
          }
        });
      };

      // function save include create, update
      PartnershipSA104.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            PartnershipSA104.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance partnership (SA104)
      PartnershipSA104.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in PartnershipSA104.list) {
            if (PartnershipSA104.list[i].id === self.id) {
              PartnershipSA104.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return PartnershipSA104;
    }
  );
