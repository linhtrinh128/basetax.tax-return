angular.module('resources.self-employed', [])

  .factory('selfEmployeds',
    function (apiService, utils, $rootScope, $q) {
      // resource is resource connect to api
      var resource = 'self-employed';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        findByTaxYear: function (year) {
          var deferred = $q.defer();

          var list = [];

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
                list.push(service.list[index]);
              }
            }

            deferred.resolve(list);
          }

          return deferred.promise;
        },

        find: function (id) {
          var deferred = $q.defer();

          if (utils.checkIfLoad(id, service.list)) {
            deferred.resolve(item);
          } else {
            apiService.get(resource, id).then(function (res) {
              res = res.data;

              if (res.data != null) {
                if (utils.appendInList(res.data, service.list)) {
                  $rootScope.$broadcast('selfEmployeds:list:updated', service.list);
                }

                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);

            });
          }

          return deferred.promise;
        },

        import: function (params) {
          var deferred = $q.defer();

          apiService.all('self-employed-test', params).then(function (res) {
            if (res.data != null) {
              deferred.resolve(res.data.data.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              $rootScope.$broadcast('selfEmployeds:created', res.data);

              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('selfEmployeds:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('selfEmployeds:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              $rootScope.$broadcast('selfEmployeds:deleted', res.object);

              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('selfEmployeds:list:updated', res.object);
              }

              deferred.resolve(item);

            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        search: function (options) {
          var deferred = $q.defer();

          var list = [];
          var results = [];

          if (!options.type) {
            options.type = 'all';
          }

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(options.taxYear.id)) {
                list.push(service.list[index]);
              }
            }

            switch (options.type) {
              case 'search':
              {
                for (index in list) {
                  if (list[index].name && list[index].name.toLowerCase().indexOf(options.text.toLowerCase()) !== -1) {
                    results.push(list[index]);
                  }
                }

                break;
              }
              default:
              {
                results = list;
                break;
              }
            }
          }

          deferred.resolve(results);

          return deferred.promise;
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('selfEmployeds:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('selfEmployeds:current:changed', service.current);
        }
      };

      return service;
    })

  .factory('SelfEmployed',
    function (apiService, $q, $uibModal, Model) {
      var _resource = 'self-employed';

      var SelfEmployed = function (tax_year_id) {
        this.id = null;
        this.business_id = null;
        this.business = null;
        this.tax_year_id = tax_year_id;
        this.FSE1 = null;
        this.FSE2 = null;
        this.FSE3 = null;
        this.FSE4 = null;
        this.FSE5 = null;
        this.FSE6 = null;
        this.FSE7 = null;
        this.FSE8 = null;
        this.FSE9 = null;
        this.FSE10 = null;
        this.FSE11 = null;
        this.FSE12 = null;
        this.FSE13 = null;
        this.FSE14 = null;
        this.FSE15 = null;
        this.FSE16 = null;
        this.FSE17 = null;
        this.FSE18 = null;
        this.FSE19 = null;
        this.FSE20 = null;
        this.FSE21 = null;
        this.FSE22 = null;
        this.FSE23 = null;
        this.FSE24 = null;
        this.FSE25 = null;
        this.FSE26 = null;
        this.FSE27 = null;
        this.FSE28 = null;
        this.FSE29 = null;
        this.FSE30 = null;
        this.FSE31 = null;
        this.FSE32 = null;
        this.FSE33 = null;
        this.FSE34 = null;
        this.FSE35 = null;
        this.FSE36 = null;
        this.FSE37 = null;
        this.FSE38 = null;
        this.FSE39 = null;
        this.FSE40 = null;
        this.FSE41 = null;
        this.FSE42 = null;
        this.FSE43 = null;
        this.FSE44 = null;
        this.FSE45 = null;
        this.FSE46 = null;
        this.FSE47 = null;
        this.FSE48 = null;
        this.FSE49 = null;
        this.FSE50 = null;
        this.FSE51 = null;
        this.FSE52 = null;
        this.FSE53 = null;
        this.FSE54 = null;
        this.FSE55 = null;
        this.FSE56 = null;
        this.FSE57 = null;
        this.FSE58 = null;
        this.FSE59 = null;
        this.FSE60 = null;
        this.FSE61 = null;
        this.FSE62 = null;
        this.FSE63 = null;
        this.FSE64 = null;
        this.FSE65 = null;
        this.FSE66 = null;
        this.FSE67 = null;
        this.FSE68 = null;
        this.FSE69 = null;
        this.FSE70 = null;
        this.FSE71 = null;
        this.FSE72 = null;
        this.FSE73 = null;
        this.FSE74 = null;
        this.FSE75 = null;
        this.FSE76 = null;
        this.FSE77 = null;
        this.FSE78 = null;
        this.FSE79 = null;
        this.FSE80 = null;
        this.FSE81 = null;
        this.FSE82 = null;
        this.FSE83 = null;
        this.FSE84 = null;
        this.FSE85 = null;
        this.FSE86 = null;
        this.FSE87 = null;
        this.FSE88 = null;
        this.FSE89 = null;
        this.FSE90 = null;
        this.FSE91 = null;
        this.FSE92 = null;
        this.FSE93 = null;
        this.FSE94 = null;
        this.FSE95 = null;
        this.FSE96 = null;
        this.FSE97 = null;
        this.FSE98 = null;
        this.FSE99 = null;
        this.FSE100 = null;
        this.FSE101 = null;
        this.FSE102 = null;
        this.FSE103 = null;
        this.created = null;
        this.modified = null;
      };

      // List reliefs
      SelfEmployed.list = [];

      // static function get self employed
      SelfEmployed.all = function () {
        return Model.all(_resource).then(function (response) {
          SelfEmployed.list = [];
          for (var i in response) {
            var instance = new SelfEmployed();
            instance.fill(response[i]);
            SelfEmployed.list.push(instance);
          }
        });
      };

      // static function find self employed by tax year
      SelfEmployed.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        var selfEmployeds = [];
        for (var i in SelfEmployed.list) {
          var item = SelfEmployed.list[i];
          if (item.tax_year_id === tax_year_id) {
            selfEmployeds.push(item);
            flag = true;
          }
        }
        deferred.resolve(selfEmployeds);
        return deferred.promise;
      };

      //extend
      SelfEmployed.prototype = new Model();

      // function import data from bookkeeping
      SelfEmployed.prototype.import = function () {
        var deferred = $q.defer();
        var self = this;
        apiService.all(_resource + '/' + this.tax_year_id + '/' + this.business_id + '/import')
          .then(function (response) {
            var item = response.data.data.data;
            angular.forEach(self, function (value, key) {
              if (item[key]) {
                self[key] = item[key];
              }
            });
            deferred.resolve(true);
          }, function error(error) {
            deferred.reject(error);
          });
        return deferred.promise;
      };

      // function save include create, update
      SelfEmployed.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            SelfEmployed.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      SelfEmployed.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in SelfEmployed.list) {
            if (SelfEmployed.list[i].id === self.id) {
              SelfEmployed.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      // function fill data
      SelfEmployed.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      SelfEmployed.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/tax-return/forms/self-employed/self-employed.tpl.html',
          controller: 'SelfEmployedController',
          size: 'lg',
          resolve: {
            data: function () {
              return self;
            }
          }
        });
      };

      return SelfEmployed;
    }
  );
