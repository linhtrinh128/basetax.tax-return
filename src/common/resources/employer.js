angular.module('resources.employers', [])

  .factory('employers', function (apiService, utils, $rootScope, $q) {
    // resource is resource connect to api
    var resource = 'employer';

    var service = {
      current: null,
      list: null,

      // find all
      all: function () {
        var deferred = $q.defer();

        apiService.all(resource).then(function (res) {
          res = res.data;

          if (res.data != null) {
            service.list = [];

            service.list = utils.sort(res.data.data, 'modified', true);

            deferred.resolve(service.list);
          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },


      find: function (id) {
        var deferred = $q.defer();
        var item;
        if (item = utils.checkIfLoad(id, service.list)) {
          deferred.resolve(item);
        } else {
          apiService.get(resource, id).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('employers:list:updated', service.list);
              }

              $rootScope.$broadcast('employers:created', res.data);

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);

          });
        }

        return deferred.promise;
      },

      create: function (object) {
        var deferred = $q.defer();

        apiService.post(resource, object).then(function (res) {
          res = res.data;

          if (res.data != null) {
            $rootScope.$broadcast('employers:created', res.data);

            if (utils.appendInList(res.data, service.list)) {
              $rootScope.$broadcast('employers:list:updated', service.list);
            }

            deferred.resolve(res.data);
          } else {
            deferred.reject(false);
          }

        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      update: function (object) {
        var deferred = $q.defer();

        apiService.customPUT(resource, object.id, object).then(function (res) {
          res = res.data;

          if (res.data != null) {
            if (utils.updateInList(res.data, service.list)) {
              $rootScope.$broadcast('employers:list:updated', service.list);
            }

            deferred.resolve(res.data);
          } else {
            deferred.reject(false);
          }

        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      delete: function (item) {
        var deferred = $q.defer();

        apiService.remove(resource, item.id).then(function (res) {
          res = res.data;

          if (res.success) {
            $rootScope.$broadcast('employers:deleted', res.object);

            if (utils.removeFromList(item, service.list)) {
              $rootScope.$broadcast('employers:list:updated', res.object);
            }

            deferred.resolve(item);

          } else {
            deferred.reject(false);
          }
        }, function error(error) {
          deferred.reject(error.data);
        });

        return deferred.promise;
      },

      setCurrent: function (object) {
        service.current = object;
        $rootScope.$broadcast('employers:current:changed', service.current);
      },

      resetCurrent: function () {
        service.current = null;
        $rootScope.$broadcast('employers:current:changed', service.current);
      }
    };

    return service;
  })

  .factory('Employer',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal) {

      var _resource = 'employer';

      var Employer = function () {
        Model.apply(this, arguments);

        this.id = null;
        this.user_id = -1;
        this.name = null;//uk
        this.paye_reference = 0;
        this.company_director = 0;
        this.close_company = 0;
        this.teacher_on_repayment = 0;
        this.created = '';
        this.modified = '';
      };

      // List employers
      Employer.list = [];

      // get all Employers
      Employer.all = function () {
        return Model.all(_resource).then(function (response) {
          Employer.list = [];
          for (var i in response) {
            var instance = new Employer();
            instance.fill(response[i]);
            Employer.list.push(instance);
          }
        });
      };

      //extend
      Employer.prototype = new Model();

      // fill
      Employer.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      Employer.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/employer/new/employer.tpl.html',
          controller: 'CreateEmployerController',
          resolve: {
            $employer: function () {
              return self;
            }
          }
        });
      };

      // method get
      Employer.prototype.get = function () {
        this.resource = _resource;
        return Model.prototype.get.apply(this, arguments);
      };

      // function save include create, update
      Employer.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            Employer.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      Employer.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in Employer.list) {
            if (Employer.list[i].id === self.id) {
              Employer.list.splice(i, 1);
              break;
            }
          }
        });
        return originDelete;
      };

      //override
      return Employer;
    });