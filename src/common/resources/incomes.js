angular.module('resources.incomes', [])

  .factory('UkIncome',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {
      var _resource = 'income';

      var UkIncome = function (form_type) {
        Model.apply(this, arguments);

        this.ASE3 = 0;
        this.ASE5a = 0;
        this.ASE5b = 0;
        this.ASE5 = 0;
        this.ASE6 = 0;

        // InterestDividendsController
        this.INC1 = 0;
        this.INC2 = 0;
        this.INC3 = 0;
        this.INC4 = 0;
        this.INC5 = 0;
        this.INC6 = 0;

        // InterestGiltEdgedOtherSecuritiesController
        this.AOI1 = 0;
        this.AOI2 = 0;
        this.AOI3 = 0;

        // LifeInsuranceGainsController
        this.AOI4 = 0;
        this.AOI5 = 0;
        this.AOI6 = 0;
        this.AOI7 = 0;
        this.AOI8 = 0;
        this.AOI9 = 0;
        this.AOI10 = 0;
        this.AOI11 = 0;

        //NoteController
        this.INC16 = 0;
        this.INC17 = 0;
        this.INC18 = 0;
        this.INC19 = 0;
        this.INC20 = '';

        //PensionsAnnuitiesController
        this.INC7 = 0;
        this.INC8 = 0;
        this.INC9 = 0;
        this.INC10 = 0;
        this.INC11 = 0;
        this.INC12 = 0;
        this.INC13 = 0;
        this.INC14 = 0;
        this.INC15 = 0;

        // StockDividendsController
        this.AOI12 = 0;
        this.AOI13 = 0;

        this.form_type = form_type;
        this.tax_year_id = TaxYear.current.id;
      };

      // List reliefs
      UkIncome.list = [];

      // get all UkIncomes
      UkIncome.all = function () {
        return Model.all(_resource).then(function (response) {
          UkIncome.list = [];
          for (var i in response) {
            var instance = new UkIncome();
            instance.fill(response[i]);
            UkIncome.list.push(instance);
          }
        });
      };

      // find by tax year
      UkIncome.findByTaxYear = function (tax_year_id, form_type) {
        var deferred = $q.defer();
        var flag = false;

        for (var i in UkIncome.list) {
          var item = UkIncome.list[i];
          if (item.tax_year_id === tax_year_id && item.form_type === form_type) {
            deferred.resolve(item);
            flag = true;
          }
        }

        if (!flag) {
          deferred.reject(false);
        }

        return deferred.promise;
      };

      //extend
      UkIncome.prototype = new Model();


      // fill
      UkIncome.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      UkIncome.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            UkIncome.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      UkIncome.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in UkIncome.list) {
            if (UkIncome.list[i].id === self.id) {
              UkIncome.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return UkIncome;
    });