angular.module('resources', [
  'services.api',
  'services.i18nNotifications',
  'app.utils',

  'resources.businesses',
  'resources.capital-gains',
  'resources.countries',
  'resources.currencies',
  'resources.customers',
  'resources.disposal-changeable-asset',
  'resources.employers',
  'resources.files',
  //'resources.finalised-taxs',
  'resources.form-data',
  'resources.foreign-incomes',
  'resources.help-center',
  'resources.hmrc-submissions',
  'resources.import-transactions',
  'resources.incomes',
  'resources.individual-forms',
  'resources.invoices',
  'resources.invoice-extra',
  'resources.invoiceDetails',
  'resources.invoiceTemplates',
  //'resources.message',
  'resources.model',
  'resources.minister-employments',
  'resources.non-residents',
  'resources.other-details',
  'resources.other-information',
  'resources.other-main-incomes',
  'resources.partner-detail',
  'resources.partner-property',
  'resources.partner-trading',
  'resources.partnerships',
  'resources.partnership-sa104',
  'resources.partnership-detail',
  'resources.partnership-forms',
  //'resources.payment',
  'resources.payment-types',
  'resources.products',
  'resources.projects',
  'resources.properties',
  'resources.quotas',
  'resources.record-type-groups',
  'resources.record-types',
  'resources.reliefs',
  'resources.self-employed',
  'resources.settings',
  'resources.subcategories',
  'resources.subscriptions',
  'resources.suppliers',
  'resources.tax-returns',
  'resources.tax-years',
  'resources.tenants',
  'resources.transactions',
  'resources.transactions_emp_14',
  'resources.transactions-emp',
  'resources.transactions_pro_14',
  'resources.transactions-pro',
  'resources.trust-incomes',
  'resources.underwriters',
  'resources.users',
  'resources.user-types',
  'resources.vehicle-type',
  //storage
  'resources.storage'
])

  .factory('all',
    function (apiService, utils, $rootScope, $q,
              Business, Currency, Customer, Employer,
              invoiceTemplates, PaymentType, products, quotas,
              recordTypeGroups, recordTypes, subcategories, Supplier,
              TaxYear, transactions, TransactionEmp14,
              TransactionPro14, UkIncome,
              reliefs, OtherMainIncome, PartnershipSA104,
              Underwriter, selfEmployeds, NonResident, transactionsEmp,
              transactionsPro, Property, subscriptions,
              users, importTransactions, tenants, otherDetails, PartnerTrading,
              DisposalChangeable, PartnerProperty, ForeignIncome, MinisterEmployment, TrustIncome,
              OtherInformation, partnershipDetails, partnerDetails, projects,
              CapitalGain) {

      var resource = 'all';

      var service = {
        // find all
        all: function () {
          var deferred = $q.defer();
          apiService.all(resource).then(function (res) {
            res = res.data;
            if (res.data != null) {
              service.set(res.data.data);
              deferred.resolve(res.data.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });
          return deferred.promise;
        },

        /**
         * Get all resources for bookkeeping
         *
         * @returns {deferred.promise|{then, catch, finally}}
         */
        getAllBookkeeping: function () {
          var deferred = $q.defer();
          apiService.all(resource + '/bookkeeping').then(function (res) {
            res = res.data;
            if (res.data != null) {
              service.set(res.data.data);
              deferred.resolve(res.data.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });
          return deferred.promise;
        },

        /**
         * Get all resources for tax return
         *
         * @returns {deferred.promise|{then, catch, finally}}
         */
        getAllTaxReturn: function () {
          var deferred = $q.defer();
          apiService.all(resource + '/tax-return').then(function (res) {
            res = res.data;
            if (res.data != null) {
              service.set(res.data.data);
              deferred.resolve(res.data.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });
          return deferred.promise;
        },

        /**
         * Set data
         *
         * @param data
         */
        set: function (data) {
          if (data.businesses) {
            Business.list = [];
            angular.forEach(data.businesses, function (data) {
              var instance = new Business();
              instance.fill(data);
              Business.list.push(instance);
            });
          }
          if (data.capital_gains) {
            angular.forEach(data.capital_gains, function (data) {
              var instance = new CapitalGain();
              instance.fill(data);
              CapitalGain.list.push(instance);
            });
          }
          if (data.customers) {
            angular.forEach(data.customers, function (data) {
              var instance = new Customer();
              instance.fill(data);
              Customer.list.push(instance);
            });
          }
          if (data.employers) {
            angular.forEach(data.employers, function (data) {
              var instance = new Employer();
              instance.fill(data);
              Employer.list.push(instance);
            });
          }
          if (data.disposal_changeables) {
            angular.forEach(data.disposal_changeables, function (data) {
              var instance = new DisposalChangeable();
              instance.fill(data);
              DisposalChangeable.list.push(instance);
            });
          }
          if (data.foreign_incomes) {
            angular.forEach(data.foreign_incomes, function (data) {
              var instance = new ForeignIncome();
              instance.fill(data);
              ForeignIncome.list.push(instance);
            });
          }
          if (data.import_transactions) {
            importTransactions.list = utils.sort(data.import_transactions, 'updated_at', true);
          }
          if (data.incomes) {
            UkIncome.list = [];
            angular.forEach(data.incomes, function (data) {
              var instance = new UkIncome();
              instance.fill(data);
              UkIncome.list.push(instance);
            });
          }
          if (data.invoices) {
            //invoices.list = utils.sort(data.invoices, 'modified', true);
          }
          if (data.invoice_templates) {
            invoiceTemplates.list = utils.sort(data.invoice_templates, 'modified', true);
          }
          if (data.minister_employments) {
            angular.forEach(data.minister_employments, function (data) {
              var instance = new MinisterEmployment();
              instance.fill(data);
              MinisterEmployment.list.push(instance);
            });
          }
          if (data.non_residents) {
            NonResident.list = utils.sort(data.non_residents, 'modified', true);
          }
          if (data.other_details) {
            otherDetails.list = utils.sort(data.other_details, 'modified', true);
          }
          if (data.other_informations) {
            angular.forEach(data.other_informations, function (data) {
              var instance = new OtherInformation();
              instance.fill(data);
              OtherInformation.list.push(instance);
            });
          }
          if (data.other_main_incomes) {
            OtherMainIncome.list = [];
            angular.forEach(data.other_main_incomes, function (data) {
              var instance = new OtherMainIncome();
              instance.fill(data);
              OtherMainIncome.list.push(instance);
            });
          }
          if (data.partner_details) {
            partnerDetails.list = utils.sort(data.partner_details, 'modified', true);
          }
          if (data.partnership_details) {
            partnershipDetails.list = utils.sort(data.partnership_details, 'modified', true);
          }
          if (data.partnerships) {
            PartnershipSA104.list = utils.sort(data.partnerships, 'modified', true);
          }
          if (data.partner_properties) {
            angular.forEach(data.partner_properties, function (data) {
              var instance = new PartnerProperty();
              instance.fill(data);
              PartnerProperty.list.push(instance);
            });
          }
          if (data.partner_tradings) {
            angular.forEach(data.partner_tradings, function (data) {
              var instance = new PartnerTrading();
              instance.fill(data);
              PartnerTrading.list.push(instance);
            });
          }
          if (data.products) {
            products.list = utils.sort(data.products, 'modified', true);
          }
          if (data.projects) {
            projects.list = utils.sort(data.projects, 'updated_at', true);
          }
          if (data.properties) {
            Property.list = [];
            angular.forEach(data.properties, function (data) {
              var instance = new Property();
              instance.fill(data);
              Property.list.push(instance);
            });
          }
          if (data.record_types) {
            recordTypes.list = data.record_types;
          }
          if (data.record_type_groups) {
            recordTypeGroups.list = data.record_type_groups;
          }
          if (data.relief) {
            reliefs.list = utils.sort(data.reliefs, 'modified', true);
          }
          if (data.self_employeds) {
            selfEmployeds.list = utils.sort(data.self_employeds, 'modified', true);
          }
          if (data.subcategories) {
            subcategories.list = utils.sort(data.subcategories, 'modified', true);
          }
          if (data.subscriptions) {
            subscriptions.list = utils.sort(data.subscriptions, 'modified', true);
          }
          if (data.suppliers) {
            Supplier.list = [];
            angular.forEach(data.suppliers, function (data) {
              var instance = new Supplier();
              instance.fill(data);
              Supplier.list.push(instance);
            });
          }
          if (data.tenants) {
            tenants.list = data.tenants;
          }
          if (data.transactions) {
            transactions.list = utils.sort(data.transactions, 'modified', true);
          }
          if (data.transactions_emp) {
            transactionsEmp.list = utils.sort(data.transactions_emp, 'modified', true);
          }
          if (data.transactions_emp_14) {
            TransactionEmp14.list = utils.sort(data.transactions_emp_14, 'modified', true);
          }
          if (data.transactions_pro) {
            transactionsPro.list = utils.sort(data.transactions_pro, 'modified', true);
          }
          if (data.transactions_pro_14) {
            TransactionPro14.list = [];
            angular.forEach(data.transactions_pro_14, function (data) {
              var instance = new TransactionPro14();
              instance.fill(data);
              TransactionPro14.list.push(instance);
            });
          }
          if (data.trust_incomes) {
            angular.forEach(data.trust_incomes, function (data) {
              var instance = new TrustIncome();
              instance.fill(data);
              TrustIncome.list.push(instance);
            });
          }
          if (data.underwriters) {
            Underwriter.list = utils.sort(data.underwriters, 'modified', true);
          }
        }
      };

      return service;
    });
