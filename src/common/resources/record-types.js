angular.module('resources.record-types', [])

  .factory('recordTypes', function (apiService, utils, $rootScope, $q, RESOURCES) {
    // resource is resource connect to api
    var resource = 'record-type';

    var service = {
      current: null,
      list: null,

      // find all
      all: function () {
        this.list = RESOURCES.RECORD_TYPES;
        return this.list;
      },

      // find
      find: function (id) {
        var deferred = $q.defer();

        if (utils.checkIfLoad(id, service.list)) {
          deferred.resolve(item);
        } else {
          apiService.get(resource, id).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('transactions:list:updated', service.list);
              }

              $rootScope.$broadcast('transactions:created', res.data);

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);

          });
        }

        return deferred.promise;
      },

      // find by group type
      findByGroupType: function (type) {
        var deferred = $q.defer();

        var list = [];

        if (angular.isArray(service.list)) {
          for (var index = 0; index < service.list.length; index++) {
            if (utils.getId(service.list[index].type_group_id) === utils.getId(type)) {
              list.push(service.list[index]);
            }
          }

          if (list != []) {
            deferred.resolve(list);
          } else {
            deferred.reject(false);
          }

        } else {
          service.all().then(function (res) {
            for (var index = 0; index < service.list.length; index++) {
              if (utils.getId(service.list[index].type_group_id) === utils.getId(type)) {
                list.push(service.list[index]);
              }
            }

            deferred.resolve(list);
          }, function error(error) {
            deferred.reject(error.data);
          });
        }

        return deferred.promise;
      }
    };

    return service;
  });