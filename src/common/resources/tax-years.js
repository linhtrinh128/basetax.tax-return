angular.module('resources.tax-years', [])

  .factory('taxYears',
    function (apiService, utils, $rootScope, RESOURCES) {
      // resource is resource connect to api
      var resource = 'tax-year';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          this.list = RESOURCES.TAX_YEARS;
          return this.list;
        },

        detectCurrentYear: function () {
          var dt = new Date();
          var y = 0;
          if (dt.getMonth() >= 5 || (dt.getMonth() === 4 && dt.getDate() >= 6)) {
            y = dt.getFullYear();
          } else {
            y = dt.getFullYear() - 1;
          }
          for (var i = 0; i < this.list.length; i++) {
            if (y - 2011 === this.list[i].id) {
              return this.list[i];
            }
          }
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('taxYears:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('taxYears:current:changed', service.current);
        }
      };

      return service;
    }
  )

  .factory('TaxYear',
    function (Model, apiService, $rootScope, CONFIGS) {

      var _resource = 'tax-year';

      var TaxYear = function () {
        Model.apply(this, arguments);

        this.id = -1;
        this.description = '';
        this.start_year = -1;
      };

      // List
      TaxYear.current = {};
      TaxYear.list = [];

      // get all TaxYears
      TaxYear.all = function () {
        var year = {};
        var list = [];
        var startYearCurrent = TaxYear.detectStartYearCurrent();

        for (var i = 0; i + 2012 <= startYearCurrent; i++) {
          year = new TaxYear();
          year.id = i + 1;
          year.description = (i + 2012) + '-' + (i + 2013);
          year.start_year = i + 2012;

          list.push(year);
        }

        TaxYear.list = list;
        return TaxYear.list;
      };

      // detect current year
      TaxYear.detectCurrentYear = function () {
        var dt = new Date();
        var y = 0;
        var listYears = TaxYear.list;

        if (dt.getMonth() >= 5 || (dt.getMonth() === 4 && dt.getDate() >= 6)) {
          y = dt.getFullYear();
        } else {
          y = dt.getFullYear() - 1;
        }

        for (var i = 0; i < listYears.length; i++) {
          if (y - 2011 === this.list[i].id) {
            return this.list[i];
          }
        }
      };

      // detect current year
      TaxYear.detectCurrentYearTaxReturn = function () {
        var today = Date.today();
        var years = TaxYear.list;
        for (var index in years) {
          var start_year = years[index].start_year;
          var range = {
            start: new Date(CONFIGS.TAX_YEAR.DATE_START + '/' + start_year),
            end: new Date(CONFIGS.TAX_YEAR.DATE_END + '/' + (start_year + 1))
          };
          if (today.between(range.start, range.end)) {
            return years[index - 1];
          }
        }
        return years[0];
      };

      // detect year
      TaxYear.detectStartYearCurrent = function () {
        var dt = new Date();
        var y = 0;

        if (dt.getMonth() >= 5 || (dt.getMonth() === 4 && dt.getDate() >= 6)) {
          y = dt.getFullYear();
        } else {
          y = dt.getFullYear() - 1;
        }

        return y;
      };

      // set current
      TaxYear.setCurrent = function (object) {
        TaxYear.current = object;
        $rootScope.$broadcast('taxYears:current:changed', object);
      };

      // set current year
      TaxYear.setDefaultYear = function () {
        return TaxYear.setCurrent(TaxYear.detectCurrentYear());
      };

      // set current year
      TaxYear.setDefaultYearTaxReturn = function () {
        return TaxYear.setCurrent(TaxYear.detectCurrentYearTaxReturn());
      };

      TaxYear.resetCurrent = function () {
        TaxYear.current = null;
        $rootScope.$broadcast('taxYears:current:changed', object);
      };

      //extend
      TaxYear.prototype = new Model();

      // function save include create, update
      TaxYear.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            TaxYear.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      TaxYear.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in TaxYear.list) {
            if (TaxYear.list[i].id === self.id) {
              TaxYear.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return TaxYear;
    });
