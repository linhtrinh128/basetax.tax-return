angular.module('resources.partnership-detail', [])

  .factory('partnershipDetails',
    function (apiService, utils, $rootScope, $q) {

      // resource is resource connect to api
      var resource = 'partnership-detail';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          var deferred = $q.defer();

          apiService.all(resource).then(function (res) {
            res = res.data;

            if (res.data != null) {
              service.list = [];

              service.list = utils.sort(res.data.data, 'modified', true);

              deferred.resolve(service.list);
            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        findByTaxYear: function (year) {
          var deferred = $q.defer();

          var list = [];

          if (angular.isArray(service.list)) {
            for (var index in service.list) {
              if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
                list.push(service.list[index]);
              }
            }

            deferred.resolve(list);
          }

          return deferred.promise;
        },

        find: function (id) {
          var deferred = $q.defer();

          if (utils.checkIfLoad(id, service.list)) {
            deferred.resolve(item);
          } else {
            apiService.get(resource, id).then(function (res) {
              res = res.data;

              if (res.data != null) {
                if (utils.appendInList(res.data, service.list)) {
                  $rootScope.$broadcast('partnershipDetails:list:updated', service.list);
                }

                deferred.resolve(res.data);
              } else {
                deferred.reject(false);
              }
            }, function error(error) {
              deferred.reject(error.data);

            });
          }

          return deferred.promise;
        },

        create: function (object) {
          var deferred = $q.defer();

          apiService.post(resource, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              $rootScope.$broadcast('partnershipDetails:created', res.data);

              if (utils.appendInList(res.data, service.list)) {
                $rootScope.$broadcast('partnershipDetails:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        update: function (object) {
          var deferred = $q.defer();

          apiService.customPUT(resource, object.id, object).then(function (res) {
            res = res.data;

            if (res.data != null) {
              if (utils.updateInList(res.data, service.list)) {
                $rootScope.$broadcast('partnershipDetails:list:updated', service.list);
              }

              deferred.resolve(res.data);
            } else {
              deferred.reject(false);
            }

          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        delete: function (item) {
          var deferred = $q.defer();

          apiService.remove(resource, item.id).then(function (res) {
            res = res.data;

            if (res.success) {
              $rootScope.$broadcast('partnershipDetails:deleted', res.object);

              if (utils.removeFromList(item, service.list)) {
                $rootScope.$broadcast('partnershipDetails:list:updated', res.object);
              }

              deferred.resolve(item);

            } else {
              deferred.reject(false);
            }
          }, function error(error) {
            deferred.reject(error.data);
          });

          return deferred.promise;
        },

        setCurrent: function (object) {
          service.current = object;
          $rootScope.$broadcast('partnershipDetails:current:changed', service.current);
        },

        resetCurrent: function () {
          service.current = null;
          $rootScope.$broadcast('partnershipDetails:current:changed', service.current);
        }
      };

      return service;
    }
  )

  .factory('PartnershipDetail',
    function (Model, apiService, $q, utils, RESOURCES, $uibModal, TaxYear) {

      var _resource = 'partnership-detail';

      var PartnershipDetail = function () {
        Model.apply(this, arguments);

        this.name = null;
        this.description = null;
        this.utr = null;
        this.address = null;
        this.email = null;
        this.date_commenced = new Date();
        this.date_ceased = new Date();
        this.business_registered_for_vat = null;
        this.profit_or_loss_is_provisional = null;
        this.tax_year_id = TaxYear.current.id;
      };

      // List partnershipDetails
      PartnershipDetail.list = [];

      // get all PartnershipDetails
      PartnershipDetail.all = function () {
        return Model.all(_resource).then(function (response) {
          PartnershipDetail.list = [];
          for (var i in response) {
            var instance = new PartnershipDetail();
            instance.fill(response[i]);
            PartnershipDetail.list.push(instance);
          }
        });
      };

      //extend
      PartnershipDetail.prototype = new Model();

      // fill
      PartnershipDetail.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function save include create, update
      PartnershipDetail.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            PartnershipDetail.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance self-employed
      PartnershipDetail.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in PartnershipDetail.list) {
            if (PartnershipDetail.list[i].id === self.id) {
              PartnershipDetail.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return PartnershipDetail;
    });
