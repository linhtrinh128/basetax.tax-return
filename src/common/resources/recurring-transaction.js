angular.module('resources.recurring-transactions', [])

    .factory('recurringTransactions', function (apiService, utils, $rootScope, $q) {
        // resource is resource connect to api
        var resource = 'recurring-transaction';

        // connect api success and return reponse
        function apiSuccess(response) {
            return response.data;
        }

        // fetch single item from api
        function fetchSingle(id) {
            return apiService.get(resource, id).then(apiSuccess);
        }

        // fetch mutilple items from api
        function fetchAll() {
            return apiService.all(resource).then(apiSuccess);
        }

        // set list
        function setList(list) {
            service.list = [];

            service.list = utils.sort(list, 'modified', true);
            $rootScope.$broadcast('recurringTransactions:list:updated', service.list);
        }

        // update list
        function updateInList(item) {
            if (angular.isArray(service.list)) {
                for (var index in service.list) {
                    if (utils.getId(service.list[index].id) === utils.getId(item.id)) {
                        service.list[index] = item;
                        $rootScope.$broadcast('recurringTransactions:list:updated', service.list);
                    }
                }
            }
        }

        // append item in list
        function appendInList(item) {
            if (!checkIfLoad(item)) {
                service.list.push(item);
                service.list = utils.sort(service.list, 'modified', true);
                $rootScope.$broadcast('recurringTransactions:list:updated', service.list);
            }
        }

        function checkIfLoad(item) {
            var id = utils.getId(item);

            if (angular.isArray(service.list)) {
                for (var index in service.list) {
                    if (utils.getId(service.list[index]).id === id) {
                        return service.list[index];
                    }
                }
            }
            return false;
        }

        // remove item in list
        function removeFromList(item) {
            if (angular.isArray(service.list)) {
                for (var index in service.list) {
                    if (utils.getId(service.list[index].id) === utils.getId(item.id)) {
                        service.list.splice(parseInt(index), 1);
                        service.list = utils.sort(service.list, 'modified', true);
                        $rootScope.$broadcast('recurringTransactions:list:updated', service.list);
                    }
                }
            }
        }

        // create object
        function create(object) {
            return apiService.post(resource, object).then(apiSuccess);
        }

        function create1(object) {
            return apiService.post('transaction', object).then(apiSuccess);
        }

        // update object
        function update(object) {
            return apiService.customPUT(resource, object.id, object).then(apiSuccess);
        }

        // remove object
        function remove(item) {
            return apiService.delete(resource, item.id).then(apiSuccess);
        }

        var service = {

            current: null,

            list: null,

            // find all
            all: function () {
                var deferred = $q.defer();

                fetchAll().then(function (res) {
                    setList(res.objects);
                    deferred.resolve(service.list);
                });

                return deferred.promise;
            },

            findByTaxYear: function (year) {
                var deferred = $q.defer();

                var list = [];

                if (angular.isArray(service.list)) {
                    for (var index in service.list) {
                        if (utils.getId(service.list[index].tax_year_id) === utils.getId(year.id)) {
                            list.push(service.list[index]);
                        }
                    }

                    deferred.resolve(list);
                }

                return deferred.promise;
            },

            find: function (id) {
                var deferred = $q.defer();

                var item = checkIfLoad(id);
                if (item) {
                    deferred.resolve(item);
                } else {
                    fetchSingle(id).then(function (res) {
                        appendInList(res.object);
                        deferred.resolve(res.object);
                    });
                }

                return deferred.promise;
            },

            create: function (object) {
                var deferred = $q.defer();

                create(object).then(function (res) {
                    appendInList(res.object);
                    deferred.resolve(res.object);
                });

                return deferred.promise;
            },

            update: function(object){
                var deferred = $q.defer();

                update(object).then(function (res) {
                    updateInList(res.object);
                    deferred.resolve(res.object);
                });

                return deferred.promise;
            },

            delete: function (item) {
                var deferred = $q.defer();

                remove(item).then(function (res) {
                    removeFromList(res.object);
                    $rootScope.$broadcast('recurringTransactions:deleted', res.object);
                    deferred.resolve(res);
                });

                return deferred.promise;
            },

            search: function(options){
                var deferred = $q.defer();

                var list = [];
                var results = [];

                if(!options.type){
                    options.type = 'all';
                }

                if (angular.isArray(service.list)) {
                    for (var index in service.list) {
                        if (utils.getId(service.list[index].tax_year_id) === utils.getId(options.taxYear.id)) {
                            list.push(service.list[index]);
                        }
                    }

                    switch (options.type) {
                        case 'search': {
                            for (index in list) {
                                if(list[index].name && list[index].name.indexOf(options.text) !== -1 ) {
                                    results.push(list[index]);
                                }
                            }
                            break;
                        }
                        default: {
                            results = list;
                            break;
                        }
                    }
                }
                deferred.resolve(results);

                return deferred.promise;
            },

            setCurrent: function (object) {

                service.current = object;
                $rootScope.$broadcast('recurringTransactions:current:changed', service.current);
            },

            resetCurrent: function (object) {

                service.current = null;
                $rootScope.$broadcast('recurringTransactions:current:changed', service.current);
            },

            checkRecurring: function(object){

                switch (object.repeat_type){
                    case 'months': {
                        object.repeat = parseInt(object.repeat) * 30;
                        break;
                    }
                    case 'years':{
                        object.repeat = parseInt(object.repeat) * 365;
                        break;
                    }
                    default :{
                        object.repeat = parseInt(object.repeat);
                        break;
                    }
                }

                var realDate = utils.formatDateTime(new Date());
                var countDay = ((new Date(realDate) - new Date(object.start)))/86400000;

                return parseInt(countDay/object.repeat);

            }
        };

        return service;
    });