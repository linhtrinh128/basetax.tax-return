angular.module('resources.partner-property', [])

  .factory('PartnerProperty',
    function (Model, $uibModal, $q, TaxYear) {

      var _resource = 'partner-property';

      var PartnerProperty = function () {
        Model.apply(this, arguments);

        this.partnership_detail_id = 1;
        this.name = '';
        this.tax_reference = '';
        this.return_period_begin = '';
        this.return_period_end = '';
        this.partnership_income_from_furnished_holiday_letting = 0;
        this.income_from_furnished_holiday_letting = 0;
        this.rent_rates_insurance_ground_rents_etc_1 = 0;
        this.repairs_and_maintenance_1 = 0;
        this.finance_charges_including_interest_1 = 0;
        this.legal_and_professional_cost_1 = 0;
        this.cost_of_services_provided_including_wages_1 = 0;
        this.other_expenses_1 = 0;
        this.total_expense_1 = 0;
        this.net_profit_1 = 0;
        this.private_use_1 = 0;
        this.balancing_charges_1 = 0;
        this.total_private_use_balancing_charge_1 = 0;
        this.capital_allowances_1 = 0;
        this.tick_allowance_1 = 0;
        this.loss_brought_forward_used_against_this_year_profit = 0;
        this.profit_for_the_year_after_losses = 0;
        this.loss_for_the_year = 0;
        this.total_loss_to_carry_forward = 0;
        this.business_is_in_the_EEA = 0;
        this.make_a_period_of_grace_election = 0;
        this.rent_and_other_income_form_uk_property = 0;
        this.tax_deducted = 0;
        this.chargeable_premium = 0;
        this.reverse_premium = 0;
        this.total_income = 0;
        this.rent_rates_insurance_ground_rents_etc_2 = 0;
        this.repairs_and_maintenance_2 = 0;
        this.finance_charges_including_interest_2 = 0;
        this.legal_and_professional_cost_2 = 0;
        this.cost_of_services_provided_including_wages_2 = 0;
        this.other_expenses_2 = 0;
        this.total_expense_2 = 0;
        this.net_profit_2 = 0;
        this.private_use_2 = 0;
        this.balancing_charges_2 = 0;
        this.total_private_use_balancing_charge_2 = 0;
        this.annual_investment_allowance = 0;
        this.all_other_capital_allowance = 0;
        this.tick_allowance_2 = 0;
        this.landlord_energy_saving_allowance = 0;
        this.wear_and_tear = 0;
        this.total_allowance = 0;
        this.profit_or_loss_for_return_period = 0;

        this.tax_year_id = TaxYear.current.id;
      };

      // List
      PartnerProperty.list = [];

      // get all PartnerPropertys
      PartnerProperty.all = function () {
        return Model.all(_resource).then(function (response) {
          PartnerProperty.list = [];
          for (var i in response) {
            var instance = new PartnerProperty();
            instance.fill(response[i]);
            PartnerProperty.list.push(instance);
          }
        });
      };

      // find by tax year
      PartnerProperty.findByTaxYear = function (tax_year_id) {
        var deferred = $q.defer();
        var flag = false;
        for (var i in PartnerProperty.list) {
          var item = PartnerProperty.list[i];
          if (item.tax_year_id === tax_year_id) {
            deferred.resolve(item);
            flag = true;
          }
        }
        if (!flag) {
          deferred.reject(false);
        }
        return deferred.promise;
      };

      //extend
      PartnerProperty.prototype = new Model();

      // fill
      PartnerProperty.prototype.fill = function (data) {
        return Model.prototype.fill.apply(this, arguments);
      };

      // function show
      PartnerProperty.prototype.show = function () {
        var self = this;
        return $uibModal.open({
          templateUrl: 'modules/tax-return/forms/partnership/partnership.tpl.html',
          controller: 'PartnershipIncomeController',
          size: 'lg',
          resolve: {
            $partnership: function () {
              return self;
            }
          }
        });
      };

      // function save include create, update
      PartnerProperty.prototype.save = function () {
        var self = this;
        var newObj = !self.id;
        this.resource = _resource;
        var originSave = Model.prototype.save.apply(self, arguments);
        originSave.then(function () {
          if (newObj) {
            PartnerProperty.list.unshift(self);
          }
        });
        return originSave;
      };

      // function delete an instance partnership (SA104)
      PartnerProperty.prototype.delete = function () {
        var self = this;
        this.resource = _resource;
        var originDelete = Model.prototype.delete.apply(self, arguments);
        originDelete.then(function () {
          for (var i in PartnerProperty.list) {
            if (PartnerProperty.list[i].id === self.id) {
              PartnerProperty.list.splice(i, 1);
            }
          }
        });
        return originDelete;
      };

      //override
      return PartnerProperty;
    }
  );
