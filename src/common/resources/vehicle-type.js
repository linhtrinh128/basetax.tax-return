angular.module('resources.vehicle-type', [])

  .factory('VehicleType',
    function (apiService, utils, $rootScope, $q, RESOURCES) {
      
      // resource is resource connect to api
      var resource = 'vehicle-type';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          this.list = RESOURCES.VEHICLES;
          return this.list;
        }
      };


      return service;
    });
