angular.module('resources.countries', [])

  .factory('Country',
    function (apiService, utils, $rootScope, $q, RESOURCES) {
      
      // resource is resource connect to api
      var resource = 'country';

      var service = {
        current: null,
        list: null,

        // find all
        all: function () {
          this.list = RESOURCES.COUNTRIES;
          return this.list;
        }
      };

      return service;
    }
  );
