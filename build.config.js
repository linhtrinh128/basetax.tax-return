/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {
    /**
     * The `build_dir` folder is where our projects are compiled during
     * development and the `compile_dir` folder is where our app resides once it's
     * completely built.
     */
    build_dir: 'build',
    compile_dir: 'bin',

    /**
     * This is a collection of file patterns that refer to our app code (the
     * stuff in `src/`). These file paths are used in the configuration of
     * build tasks. `js` is all project javascript, less tests. `ctpl` contains
     * our reusable components' (`src/common`) template HTML files, while
     * `atpl` contains the same, but for our app's code. `html` is just our
     * main HTML file, `less` is our main stylesheet, and `unit` contains our
     * app's unit tests.
     */
    app_files: {
        js: ['src/**/*.js', '!src/**/*.spec.js', '!src/assets/**/*.js'],
        jsunit: ['src/**/*.spec.js'],

        atpl: ['src/app/**/*.tpl.html'],
        ctpl: ['src/common/**/*.tpl.html'],

        html: ['src/index.html'],
        less: 'src/less/main.less'
    },

    /**
     * This is a collection of files used during testing only.
     */
    test_files: {
        js: [
            'vendor/angular-mocks/angular-mocks.js'
        ]
    },

    /**
     * This is the same as `app_files`, except it contains patterns that
     * reference vendor code (`vendor/`) that we need to place into the build
     * process somewhere. While the `app_files` property ensures all
     * standardized files are collected for compilation, it is the user's job
     * to ensure non-standardized (i.e. vendor-related) files are handled
     * appropriately in `vendor_files.js`.
     *
     * The `vendor_files.js` property holds files to be automatically
     * concatenated and minified with our project source files.
     *
     * The `vendor_files.css` property holds any CSS files to be automatically
     * included in our app.
     *
     * The `vendor_files.assets` property holds any assets to be copied along
     * with our app's assets. This structure is flattened, so it is not
     * recommended that you use wildcards.
     */
    vendor_files: {
        js: [
            'vendor/jquery/dist/jquery.js',
            'vendor/bootstrap/dist/js/bootstrap.js',
            'vendor/DateJS/build/date.js',
            'vendor/money/money.js',
            'vendor/underscore/underscore.js',
            'vendor/moment/moment.js',
            'vendor/spin.js/spin.js',
            'vendor/angular/angular.js',
            'vendor/angular-bootstrap/ui-bootstrap-tpls.js',
            'vendor/angular-cookies/angular-cookies.js',
            'vendor/angular-sanitize/angular-sanitize.js',
            'vendor/angular-resource/angular-resource.js',
            'vendor/angular-route/angular-route.js',
            'vendor/angular-filter/dist/angular-filter.min.js',
            'vendor/angular-animate/angular-animate.js',
            'vendor/angular-stripe/release/angular-stripe.js',
            'vendor/angular-loading-bar/build/loading-bar.min.js',
            'vendor/angular-spinner/angular-spinner.js',
            'vendor/angular-ui/build/angular-ui.js',
            'vendor/angular-ui-event/dist/event.js',
            'vendor/angular-ui-indeterminate/dist/indeterminate.js',
            'vendor/angular-ui-mask/dist/mask.js',
            'vendor/angular-ui-router/release/angular-ui-router.js',
            'vendor/angular-ui-scroll/dist/ui-scroll.js',
            'vendor/angular-ui-scrollpoint/dist/scrollpoint.js',
            'vendor/angular-ui-select/dist/select.js',
            'vendor/angular-ui-uploader/dist/uploader.js',
            'vendor/angular-ui-utils/index.js',
            'vendor/angular-ui-validate/dist/validate.js',
            'vendor/angularjs-toaster/toaster.js',
            'vendor/restangular/dist/restangular.js',
            'vendor/angular-moment/angular-moment.js',
            'vendor/x2js/xml2json.min.js',
            'vendor/angular-xml/angular-xml.min.js',
            'vendor/ngDialog/js/ngDialog.js',
            'vendor/select2/select2.js',
            'vendor/ng-csv/build/ng-csv.js',
            'vendor/ng-table/ng-table.js',
            'vendor/angular-bootstrap-checkbox/angular-bootstrap-checkbox.js',
        ],
        css: [
            'vendor/angular-ui/build/angular-ui.css',
            'vendor/angular-ui-select/dist/select.css',
            'vendor/select2/select2.css',
            'vendor/angularjs-toaster/toaster.min.css',
            'vendor/ng-table/ng-table.css',
            'vendor/angular-loading-bar/build/loading-bar.min.css'
        ],
        assets: [
            'vendor/bootstrap/dist/fonts/*',
            'vendor/font-awesome/fonts/*'
        ]
    }
};
